<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable = ['ticket_id','admin_id', 'test_title', 'test_title_slug', 'total_question'];

    public function question()
    {
        return $this->belongsTo(Questions::class, 'test_id');
    }
    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticket_id');
    }
    public function questions()
    {
        return $this->hasMany(Questions::class, 'id');
    } public function tickets()
    {
        return $this->hasMany(Ticket::class, 'id');
    }
    public function exam_register()
    {
        return $this->hasMany(ExamRegister::class, 'id');
    }
    public function result()
    {
        return $this->hasMany(Result::class, 'test_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExamRegister extends Model
{
    protected $fillable = ['exam_id'];

    public function exam()
    {
        return $this->belongsTo(Exam::class, 'exam_id');
    }
}

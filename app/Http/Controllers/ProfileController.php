<?php

namespace App\Http\Controllers;

use App\Models\ExamRegister;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index($id)
    {
        $user = User::find($id);
        return view('front.profile', compact('user'));
    }
    public function tests_user($id)
    {
        $register_exam = ExamRegister::where('user_id', $id)->get();
        return view('front.tests_user', compact('register_exam'));
    }

    public function update_user_profile(Request $request)
    {
        $user = User::find(Auth::id());
        $user->name = $request->name;
        $user->email = $request->email;
        $user->surname = $request->surname;
        $user->phone = $request->phone;
        $user->save();
        return response()->json(['status' => true]);
    }
    public function password_change()
    {
        return view('front.password_change');
    }
    public function pass_change(Request $request)
    {
        $id = $request->id;
        $user = User::find($id);
        if (Hash::check($request->old_password, $user->password)) {
            $user->password = Hash::make($request->password);
            $user->save();
            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'error']);
        }
    }
}

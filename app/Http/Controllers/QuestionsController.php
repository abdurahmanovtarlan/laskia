<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionRequest;
use App\Models\Exam;
use App\Models\GroupSubject;
use App\Models\Questions;
use App\Models\QuestionsOption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class QuestionsController extends Controller
{
    public function index($slug)
    {
        $exam = Exam::where('test_title_slug', $slug)->get();
//        $group_subject = GroupSubject::where('group_id',$exam[0]->group_id)->get();
        $group_subject = DB::table('group_subjects')
            ->leftJoin('groups', 'groups.id', '=', 'group_subjects.group_id')
            ->leftJoin('subjects', 'subjects.id', '=', 'group_subjects.subject_id')
            ->where('group_subjects.group_id', '=', $exam[0]->group_id)
            ->get();
        $questions = Questions::where('test_id', $exam[0]->id)->get();
        return view('admin.question.index', compact('questions', 'exam', 'group_subject'));
    }

    public function create($id)
    {
        $data['exam'] = Exam::find($id);
        $data['id'] = $id;
        $correct_options = [
            'option1' => 'Variant #A',
            'option2' => 'Variant #B',
            'option3' => 'Variant #C',
        ];

        return view('admin.question.create', compact('correct_options') + $data);
    }

    public function store(Request $request)
    {
        $question = new Questions();
        $question->test_id = $request->test_id;
        $question->question_text = $request->question_text;
        $question->answer_explanation = $request->answer_explanation;
        $image = $request->question_image;
        if ($image != null) {
            $question_image = $request->file('question_image');
            $question_image_name = time() . '.' . $question_image->getClientOriginalExtension();
            $request->question_image->move('question_image/', $question_image_name);
            $question->question_image = $question_image_name;
        } else {
            $question->question_image = '';
        }
        $question->save();
        foreach ($request->input() as $key => $value) {
            if (strpos($key, 'option') !== false && $value != '') {
                $status = $request->input('correct') == $key ? 1 : 0;
                QuestionsOption::create([
                    'question_id' => $question->id,
                    'option' => $value,
                    'correct' => $status
                ]);
            }
        }
        return response()->json(['status' => true]);
    }

    public function show($id)
    {
        $exams = Exam::get()->pluck('test_title', 'id');
        $question = Questions::find($id);
        $questions_option = QuestionsOption::where('question_id', $id)->get();
        return view('admin.question.show', compact('question', 'exams', "questions_option"));
    }

    public function delete(Request $request)
    {
        $question = Questions::findOrFail($request->id);
        $question->delete();

        return response()->json(['status' => true]);
    }

    public function edit($id)
    {
        $question = Questions::findOrFail($id);
        return view('admin.question.edit', compact('question'));
    }

    public function update(Request $request, $id)
    {
        $question = Questions::findOrFail($id);
        $question->update($request->all());
        return response()->json(['status' => true]);
    }
}

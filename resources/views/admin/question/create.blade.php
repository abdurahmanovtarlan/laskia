@extends('admin.base')
@section('content')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="{{asset('admin/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('admin/ckeditor/samples/js/sample.js')}}"></script>
    <link rel="stylesheet" href="{{asset('admin/ckeditor/samples/css/samples.css')}}"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">

    {{--<link rel="stylesheet" type="text/css" href="{{asset('admin/assets/libs/quill/dist/quill.snow.css')}}">--}}
    {{--<script src="{{asset('admin/assets/libs/quill/dist/quill.min.js')}}"></script>--}}
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card exam_add">
                    <div class="card-body">
                        <form method="POST" action="" enctype="multipart/form-data" id="question_form">
                            @csrf
                            <div class="form-group">
                                <label for="test_id" class="font-weight-bold">Testin adı <span
                                        class="text-danger">*</span></label>
                                <select name="test_id" class="form-control" id="test_id">
                                    <option value="{{$exam->id}}" selected>{{$exam->test_title}}</option>
                                </select>
                                <div class="invalid-feedback font-weight-bold ">
                                    Test seçin.
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="texto" class="font-weight-bold">Sual <span
                                        class="text-danger">*</span></label>
                                        <div id='texto'></div>

                                <div class="invalid-feedback font-weight-bold question_text">
                                    Sual daxil edin.
                                </div>
                                <hr>
                                <div id="ques"></div>
                            </div>
                            <div class="form-group">
                                <label for="question_image" class="font-weight-bold">Sual sekili<span
                                        class="text-danger">*</span></label>
                                <input type="file" class="form-control" id="question_image" name="question_image">
                            </div>
                            <div class="form-group">
                                <label for="option1" class="font-weight-bold">Variant A<span
                                        class="text-danger">*</span></label>
                                <textarea class="form-control" name="option1"
                                          id="option1"></textarea>
                                <div class="invalid-feedback font-weight-bold ">
                                    Variant daxil edin.
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="option2" class="font-weight-bold">Variant B<span
                                        class="text-danger">*</span></label>
                                <textarea class="form-control" name="option2"
                                          id="option2"></textarea>
                                <div class="invalid-feedback font-weight-bold ">
                                    Variant daxil edin.
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="option3" class="font-weight-bold">Variant C<span
                                        class="text-danger">*</span></label>
                                <textarea class="form-control" name="option3"
                                          id="option3"></textarea>
                                <div class="invalid-feedback font-weight-bold ">
                                    Variant daxil edin.
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="option4" class="font-weight-bold">Variant D<span
                                        class="text-danger">*</span></label>
                                <textarea class="form-control" name="option4"
                                          id="option4"></textarea>
                                <div class="invalid-feedback font-weight-bold ">
                                    Variant daxil edin.
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="option5" class="font-weight-bold">Variant E<span
                                        class="text-danger">*</span></label>
                                <textarea class="form-control" name="option5"
                                          id="option5"></textarea>
                                <div class="invalid-feedback font-weight-bold ">
                                    Variant daxil edin.
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="correct" class="font-weight-bold">Düzgün cavab</label>
                                <select name="correct" class="form-control" id="correct">
                                    <option value>Seçin</option>

                                    @foreach($correct_options as $key=>$item)
                                        <option value="{{$key}}">{{$item}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback font-weight-bold ">
                                    Düzgün cavabı seçin.
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="answer_explanation" class="font-weight-bold">Cavabın açıqlaması</label>
                                <textarea class="form-control" placeholder="" name="answer_explanation" cols="30"
                                          rows="5" id="answer_explanation"></textarea>

                            </div>
                            <button class="btn btn-primary px-5 btn-block" type="submit">
                                Saxla
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.min.js"></script>
    <script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML"></script>
        
    <script type="text/x-mathjax-config">
        MathJax.Hub.Config({
        extensions: ["tex2jax.js"],
        jax: ["input/TeX", "output/HTML-CSS"],
        tex2jax: {
        inlineMath: [
        ["$", "$"],
        ["\\(", "\\)"]
        ]
        },
        "HTML-CSS": {
        linebreaks: { automatic: true }
        }
        });
    </script>

    <script src="{{asset('admin/summernote-ext.js')}}"></script>
    <script>
        $(document).ready(function () {
                $('#texto,#option1,#option2,#option3,#option4,#option5,#answer_explanation').summernote({
                    height: 400, // set editor height,
                    toolbar: [
                        ['insert', ['equation', 'draw', 'hello']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['style', ['style', 'bold', 'italic', 'underline', 'strikethrough', 'clear', 'superscript', 'subscript']],
                        ['fonts', ['fontsize']],
                        ['color', ['color']],
                        ['undo', ['undo', 'redo']],
                        ['ckMedia', ['ckImageUploader', 'ckVideoEmbeeder']],
                        ['misc', ['link', 'picture', 'table', 'hr', 'codeview', 'fullscreen']],
                        ['height', ['lineheight']]
                    ],
                });
            $('#question_form').submit(function (e) {
                e.preventDefault();
                var editordata =  $('#texto').summernote('code');

                var test = $("#test_id option:selected");
                var option1 = $('#option1').summernote('code');
                var option2 = $('#option2').summernote('code');
                var option3 = $('#option3').summernote('code');
                var option4 = $('#option4').summernote('code');
                var option5 = $('#option5').summernote('code');
                var correct = $("#correct option:selected");
                var answer_explanation =$('#answer_explanation').summernote('code');
                if (test.val() == '') {
                    $('select#test_id').addClass('is-invalid');
                }
                if (option1 == " ") {
                    option1.addClass('is-invalid');
                }
                if (option2 ==  " ") {
                    option2.addClass('is-invalid');
                }
                if (option3 == " ") {
                    option3.addClass('is-invalid');
                }
                if (option4 ==  " ") {
                    option4.addClass('is-invalid');
                }
                if (option5 ==  " ") {
                    option5.addClass('is-invalid');
                }
                if (correct.val() == " ") {
                    $('select#correct').addClass('is-invalid');
                }
                if (
                    test.val() !=  " " &&
                    option1 != " " &&
                    option2 !=  " " &&
                    option3 !=  " " &&
                    option4 !=  " " &&
                    option5 !=  " " &&
                    correct.val() !=  " "
                ) {
                    var formData = new FormData(this);
                    formData.append('question_text', editordata);
                    formData.append('option1', option1);
                    formData.append('option2', option2);
                    formData.append('option3', option3);
                    formData.append('option4', option4);
                    formData.append('option5', option5);
                    formData.append('answer_explanation', answer_explanation);
                    $.ajax({
                        type: "POST",
                        url: "{{route('question_store')}}",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data.status == true) {
                                var exam = '{{$exam->test_title_slug}}';
                                window.location = `/coders/question/${exam}`;
                            }

                        }
                    });
                }
            })
        });
    </script>
@endsection

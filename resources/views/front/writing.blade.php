@extends('front.base')
@section('content')
    <link rel="stylesheet" href="{{asset('front/css/timer.css')}}">
    <script src="{{asset('front/js/timer.js')}}"></script>
        <div class="container" style="margin-top: 100px;">
            <div class="row justify-content-center">
                <div class="col-md-7">
                    <div class="card mb-2 shadow-sm">
                        <div class="card-body pb-1">
                            <div class="d-flex justify-content-between align-items-center">
                                <h2>Writing</h2>
                            </div>
                        </div>
                    </div>
                    <form action="{{ route('ielts_store_writing') }}" enctype="multipart/form-data" id="writing_form"
                          method="POST">
                        @csrf
                        <input type="hidden" name="ielts_id" value="{{ $ielts[0]->id }}">
                        <input type="hidden" name="writing_id" value="{{ $ielts_writing[0]->id }}">
                        <input type="hidden" name="slug" value="{{ $slug}}">
                        <?php $i = 1;
                        ?>
                        @foreach($questions as $key=>$question)
                            <div class="card question-card mb-2 shadow-sm">
                                <div class="card-body">
                                    <div class="question ml-2">
                                        {!! ($question->question) !!}
                                    </div>
                                    <input type="hidden" name="questions" value="{{ $question->id }}">
                                    <div class="answers">
                                    <textarea rows="4" class="form-control mt-3 rounded"
                                              name="text_answer"
                                              placeholder="Answer..."></textarea>
                                        <span class="text-warning text-small ml-2 number_text font-weight-bold"
                                              style="font-size: 12px"></span>
                                    </div>
                                </div>
                            </div>
                            <?php $i++; ?>
                        @endforeach
                    </form>
                </div>
                <div class="col-md-5">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <div id="DateCountdown" data-timer="{{$ielts_writing[0]->ielts_writing_time}}"></div>
                        </div>
                    </div>
                    <img src="{{asset('front/img/writing1.svg')}}" alt="" class="w-100 mt-5">
                </div>
                <button class="btn-block shadow btn-primary m-3 py-2 rounded next_btn">
                    Növbəti
                </button>
            </div>
        </div>
        <script>
            $("#DateCountdown").TimeCircles({
                animation: "smooth",
                bg_width: 0.1,
                fg_width: 0.03,
                circle_bg_color: "#60686F",
                time: {
                    Days: {
                        show: false,
                    },
                    Hours: {
                        text: "Hours",
                        color: "#99CCFF",
                        show: true,
                    },
                    Minutes: {
                        text: "Minutes",
                        color: "#BBFFBB",
                        show: true,
                    },
                    Seconds: {
                        text: "Seconds",
                        color: "#FF9999",
                        show: true,
                    },
                },
            });
            setInterval(function () {
                var remaining_second = $("#DateCountdown").TimeCircles().getTime();
                if (remaining_second < 1) {
                    $("#writing_form").submit();
                }
            }, 1000);
            $('.next_btn').click(function () {
                $('#writing_form').submit();
            })
            $(".answers textarea").keyup(function () {
                var number_text = $(this).val().trim().length;
                $(".number_text").text(`Number of words: ` + number_text);
            });
        </script>
@endsection

@extends('admin.newbase')
@section('content')
        <div id="accordion" class="mb-4">
          <div class="card card-box" style="overflow: hidden !important;">
            <div
              class="card-header py-3 bg-yellow font-weight-bold"
              style="cursor: pointer;"
              id="headingThree"
              data-toggle="collapse"
              data-target="#collapseThree"
              aria-expanded="false"
              aria-controls="collapseThree"
            >
              Bilet əlavə edin
            </div>
            <div
              id="collapseThree"
              class="collapse"
              aria-labelledby="headingThree"
              data-parent="#accordion"
            >
              <div class="card-body">
                <form>
                  <div class="form-group">
                    <label>
                      <i
                        class="icon-copy dw dw-ticket font-weight-bold mr-2"
                      ></i
                      >Bilet</label
                    >
                    <input class="form-control" type="text" id="ticket" />
                    <div
                      id="ticket_feedback"
                      class="form-control-feedback"
                    ></div>
                  </div>
                  <div class="form-group">
                    <label
                      ><i
                        class="icon-copy fa fa-code mr-2 font-weight-bold"
                        aria-hidden="true"
                      ></i
                      >Bilet kodu
                    </label>
                    <input class="form-control" type="text" id="ticket_code" />
                    <div
                      id="ticket_code_feedback"
                      class="form-control-feedback"
                    ></div>
                  </div>
                  <div class="form-group">
                    <button id="save_btn" class="btn bg-green btn-block">
                      Saxla
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="card-box mb-30">
          <div class="pb-20 pd-20">
            <div
              id="DataTables_Table_0_wrapper"
              class="dataTables_wrapper dt-bootstrap4 no-footer"
            >
              <div class="row">
                <div class="col-sm-12">
                  <table
                    class="data-table table stripe hover nowrap dataTable no-footer dtr-inline"
                    id="ticket_table"
                  >
                    <thead>
                      <tr>
                        <th>Bilet</th>
                        <th>Bilet kodu</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                       @foreach($tickets as $ticket)
                   <tr id="{{$ticket->id}}">
                        <td>
                                                        {{$ticket->ticket_name}}
                                                    </td><td>
                                                        {{$ticket->ticket_code}}
                                                    </td>
                        <td>
                          <div class="dropdown">
                            <a
                              class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle"
                              href="#"
                              role="button"
                              data-toggle="dropdown"
                            >
                              <i class="dw dw-more"></i>
                            </a>
                            <div
                              class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list"
                            >
                              <a
                                class="dropdown-item"
                                href="#"
                                id="edit"
                                data-toggle="modal"
                                data-target="#tikcet_edit_modal"
                                type="button"
                                ><i class="dw dw-edit2"></i> Edit</a
                              >

                              <a
                                class="dropdown-item"
                                href="#"
                                id="delete"
                                data-toggle="modal"
                                data-target="#confirmation-modal"
                                type="button"
                                ><i class="dw dw-delete-3"></i> Delete</a
                              >
                            </div>
                          </div>
                        </td>
                      </tr>
                            </tr>
                                            @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    <!-- Edit model start -->
    <div
      id="tikcet_edit_modal"
      class="modal fade"
      tabindex="-1"
      role="dialog"
      aria-labelledby="tikcet_edit_modal_title"
      style="display: none;"
      aria-hidden="true"
    >
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="tikcet_edit_modal_title">
              Bilet yenile
            </h4>
            <button
              type="button"
              class="close"
              data-dismiss="modal"
              aria-hidden="true"
            >
              ×
            </button>
          </div>
          <div class="modal-body">
            <form action="" id="ticket_edit">
              <div class="form-group">
                <label for="">Bilet</label>
                <input type="text" class="form-control" id="ticket_update" />
              </div>
              <div class="form-group">
                <label for="">Bilet kodu</label>
                <input
                  type="text"
                  class="form-control"
                  id="ticket_code_update"
                />
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" data-dismiss="modal">
              Bağla
            </button>
            <button id="ticket_update_btn" class="btn btn-success">
              Yenilə
            </button>
          </div>
        </div>
      </div>
    </div>
    <!-- End edit -->
    <!-- Delete Modal -->
    <div
      class="modal fade"
      id="confirmation-modal"
      tabindex="-1"
      role="dialog"
      style="display: none;"
      aria-hidden="true"
    >
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body text-center font-18">
            <h4 class="padding-top-30 mb-30 weight-500">
              Davam etmək istədiyinizə əminsiniz?
            </h4>
            <div
              class="padding-bottom-30 row"
              style="max-width: 170px; margin: 0 auto;"
            >
              <div class="col-6">
                <button
                  type="button"
                  class="btn btn-secondary border-radius-100 btn-block confirmation-btn"
                  data-dismiss="modal"
                >
                  <i class="fa fa-times"></i>
                </button>
                Yox
              </div>
              <div class="col-6">
                <button
                  id="confirmation-btn-delete"
                  class="btn btn-danger border-radius-100 btn-block confirmation-btn"
                  data-dismiss="modal"
                >
                  <i class="fa fa-check"></i>
                </button>
                Hə
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script>
      // Create Ticket
      var ticket = $("#ticket");
      var ticket_feedback = $("#ticket_feedback");
      var ticket_code = $("#ticket_code");
      var ticket_code_feedback = $("#ticket_code_feedback");
      var save_btn = $("#save_btn");
      var ticket_table = $("#ticket_table");
      save_btn.click(function (e) {
        e.preventDefault();
        // input value control
        if (ticket.val() == "") {
          var form_group = ticket.parent().addClass("has-danger");
          var input = ticket
            .parent()
            .children("input")
            .addClass("form-control-danger");
          ticket_feedback.text("Bilet daxil edin.");
        }
        if (ticket_code.val() == "") {
          var form_group = ticket_code.parent().addClass("has-danger");
          var input = ticket_code
            .parent()
            .children("input")
            .addClass("form-control-danger");
          ticket_code_feedback.text("Bilet kodu daxil edin.");
        }
        ticket.keyup(function () {
          var form_group = ticket
            .parent()
            .removeClass("has-danger")
            .addClass("has-success");
          var input = ticket
            .parent()
            .children("input")
            .removeClass("form-control-danger")
            .addClass("form-control-success");
          ticket_feedback.text("");
        });
        ticket_code.keyup(function () {
          var form_group = ticket_code
            .parent()
            .removeClass("has-danger")
            .addClass("has-success");
          var input = ticket_code
            .parent()
            .children("input")
            .removeClass("form-control-danger")
            .addClass("form-control-success");
          ticket_code_feedback.text("");
        });
        if (ticket.val() != "" && ticket_code.val() != "") {

                $.ajax({
                        url: '/coders/ticket/create',
                        type: "POST",
                        data: {
                            '_token': '{{csrf_token()}}',
                            'ticket_name': ticket.val(),
                            'ticket_code': ticket_code.val(),
                        },
                        success: function (data) {
                            if (data.status == true) {
                                Swal.fire({
            position: "center",
            icon: "success",
            title: "Bilet daxil edildi",
            showConfirmButton: false,
            timer: 1000,
          });
          var table_tr = ticket_table.find("tbody tr");
          if (table_tr.length % 2 == 0) {
            var output = `<tr id="2" class="even">
                        <td>${ticket.val()}</td>
                        <td>${ticket_code.val()}</td>
                        <td id="">
                          <div class="dropdown">
                            <a
                              class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle"
                              href="#"
                              role="button"
                              data-toggle="dropdown"
                            >
                              <i class="dw dw-more"></i>
                            </a>
                            <div
                              class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list"
                            >
                              <a
                                class="dropdown-item"
                                href="#"
                                id="edit"
                                data-toggle="modal"
                                data-target="#tikcet_edit_modal"
                                type="button"
                                ><i class="dw dw-edit2"></i> Edit</a
                              >

                              <a
                                class="dropdown-item"
                                href="#"
                                id="delete"
                                data-toggle="modal"
                                data-target="#confirmation-modal"
                                type="button"
                                ><i class="dw dw-delete-3"></i> Delete</a
                              >
                            </div>
                          </div>
                        </td>
                      </tr>`;
          } else {
            var output = `<tr id="2" class="odd">
                        <td>${ticket.val()}</td>
                        <td>${ticket_code.val()}</td>
                        <td id="">
                          <div class="dropdown">
                            <a
                              class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle"
                              href="#"
                              role="button"
                              data-toggle="dropdown"
                            >
                              <i class="dw dw-more"></i>
                            </a>
                            <div
                              class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list"
                            >
                              <a
                                class="dropdown-item"
                                href="#"
                                id="edit"
                                data-toggle="modal"
                                data-target="#tikcet_edit_modal"
                                type="button"
                                ><i class="dw dw-edit2"></i> Edit</a
                              >

                              <a
                                class="dropdown-item"
                                href="#"
                                id="delete"
                                data-toggle="modal"
                                data-target="#confirmation-modal"
                                type="button"
                                ><i class="dw dw-delete-3"></i> Delete</a
                              >
                            </div>
                          </div>
                        </td>
                      </tr>`;
          }
          ticket_table.find("tbody").prepend(output);
          ticket.val("");
          ticket_code.val("");

          ticket.parent().removeClass("has-success");
          ticket.parent().children("input").removeClass("form-control-success");

          ticket_code.parent().removeClass("has-success");
          ticket_code
            .parent()
            .children("input")
            .removeClass("form-control-success");
                            }
                        }
                    })


          
        }
      });

      //   Update ticket
      var ticket_update = $("#ticket_update");
      var ticket_code_update = $("#ticket_code_update");
      var ticket_update_btn = $("#ticket_update_btn");
      var edit_btn = $("#edit");
      edit_btn.click(function () {
        var ticket_id = $(this).parents("tr").attr('id');
        var ticket = $(this).parents("tr").children("td:nth-child(1)");
        var ticket_code = $(this)
          .parents("tr")
          .children("td:nth-child(2)");
        ticket_update.val(ticket.text().trim());
        ticket_code_update.val(ticket_code.text().trim());
        ticket_update_btn.click(function () {
          if (ticket_update.val() != "" && ticket_code_update != "") {
 $.ajax({
                        url: '/coders/ticket/update',
                        type: "POST",
                        data: {
                            "_token": "{{csrf_token()}}",
                            id: ticket_id,
                            ticket_name: $('#ticket_update').val().trim(),
                            ticket_code: $('#ticket_code_update').val().trim(),
                        },
                        success: function (data) {
                            if (data.status == true) {
                               $("#tikcet_edit_modal").modal("hide");
            Swal.fire({
              position: "center",
              icon: "success",
              title: "Bilet yeniləndi",
              showConfirmButton: false,
              timer: 1000,
            });
            ticket.text($('#ticket_update').val());
            ticket_code.text($('#ticket_code_update').val());
                            }
                        }
                    })
            
          }
        });
      });

      //   Delete
      var deleteBtn = $("#delete");
      var confirmation_btn_delete = $("#confirmation-btn-delete");
      deleteBtn.click(function () {
        var ticket_id = $(this).parents("tr").attr("id");
        confirmation_btn_delete.click(function () {
         
           $.ajax({
                    url: '/coders/ticket/delete',
                    type: "POST",
                    data: {
                        "_token": "{{csrf_token()}}",
                        id: ticket_id
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.status == true) {
                                        Swal.fire({
                        position: "center",
                        icon: "success",
                        title: "Bilet silindi",
                        showConfirmButton: false,
                        timer: 1000,
                      });
                                        $('#' + ticket_id).fadeOut();
                        }
                    }
                })
        });
      });
    </script>
@endsection

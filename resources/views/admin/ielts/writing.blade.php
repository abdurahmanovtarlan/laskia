@extends('admin.newbase')
@section('content')
    <link href="{{asset('admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div id="alert"></div>
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <a href="{{route('writing_create_question',$ielts[0]->ielts_name_slug)}}"
                                           class="btn btn-info add_question shadow ">Sual əlavə
                                            edin.</a>
                                        <hr>
                                        <table id="question_table"
                                               class="table table-striped table-bordered dataTable hover">
                                            <thead>
                                            <tr>
                                                <th>Sual</th>
                                                <th>Vaxt (dəqiqə)</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if (count($ieltswritings) > 0)
                                                @foreach ($ieltswritings as $writing)
                                                    <tr id="{{ $writing->id}}" class="question">
                                                        <td>{!! $writing->question !!}</td>
                                                        <td>{!! $writing->ielts_writing_time !!} dəqiqə</td>
                                                        <td><a href="" class="btn btn-primary btn-sm">Bax</a>
                                                            <a href="" class="btn btn-danger btn-sm">Sil</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="7">Melumat yoxdur</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
    <script src="{{asset('admin/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('admin/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#question_table').DataTable({
            responsive: true,
        });
    </script>
@endsection

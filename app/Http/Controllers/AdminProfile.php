<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class AdminProfile extends Controller
{
    public function index($id)
    {
        $user = User::find($id);
        return view('admin.profile', compact('user'));
    }

    public function password_change($id)
    {
        return view('admin.password_change');
    }
}

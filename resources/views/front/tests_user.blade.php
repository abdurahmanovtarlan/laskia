@extends('front.base')
@section('content')
<div class="container setting">
    <div class="row">
        <div class="col-md-3 mb-4">
            <div class="">
                <ul class="list">
                    <li class="list-item "><a href="/main/{{Auth::id()}}">Əsas</a></li>
                    <li class="list-item active"><a href="/tests/{{Auth::id()}}">Testlər</a></li>
                    <li class="list-item "><a href="{{route('result_all')}}">Nəticələr</a></li>
                    <li class="list-item"><a href="/password_change/{{Auth::id()}}">Parolu dəyişdirin</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-8">
            @if (count($register_exam) > 0)
            @foreach($register_exam as $exam)
            <div class="card mb-2 shadow">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <div>
                            <a href="{{route('test',$exam->exam->test_title_slug)}}">{{$exam->exam->test_title}}</a> <span class="text-muted text-small">/ {{$exam->exam->total_question}} sual</span>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            <div class="card mb-2 shadow">
                <div class="card-body">
                    Məlumat yoxdur
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
@extends('front.base')

@section('content')
<div class="container setting">
    <div class="row">
        <div class="col-md-3 mb-4">
            <div class="">
                <ul class="list">
                    <li class="list-item "><a href="/main/{{Auth::id()}}">Əsas</a></li>
                    <li class="list-item "><a href="{{route('result_all')}}">Nəticələr</a></li>
                    <li class="list-item active"><a href="/password_change/{{Auth::id()}}">Parolu dəyişdirin</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-8">
            <div id="alert" class="ml-3"></div>
            <div class="setting-section">
                <form action="" method="POST">
                    <!-- <div class="form-group">
                <label for="password">Şifrə</label>
                <input type="password" class="form-control" id="password" />

              </div> -->
                    <label for="password" class="m-0">Əvvəlki şifrə</label>
                    <div class="input-group ">
                        <input name="old_password" type="password" class="form-control" aria-label="Default" id="old_password" placeholder="" aria-describedby="inputGroup-sizing-default" />
                        <div class="input-group-append">
                            <span class="input-group-text password">
                                <i class="far fa-eye"></i>
                            </span>
                        </div>
                    </div>
                    <div class="invalid-feedback"></div>
                    <label for="password" class="m-0 mt-3">Şifrə</label>
                    <div class="input-group">
                        <input name="password" type="password" class="form-control" aria-label="Default" id="password" placeholder="" aria-describedby="inputGroup-sizing-default" />
                        <div class="input-group-append">
                            <span class="input-group-text password">
                                <i class="far fa-eye"></i>
                            </span>
                        </div>
                    </div>
                    <div class="invalid-feedback"></div>
                    <div id="password-strength-status"></div>
                    <label for="confirm_password" class="m-0">Təkrar şifrə</label>
                    <div class="input-group">
                        <input name="confirm_password" type="password" class="form-control" aria-label="Default" id="confirm_password" placeholder="" aria-describedby="inputGroup-sizing-default" />
                        <div class="input-group-append">
                            <span class="input-group-text password">
                                <i class="far fa-eye"></i>
                            </span>
                        </div>
                    </div>
                    <div class="invalid-feedback"></div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-block rounded-0 shadow update mt-3">
                            Yenilə
                        </button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>
    $(".password").click(function() {
        $(this).find("i").toggleClass("fa-eye-slash");
        var password_input = $(this).parent().prev();

        if (password_input[0].type === "password") {
            password_input[0].type = "text";
        } else {
            password_input[0].type = "password";
        }
    });
    $(
        'input[type="text"],input[type="password"],input[type="email"]'
    ).focusin(function() {
        $(this).parent().addClass("input-focus");
    });
    $(
        'input[type="text"],input[type="password"],input[type="email"]'
    ).focusout(function() {
        $(this).parent().removeClass("input-focus");
    });
    var password = $("#password");
    var old_password = $("#old_password");
    var confirm_password = $("#confirm_password");
    $(".update").click(function(e) {
        e.preventDefault();
        if (password.val().length < 6) {
            password.parent().addClass("is-invalid");
            console.log(password.parent().next());
            password.parent().next().text("6-dan artıq simvol olmalıdır");
        }
        if (old_password.val() == "") {
            console.log(old_password.parent());
            old_password.parent().addClass("is-invalid");
            old_password.parent().next().text("Əvvəlki şifrəni daxil edin.");
        }
        if (password.val() == "") {
            password.parent().addClass("is-invalid");
            password.parent().next().text("Şifrə daxil edin.");
        }
        if (confirm_password.val() != password.val()) {
            confirm_password.parent().addClass("is-invalid");
            confirm_password.parent().next().text("Şifrələr eyni deyil.");
        }
        if (password.val() == confirm_password.val() && old_password.val() != '') {
            var user_id = '{{Auth::id()}}';
            $.ajax({
                type: "POST",
                url: '{{route("pass_change")}}',
                data: {
                    '_token': "{{csrf_token()}}",
                    'id': user_id,
                    'old_password': old_password.val(),
                    'password': password.val().trim(),
                },
                success: function(data) {
                    if (data.status == 'success') {
                        $('#alert').append(`
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                    Şifrə yeniləndi.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                    `);
                        setTimeout(function() {
                            $(".alert").alert('close')
                        }, 2000)
                    } else {
                        $('#alert').append(`
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    Əvvəlki şifrə yanlışdır.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                    `);
                    }
                },
                error: function(err) {
                    $.each(err.responseJSON.errors, function(key, value) {
                        $('#alert').append(`
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    ${value}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                    `);
                    });
                }
            });
        }
    });
    password.keyup(function() {
        if (password.parent().hasClass("is-invalid")) {
            password.parent().removeClass("is-invalid");
        }
        if (password.val() == "") {
            password.parent().addClass("is-invalid");
            password.parent().next().text("Şifrə daxil edin.");
        }
        if (password.val().length < 6) {
            password.parent().next().text("6-dan artıq simvol olmalıdır");
        }
        if (password.val().length > 6) {
            password.parent().next().text("");
        }
    });
    confirm_password.keyup(function() {
        if (confirm_password.parent().hasClass("is-invalid")) {
            confirm_password.parent().removeClass("is-invalid");
        }
        if (confirm_password.val() == "") {
            confirm_password.parent().addClass("is-invalid");
            confirm_password.parent().next().text("Təkrar şifrə daxil edin.");
        } else if (confirm_password.val() != password.val()) {
            confirm_password.parent().next().text("Şifrələr eyni deyil.");
        }
    });
    $("#password").keyup(function() {
        var number = /([0-9])/;
        var alphabets = /([a-zA-Z])/;
        var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
        if ($("#password").val().length < 6) {
            $("#password-strength-status").removeClass();
            $("#password-strength-status").addClass("weak-password");
            $("#password-strength-status").html(
                "Zəif (6-dan artıq simvol olmalıdır.)"
            );
        } else {
            if (
                $("#password").val().match(number) &&
                $("#password").val().match(alphabets) &&
                $("#password").val().match(special_characters)
            ) {
                $("#password-strength-status").removeClass();
                $("#password-strength-status").addClass("strong-password");
                $("#password-strength-status").html("Güclüdür");
            } else {
                $("#password-strength-status").removeClass();
                $("#password-strength-status").addClass("medium-password");
                $("#password-strength-status").html(
                    "Orta (əlifbalar, nömrələr və xüsusi simvol daxil edilməlidir.)"
                );
            }
        }
    });
</script>
@endsection

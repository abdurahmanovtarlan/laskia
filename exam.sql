-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 23, 2020 at 07:38 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exam`
--

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `test_title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `test_title_slug` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_question` int(11) NOT NULL,
  `timer` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `ticket_id`, `admin_id`, `test_title`, `test_title_slug`, `total_question`, `timer`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Abituriyent Online Sinaq', 'abi8j8ERlKBdumXEUvtveCJriy8j8ERlKBdumXEUvtveCJn8j8ERlKBdumXEUvtveCJon8j8ERlKBdumXEUvtveCJin8j8ERlKBdumXEUvtveCJsinaq', 90, 9000, '2020-06-22 17:12:47', '2020-06-22 17:12:47');

-- --------------------------------------------------------

--
-- Table structure for table `exam_registers`
--

CREATE TABLE `exam_registers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `exam_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `group_subjects`
--

CREATE TABLE `group_subjects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `point` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_05_06_151606_create_exams_table', 1),
(5, '2020_05_06_162728_create_questions_table', 1),
(6, '2020_05_07_175443_create_questions_options_table', 1),
(7, '2020_05_07_191116_create_tests_table', 1),
(8, '2020_05_07_191159_create_test_answers_table', 1),
(9, '2020_05_07_191820_create_results_table', 1),
(10, '2020_05_16_162618_create_exam_registers_table', 1),
(11, '2020_06_10_133903_create_tickets_table', 1),
(12, '2020_06_14_174519_create_groups_table', 1),
(13, '2020_06_14_174604_create_subjects_table', 1),
(14, '2020_06_14_184318_create_group_subjects_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('coders@gmail.com', '$2y$10$c53HtOmuxjTyB6LxRWbOiuh3BV/KoZ7PbbLCKHNOVSxoSxTZklNqi', '2020-06-23 04:48:38');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `test_id` bigint(20) UNSIGNED NOT NULL,
  `question_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `question_image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer_explanation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `test_id`, `question_text`, `question_image`, `answer_explanation`, `created_at`, `updated_at`) VALUES
(1, 1, '<p>1. &ldquo;&nbsp;&Ccedil;i&ccedil;əklərin və otların ətri qarışmışdı bir-birinə,&ccedil;eşmələrin səsi&nbsp;qarışmışdı quşların səsinə&nbsp; &ldquo; c&uuml;mləsinin sintaktik təhlili hansı varianta uyğundur?&nbsp;</p>', '', NULL, '2020-06-22 17:57:26', '2020-06-22 17:57:26'),
(2, 1, '<p>2. Fonetika b&ouml;lməsinin vəzifələrinə aid deyil:&nbsp;</p>', '', NULL, '2020-06-22 18:04:30', '2020-06-22 18:04:30'),
(3, 1, '<p>3.&nbsp;İdi, imiş, isə&nbsp;hissəciklərinin hər &uuml;&ccedil;&uuml;nə aid edilə bilməz:&nbsp;</p>', '', NULL, '2020-06-22 18:05:27', '2020-06-22 18:05:27'),
(5, 1, '<p>4. Orfoqrafik normanın pozulduğu s&ouml;z hansıdır?&nbsp;</p>\n\n<p>Fırlanır&nbsp;karusel&nbsp;bir təkər kimi&nbsp;</p>\n\n<p>Daima fırlanan, d&ouml;nən&nbsp;yer&nbsp;kimi...&nbsp;</p>\n\n<p>Tazıların&nbsp;&uuml;st&uuml;nə&nbsp;Dovşanlar&nbsp;da g&uuml;lərmiş...&nbsp;</p>\n\n<p>Dəyişməsin neyləsin, axı,&nbsp;d&uuml;nya&nbsp;fırlanır.&nbsp;</p>\n\n<p>&nbsp;</p>', '', NULL, '2020-06-22 18:10:04', '2020-06-22 18:10:04'),
(6, 1, '<p>5.&nbsp;Hansı c&uuml;mlədə sual əvəzliyi sual intonasiyası yaratmayıb? (Durğu işarələri buraxılmışdır.)&nbsp;</p>', '', NULL, '2020-06-22 18:12:24', '2020-06-22 18:12:24'),
(7, 1, '<p>6. Müəyyən edin:        </p>\r\n\r\n<table border=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>  Doğru </p>\r\n			</td>\r\n			<td>\r\n			<p>Yanlış  </p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>1.Başqasının nitqı iki üsulla verilə bilər. </p>\r\n\r\n			<p>2.Vasitəsiz nitq vasitəli nitqə çevrilərkən bütün durğu işarələri atılır. </p>\r\n			</td>\r\n			<td>\r\n			<p>3.Müəllifin sözlərində xəbərlər yalnız təsriflənən feillə ifadə olunur. </p>\r\n\r\n			<p>4.Vasitəsiz nitq vasitəli nitqə çevrilərkən, adətən, tamamlıq budaq cümləsi yaranır. </p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', '', NULL, '2020-06-22 18:13:34', '2020-06-22 18:13:58'),
(8, 1, '<p>7. Köməkçi nitq hissələrinin ardıcıllığını müəyyən edin.      </p>\r\n\r\n<p>Allah! Bilirik, cism deyil, bəs nədir Allah? </p>\r\n\r\n<p>Ən yüksək olan həqiqətdir Allah. </p>\r\n\r\n<p>Dondunsa, təkamüllə gözəllik qabağında, </p>\r\n\r\n<p>Dərd et, bu təəccübdə, bu heyrətdədir Allah. </p>\r\n\r\n<p> 1.qoşma </p>\r\n\r\n<p> 2.bağlayıcı </p>\r\n\r\n<p> 3.ədat </p>\r\n\r\n<p> 4.modal söz </p>\r\n\r\n<p> 5.nida </p>', '', NULL, '2020-06-22 18:19:10', '2020-06-22 18:19:56'),
(9, 1, '<p>8. Omonim k&ouml;mək&ccedil;i nitq hissələri&nbsp;&nbsp;hansılardır?&nbsp;</p>', '', NULL, '2020-06-22 18:21:56', '2020-06-22 18:21:56'),
(10, 1, '<p>9.&nbsp;Tabesizlik bağlayıcıları haqqında yanlış fikirdir:&nbsp;&nbsp;</p>', '', NULL, '2020-06-22 18:22:43', '2020-06-22 18:22:43'),
(11, 1, '<p>10.&nbsp;Eynik&ouml;kl&uuml; s&ouml;zlər cərgəsini m&uuml;əyyənləşdirin.&nbsp;</p>', '', NULL, '2020-06-22 18:23:17', '2020-06-22 18:23:17'),
(12, 1, '<p>11.&nbsp;&ldquo;Qarşı təpədə&nbsp;d&uuml;şmən qoşununun əsgərləri can verirdilər&rdquo; c&uuml;mləsində məcazi mənalı ifadənin fərqli mənada işlənmiş qarşılığını m&uuml;əyyən edin.</p>', '', NULL, '2020-06-22 18:24:10', '2020-06-22 18:24:10'),
(13, 1, '<p>12.&nbsp;Uyğunluğu m&uuml;əyyən edin.&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp; 1.sadə&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp; 2.d&uuml;zəltmə&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp; 3.m&uuml;rəkkəb&nbsp;</p>\n\n<p>a) xoşbəxtlik&nbsp;</p>\n\n<p>b) sarsıntı&nbsp;</p>\n\n<p>c) salamlaşmaq&nbsp;</p>\n\n<p>d) mənsiz&nbsp;</p>\n\n<p>e) məcburiyyət&nbsp;</p>', '', NULL, '2020-06-22 18:25:09', '2020-06-22 18:25:09'),
(14, 1, '<p>13. Hansı c&uuml;mlədə qoşmanı sinonimi ilə əvəz etmək m&uuml;mk&uuml;n deyil?&nbsp;</p>', '', NULL, '2020-06-22 18:25:49', '2020-06-22 18:25:49'),
(15, 1, '<p>14. Hansı s&ouml;z&uuml;n tərkibindəki alınma şəkil&ccedil;i dilimizdə tək işləndikdə leksik məna bildirmir?&nbsp;</p>', '', NULL, '2020-06-22 18:26:16', '2020-06-22 18:26:16'),
(16, 1, '<p>15. Hansı cərgədəki s&ouml;zlər bir-birini əvəz edə bilən sinonimlərdir?&nbsp;</p>', '', NULL, '2020-06-22 18:27:12', '2020-06-22 18:27:12'),
(17, 1, '<p>16. Y&ouml;nl&uuml;k halda tələff&uuml;z&uuml;ndə qapalı sait olan s&ouml;zlər hansılardır?&nbsp;</p>\n\n<p>1.dəvəyə&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3. nənəni&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5. anlayan&nbsp;</p>\n\n<p>2.&ccedil;i&ccedil;əyə&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4. Dəvə&ccedil;iyə&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 6.babaya&nbsp;&nbsp;&nbsp;&nbsp;</p>', '', NULL, '2020-06-22 18:27:59', '2020-06-22 18:27:59'),
(18, 1, '<p>17. Yanlış cavab hansıdır?&nbsp;&nbsp;&nbsp;</p>', '', NULL, '2020-06-22 18:28:31', '2020-06-22 18:28:31'),
(19, 1, '<p>18.&rdquo;D&uuml;nən oxuyanda &ccedil;ox məlumatı dəqiq &ouml;yrəndim&rdquo; c&uuml;mləsinin tərkibindən ikinci s&ouml;z &ccedil;ıxarılsa nə dəyişər?&nbsp;</p>', '', NULL, '2020-06-22 18:29:16', '2020-06-22 18:29:16'),
(20, 1, '<p>19. (-ın<sup>4</sup>&nbsp;)&nbsp;&nbsp;yiyəlik hal&nbsp;şəkil&ccedil;isini qəbul edərkən son saiti d&uuml;ş&uuml;r:&nbsp;</p>', '', NULL, '2020-06-22 18:32:23', '2020-06-22 18:32:23'),
(21, 1, '<p>20. Hansı n&uuml;munədə isim işlənmişdir?&nbsp;</p>', '', NULL, '2020-06-22 18:33:30', '2020-06-22 18:33:30'),
(22, 1, '<p>21&nbsp;. Doğru fikri m&uuml;əyyən edin&nbsp;</p>\n\n<p>&nbsp;__İsim_________</p>', '', NULL, '2020-06-22 18:34:40', '2020-06-22 18:34:40'),
(23, 1, '<p>22. &ldquo;Kəndimizdə&nbsp;daş&nbsp;binalar &ccedil;oxdur&rdquo; c&uuml;mləsində altından xətt &ccedil;əkilmiş s&ouml;z&uuml;n morfoloji sualı hansıdır?</p>', '', NULL, '2020-06-22 18:35:09', '2020-06-22 18:35:09'),
(24, 1, '<p>23.Hansı c&uuml;mlədə işlənmiş tire (- ) işarəsi fərqli funksiyaya malikdir?&nbsp;&nbsp;</p>', '', NULL, '2020-06-22 18:35:47', '2020-06-22 18:35:47'),
(25, 1, '<p>24. M&uuml;əyyən edin.</p>\n\n<table border=\"1\">\n	<tbody>\n		<tr>\n			<td>\n			<p>Doğru&nbsp;</p>\n			</td>\n			<td>\n			<p>Yanlış&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td>\n			<p>1.Dil cəmiyyətdən kənarda da formalaşa bilər.&nbsp;</p>\n\n			<p>2.Azərbaycan dili t&uuml;rk dilləri ailəsinin oğuz qrupuna aiddir.&nbsp;</p>\n			</td>\n			<td>\n			<p>3.Leksik norma s&ouml;z&uuml;n mənasına bələd olmağı, onu yerində işlətməyi tələb edir.&nbsp;</p>\n\n			<p>4.Miqdar saylarından sonra gələn isimlər həm təkdə, həm də cəmdə olur.&nbsp;</p>\n			</td>\n		</tr>\n	</tbody>\n</table>', '', NULL, '2020-06-22 18:36:49', '2020-06-22 18:36:49'),
(26, 1, '<p>25.Mətndə t&uuml;nd qara şriftlə verilmiş ifadənin izahına uyğundur:&nbsp;</p>\n\n<p>Qocanın&nbsp;<strong>g&ouml;zləri yol</strong> &ccedil;əkdi. Elə yanıqlı ah &ccedil;əkdi ki, elə bil ağzından alov p&uuml;sk&uuml;rd&uuml;.&nbsp;&nbsp;</p>\n\n<p>Lakin Cavad gələndə artıq Əziz Şərifin&nbsp;son nəfəsi idi.&nbsp;Bu an onun &uuml;rəyindən nələr ke&ccedil;di, kimləri xatırladı?&nbsp;</p>\n\n<p>&nbsp;</p>', '', NULL, '2020-06-22 18:38:04', '2020-06-22 18:38:04'),
(27, 1, '<p>26.Mətndə altından xətt &ccedil;əkilmiş ifadənin kontekstə uyğun qarşılığını m&uuml;əyyən edin və fərqli mənada işləndiyi c&uuml;mləni m&uuml;əyən edin.&nbsp;</p>', '', NULL, '2020-06-22 18:38:34', '2020-06-22 18:38:34'),
(28, 1, '<p>27.Doğru cavabı m&uuml;əyyən edin.&nbsp;</p>', '', NULL, '2020-06-22 18:41:59', '2020-06-22 18:41:59'),
(29, 1, '<p>28.&rdquo;Vəziyyətlərinə g&ouml;rə dəyər qazanan mərdlər var&rdquo; c&uuml;mləsi əsasında doğru cavabı m&uuml;əyyən edin.&nbsp;</p>\n\n<p>Səbəb: Nə? sualına cavab verir</p>\n\n<p>&nbsp;Nəticə:&nbsp;</p>', '', NULL, '2020-06-22 18:47:13', '2020-06-22 18:47:13'),
(30, 1, '<p>29.&nbsp;Hansı s&ouml;zdən istifadə edilməsə, şəxsə g&ouml;rə c&uuml;mlənin n&ouml;v&uuml; dəyişər?&nbsp;</p>\n\n<p>&nbsp; &ldquo;Şagirdlər1&nbsp;həyatda2&nbsp;&ouml;z3&nbsp;m&uuml;əllimlərinin4&nbsp;&ouml;yrətdiklərindən5&nbsp;qidalanırlar&rdquo;.&nbsp;</p>', '', NULL, '2020-06-22 18:47:46', '2020-06-22 18:47:46'),
(31, 1, '<p>30.Tərkibində ara s&ouml;z işlənmiş c&uuml;mlə ilə bağlı doğru fikri m&uuml;əyyən edin.&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp; 1. Modal s&ouml;zdən istifadə edilə bilər.&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp; 2. Ən azı iki məqamda verg&uuml;l işarəsindən istifadə edilməlidir.&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp; 3. Danışanın ifadə etdiyi fikrə m&uuml;nasibət bildirilməyə də bilər.&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp; 4. C&uuml;mlə &uuml;zv&uuml; olmayan ifadədən istifadə edilmişdir.&nbsp;</p>', '', NULL, '2020-06-22 18:48:11', '2020-06-22 18:48:11'),
(32, 1, '31. a * b = 31 və b * c = 17 olarsa, a + 3b - 2c -ni tapın. (a,b,c E N)', '', NULL, '2020-06-22 18:48:50', '2020-06-22 19:06:33'),
(34, 1, '<p>32. &nbsp;Hesablayın.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img data-cke-saved-src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAO8AAAA6CAYAAABYgw4lAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAcBSURBVHhe7Zy7rSxFFEVvAAQAAeDgEcAjAEQA+JAAwkYvAEQCeJggYWNABGSAgYlHDlCLN1s6lOrb0/+7l3Q0093V1dWnzq7f1L0vxhhjjDHGGGOMMcYYY4wxxhhjjubzZH8n++fx+V2y95IZY07MZ8kQbW6/JbOAjTkpiPPHZPS84tNk6oURtjHmhHyc7JN3X/+HemOL15iL8XWyP5N98N+RMeYy/JAMAZsNoEWkZYwLDDLmKwyHzOsG8ZXiA2sJk9j5PZl73Y3Asb8+Po2ZgXlsTbxawHLjvyEWr1lKS7xfJvMi1cZYvGYpNfFyrjWcNith8V6TM+xkKomXHheLUK7vkznGVsbivR6IJl84wvbeyZSLl++lcmGsOpuVsXivxZl2MkXxtoSL7VmuV4PFey3OtJMpitccgMV7DxDR3juZLN6DsXjvwRE7mSzeg1kiXuZdXzw+zbos8e1RO5ks3oOZFS9B9c3j02wDdfFtshEfk+aonUybiffDZPy25L2VbWbFy++JSwKF+5iTUS+mDz7GZz2O3Mm0iXgJRG22958jtZkRL5U1ElAlLN55mMe2hIlw1hQPPznR4Y1qZrOeF3h5i7fNqHgZnrEJ4KhW/jWCr2sbL9beyYQI/0g20+FZvAczKl6GcVTuEXOrZxh9vzNS8zmC0eaH3J7dyTSjGYv3YEaDm0q6oi9H3++MUGZ8HgXSEi727Mjo1OJlzvVLMr0sC1qxZWPo8VUybTdjGFIattyFj5L99fisoSFzbQiHf/Gj/IU/3yYT+eKhfMwx15gPl+oih2vUXexdGDpSV62y9cRLGVT+Up33YgZ6PhAz78D3lt+3YEa8PyVbugbSJS8IjsNJai1ipanF4hppSIvDyEOVtqcT92JEvPgPP8aAi3Be8y/5TGl1L/5TXcin+BkRc4/qpvYM6kf1oPpBHNRhC57XEi/5MjRlsQZUDtX1SMxAywdiyTuQx6iY1mDmebuJF4eWWjFVDun4zmd0uq7HiroTI+KVD/JgBHyLz6J/OJf3OnlQ5Me1+snRn8KVer8S5F8Tr54Zh36cIyA5j6hKZYoxQ77YiA/EzDvgJ9KOvOsa5PXSYjfx1gJQFci1N4/vsbJ032Zj+4N5VrzyH70Jw8VaL5IHRX6sfKLvS/TSka96t5aRTu9Va5hHYoY0Oibflg9E7x0icSQ4y6gvYmzn9dJiN/FqyJJXBOglSYPhLP3JFS+ma3dkRry1BiwOJbFSj5IHRX48G9CjAUaaWs+rmKjVra73YgZGfBAZfYe94y+vlxa797yl4CBdbN0okH7zikK+I8/2vBECmJ4Hv+UBkAdFfjwqXtKzdZDyjAQ06Wvi7b3XTMyIlg/EzDsg3tJztiKvlxa7z3nzViwPGpyFrQEvxvN7w6gjeVa8+Czf64x/cz/nQZEf5/VQg0377ycjXak8OeRfEy/nKANlZX4an8viE402z+nFDDbiAzHzDlx/deKVg6MD5dAoqNiyxXtyU0XNsJZ4tcBBOfgk39my1BgRr4K8FGjyWSwT5Y0BEP1KHeg4BqWeEe8T1BHPZkVY6TkmD45rC0NAXr3V5ljPMtV3L2ZgxAdL34E0JZ9sAWX/ORnPUxlbbCJeHqxgl6k35Vqcm/CbWxSXfnOL98qUx570gutZRsSr4Cw9k2N6Ehb75Nc43yvVRTSuEdAEjM7lwSqxxOkLfuFcryHriRd4vqZJWL7o1IuZng9gyTtwrub3tSnFGQ1Hi02HzUtAvNHpAqfrd7y9oMK2/n9FI+IFgm+vHsC8Q+I9otMY4VTiRQy1Vo6WtiTqLeF5W/+/olHxUpYr7m2+Mmf3+anEq6ENPWwUMMJlWJRDL6RhEkMtnKy5C/dwjuukI78lWwJL1HpB8pndPjgq3rP3Anek1ZmcgdMNmxmixvkP3+OwNYJINJTGwRxjiEpzOImM8xwjpNEtgTVIn4tIvbGeoYYkzs1KjIoXzh5Md4N6XmN0tRWnE+8oEmh0LufiqiHOjz1kfqzebEYQiFK9eQkaGsQ72qPPiBeorDMH1F2g7s4ujMuKV8Kjp8tXJ8Xa4iVN7/8VzeY5K17yZINBrfEwz3MVH19WvIBg6eE0XM17u7XFyxB9pNerzYlLzIoXKCtrACNlNnNcybeXFq/Q4hQCbon1GfEiyJHFIvKe2T64RLzGwKWHzb0tcWuJlx5XC2OC9DQYykvQas9sH7R4zVIuP+el8BIei0USp65LzDrWSjCQjvRR0Dn0thqW5yZxkobvS7YPWrxmKZcWb21LHIZIc7HJuNbbEggt4WLq4UlHnvEnLa5xLjYuJSxes5RbzHmvDA1Gb++vMSXoIEbWYcxGWLxmKRbvwSDaOHyPFufn5vXSmr5ZvMYYY4wxxhhjjDHmLry8/Aslpvf24OsiiQAAAABJRU5ErkJggg==\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAO8AAAA6CAYAAABYgw4lAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAcBSURBVHhe7Zy7rSxFFEVvAAQAAeDgEcAjAEQA+JAAwkYvAEQCeJggYWNABGSAgYlHDlCLN1s6lOrb0/+7l3Q0093V1dWnzq7f1L0vxhhjjDHGGGOMMcYYY4wxxhhjjubzZH8n++fx+V2y95IZY07MZ8kQbW6/JbOAjTkpiPPHZPS84tNk6oURtjHmhHyc7JN3X/+HemOL15iL8XWyP5N98N+RMeYy/JAMAZsNoEWkZYwLDDLmKwyHzOsG8ZXiA2sJk9j5PZl73Y3Asb8+Po2ZgXlsTbxawHLjvyEWr1lKS7xfJvMi1cZYvGYpNfFyrjWcNith8V6TM+xkKomXHheLUK7vkznGVsbivR6IJl84wvbeyZSLl++lcmGsOpuVsXivxZl2MkXxtoSL7VmuV4PFey3OtJMpitccgMV7DxDR3juZLN6DsXjvwRE7mSzeg1kiXuZdXzw+zbos8e1RO5ks3oOZFS9B9c3j02wDdfFtshEfk+aonUybiffDZPy25L2VbWbFy++JSwKF+5iTUS+mDz7GZz2O3Mm0iXgJRG22958jtZkRL5U1ElAlLN55mMe2hIlw1hQPPznR4Y1qZrOeF3h5i7fNqHgZnrEJ4KhW/jWCr2sbL9beyYQI/0g20+FZvAczKl6GcVTuEXOrZxh9vzNS8zmC0eaH3J7dyTSjGYv3YEaDm0q6oi9H3++MUGZ8HgXSEi727Mjo1OJlzvVLMr0sC1qxZWPo8VUybTdjGFIattyFj5L99fisoSFzbQiHf/Gj/IU/3yYT+eKhfMwx15gPl+oih2vUXexdGDpSV62y9cRLGVT+Up33YgZ6PhAz78D3lt+3YEa8PyVbugbSJS8IjsNJai1ipanF4hppSIvDyEOVtqcT92JEvPgPP8aAi3Be8y/5TGl1L/5TXcin+BkRc4/qpvYM6kf1oPpBHNRhC57XEi/5MjRlsQZUDtX1SMxAywdiyTuQx6iY1mDmebuJF4eWWjFVDun4zmd0uq7HiroTI+KVD/JgBHyLz6J/OJf3OnlQ5Me1+snRn8KVer8S5F8Tr54Zh36cIyA5j6hKZYoxQ77YiA/EzDvgJ9KOvOsa5PXSYjfx1gJQFci1N4/vsbJ032Zj+4N5VrzyH70Jw8VaL5IHRX6sfKLvS/TSka96t5aRTu9Va5hHYoY0Oibflg9E7x0icSQ4y6gvYmzn9dJiN/FqyJJXBOglSYPhLP3JFS+ma3dkRry1BiwOJbFSj5IHRX48G9CjAUaaWs+rmKjVra73YgZGfBAZfYe94y+vlxa797yl4CBdbN0okH7zikK+I8/2vBECmJ4Hv+UBkAdFfjwqXtKzdZDyjAQ06Wvi7b3XTMyIlg/EzDsg3tJztiKvlxa7z3nzViwPGpyFrQEvxvN7w6gjeVa8+Czf64x/cz/nQZEf5/VQg0377ycjXak8OeRfEy/nKANlZX4an8viE402z+nFDDbiAzHzDlx/deKVg6MD5dAoqNiyxXtyU0XNsJZ4tcBBOfgk39my1BgRr4K8FGjyWSwT5Y0BEP1KHeg4BqWeEe8T1BHPZkVY6TkmD45rC0NAXr3V5ljPMtV3L2ZgxAdL34E0JZ9sAWX/ORnPUxlbbCJeHqxgl6k35Vqcm/CbWxSXfnOL98qUx570gutZRsSr4Cw9k2N6Ehb75Nc43yvVRTSuEdAEjM7lwSqxxOkLfuFcryHriRd4vqZJWL7o1IuZng9gyTtwrub3tSnFGQ1Hi02HzUtAvNHpAqfrd7y9oMK2/n9FI+IFgm+vHsC8Q+I9otMY4VTiRQy1Vo6WtiTqLeF5W/+/olHxUpYr7m2+Mmf3+anEq6ENPWwUMMJlWJRDL6RhEkMtnKy5C/dwjuukI78lWwJL1HpB8pndPjgq3rP3Anek1ZmcgdMNmxmixvkP3+OwNYJINJTGwRxjiEpzOImM8xwjpNEtgTVIn4tIvbGeoYYkzs1KjIoXzh5Md4N6XmN0tRWnE+8oEmh0LufiqiHOjz1kfqzebEYQiFK9eQkaGsQ72qPPiBeorDMH1F2g7s4ujMuKV8Kjp8tXJ8Xa4iVN7/8VzeY5K17yZINBrfEwz3MVH19WvIBg6eE0XM17u7XFyxB9pNerzYlLzIoXKCtrACNlNnNcybeXFq/Q4hQCbon1GfEiyJHFIvKe2T64RLzGwKWHzb0tcWuJlx5XC2OC9DQYykvQas9sH7R4zVIuP+el8BIei0USp65LzDrWSjCQjvRR0Dn0thqW5yZxkobvS7YPWrxmKZcWb21LHIZIc7HJuNbbEggt4WLq4UlHnvEnLa5xLjYuJSxes5RbzHmvDA1Gb++vMSXoIEbWYcxGWLxmKRbvwSDaOHyPFufn5vXSmr5ZvMYYY4wxxhhjjDHmLry8/Aslpvf24OsiiQAAAABJRU5ErkJggg==\" style=\"height: 100px; width: 1000px;\"><br></p>', '', NULL, '2020-06-22 18:53:48', '2020-06-22 20:39:25'),
(35, 1, '<p>33.&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGcAAAAzCAYAAACKXhYYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAL4SURBVHhe7ZrLrdRAEEUnABIgAAIgAAgAsUbsIQMCIABEAuxYEgELiIAMWJABOUAd5l2paNkeu/1re+6RSrbb8/HUrU+7xxdjzIl5HPYr7E/Y77CnYaYBHoV9D3v37+i65ZhxszNkzY8wZQtbjhk3DfA5TNlC5iiLTAPknmNhGoTsQRxEcklrCIR5ed39r8SZnSknAOUEweyI+o16DRnk0tYQZAk3n2vdhDobG4Y+5pWHBqFMvg1z5vRAP9E9DLOwj2FbzMgoZ+8fthZnABzzJexV2FbNXsJYnBuQJWSL7mfWhu/Rd1mcEeSbzRq0itBlnBMKhK7XbRUch4LopaztsXbmzLnBm7BnYQiEk56EbYXF6YEyonsMyg3i0Ki3xOKYZckRxT5rWm6Y4+ibdOA/TMd5ojIafbiXNep4Hpb9RnB/C3sRxk02cB7/Vk2C1AMsznzIFrKESQ9+FQhTtRqSxSmbpsrc6zClL1+ucY73mBa3Cr7pagmMzRIHtXOJY1yCaAxjX/+1VH/pCVFJY1tCAM8ua3xwzhwJpGgoz7PluOuCjgiRryZe2q2mrpJWgm8+hVUFcBYn78O9iTOHvpKmhdcq1hanKwpbsZLazOH3d5U0SlmXYKNZW5x7AP+U4jGW+wzHk4VStNDofz7sq+Hnc5ow6DzCaMY2FFX3AL8/Ox5R8Es2+dSYK0SMouPeM6gpKG2LLFOY5VlsmcJcUbNiixPHPgVDM/sQNvQ6SpzFmQkliCnz1KdgcP7XsL5/MBF877J2+B5IZOf7lyngfHoLi6EZRK5epliI0/TAcm6eI26s5ffPWqZYiFP0QJxIWauJKmUOfygJxqZmIe9BYLY4b40nQLmmw4lT+xQMP7bsOYxlkTkeK1Rt7xsL15WvrWlwGlGPU4gmHDP2KRicV87WFP3ZpixT8Fm1ve8WXMPePfDwlL2vhPNlAMiGZmMt9MBDg/Nqe98QfN4a2XhXrPEEKKJksTm2UBPAWbW9b4i5PdAYY4w5KZfLX2pUIcxTXCFHAAAAAElFTkSuQmCC\" />&nbsp;olduğunu bilərək,&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEsAAAAzCAYAAADfP/VGAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAIcSURBVGhD7ZnBTcQwEEW3AAqAOw3QBqIACuCOEAXQBzeOVMCFCuiAAx3QA8xDO9IoiuM4zmZt9j/pK2ajXZLvmYmd2QkhRBkXpg/T1d9fYpIX07dJZmW4Md2ZFFkZSL+n/VFmZXCjZFYG0g+BzJrgzPRu+hmRGygSKLIKkFkFyKwjEmuiFrsZWOS6QewOMA4DRQZMI8VJ9aZ5NJEKHJnZZ9PWs4xZr6YuIstn9t50jAtm3cZkdQEGMbM1RZbfSC1iqUkpSD2iuYuoAr/RY8yu7z274cHE0wnDLk3npi1gcnzrRFRzDc1ya/I1ji86MW4LSM2YqlpriQYhJD/3R1Lky6RXIyO4OcrjmcTIEhmiWUPjSEce86xReJLwCEY+PjncoGtTTElPUTeGzzjn4y42pGvjZnGMY4impMY5fH3TqoqIBsUxRFNS45MiGhTHEE1JjU+GuIOnHmEUYz5j3+bn2K742F9zMJ7a5QshNmGYxl7rxAjqnCSIxozxL5+QcbtCFMztsmDCmyllGJ+30DlZvTR4FJR2WfgeF8BSYkgrnZPVSwNfHnZZuFlmY66iMURdi50TD4qq0uChWhoJ/HMWrsPIWto5WVoS5sL1VpeGJV2WVM3iRv3NKudKOyc++4dovFaVhpouy9jTcI3OCQZNNV457wV7qKntVquloQo3Y3EEJOiuqTqHQzRea0tDcxyq8bpGaRBCiA7Y7X4Bp7Dawos2dwkAAAAASUVORK5CYII=\" />&nbsp; limitini hesablayın.&nbsp;</p>', '', NULL, '2020-06-22 19:07:22', '2020-06-22 19:07:22'),
(36, 1, '<p>34. <em>M(4;11;0)</em>&nbsp;və&nbsp; <em>N(4;6;-5)</em>&nbsp;n&ouml;qtələri&nbsp;verilmişdir. <em>MN</em>&nbsp;&nbsp;par&ccedil;asını <em>MK:KN = 3 : 2</em>&nbsp;&nbsp;nisbətində b&ouml;lən K n&ouml;qtəsinin koordinatlarını tapın.&nbsp;</p>', '', NULL, '2020-06-22 19:10:16', '2020-06-22 19:10:16'),
(37, 1, '<p>35.&nbsp;&nbsp;<em><strong>b </strong></em>-&nbsp;nin&nbsp; hansı&nbsp; qiymətlərində&nbsp;<strong>&nbsp;&nbsp;x2+ 4bx+64 =0&nbsp;</strong> tənliyinin&nbsp; iki&nbsp; bərabər&nbsp; k&ouml;k&uuml;&nbsp; var?&nbsp;</p>', '', NULL, '2020-06-22 19:11:12', '2020-06-22 19:11:12'),
(38, 1, '36. 4 < x < 6 və 1 < y  < 2 olarsa, 2x/y ifadəsini qiymətlədirin.', '', NULL, '2020-06-22 19:14:48', '2020-06-22 19:16:23'),
(39, 1, '<p>37.<strong> MN ∆ABC</strong> -nin   orta  xəttidir. <strong>S</strong>Δ<strong><sub>MBN</sub></strong>  <strong>= 5,25 sm<sup>2 </sup></strong>olarsa, ABC üçbucağının sahəsini  tapın.   </p>\r\n\r\n<p> </p>', '1592854112.png', NULL, '2020-06-22 19:28:32', '2020-06-22 19:34:10'),
(41, 1, '<p>38. İki kəsən arasında qalan bucağın tapılması qaydasına&nbsp;g&ouml;rə x-i tapın.&nbsp;<img src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCAFsAWwDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9U6KKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA8z/AGgvEGs+DfhD4p8R6HrFro17omnXGpLNd2n2mJ/Kid9jJvX71fIX7DH7Svx2/a80vxre3uveH/D0GjJDb2lzBonnJLdPuYq6GUbkCr/Cy/ertv8AgrF8UP8AhAv2T9R0mCTy73xPfQ6Unr5X+tl/8dTb/wADrR/4JafDL/hXP7JPh27kjC3viWeXWZT/ALDnZF/44i/99UqevNIJ+7ylT9n/APbuvPF/xy1z4KfE3w9aeEvH+m3EsFtc6fcNJZaiy5bYm/5kZk+dfmO4f3T8tfZY61+LHxYvpfFn/BXKw/4R0FrmHxZpsErR9f3CRfaP/HUf/vmv2nX7tC96EZCl7tRodRRRTGFFFFABRRRQAUUUUAFJXEfFTwPq3xC8IzaLpHjHV/BM8rLv1TRFi+0bOcqrSo2zPHzL81fBH7Ofxs8d/s4/Hb4wfBnxrruofEbxDuhvvDLahM/najdyBNqLu3bFdZYnf+75TtSXxcofZ5j9L6K+Svgr+zP8X/Bn7Qlx8SPG/wAX28U2Oo6c8d7oNvby29tDO33Yo4t7L5UX8LfKzV9a0wK080VrC0kjrHEi5Z2O1VFefeC/2hvhp8SPEV3oHhfx3oeu6za7vNsbG9SWX5fvYUfeA/2a0fih8ONJ+LPhl/DuuyXLaLNKjX1lbytF9tiXJ+zyMvzeWzbd237wXbXxF+1l+yv4P+Gvxh+AmufCPw/b+FfGN74tt7RrfRU8uKS1TMssrR/d+RfvN/dalH4veCWx+iTOsSszNtVerNXnfhH9oL4bfEDxRc+G/DfjnQ9a1+2Debp1jfLLMu373yj72K6Lx1peka14V1K01+SNNFkiP27zZvKjaBfndZHPRCoO7/Z3V8Za18IPCPxq/ak+F3iz4N6ZpVjongW9mPiLxNocSRWkjxFNmnoUH7+T5m3FflVH+9niiPxcoP4eY9N/aS/aW134e+IvFXh/wslraTeG/Cp8SXeoXkP2gT3MszwWVhFEHX5pZUbc3YV9GeGby+1Lw5pF3qdt9h1K4s4Zbm0HPlSsil0/4CxIrMvvhv4X1fxVbeJb7Q7G61+1VEi1Ga3V5lVHZ0+Y/wB1nZl/u7q6tacfhB7jqKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooqKSRYUZnZUVf4moA/G7/grB8Z9L+MHxm8I+BtB1SO70jw6GjvrqI7oEupnVXG7/AGERf++mr7+8VftA+Gfg/wDCXT/Dvwzi/wCFk+JrDTINP0jRPC3+m/MqeVE87xblii+T7z+lM/aS/bm8A/AnwHq2s6VqOm+OdcsZIojoul6lG7o0rMoMrLv8pfkbqM8VxHgX9uf4hfGLwboOtfDf9nvXfEbalFuluLvWYLLT43XeJVW4dPn2soHKru3Uo/A4xFL4oyMH9gv9hjWfhV4s1b4s/FRo7r4law8s0diGSRdPErlpWZk+VpX3fw8L/wACr7wr5Ntz+2L42gjml/4Vn8OkK+ckO261G4+b/llL/BuX+8lPX9mH44eIP9F8UftSa+2mTDdJb+G/DdlpVwr+iXS7nVd3/fVUM+r6K+V7f9hl9RWS38S/HH4seKdLkX/jxuvEXlJu3qytuiRW7U3/AIdz/Dz/AKHD4kf+FbcUgPqqivlX/h3P8PP+hw+JH/hW3FRL+w/rNqjRaf8AtEfF2wsI/lgtV11GWCLoiBmi3fKvrQB9X0V8pL8Cf2m/Dp+1aT+0hZ+IrhfkXT/EHg21it9v95ngbfup6+Kf2vPBKn7f4G+H/wASbO3HlD+xdYl0q9uv+mv79WiT/doA+qqK+TLX9vK38J2Ub/FX4UePfhnDsVptRuNJa/0+AZ/jlgyy/N8q/J/EtexfC/8AaU+F/wAZGVfB/jjR9bu9u5rOG523C/8AbJ9r/wAX92gD05mCqSa/L74ueNn0n/goJ8FvjHb2sMXgvxJdT+HNPvcndexov2Vrhv8AYZ7g7P7yRbv4q+kvH37Jvjzx5+0Vrniw/F7VtN+G+u2kFpqHhOxLq0kcSIrQK+7bEjsjMzptf53X+Ktz9sD9kcftG/DHw/4f8PalbeE9W8N38N3pF15BaG3CDaU2r90bcYx3RaUfijMJfDKJ7h4o1q8s9D1tNAhh1jxJZWLz2ulfaER5pNjeUrlm+RXddu5vRq+UfDXxZ/bSvvEmk2+u/BLwhpOiT3kUd7fJq0Uz28DOPNfYt78xVd3FfSfwn+Gcvw90GVdS1668WeJb91m1XxBeRLFLeyqu0Hy0+WJVX5VReFr0BaPhkG8Tn/Fniyy8G6O19e+bJubyoLW3XzJrmU/diiT+Jmrz/wAB+Abibx5N8QfG0kLeMby2e00rSvMVk0azyrPFF/flc7Wll/3F+6oryX4zfsgfFb4sfEibxTYftE6x4LgjVodP0zRdGMaWcLdV3rdLvZv4n9q7L9m/9mPxF8F9f1nXfGXxT1v4ra7fWqWdtfavE0X2GBX3uiK0sv322n/gFEf5gZ7d4q8JaJ440ebSfEGkWWt6ZMP3lpqFus0TfVWr4X/Z9+Fdp8C/+CkXjnwv4BWSy8DXnhRNU1DSUb/R7Od5UEaL/wCPMv8Asu1eseOv2RfHmpfHDUfiJ4M+PviXwSmpeX9q0T7Cl/bjauzZEkr+Ui7f70T7W+avYfhP8FdH+Ettq09ndXmra/rNwLrWNf1V1lvdQlC7dzsoVVVeioiqqjotEd+YJfynpVFFFMAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAoorifil8XvB3wZ8Nza94z8Q2Xh/TVyqyXku1pW67Il+9I3+yuTQB21eb/ABc+PvgH4C6LHqXjrxPZaDDKG8mGZ91xcbevlRLln/h4Vf4hXgcnxY+PP7S1ylv8KNAj+FPgSR3J8ceKrbzL+8i/ha1sW+7u+8Gl7N/Dj5vSPhn+xj8Nvh14gi8T3djeeNvG68v4q8XXTahfM395d3yp1/gVfvUAefr+0V8bvjxCB8Hfha/hLQriKKWLxh8Rm+y/K3LiKyTc7/727b8n+0tS6b+xHrPxEc3Xx4+KfiH4mGTDHQbCV9I0eP8AvJ5MDBpP97K19b0UAfDP7bHwN+H/AMH/ANk3UfDHw/8AB+k6DqWv6tpWjWcNlap9pv5ZbuL915rfM7MkTffb+CvtDw74d0rwjotnpGjadb6TpVnH5VtZWkSxxRIP4VVelfNP7aka6140/Z28KzYSz1L4gWt7JMo/eq1rE8qBfZm+9X1ZQAUUUUAFFFFABRRRQAUUUUAFeLfEr9kP4Q/FieS58QeBNJOqO246tZRfY71W+Yq6zxbX3bm3dete00UAfI91+zb8avhCySfB74xXWp6ZFz/wi/xET+0LfH8KRXSL5sS9F/76ao4f24L/AOGOqNovx3+HOsfDWcyR26eJbFG1PQZSyMQ/2hFzHu2fc2ttz82NrV9eVR1DT7PWLOW0vrWG9tZl2y29xGro6/7St1oAo+FPF2h+OdBttZ8OatZ63pF0u6G90+dZYX+jLW5Xy7rX7Eul+EdTuPEHwQ8Q33we8SszStb6Y3n6PfPk/JcWbnZtx8vybdny7fu1i6L+2J4t+EOpWPh39ozwU3gmSeUwQeOdFL3Xh+6b+He/3rdm5+Vv/Hf4QD68orN0bWLDxBpdrqOmX9tqWn3UfmwXdpMssUqH+JXU7WWtKgAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKyfEPiLTfCOh3us6zfQaXpVhE09zeXUmyKGNfvOzfSvkXVfHnjv9t7VZdE+HF5qHgX4KxXTRah8QLSTyNQ1sx8NBYKwykW48y/xbH/3WAOq+LH7XNxqXiyf4afA7Tbb4jfEwq5uZVmxpWiLt/1t1cfdZkfb+6X5uq/K1Wfhr+xnB/wlg8d/GPXf+FteP+kE2oWqrpumKSvy2tr9xD8o+fG6vYPhL8H/AAf8DPCNt4a8GaLb6Lp0W0yLAn724k27PNlbq7tt+81d7QAUUUUAFFFFAHyp8VFPir/goF8EdGgJY+GfDuteIrxbg5iaKfZaxeX/ANNFlH/fNfVdfKPglh4n/wCCifxEv5M30XhrwXp2mwXC/ds3nmeV4W2/xN97a/zV9XUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFZPiLw3pXi7RbrSdb0211fS7pPLnsr2FZYpF9GVhg1rUUAfIGr/s1+Pv2cb6fXv2dtX8/RJLhru++Geuz7tPuCzrv+xyt81q23d32/d9K9K/Z7/aw8K/H6C80yJLjw3430x/J1bwlrBWHULWVcCXYn/LSJW+XeP+BBa91rw74+fsu+HPjfJaa1BeXvg/4h6bzpXjLQT5F9bfK4VGb/lpF8/zJ+q0Ae40V8qfCv8Aak1/wZ4ys/hb8fbKz8J+N5EWLSPEEMudN8SrvEQeJvuxTs20+U39/wDhztr6roAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigArmPH3jzQ/hr4T1HxL4k1OHSdF0+Jp7i6uH2Iqj8PmY9FX+KtHxF4i03wnod7rOs30OmaVYwtPdXl1JsigjX7zM30r4ttPBWof8FB/iFp/jDxJFqGn/ALPmgy7tC8PXI8pvE90rtm9lT/n3/hTd94f3dzUAWPCFv4m/4KBatZ+JfFmlXHhT4A6fdefpXh2Z2W68VSo/yT3X921+7+6/iYdWr7SsdOtNHsYbSxtobO1gXbHBbxhERf8AZVaWzsbfTbWG2toY7e2hRY44ol2qqr91VWrtABRRRQAUUUUAFFFRTTLbxPI/Cqu5qAPlj9kP/ioPjt+0/wCL2/cy3XjKLQDafeVU061SJZd/+35udv8ADivqyvlX/gnHC95+z7d+IwN9r4k8Tatq9ncMPnnge7dVZ/4t3yN97mvqqgAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAOE+Lnwh8L/GzwXf+GPF2lxajpl1GyBmX97A/wDDLE38DqfmDV8zeB/jz4s/ZW8faR8JfjTPJqnha8PkeGPidKcJOu9Fit7/AD8qSp91pd39z1319qVy3j7wHofxK8Jaj4a8SaZDq2i6hC8Fxa3CbkZT+PysOqt/DQB06usi7lO4U6vi74E+O/Ff7K/xFg+Cvxa1Y33hS/bHgDxhc7ylwm7aumzyMfllRNm1W+m77lfaNABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABTJJFRWZjtVerU+vlP9qL4ieI/H3j3RP2f/hrqb2HiPXlF14q1q1+Z9C0TG2Uqy/6qeUNiLd/47vRgAcf4ma6/b8+Jt74X064e1+AvgrVEGq6layc+KtQiz/osTrmN7VM/P/e+T/YZfs+ysbfTbWG1toY4LWFFjjhjXasar91VWuc+F3w20H4Q+A9F8HeGrUWWiaTAsFvG7bmx/fZv4mZvmJ967CgAooooAKKKKACiiigArhfjX4iufB/wa8d63ZyRwXum6HfXcEsn3VlS3dk/8exXdV82f8FEfEC+Hf2OfiO7xNOby1g05Qp+609xFFv/AOA7t34UAbn7Dfhu28K/si/CiwtY3iRtBgu3WT73mTjz5f8Ax+Vq93rB8F6A3hPwdoWivKtwdNsYLNplXaH8uNU3f+O1vUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAef/Gf4QaD8dPhxrfgzxJbrNZahA6RXCorS2k235J4tw+WRGO5T7V4z+zL8XvEXhnxfe/AT4pXMbeOvDtlC+i667Oq+JdOC7UuE835nnVU/e/e+bf/AHWr6mrwf9qj9nn/AIXl4X07U9Fu20n4jeFJzqnhXV1fYsF6mGSOU7W3RMyKrj/9VAHvFFeKfst/HofHj4ZwajqNqdH8ZaXL/ZfiXRJV8uWxv4xtlzF95Uc/Mu7+H/dNe10AJ3pPXmvkX/gp1rmieGv2UfEmqX9pFNrRaGz0i6xtntp5ZV3NE/3lOxHPH92vnr4AfBvw/Yf8E1vFPjPx+JJdX1Oyv9VttYuJG+2Wir8lqsUv3l3OittB+bzKi/uyl2K5dYx7n6gUtfnn/wAEg/it8QPiN8OfGNl4s1HUNa0LR7m1i0jUNSkeaUbkfzYFkbllQJFx/Dv96/QutXHlM1K4tFFFSUFFFFABRRRQB5h+0N8ZtP8AgL8KdY8W3lvLf3FuFt9P02Bd0t7eStsggVRydzkf8B3Vw37IXwR1X4a+GNU8W+OXF98VPGt1/a/iC+YFhDu5isoiw3LFEp+5/C2/+EKK4HRGtv2tP2srnVGjluPh18Hrl7SzeVQYr7xHu+eUdmW3QLtx829lfd92vsagAooooAKKKKACiiigAooooAK+Uf2/M674d+E3hOMfaZdf+IOkwzaU/Md/axu8sqSL91k+RG2t/dr6ur5R/aO3+Jv2v/2ZvDErE2sV3rHiCQw8SxSW1qvlFv8ApmzOy/8As1AH1dRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRVS6uorG3knuJEhgjVmkkkYKqr/eb8qALdFfPXj39uL4TeCtXm0TT9cm8c+KVbYvh/wAF2zapdvJvZPK/dnYr7127XZfmZf71cwvxK/aU+MXy+Dvh5o/wj0OcALrHj6d7rUhE3R0soPuS7XRtkrbVZHRqAMb9om3b9lT4u2P7QGhxW1v4T1Jf7L+IGmxzeW10p/4972KPdsedGXZ/ebeF/jZl+ifhB8YvCnx18D2ni/wZqX9qaLdMyJKUZGV1+8jI33WH9a8MvP2G3+JHh3W7T4u/Fbxh8QZ9Ztvs8tpFOlhplm29ZVlt7RFZVlRw2133fK+3bWH+xbGP2fvFHj79nm70xop/D88mv+HrrYUk1vTZ2++zO215UYrEzLtTj/ZalH7Vwl9nlPn7/gsb4zu/FHif4XfCbSD517eTtqUluv8AFJI/kW//ALW/Ot39u74Tan8C/wBnnwPdyeJLnxr8P/Dktnpt74I1spa2txx8jq9mtuzsuz7su7ru6ir3iL9lv4v/ABG/bo034y+LvBSnwjpV3E9npVpqlrLd+XAh8jKtKif63Dt83c/e79r+198A/jf+2VJpfhCDS9J+HPw+0+5W9mutW1Bbq8vH+6p8q33Ku1Wf5d/f79TG6pru2VL+J/dse8fsb+OvBfxD/Z38L654B8N2/hDQZxIo0W2iVFt5lkZJR8o+b51Y7urV7nxXnnwH+Duj/AP4U6B4E0MySWGkw7TPLjfPIzF5ZW92dmNeh8VpU30M47C0UUUigooooAK8I/bE+NV18E/gvqN3ookufGevTroHhu0hG6WXUbn5Idq5/h5f/gP+1Xu9fI1vbx/tCftxz3U8UkvhT4O2n2eD5/3U+t3SK7swz/yyi4+79/8Ai/hoA9g/Zn+C9t8APgz4e8HRuZ7+3h+0aneM29rq/l+eeXJ/2y2P9kLXrNFFABRRRQAUUUUAFFFFABRRRQAV8oq3/CSf8FJHktPlj8N/DnyLxZB/rGub3emwD/d+bdtr6ur5W/Z2hPiP9sL9pTxPI8eoWtrcaN4f0++R1byfKs/Nu7ZdvTbK6bv9qgD6no/hor80f28Php4w/Zm17wd8dPD/AMSPGPiG207xDE19pWu6l9ot4PMznykRVRImVPKKbf4hS66h0P0u4rwT4/fH6++HfifT/C+ipZQalJot/wCJNR1PU43ltbDTrRPncojozs8roq/MteQ/Fv4PeL/2/PDP9raR8QbzwF8PEdH0C2tYHf8AtbafmvZ/nRtv/PJP9nf/ABV9J/8ACk/DGrWfhweIrGPxLq+iaemnLqd8CZZ0+TeJef3iu0SPsfcu5aOV/aCMi38FPF2sfEL4R+DvE+v6euka1q+kwXt3YqrJ5MkqK2z5vmXrXdtSRqEXA6UrVT3BC0tJS0gCiiigAooooAKKKpahf22mWMt3eTx2trCm+SWdtiqv+0xoAu0V82eNv2+vg74W1s6FpWvXPjvxGSyLo/g60fUpmcfw7oxszu+X738VYw+JX7SnxhATwV8O9J+EuiSkFdY8fXBuNQ8o/wASWUH3JNrq2yVsBkdWoA+orq6hsbeSeeVIYY13SSSuFVVx95q8E8e/tx/CXwXqzaLZ67L438ThtqaB4NgbVLt33MmzEXyq29duHZfvL/ermof2Hf8AhOriK9+M3xJ8VfFOYOJW0mS5Om6Krct8tlB9778q5Z/uPtxXu3gH4U+D/hXpa6Z4Q8MaX4bsgACmmWiRb/lVdzMvzM3yL8zc/LQB4IvxK/aU+MjBfBvw70j4SaHMMLrPj6c3WoeU38aWUH3Jdro+yVtu6J0apIf2H08eXEN/8ZviT4q+KVxvWV9Ie7/s3RFYZb5bOD7335V+ZvuSbcV9V0UAcX4B+FvhD4V6Oml+EPDGk+GbSNcGHS7NYt42ou5mX5nbCr8zfM22u0oooAK+Yf2ztB1DwzpPhz42+HIN3iL4b3Jv7yJGCNfaO/yXtuTnDfIPMXd/cO35jX09Wfquk2uuaZd6ffQpc2V1E8E8D/ckRlKsv5UAVfCvijTfG3hnSvEGkXC3mlapbRXlrOv8cTruU/rW1XyV+xDd3Pwx1L4ifAXUZ7ueTwHqnn6NJfMu+XR7r95b7P72x965/wBpfu/dr61oAKKKKACiiigAooooA4f41fEqz+D/AMJ/FvjW+VXi0PTJ70RMf9ayL8if8Cfav/Aq8w/Yb+Hd34E/Z80bUNY8w+KfFssvijW5JS29rq8Pm4fd825YvKQ7v7tcb+3fFP8AEa4+FPwWtG/5HjxNFJqoDfN/Zdn+/uMLsb/Y+bp8o3fKxr6wtbWKzt44IIligjXbHGi7VVf7uKALNFFFABRRRQAUUUUAFFFFABRRRQAV8qfsBj+2NB+Lnitx5dx4g+IWrXEluv3I/LZIvl/75r6N8aeILPwj4Q1zXL9iljptjPe3DKNzeXFGzv8A+OrXy9+wNP4p+H37Ifw2g1Xww19pktlLqCXui3f2q48meeWdGlgZVbdtlX5Ymlb5f+A0AfXvpXyV/wAFA4br4l/B/wAUfDHw9awXutNo83iG+aVdyWdrbfvU/wC2ssqBE/4G38Ndh+0X8Mrf9sD4Ty+FfDXjq68JX9rfwXr3FrC63EDLv2xT27NFImefkfadyD+7XZfBn4I2Xwt8HXmm3er33izXNWIk1nxBqzbrrUZNuz5v7qqvyqv8IqHHmiyoy5ZI8w/4J/8AxWsvFn7HHw71G/vYbd9PhTQpWlk2/vYpfs8Sn/ab91/33X1JXyf+zL+wvZ/s8atqUkvjLU/E+hLqkupaL4fkXyrTTnYbfMZd7ebKqfJv4/3a+r60lLm1M4x5dBaKKKRQUUVTvby20uzmu7ueO1tYUaSSaR9qKo/iZqALlFfNXj79vz4QeC9Ul0XStZufH3iYblTRPB9s2o3Dt83y71/dr8y7fvfxLXJt8SP2pfjUyr4R+H+jfBrQpBhdX8a3H2zUNvzfMlpEPkf7nyy/xLQB9aX1/baXZzXd3PHa2sS75JpX2Kq/3mavnjx/+398H/BupPouk6xdfEDxNkpHofgu1bUp3cHbt3p+6X5vl+/3rmrb9gCz8cXkWo/Gj4i+Kvi5drJ5v9n3N01hpKNz921ib/aYff8Au/LX0R8PfhP4N+FWljT/AAh4X0vw3ZqMbdPtViLf7zD5m/4FQB85J8Rv2pvjVuXwl4C0X4NaHM3y6r4yn+2ansz95LWP5Uf/AGJf7u3/AGqlsf2AbLxtdRah8afiL4r+Ld6kgl/s+7u2sdJVv9i1iPu4+/8AdbbX1zRQBxfw/wDhV4N+FelLpfg/wxpXhuyXAKaZZpFv/h3Oy/MzfL99ua7SiigAooooAKKKKACiiigAooooA+Qf2mifgv8AtKfB/wCMlvGy2Oo3J8D+JH3SbPstyd1rL8i/wS7vvf7H+9X19Xin7Xnwl/4Xd+zr428JJD52o3Ng0+n4Tey3cX72Hb8jEEsgX5fm2u1Xv2U/it/wuz9nzwP4ykdpL2+09Y70ldv+lRZiuP4V/wCWsb0Aeu0UUUAFFFFABRRTHZY13M21V60AfJfhT/i7P/BQrxhrEtvI2nfDPw5b6Ha+YflW9vszyyqC3DeV8m5f4fwr63r5L/4J/wDneJPCvxO+IF86tfeMfG2pXyOqEkW8TLBEiSt/rY02Ns/hXca+tKACiiigAooooAKKKKACiiigAooooA8J/ba8VN4N/ZO+KGpxXkdjcf2LLaxTS7cF5/3Kp838TeZt/wCBV3HwL8Jx+CPgv4G8Px2klgNN0Wzt2tZd2+J1iTerbuc7s14f/wAFH86v+znbeED+5bxh4p0bQVu8bjal7xJfN2/x/wCp+78v3utfV1AHL+IfAumeJJobmZGttVtyfs2rWp2XcH+64/hyF+R9yNtXcrVX0PXry01SPw7rbrLqXltNa3iLsW/hXbubb/DKu9Ny/wC1uXuq9hXKfELR01Lw3cTxXcOnX2mn7faahcHEdrLGrbXf/pnjcrf7LPQB1dFfL+sft+fC6yhtNO0ue/8AGPjS4gVj4T8I2b6ldxS52mN3RfKXbJhPmYHLL8vNUf8AhNv2nPjIQPDHgvQ/gtoVxgpq3i64/tLVPJb/AJapZRfJFLtdG8qVvleJlf71AH1BfX9vptncXd3PHbWsKNJJNM+xI1XlmZj91eK8B8bft1fCrw3rU+gaFqd58RfFSfIug+DLRtRuGbeybWZP3a/Oqr8zfxr/AHqwrP8AYV0nxheQaj8ZPHHij4u36uswsdUuzZaSko+bellBtUfM0q7WZl2Sba9+8EfDnwz8M9Hj0nwpoGneHtNTpBpdssCN8qruYL95tqqNzc/KKAPnseNv2nvjEwXwz4M0L4LaDcYCat4uuP7S1URMOJUtIvkSXY6N5UrfK8TI9WbP9hPSfGF3Ff8Axi8ceKPi7eqwk+xaldNZaSkoy29LKDav8cq/OzDY+2vqaigDj/h38KfBvwp0v+z/AAh4X0vw3Z4G5dPtliL/AO+w+Zv+BV2FFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAV8kfsVtc+B/H3x8+E0uYoPDHif+1NPt1QKsNnqKNPEqbW2qvyt8qhdu6vrevk/VBN8O/wDgo1o1xmK2034h+DpbI/L5Sz3ljL5v/bWURP8A8BSgD6wooooAKKKKACuC+OviiTwP8FvHviC3lhgudL0K+u4WuP8AVeakDsm723ba72vmv/gohrkejfsd/ERXjZ/t1vBpq7f4WnuIolb/AHctQB0P7EXhWLwb+yT8KdNiilhP9gwXcqTcMss48+X/AMflavc6wvBeiSeG/CGh6TNIsk1jYwWjsv3WZI1TP/jtbtABRRRQAUUUUAFFFFABRRVDVNUtNHsZr7ULqCxsoFMktxcyrFFGv95mb7tAF+ivm7xd+3t8IfD+p/2PoWtXXxA8TNvEWh+DbOTUrh9rbOXT92vzf33H3qwz46/aa+Mny+GPA+i/BfQ5DlNW8YXK6jqTr/fSzi+SN/m+5K33loAyf2uvE2ja1+0J+zv4HfV7NbyDxDL4i1CyurhVijtbW3d98u7uedu7+41dv42/bs+E3hXWZNB0bVrn4g+KslV0LwXaNqVw7fc+Zk/dL821fmf+Ja+UfFH7Hdr46/4KBeFLTxx4suvGupN4Vj17xJeJYR20V7PC/wBlSNok+SBJURPl/i2v/er9D/Avw58L/DXR10rwroGneH9NXn7PptqkKthdvzbR8zUR+EJbnz43jn9pn4xYTw14K0X4L6FORt1bxbc/2lqZiOP3q2UXyxOFbd5UrfeTa33qmtP2FdM8ZXUV/wDGPx14o+L9+jrL9j1K5ax0lZB8+9LK32r97eNrsy7W24r6oooA5XwL8OfC/wANdHXS/Cmg6d4f05cf6PplslujfKi7m2/ebaijd1+WuqoooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACvlP9sYjwr8Vv2cPiAv7s6P4xbR57iY/6PDa6jbtBKz/7XyLtb1r6sr5V/wCCkStp/wCy/feKOXHhPXNJ137Ket15d7EnlFv4P9b975vu0AfVVFRQyebGjf3l3VLQAUUUUAFfKf8AwUfzqvwB0rwlHiO78X+LdF0O2uG+5BK92kod/wDZxC3519WV8rf8FBP+RX+C/wD2VTw//wChy0AfVNFFFABRRWdqepWmi2M99f3MNnZ28bTT3FxIqRRKo3Mzs33V/lQBo0V8x+PP+Cgnwm8K6o+h+HtQvviT4sy6pofgyyfUHdl3gbpV/dL8ybfvbhuVtu3mua/4Tj9q342MR4a8G6H8DtBl3LHqfii4Go6qy/OPMW1QbI2+58kqn5l6srUAfWOpapaaPYT319dRWdnbo0s9xcSKkUSL95mZuFWvnLxx/wAFAvhJ4a1p/D/hzUb/AOJPikliNF8G2b38ny7hhpF/dfeULw/8a8beawrH/gnvonjK8i1P4z+OfE/xl1KOXzRb6rdNZaYsnzfOlrA3y/fb5d2P4fu8V9JeBvhr4W+GekjS/Cnh7TPDmn5yLbTbVIFP+9t+9QB8z/8ACbftW/G5mXw94M0H4HaFKPk1PxNOup6rs+fEi2yjZG33PklX7y/xLU1n/wAE99A8W38Op/Gbxx4n+Mupxv5gh1a7ey0xJefmjtYG+T7z/LvK4bbX11RQBy/gf4a+Fvhpo66X4U8Pab4c09eRbaZapAp/3to+auooooA+U/hOP+Ek/wCCg3xz1SEeVF4d8PaHok6v96WWdXuldf8AZ2/LX1ZXyl+xT/xUfjL9oLxtk3ya347nsrbVJPv3FrZxJFFHj7yrF86Lur6toAKKKKACkpaKAG9hQKDX5e/ty/sUftAfGT9qCz8W+Drj+0tBP2f+zr3+1YrX+wtm3d8rMrj5svuiVm/Gp+0oj6H6hrjtRxXEax4ntPhH8KH1vxfqyyw6DpaS6pqbLs81o4hvfb6sw+7/ALVfCf7NXj3xV/wUU+NOv+JvFwez+DXhSVBp/hNW/wBHvLpjui+1f899q/Myt8n3Pl60+vKhfZ5pH37oXxF8KeKtQlstE8TaPrN9Cu6S20/UIp5FHqyoxxXSrXwD/wAFdrWy8M/A/wAH+KdM/wCJR4q0fxBBHpWpWZMVxbqYZS6Ky9F+VOP9mvor9i34wan8c/2afBfjDWQTrF1avBeSbdvmSxStE0o/3tm78aI+8pPsEvd5f7x7tRRRTAKKKKACiiigAooooAKKKKACiiigAooooAK+df8AgoL4Z/4Sz9jP4qWH2r7H5Wlf2hvxuz9mliuNn/AvK2/8Dr6KrxL9tf8A5NI+L3/Ys3v/AKKagD0rwH4mh8aeB/D3iG2ikhttW063v4opvvIssSuqt7/NXR15/wDs/wD/ACQb4b/9izpv/pLFXoFABSUtfMfxt+NnhvxX46uPhHH460bwpZW8SzeLdWu9VitZoIG+5ZW+91/ey/xuv+qT/adaAPOrb9tw/Fn9unwd8JvBF4reEtMN++tahE2RqFxHay4iX/pkjf8AfTf7tdv/AMFBP+RY+C//AGVTw/8A+hy18P8A7FMOiXv/AAVE8WyeHo7OLQLN9Y/s5NObdbrAv7pPK28bdpFfVH/BWjQ/FGpfs12WpaBqa2VppOt2tzfR7tkshZhFA0Tfwssrr3XqaPsRYvim4n2jqGpWej2c93e3ENnZ26NLLcTyKkcSr8zMzN90V89eMP27/hZousSeH/Ct1qXxO8VKcDRPBdk+oPu3bPmlX90q79q/f/jX+HmuU+Hf7Clh4t8O+HNQ+Ouua18TPE9taWvm6XqWqyvpVrLEq7dkSbVlP39zPu3eY/8Aer6f8H+B9A+H+jx6T4a0TT9A0yP7tpp1ukEWQoX7q/7Kr+QpuPLIUfeR84/8JN+1F8ZMDSPDPh/4G+HpjldQ8QTrq+sGJukq26fuo32Pu8qXlXhZW+VqiX/gn74e8aalFqHxi8b+LfjDeqVm+y6xfNZ6cs2X3NFa2+1UXdK+1N2FVtn3a+taKRRy/gf4a+FvhppP9m+FPDul+HNPzkW+mWiQKf8Ae2/erqKKKACiiigAooooAKjmby43b+6tSV5T+1N4ii8I/s2fE/VZ42mhg8PXysqfeO6F0/8AZqAPK/8AgmtGdR/ZV0rxRIAl54s1jVtcuYlHyRSSXsqFU/2cRLX1VXk37KnhuTwj+zT8L9InlWaW18PWStLH90/ulb+tes0AFFFFABRRRQAUUUUAfBX/AAWN8W6h4f8A2YdL02ydo4Na8QQWt4y/xRJFLLs/F0T/AL5rsf8AglT4Eh8G/sf+HLxY9lzr13dapO397L+Uv/jkSV7L+0x+z9ov7Tnwj1XwTrUzWYndZ7W/iXe9rcJyku3Pzdxt/utXnX7O/wAP/jF+z98E7X4ePo3hrxHc6KXt9K1hNYlt4ZYWferyxfZ2ZNm5vlXfu29s0oe6pBP3uU+Sv+CvXja++JHxI+G3wX8NRNqermX7fJZwfM7XE/7q3T/vne3/AAOv0K/Zw+EcfwL+B/g/wPEyyy6RYrFPKo+V52O+Vvxdmryv9nv9jKz+GnxG1n4p+OdZ/wCE6+KuszNNLqxgEVvYq4x5Vqh/2fl3f3R/DX1HTj7seUH7zuFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABXiX7a/8AyaR8Xv8AsWb3/wBFNXttfMX/AAUo1S60f9iX4nT2lw9rK1ta27NG2393Le28Tp/wJGZf+BUAeu/s/wD/ACQb4b/9izpv/pLFXoFZ+l20VnpttBBEkMEcSrHDEu1VXbwq1oUAfPf7SX7QWpfDdbDwz4Z0XXb3xPrE8Vr/AGla6Fc3tlo8Tvte7ldF2tsU7lT2+biuk0z4QfDP4d+BYprzw1ZavBbx+bcanqGk/b725ldt7SynY0rs7tu/GvYKSgD8gf2F9H17wn+3d4i8War4A8UeHPDeuf2jb6fPceHbm3t4/NmV4Ub93tjXYn4cV9/ft5+HG8Vfsh/E+zjsf7Qnh0lr2OHbu2tA6y7/APgGzd/wGvfj2rnfH3hu38ZeBfEnh27V3ttW025sJUiba7JLEyNt98NR0iv5Q+1KRk/BHXrTxX8G/A2s2dy13b3+h2dzHcPu3SboEO75vmru6+bP+Cefi2bxV+yR4CivRt1LQ4ZdBu4mTaYmtZXiVf8AvhYq+k6ACiiigAooooAKKKKACiiigAr5e/4KPaxeWP7InjHTdLmKa1r09jotjbxqpe6ee8iR4E/2mi82vqGvlL9uzGuTfAvwljyU1v4h6c/2v73leRvl+7/FuoA+mdC0Sz8P6LYaVYQ/Z7CxhS2todzNsjRdqL83+yBWpRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAV8p/wDBQ5hqvwr8H+FI/wDSZfE3jXRtNfTev2+L7R5rxMv8S/u93/AK+rK+Uv2kc+Mf2sP2aPBik3Fra6hqnim/hh4kg+y2u20mZv7nmuy/7VAH1WqBFCjpTqKKACiiigAooooA+RP2JHi8DfE79oT4XNIkJ0Xxg2tWFqLguiWd/EssSIrfMNm35v8AaevruvkrxYy/Cj/goN4O1wyzJpXxJ8Nz6DOu0+Ul/aOssB3Y/ii3Iqf7zV9a0AFFFFABRRRQAUUUUAFFFFABXyl8ZmHiz9vb9nnRrclz4b0zX/EN/DMP3TRywJawMufvSLLu/wB0c19W18o6BnxZ/wAFHPFE0gN1F4R8CWtpFLbcLay3Vx5rRS4/jZF3qrfw80AfV1FFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABXyl4LC/Eb/goR8RdSnO+2+HvhjTdEtkbjFxfb7qSRdv8AspsZWr6f1LVLbRdNu7+9lW3s7WJp55W4VUVdzNXzB/wT502fXvhl4o+J979oW9+Ivia/15IZ1KNFa+a0UCY6H5Y925fvKyUAfVtFFFABRRRQAUUUUAfLH/BQbwne6p8DU8a6FEzeJvh3qlr4ssXWPc22BwZ0+4zbPKLsyjaP3a7vlFfRHgfxdY+PfBmg+J9Nbfp+tWNvf27f9M5UV1/Rqn17RbLxRoepaPqECXOn39tJa3MMq7kkiddrKyt6q1fMH7AOuXHhPw/40+CWuXIbXfhnrU2nW5lGJbnTJW820uP919zfhtoA+t6KKKACiiigAooooAKKKKACvlP9lEf2/wDtLftSeLSPJa68S6foP2MfMEXT7Pyll3f7fm52/wAOK+rK+Uf+CeG3xB8G/E3jYA3MPjHxhrGr22oS/wCuurX7Q0UTy/xceUy7W9KAPq6iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD50/bs8baj4V/Z+1jRtDkI8UeMJ4PC2jqrNuNxeP5bN8nzfJF5rfIG6fdr2H4beBbD4Z+A/D3hPSU2afo1hDYRfKqlliQJvbb/E33vrXzj4k+z/HT9vLRdIeE3nh/4R6U2qXRlT91/a95s+zr/tMkS71P8LD+9X1xQAUUUUAFFFFABRRRQAV8d/tEyH9nb9qT4d/GW0leHw74rli8DeK43mVIU81t1ldNu+7sZW3v/cT/AGju+xK86+PXwj0z48fCTxP4H1QKkOr2bRQXDLu8if70Uq+6Oqt+FAHotFfOP7Fvxg1Lx58N5fCfi8PafErwNKuh+ILORGVyyfLFPlh8yyoofcvrX0dQAUUUUAFFFFABRRRQBynxM8STeDfhz4q8QWqwtcaTpV1fRpcHEbNFE7ru/wBn5a8p/YI8P23hX9jv4VWNskkaNo63jLL97zZ3eeX/AMflaqf/AAUM8SL4T/Yx+Kd9JbtdCTTVsNitt/4+Z4rff/wHzd3/AAGvaPh54abwX4B8N+H5ZhcyaTpttYNcKu1ZPKiVN3/jtAHS0UUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVxnxb+Imm/CH4a+JPGeqyxx2Oi2Mt03mvtDsqnYn+8z7VH+9XZ18lfGvUof2kP2jPC/wasma48K+E5ovE/jZo1bazod1hYO33fnf96y/eKJxt2tQB1f7Evwz1vwT8IP7f8WrM3jnxtfS+Jtee6TbNFLPjy7c8bgIowi7G+6xf7v3a+iqT7tLQAUUUUAFFFfOH7RHwF+IPx68TWVvo/xT174VeG9JgzG3hieWK61C4f77SlJU/dIqqqr/AHnc0AfRv8NHYV+e3gT9m3xvof7U3h/w1B+0H8SPGmkeH4V1rxNDe6zdR28W4/6JasPOYM0rI7sjfwL/ALVfZPxQ8Ia58QNHTQtL8U33g2wuSwvtU0VlXUdn8KwO6MsW7u+3dx8v96j7PMHU7/ivF9V/ai8G6b46t/DATUr/AM3XofC8mrWkUbWUGqSxPKlq7b97PtUbtiMqbl3la8N/ZIk+IHgb9qX4rfDK88ca98SfAGh2lvcQav4huGvbi0upNjC3a4P8e133L/sD5Vr2HwV+ynoXhTxomtXeq3Wr2tnruoeJNM06aFVWC/vGzLNK3/LVkyyxH5diuw+Y/NS/lfQP5onm37TGn3n7Nvxs8P8A7Q2ixzv4dvjB4e8fWUSOyLYs/wC61Lao+/EwVWb+7tXb8z19babqVtrWnWl/ZSpcWd1Es8Eq/dkRlDK35VT8VeFdL8a+GtW8P61apfaTqlrJZXdu/wB2WKRdrr/3zXyb+yr4s1H9n34nXv7M/i6XfFZwS6j4G1dz/wAhHTC7t9nYfN+9i+f+L7qD5ePmYH2bRRRQAUUUUAFFFFAHyp/wUN/4qD4X+CvBKj7QPFvjXSdNn03/AJ/rdZ/Nli/8hq3/AAGvquvlL9p4f25+1J+yp4Uufl0+61vWNceSIYm8+xsPMiX/AHGaVt/y/wDfNfVtABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFRSSLCrOzKiL8zM1AHmv7Qnxu0v9n34W6p4t1GFryaEeRp+mx7vMvrx/8AVW67Vb5nbiuQ/ZB+DOo/DP4e3Ot+LE834jeMblte8S3LO7P9ol+dYPm+6sKsItq/L8hrzbwPJN+2V8erbx+7PP8ABzwDdy2/hy0ubdxHrmrKrJLqG1vkZIcssTbc7t33TX2PQAUUUUAFFFFACHpXFfFr4lWHwj+H+q+KdQSS5W1VUgs4f9bdXDtsigT/AGndlX8a7UV4z4p8Caz8RvjpolzrNo1v4E8IwrqVnG8qldU1SXeiuyf3bdOV3fxy5/hpeQF/9nv4Z6j8PvBcl54jkS88beIrptX8Q3a/xXUv/LJf9iJdkS/7KCov2hvhZ4w+KXgy403wb8R9W+Huq+Wwjn06KJkkYq332K+an+8jrXqd5562spt1VpwjeWrfd3fw5r5E+H/xo/avvrjUNE8U/A3RE1JneOz8R2uurb6ZF/dZ0Lyysv3fu/eofve6Efd94g/4Jn/EHW/Evww8U+FfE+nWdp4i8G69Lo99c2MAUXzhcmWVgP3spYPuc/M3DN1r7Mrx39mv4D2vwB8Bz6U19/a/iHVL2XV9c1bbs+2Xsp+d1X+FeAqj0WvYquRMQrw39qj4Ar8dvA9o2kXP9k+PvDd0us+GNajCq1rfxqTEruUb90zbdy+yt/CK9yoqSjwX9lH9oaX45eEdQsdetk0f4i+FbltJ8U6Ps2JBeqSu6Ib23RMVbad3Y+1e9V8t/tMfA/xNY+MNI+NXwiijh+IWhfJrGlRP5H/CUaauGa0kP3fN+X5HZe/+yleufAv46eGv2gPA8XiTw5LJGFf7PfabdrsutOul+/bzp/C6mgD0iiiigAooooA+U9S3eKP+CkWkR252L4W+H8s1x538b3N1sXyvwX5vu19WV8o/AUDxR+29+0rr0h+3Q6XDoGg6beqdyW6/ZWlu7ZHH8Sy7Gdf4WPNfV1ABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFfIP7TXxE1j43fEJP2cfh3dSW2oXka3HjfW1RdmmaM/wAssUbN/wAvEu9Avs3vuTsf2mfj9qPhG5t/hl8NoDr3xg8SQuun2dvtKaRC3ynUbpudkSHpu+82BXV/s0/s/wBh+z74CWwNx/bXirUX+2+IPEMwZrjU7xvvyszfNtz91e350Adx8PfAWjfDHwZo3hfw7ZrZaNpNstpbRIv8Kr95sD5mb7zN/E3JrqqKKACiiigAooooAKKKKACiiigAooooAKKKKACvjr4yfBLX/wBnv4paj8e/g5pMuqzXUbt4x8C28jRQ6zH95rqBV/5el+991t/zn7zNv+xaKAPMfgL8efC37RHw/s/FnhS782CQiK6spjtuLGfq8Eq/wsuf+BV6dXyT8Vv2a9d+EfjzUfjT8CYmh8UTsJfEfgnft0/xHB1fYv8Ayyuf4lf+9n+8+/1b4A/tLeEv2ifDst9obT6brFm7Rah4d1QCHULGRcZEsXXb8y/MPl+agD2CiisnxFrlr4Y8P6lrN87JZabbS3czqu5vLjUs/wCimgD5p/YRI1k/G/xW/wC6udZ+IuppJbqfli8jZEv/AH1X1ZXzB/wTk0G40f8AY98C3GoLu1TVvter3Vzu3SXLXF3NKksr/wATmJ4vvc/LX0/QAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFeAftD/tQ6d8I76z8G+G7X/hL/AIt60qpo3hS0+d3Z94We4b/llAmwszt/Ch92Wn8dv2lrzw94jn+Gfwt0h/GfxcuYFdLWFd1josUjbftV/KPliRPvbD8zfJ/ezWh+zt+yvovwQuL/AMUapey+LPifr0edd8W3p3TXUjOXdYl/5ZRZwAi/won90UAQ/su/s2P8G49f8WeKNU/4SP4peL5/tviLWAzeUjsdwtLdf4IItxC9+ey7VX6BoooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAr5t/aA/ZRj8d+Io/iP8O9W/4QL40WCKtn4ijBNvdovH2e8iwyyoy/Lu2Mw2p94Jtr6SooA+Wvg7+15K3jW3+F3xl0T/hXPxQEe6BJZlbTdYRQB5trN9358N+76/L/AHvlHf8A7X3iKfwp+y/8UtUtbhLS7h8P3ghlkC/feIov3v8Ae2/jXSfGL4J+Dfj34Qm8OeNNFh1jT3+eJ3+WW2l2svmxOPmV13Hmvzo/b0uviJ+zV+z5q3wx1/xTN8RfBPjC5jh0O/1WFv7T0lLe4S6eKef/AJeN37tVZvm+/wDd2hamUuUqMeY/Qv8AZr8OQeEf2ffhzpNtayWcdr4fsU8iTduRvIRnDbu+5mr06vJvgL8f/Cvx28I2mp6Df2/9qLbxvqGiSXGbvT5SuWSVPvdSQG2/NXq/8NXJcr5TOMuZXHUUUUigooooAKKKKACiiigAooooAKKKKACiiigAooooAKKK+evjV+2P4L+E+qDwxpi3XxA+ItyzRWXgzw5/pF5K4z/rdo2wqv8AFu+bAbCttoA911bVrPQ9PnvdQvILCygXdLcXMixxRr/tM3SvkbWfj548/a21a98Lfs/3H9geCIG+z6r8VruDdFu2Hfb2EDbWeUbk/e/w/wCz8jtPZ/s4fEP9prVrPX/j/qY0nwtG63Fp8LdBnzbIwdG/0+4X/j5/1f3B8v8Adavq7QdD07wvotlpWlWUGnaZZRLb21rbRhIoUX5VVVHSgDg/gT8BfB/wB8Kro3hXT2jln2yahqt03m3uoTbcebcS/wATdf8AZGflr1CiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAoopKAILm6isreSeeRYoY13PI7bVVa8e1b4zfCT4jWd9p8rx+P9M06Tddf2foFzrlpBKm/7zxQSx712v8A7Vfn78bvjd4j/b1/a4sPgb4Z1K40z4Y2t+8N+9q5T7elv81xNL03r8hVF6fdav1N8G+D9H+H/hnTvD/h/T4dL0fT4lgtrS3XakajsKF7y5gfuy5T5S1n4afAr9rzxPf+IfhZ4qTw78VNHiW7HivwvDLb3dk0zOu26iKors+2RWSX97977tWh8Yv2gv2dYVi+KPgaL4reFoZtj+MfA3y3sUO3Pmz6dt+Y56+VtVdjf7O73L4afBPSvhv8RviV4r08RxS+M761vJYo127PKt1i/wDHn81/+B16fQB5T8F/2lfhp8frMTeB/Fmn6zceV50unq/l3cKn5f3kDfOv3fSvVq8Q+Mn7H/wr+Ol2dQ8Q+HY7bxD8hj8QaO32PUI5E+46yp94rx94N91f7orzif4P/tJfB24jPw7+Jth8StBT/mBfECDbdR/MvC3sXzv8v9/+7/tUAfW1FfIV1+3D4m+Gck0Hxj+CHivwWIkaT+19B263p8qouWbzYtuzo7bW+6q/NXpvw9/bQ+CHxQXPh/4l6DNLsZvJvrj7FLtXq3lzhH/i9KAPcKKghmiuoVkjdZYXXcrq25WWp6ACiiigAooooAKKKyde8SaV4VsDe6xqdlpNoG2me+nSCLd/vNxQBrUV83+Mv2+/gl4S1A6Xb+L/APhLfEDbxFovhW0l1S4ldG+dF8pSm5cM3zMvyqa55f2jPjn8VLiSD4bfAq50HTXKmLxL8Qr5bJArdHWyX943ysr/AH/7y0AfWNfPXxV/bc+Gfwx1pfDtrqc3jfxrLK1vB4V8KRfb755gu7a2z5Y+v8Tf+gtjkbj9kPx38XoT/wALv+MWreINNuD+98K+E4V0jTNp6xO6/vZ0+98zbW217n8Lfgn4H+CulnTfBPhjTvDdtJs80WcWJJdv3d8n3n6t94/xNQB4E/h79o79pVbN9fvLf4A+BbnabnR9JuftviCeL5dyNdBFSDd8/wBz51/iWvbvg3+zr4B+A+nTW3hDQIbKe4y13qU7NPe3bscs0s7/ADOfxr0+igAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAG/w1jeLYby48N6tHY5F89pKsG3/AJ6bG2/ritukNRKPNGxUXyu5+Nv/AARx02BP2kfG7agmzVbbQpEjWX76t9oiEtfsXIywrI7MFReWY1+ZX/BQH4cad+y1400348fC66vPCXjrUbtftq2ro9jcNIQJWeB1IJcH5hnBPzY3c16N+wr8cvF/7bGi6nc/EbUEGnaXcvG2jaPCttaXuBGUNxndI4UsfkDqjfxq1aL95GL7GTVpSfc+77W7ivLeO4glWaCRdySRtuVl9c1YpKWkWFFFFABXm3jj9nf4ZfEySWXxT4B8O65cyyrNLcXenxPNI6rtXdJt3N8vvXpNFAHydcf8E1/g/Y3j3Xg+TxT8O7mRWWeTwt4iuoBcf3d4d34X+6u2i3/ZL+LvhmNJfCf7UPjK0vSvlO/iTTLXWrfy/wDZil27X+VPn/3v71fWNFAHygvwh/ar8Mt5+m/tA+HPGU7fumsPEnguKxtkXP8ArVe1ffv+ULtPy/O3+zUv/CM/tk/9Dv8ACf8A8FF7/wDFV9VUUAfKv/CM/tk/9Dv8J/8AwUXv/wAVUB+Av7T19/pNx+1FZ6VLIfNaysfh9YSwQOxz5UTyvuZF+6rN8xHWvrGigD5WuP2Kdd8TXUx8V/tAfE/WrK4ZppbG11CLT08z/Z8pPkX/AGF+WrHhn/gnD8CNFvm1LUvC1x4y1qXcZ9U8UalcX805b+N1d/KZv9rZX1DRQBzHgv4eeGfh7p32DwxoGmeH7L5f3Ol2iW6ttXau7aBuwtdPRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAH/2Q==\" style=\"height:250px; width:250px\" /></p>', '', NULL, '2020-06-22 19:32:56', '2020-06-22 19:32:56'),
(43, 1, '<p>39. Verilir:</p>\n\n<p>A &isin; &alpha;,&nbsp;C &isin; AB&nbsp;&nbsp;</p>\n\n<p>BB1||CC1&nbsp;</p>\n\n<p>AC:CB=2:3</p>\n\n<p>BB1=15 sm</p>\n\n<p>Tapın:&nbsp; CC1=?</p>', '1592854781.png', NULL, '2020-06-22 19:39:41', '2020-06-22 19:39:41'),
(44, 1, '<p>40.&nbsp;&nbsp;a, b&isin;N,&nbsp;&nbsp;&nbsp; a:b=2:5,&nbsp; ƏKOB(a;b)-ƏBOB(a;b)=45&nbsp;&nbsp;olarsa,&nbsp;&nbsp;a+b&nbsp;&nbsp;&nbsp;cəmini&nbsp;&nbsp;tapın.&nbsp;</p>', '', NULL, '2020-06-22 20:12:19', '2020-06-22 20:12:19'),
(45, 1, '<p>41.Həcmi, tilləri&nbsp;2 sm, 4 sm, 8 sm olan d&uuml;zbucaqlı paralelepipedin həcminə bərabər olan kubun tam səthinin sahəsini tapın.&nbsp;</p>', '', NULL, '2020-06-22 20:13:10', '2020-06-22 20:13:10'),
(46, 1, '<p>42.&nbsp;x-in hansı qiymətində&nbsp;&nbsp;&radic;x&nbsp; , &radic;(x+3) ,&nbsp;&radic;4x ədədləri həndəsi silsilənin ardıcıl hədləridir.&nbsp;</p>', '', NULL, '2020-06-22 20:15:36', '2020-06-22 20:15:36'),
(47, 1, '<p>43.&nbsp;x<sup>2 </sup>(5&minus;x)/x+4&gt;0 bərabərsizliyini &ouml;dəyən natural ədədlərin cəmini tapın.</p>', '', NULL, '2020-06-22 20:17:03', '2020-06-22 20:17:03'),
(55, 1, '<p>44.&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHcAAAArCAYAAABCdRcGAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAANFSURBVHhe7ZrBsRQhEIY3AAPQAAzABAzA8mx51wyMwwS8edMYNAIz8GAG5qB86/71urqAgQF23479VXW9ZWbgQf90w7JzCoIgCIIgCIJVPEv2K9nrc6kf1f9zsb3tBAsYEfdJsk/JaAM+JAuBD8LLZC/+fTyD2N+TfT6XgsOBsCHulVHKxEjDSqU+Lav8NhlRyPOtYilybVrmM238TqYop23KPEudYACc+jWZHPkmGSJKSK2TtiwxuG6FqeH/j4XJxSR5n6ylraARnIlAuQjUPUVbrvzz8neLj8lKz/W0E3SCUxHNp1ldr4lryyWIzNozuZQdTAbnKg3DDHG5h7iCOuykLaTjL8liszUZ72wcLDFGxeU6k8Wa3bABGyieoy2tyVr3rwozjMH8SPacCwY68+3yt0brc9dCAsn5ih76ZzdQr1z53eWvyrRjsTtwa2qf+9TDp6DUjG99W8uhU9jTZLktvZ+RNeh8bCAeCTXx9kYiwjJLe+sFEyH9IqzSiYfrdsPQA/VK7QYL0Rpg1wsvIlGXWyO0kbBrUe7kJaL3xtRS8tZaq8gsnbxQj/q0Y6GOnVTeItongSNLzkS8mrgIWts4Sdy9aT0YoBRZYkvcrZOXmeLmIvx/tGa2xNtKy1snL6XJE2l5MYq6miMRp/Slu+XkhXuxoXpAk50JvHSp2lovBeLbjvC59eSFZyMKH7A/Ny49xWt1PB0oRW+N5QO4c2o/EXaDk1kfObTgHJXPrY5vjXJBu7m1dhRlCftdegT1U2t9T39Hx8jZ9YwxnJFjGARv7fU2zGBu+cOBnEn/Z4hL/ZG3F9WfPeJSZ++kODSIMEPcW729iKixDykwS9wcOH2l45UdZBG9jlXiKnK9w60gpGGlcZ+WVd7z5qSF9qi7dW5/SFaJiyP924v+2qo3J3MwTiZH6dz+kNTExRk4uGS1SMp9NaGMQLl6uqfIzZVHXlgYrX+XrIhc2pQoHonmJ4eu18S15V4YH+PcW/8umS0uzqNNgSj+7UXgOQQuiTdb3K1z+0PCYGeJK8Gs2U2TF5r/rYmwUtxH88bktbCbFgzHMfi9IJIVVZZLvf6e30D1vjlZgj7xfMu5fRAEQRAEQTDO6fQXaPOBGKvNBbIAAAAASUVORK5CYII=\" />&nbsp;&nbsp;funksiyası verilmişdir.&nbsp;&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAvCAYAAABOtfLKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAJRSURBVGhD7ZnLTQNBDIZTAAVAARRAG4gzogDuSFAABSAa4MYNaoAK6IADHdAD+MuuJSvazdrzyoD2k6ydRPP6Z2xPdrJZWfn7nI+WQ4k+srkXuxiK2VyJXQ/F9jBwKSEK/d0NxXaciT0OxeLQL/034UjsQexk+6k8CHkVY5zqMFhtV2i2OwjxDsTqPon9zNiz2BRNYkcn53Ex6t6InYqR9WjDd7jQ0mJQl3GquhqDMLEIto03HiKLlgyTiZ4F1NedwK286bx63ER9mZUl87HSlN/Gpwe7CFVAjHdlAeFan+dcwE9h2xaBDr/EdDUjYqj3LqbxgRD6IyFc8sUC2WLY2m+xDzG2mMnY1bwdbQnafo5PhcmRjokFT5ZiXM9Yk9AYOxZDBD/8eNrV8YopQbIYJmzdCSjv5vruxeDDCKGxBTG7vo3oLD8OEIoZVhw3sj8v6GAf3YpRaLDrYnN0L0YD30PXYtgNdsXbyCPGuq3H5giLoYHXxcAjphQhMZoAvC4G3YqZOqWX6FYMlSO7At0cmsTFixiHJBdulL2xonQjxh6Sqa+kUTGaLTVzRVx0r5gSRMQgxL6MRTNnV2L49W13HxH6auGhuhjchBVOARGRW9Dqr810Hr3QANpFbymrX2jgKtGrJnZSE4A3ZhBd/aopZxAV5XFT+k/NuCGYTMr2MzGOBo+YnNgMgZDUgchQnrOm2cV5qqt5kwD9RuMyC1Z3Kc1SRwMfw8U8MdBsVyykaI/LRKC/lNRfBNyhlKCD/kGrlPjbmyutg/91vrLy/9hsfgHrVpBTpRRAuAAAAABJRU5ErkJggg==\" />&nbsp;&ndash;&uuml; tapın.&nbsp;</p>', '', NULL, '2020-06-22 20:25:56', '2020-06-22 20:25:56'),
(61, 1, '45. Rombun diaqonalları 1 m və  √3  m-dir. M nöqtəsindən rombun tərəflərinə qədər olan məsafələr  bərabər olub  √83/4   m-dir. M  nöqtəsindən rombun müstəvisinə qədər olan məsafəni tapın.', '', NULL, '2020-06-22 20:32:55', '2020-06-23 04:51:17'),
(62, 1, '<p>46. 30%-li&nbsp;&nbsp;&nbsp; 600&nbsp; q&nbsp;&nbsp; duzlu su məhluluna&nbsp;&nbsp; nə qədər&nbsp;&nbsp; su&nbsp; əlavə&nbsp;&nbsp; etmək&nbsp; lazımdır&nbsp; ki,&nbsp; 20%-li&nbsp;&nbsp; məhlul&nbsp;alınsın?&nbsp;</p>', '', NULL, '2020-06-22 20:44:21', '2020-06-22 20:44:21'),
(63, 1, '<p>47. <img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIcAAAAYCAYAAADQ1+6cAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAI4SURBVGhD7ZfBUcMwEEVTAA1QAAVQAA0wdAEdcGYoANIAN25QA1y50AMd0APss7MzzmZXlmxn4sO+mR0rWuv7S1rFySZJkiRJjsffQjEXT3NKzMXTnBJJkiRJkiRJkiTJybiX+JW47D6tj2P5W8O88fDaN/egn7/Lnj8vdybxKRGNmcy5xIsED1gjx/J36nnrJtviYGPv+uZB8UQ5+rQg6KNQFpnXjQQPmgNGHnbXpWnx1+JjiXnPxW6+peQxylEk3xIUfxUXEgygUp93bURYxHeJJwm+jqwoFUk/gZmIlk2JND2PVxIlf5ZaH6V5t/jT0zqVUnHg8bZvHlDK4Ym5ja1BBxP8krjuPu0b0ipj4ojRrwtCm6CfvuFCWbinZlMiTa6ex5I/j1ofkW6rvyGM0fe+DXuvEhUHhfojwVjuGVLKAV69/gMQ+pBgERQG6qSHbcAon2uqr7QY3jsv0ix5jPwNafUBni5F1epvLuhEhQPkKQQ8WLwc7erfUQgQip4YRBBASMW5vu2udtwYaI2d2EjT9g9PdeQvosZHNG9eFS3+rA90l/rmUHgGhenN2cs9Snj3uvBg/XXLVyLvWDWjJxPYCE6Nngb6t32zG6caETWbEmlGHkv+Imp8RLqt/paAZ5a08BXlbQ4tXR8KeGzPupv1BxcDEODUsCgsIG0qmzwTV/R0kNOFLFGzKZFm5JF25C+ixkc071Z/rOEc0OVZhN1krx+iHG3tJ/CL1yRJkiRJkiRZE5vNPxIjDA1f67aTAAAAAElFTkSuQmCC\" /> olarsa, <img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAYCAYAAAALQIb7AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADWSURBVEhL7ZTBDcIwEARdAA1QAAVQAA2glEEHKQNogB+/pAbefKiFHsKOk5UiPhgUf9CNtLJ9ie+8vigpqMGwoILgj2ilp7TNq8qspYu0yqvKNBLuFmUjPST+BKdpvpN66ShxlcRwag4SccShiuDFu7TPq9HJVaJPFOAgXCMxJ2WOiBMrKsZJbxIJDcXY7NG4GIfA8dd9JOG8J3M3fBi+NsZuGt/3FMNpuXvgGumPHTghhXFil8TP4zTv8f6PkMDNxxWJcMSckQ+GZ+4n2D3PfnIYBCKlF8h7PMa8k75nAAAAAElFTkSuQmCC\"  style=\"height: 50px; width: 10px;\"/> ədədinin ən kiçik qiymətini tapın.</p>', '', NULL, '2020-06-22 20:44:54', '2020-06-23 04:52:47'),
(64, 1, '<p>48.&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHsAAAAYCAYAAADap4KLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAJeSURBVGhD7ZfNTcQwEIVTAA1QAAVQAA0gyqADzogCgAa4cYMa4MqFDjjQAT3AfNm81WDFE29+OKD5pFHsJPa8eXaS3S5JkiT5T3yvFEmSJEmSJEmSJMlsriy+LE773raQS3/lLjjhID86/koLoOdx19wzpWNtv44tPi3831x54/2ivRiSPVgc9b3twJxnC/LQfrcgN3Ckz/ny2lbISL/YLTror+kXeXyOGwv6pZaP4bgIdtEqu2YCcvg8mKw+GmQ6Jr5alE/+ITDH9XCMIL9f7BYdh/jVqkP4+/1m8ws/yYkFN7OT74Y2A5mUp+3WgleTJheXFnqlLTEfSmP9YkfXRK2GMVpNLvNO6Zjyq6RVh6AePAfGsNnQ0LzBuPHN4rzv/S5IuwcjmVwTA22C85wrF1ti9E3xofk9jOfbJHO4RwVw9MX4axDVMAba5i52pCPya4xWHYKF9hsYr/AsqnUPN79YIE4gXgJ9GySehPq+rgnza0P4HzmRyVM1CLTWNh/nx2phHm9kpAPKvPLLM0cHcL7cGCz+mQXjorE9pXjtTAxkID809KRxfBqO5bgxGF8ryhs4Bgb5e3xf88rEqIYazDHnyY500K/5VaNVBzAPP86Ef+BKLaMgXN8AXoF8a1QMA2UiTw0Te4Pvd81+nOZYA3L41znQZgEp0BcJUQ01Wk0uFzvSEflVo1UHMJefj/za1ASehfm4qB8SDEYsu1PfHNo8iVzX9xCUiGsqcCloYb7a64ic+jFIW0Q11GgxWXrKN1FNR+RXjRYdQn+5PNQqjWutQ5IkSZIkSbIdXfcDIPje80UzsogAAAAASUVORK5CYII=\" />&nbsp; olarsa,&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAASCAYAAAAQeC39AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEhSURBVFhH7ZRNDcJAEIVXAAJAAAIQgAHCmXAHBwjAABjgxhENoAAHHHCAB3hf0w2TTdMOaW/Ml7z0h+7Mm2F2UxAEQQdL6SaNqqd/ZCrdpZe0qe8n0i+sJdYjOtoHcj9rcZ9j7yRiv6Wz1MpMekiL6imlo9S5qICEjAYNIvFW6gsFnSSKQsTNvri2No8ZxZD9iEUY9YKBa30dEjwdpFX19PXlyme7ABR6kfgXSwjEaJSdIpmnEeRhhJpkPWR4h/BkfZWeG+EDazSP1C+nTRljCGgi+zw3mCuF4cuVj4/YDyzg0GCm99JY8kIziEMM9ukQ+8sWAuTA11xiDPm9tflUzmlDd9j4GKQ47r3k7jJSnpH0QBwbK/uicfagC4Ig6EtKH1DRP4ajit+1AAAAAElFTkSuQmCC\" />&nbsp;tapın.&nbsp;</p>', '', NULL, '2020-06-22 20:45:58', '2020-06-22 20:45:58'),
(65, 1, '<p>49. n&nbsp; = 10 * 15<sup>4&nbsp;&nbsp;</sup>* 18&nbsp;ədədinin sadə b&ouml;lənlərinin sayını tapın.</p>\n\n<p>&nbsp;</p>', '', NULL, '2020-06-22 20:47:01', '2020-06-22 20:47:01');
INSERT INTO `questions` (`id`, `test_id`, `question_text`, `question_image`, `answer_explanation`, `created_at`, `updated_at`) VALUES
(66, 1, '<p>50. <img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC4AAAAeCAYAAABTwyyaAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAG9SURBVFhH7Ze9TQQxEEa3ABqgAAqgACgAESNyyBG6GFEBDZBRBQGkJHRAQAf0APNuPdKcZc/Y3C3HwT7p0/6c7fnWM97zDjMz/5hD0eV4+uOciQ7G0z4wfTuebg3i46OZPdG9aH95tT2Ijw/8NLFI+g00e+EpX0VdKZoQfOAnzP6p6FnUnJ6JwfC7CF8uD0k1TkSPIh2IGfkQ6cNy/zMdS9DmXPQislnVuPmEcc3YnqewkZpCtDkWYcISGaefjmFrl3NmtlQS9HGrQNPiLQY634nywD1olmx/xr1JxxzvoZaUBiyh7WqzGlHKLGOSwRL4IZ4trRVajWvgqF2N3DjXV+lYYmPGmZknkVt3AbZuGa9qStiIcQJdiCgTt+4C1Dj7EfYlHqFxb3Fyj0VJENoxyFs6Xqd7QLuWhcvvxKotSIu2rU4SA9jas/Da46l1QWpb/tXsLk7fOFE2QjMGW1ZVmho1wM7OMxXVtQVPpclcYd3aBfp6dYvh2qsvRzMbvnoJus4miy0ByiEwBo5EPR8n+GjaZAH1Fy2uXhiPNdL7RdXlhfTs5IcEkKKd+3RTWv4cpoKS+u46m5n5AwzDF+wxaFrKZRuZAAAAAElFTkSuQmCC\" style=\"width:10px;height:50px;\"/> cütü <img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGUAAAA4CAYAAADkbDbmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAPGSURBVHhe7ZvLrdUwEIZPARQABVAADbAHsUbsoQFgjWgAGmDHDhZUAFuERAFXYkEH9ADzcTLSaJTE48ROjJhPGp3rPPyYf+zYie8lSZIkSQ7ilthzsR9iLzmQnMsdse9ib8VucyA5n/dij65/JiNAL/k8/SaDcE/soxjPlGQQUpQBSVEGJEXpA8/on2K/J6uaSKUo7fGTJ9Z9CBSeTPUShfzeif2PszrWetaf+IB1IL4OkaL0Bx+zMA9TEoWxkDHxl5gq/USM9BexpftqRGHxShk2P8pdy78W6qFjPHnTFqJXhxWfbkXJv7NEb2JcxHnPxFScNWpEARxlHcL9r6Zfj4q4ZJyfg7xx/FMx3vPRDsb+h2Kvxe5O6Vai4DOtU5dnCtfxsnJNkK3OAp8/DXh8/bMZCI8oCAKkP4nZ9Fzv3NMuUHHCL3qJmhuxkiicp8JUPALX1/QUriOaNH8EaRWxCs6zTidNHW062r4a1HfNRWHY+iBWigpljyj0ltY9Uh2jTvdBQLrnO8AqwSOi8GBXZ+lQV4rkWlHUaW/EWg9bYOsOtMf2GtI4rkcP9WUXWROF7sYsi14C6rjInJtrt4gS7Ym10BY7fPjI5VykXRHI1/ZcK36I6PB1BA/ERqjH6fgIOhLt1vwSHCnIRNUDqDGIwTRYh8dEQIyqB1DSD0TgXYydoycnwmyI9UaLWUbSkOwpA5PPlEE5c/aVLHDmOiVZ4IXYt+ufySikKAOSogxIijIg/4IoLHT5hLD2EWsrLKb52KV5DzETpRK9vmG0gtmhOq2l41if2W8+Ws7pwowuCg5jp0kN3tlL3Bezr5u4r+eHtjCji0L9antIVJQ58MXwoqhT5jbjqbP8t+7qz58LaORqOVjEYVtF0fJUfO7X5w3HaL/dsOfTzSiJojDecp3fjMf9tlI0bG4THfda53or1cEKpE5bYqsotMu/CyQPHM+X0SM28P0lKgqVmduM549TuR67URTqOlffvaIDb859+/APotRu4NtFVBSNVK63IAI9RY/32KJjidR3S09hJPBtA8qyTidN3jY9d98uIo2Epc14VhSizEeawn1zEawWqQNQTskJtaKQn30pSxuYmflA9AFIussGPgooOWRtM55WvNcmOostf40aUWiXDxB9RvryuNb2GvVd89FhTRSip7QZT49FI70Gylia5a3BNRFRdLHoTdvCeduDOI6/FM5ZXzQjGn1r5Ca6xuBMRKlRW4XkNzfRdQLnfhVj7h1xMNfnJroD4J8nmaczhttxM0mSJEmSJKnncvkDClo18JCpXJgAAAAASUVORK5CYII=\"  style=\"width:10px;height:50px;\"/>   tənliklər sisteminin həllidir. <img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAUCAYAAAAgCAWkAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAFGSURBVFhH7ZTNbcJAEIVdAA2kgBRAGyhnxD00gDhHKSBKARw5poZQAR1woIP0QN5n+Umj9fovOJdkP+nJXoNn3szsuioU/h8L6dSI+8KfwyO+NVfWG+krrGEr8ey1Xs3HUSL3VXqQcn4QeVNPnRB0Lz1JFMOVl5fSurnyn1HBJkKuNC5eyMezN2klRU+d0BE6s5OYABCMZy6EoCT8jWIwjYz90FQm8iiBPfF7J5g9Sy8SRm38U6IYcAICzonj0nWDn4t0kOLzUTsDg+xRv+gEH809DI2YRMToU+y+sfEY135y0xpsZloxxgkWX+R+cMQ/gLhpt/GT5hpqZk2u4tS4t12us/dCzDETyBXdIh1zzrgTcCCfpd6AE4jG+fiwzm276Ikz7Ca3SCt2gnjwCMyI36W5CgHH5Wz2fbHsiY+U/1coFApV9Q3dZ2KgTwtI4AAAAABJRU5ErkJggg==\" style=\"width:10px;height:50px;\" /> hansı qiymətində <img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADkAAAAeCAYAAACWsbYLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAFiSURBVFhH7ZbNaQMxEIW3gDTgAlxACrALCD4H3517MD4HV+AiUkUO8TWXdOBDOnAP8fu8GhALIaw0i8SiDx77A6t9MxqN1DUajVnwLv1KV+mRF4Vx97MJgoN0lh7uT2WY3M9C+gzXGpjED4Md+9sqmMTPs1TLLIK7H9YBa6AW3P0wIB0N6Gbr/rYY7n7IFu3a9COVLNkkP0/Sh2Rtmcyw/3i05rElxf+20pcU73/MGkrygwnLCIMw7fzEi7FB4sH8xN9xn1VBZOckDQf2YGyQYJUUf4fHt3BNxga2kvUiJUgCYalYYwGX5mID585kXG5/6b+yGwbJ82u4ZkGWOB55nwNTZhII0LzgLW5CSTDQTsKQ9/aQG+RS4lSTDD+n4djRiGxdwnUf3uWSGiTfkPDsZsNWETcbBiN73xIZ9CA3SM+qqg6XdVgzLttFjVDWLJWV9MKLOcIapD/MNsBGozFruu4G2D1XLcn67PUAAAAASUVORK5CYII=\"  style=\"width:10px;height:50px;\"/> ifadəsinin qiyməti ən kiçik olar?</p>', '', NULL, '2020-06-22 20:47:34', '2020-06-23 04:54:06'),
(67, 1, '<p>51.&nbsp;ABC &uuml;&ccedil;bucağının AA1&nbsp;və BB1&nbsp;medianları&nbsp;perendikulyardır və uzunluqları 24 sm və 18 sm-dir.&nbsp;&Uuml;&ccedil;bucağın &uuml;&ccedil;&uuml;nc&uuml; medianının uzunluğunu tapın.&nbsp;</p>', '', NULL, '2020-06-22 20:48:24', '2020-06-22 20:48:24'),
(68, 1, '<p>52.&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMMAAAAjCAYAAAApO74oAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAASSSURBVHhe7ZzN0dMwEIZTAAVAARRAAVAAw5nhDg0wnBkKYGiAG0cq4AAV0AEHOqAH0BP7HfbbT5LtxEoca58ZTWzJ+tl3tZLifHAIgiAIgiAIgiDomMcpvRwug85hHjwaLvsDwz+m9OB4d7s8SenPmLgO8kzpxDx4P352x6eUbn3yENDfx097Hdxlrk7MB+ZFV+zF6BcpfRkuj3BNXnCXuTqxK3xN6dYXyUXsZdK8G5PgOoLhPkt08oGza9gef6a0h+hf4uRrwO77O6XckeSSLNGJecH8uPaYT4bt7XNKf8dUm+yI8COlLXxRYgXSmEnWYYI3Xtgju7gXfhVr9T0IrdBsqW5rBsOUVmvpxFgZ85YWldngHN4KyXiJUnprgChbCAbGxhitg70D5JhXx7vh004uPu0XQxaELQT52kxptaZO5DM/bPDcDAjlJ73E86vHlgxlDLmdwOIDNzd+JgWToxT8e2BKq7V18u0V4UEboSTb6RbQSuEFLOVb2Fm+pSTbeLtgt1xfzrUtB/rRls0qhPgfUhIKVpX7+lAaK/d21WuN/J3rs2YnE4kVmnLVe5gSRxQdY6hDXfuMZ0or6q2t06y6FGKsjZjXKU1G0IVBwF/jp0XCeuGE6j0/3v1/XquEhNd2jGMo86sNE+jNcHmsx71dMOgf5ypR3x+R1LfP59731xr6zK2UNTu1EmtSUU4bsvftmCc7rT6WKa1a6ESfi+siRK0CBlpDcqkkwjnQZm7CS7hcmZzly+yXQNr141WbylfAWOdoEbFoImiX8SuRJlPOybn8luTsnmMndbxdPk+6+/YtNa1a6LQ4GHjw2XC5KRgXRxsE9FBWCgaVlcST83N1rYPlXBxROgJ52Gl43rbdwsmnILt9f3PsXCsYLF6rFjotCgYMaf0HboiDMbXkBUTY2i+I5wRDrS553sE6D5N0Rq6BLbbt0ngWOcpwip5AP7kjJ0zZSXtrBwPwrLRaWyeYXZfBz/2DJgZtxc6lJSJMwZGmthJIODvphMpK48F5ODFXTnve6aAvjNiZK7dYB4P682Mt9dUK+mOy1vxdshOb/Fh93rnB0EIn6s4Khq3+qasPBETm/GoNqgWDnIJD/UsC2iXlnAvWOdTziwV1S6uroM9cu3YiaoxLJs650Feuvzl2Uq9FMHitqLumTrOCge8I9gE65W3StdGq5JMVCBAwt4oInJlrR85TMNGujgP+Bx45gjGpb/sMeThTb6y4580KfXvUn95eMW7bV2usXozR9jvHTspz2tg89UG+2hHcz9FqbZ0m66tDP1FyTrwkiOXHpOQnvZxRWzEQnrOv2vC/I3Bty/2XR/pggXiakp6zZ2nK7Tk79zuFBd1z7VwC+dz/1gJTdqK9bERvaa88Jhvt86k8HxBLtFpTJ7/T7JZuDA1OQkF76hHrpmAnO2cLDfaNguHaJ56LQBCwjbKtBoFHx61uFku2wC4iP1gM86KLI5Ig+ku/Ugd9w9ux7k4NXRodVGE+MC+6g12h2/8aJLgH84B/JNbtixUMj/9ELICu/xOxIAiCIAiCIAiCIFiJw+Ef+j/Yjg9yC0QAAAAASUVORK5CYII=\" />&nbsp; kompleks ədədi triqonometrik şəkildə yazın.</p>', '', NULL, '2020-06-22 20:48:53', '2020-06-22 20:48:53'),
(69, 1, '<p>53.&nbsp;H&uuml;nd&uuml;rl&uuml;y&uuml; 4 sm, iti bucağının tangensi 2 olan bərabəryanlı trapesiyanın diaqonalı yan tərəfə perpendikulyardır. Trapesiyanın b&ouml;y&uuml;k oturacağını tapın.&nbsp;</p>', '', NULL, '2020-06-22 20:51:29', '2020-06-22 20:51:29'),
(70, 1, '<p>54.&nbsp;<img alt=\"Fiqur\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJMAAABXCAYAAAAAnHe3AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAJcSURBVHhe7dsxbupQGERhSspXUNKxAZbgJVCzAGp2wFsN62ADiDJ1qpSkTeUwvBcpCgbs67EUec6Rpr7F/5X2xNyf+Xz+stvtajbubbfb+nJv7e/18uaukE6nU03j7nw+18vl8r2qqkEwASmkL0j7/b7ebDZ2TEAK6Tuk4/FoxwSkkH5CcmMCUkhNkJyYgBTSPUguTEAK6REkByYghfQMktYHE5BCagNJK8UEpJDaQtJKMAEppC6QtK6YgBRSV0haF0xACqkEktYWE5BCKoWktcEEpJD6QNKeYQJSSH0haY8wASkkByTtHiYgheSCpDVhAlJITkjaDabZbPa6Wq0av/Nl45oTknaDSd/xNj3MxjXdWcdvQlG6G0x6iMaf7gwmsgQmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsgUmsuXGdDgc6sVi8XYhtPwn6RKYMnJiaoSkwJSRC9NdSApMGTkwPYSkwJRRX0xPISkwZdQHUytICkwZlWJqDUmBKaMSTJ0gKTBl1BVTZ0gKTBl1wVQESYEpo7aYiiEpMGXUBlMvSApMGT3D1BuSAlNGjzBZICkwZXQPkw2SAlNGTZiskBSYMvqJyQ5JgSmj75gGgaTAlNEXpsEgKTBlpDuv1+vhIKmqqq4PsXFPd55Opx+Xkw8D6X/6u4BlbFBIRES/rcnkE2JxOUDVoQIxAAAAAElFTkSuQmCC\" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BM=8sm,&nbsp;MN=12 sm olarsa,&nbsp;ABCD&nbsp;d&uuml;zbucaqlısının sahəsini tapın.&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALsAAAAdCAYAAADy1R2ZAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAARLSURBVHhe7ZrNbdVQEIVTAAVAARRAG4g1ogAaQKwRBSAKgB07qIANWzZ0wIIO6AHmI5xoGM39s1+CLc0njZLY92fm3HNtP79cFUVRFEVRFEVxWh5ZPL7+tSgOz3MLPLsMnehcFGfitcWS4R9YvLe49+evc/DE4uX1r8UtcnSd8e7Hvz+n+GBBUS0o9lcjPlv4Rx82zBeLrxac7wmlthrrp8XsLr2NRWDMng7k+8Liu4Vy/mbx0OKSd8ae3uhKDvctPMyPfmoXtcEMPyz8WL1axRl0Jr+pHBGBgUYmk1hsDMHkMmtMnnYc9+0jWlQWafWzwqUXgfoRt6UDApMnd0DfBg1kot4CrsJYzOdrxAToxHq1Lgz0Ixdyilc7XVxYE36f4Qw60w5Nhld3CkGAUfEMSBJxIonrTc1YbyxIoDU243FX6LXpcclF0EbODAJvLaixNd+02AtQX8vQvXwxC1fFlil6Rss4g854Bw9181Sj3tVXMFCWJMejsCTFBwfGzYzMGBj9qUW8es1yyUVA5HcWvfp6c9FHn3nIKzPaKmjXMgVkeTH/KwsecdA9ak9O2Xr0OKrOkZbXbqAzE4+KYQAG8psCQVs7EYEkUlYY/dRmqzE0/l4YQxHFYtOyGUcGoT42N+ypSWhdehchzePbjPKg7apm9D+izhFq610cbiYZFSPxEc8Hps2S0+sghIq3Yo7RD4YJdrjEIpCXzyWKzfjUuXeeVbQu3qgRzpGbNzvH1EdrpvPURX29MTNofwad6Ru99g8SdTQJBfuBSPSZBcdi4vzOrZSfcdFYAL0m4jx9Y/9Z9i5Clos3jo51Bbwlhgtn0CYahOd138ePQ2z5XHEWnYeacYIGo2Ky3Qj0i4Izpm41FMjVhfP0/WShZGbnbrF3EaiJ3H348f6X2TVvprfIcuOYLjJC+lMrenmTzXIWnRmzO8aM4ZRM1oZjMXnEIcCLTZvYjr5qu8qeRYi5+E3pIe+7NnsrF4+08+alX/Y8qxp4C7NFr7PozJi7za420ZTaBNGwel4H34arur/qUCCFI8AWti4CuUVTtGrk72iqCH3jlxx70JwxF9F6fKR91ke1EVqXFRjzDDoPza6d1poEMTNT6osNEtQHD2CizNQxCRXdK27E6iKQE++feeUZNxgiUksmJvVxDnPxxYbgbRTjxT7kRPstBiFH5ol6M5f/MinOST//iBhB57g5ZjmqzhFy7F48JW5mOu22LBCcYiQuEzCRP69zJKGdHNvFti3Iz/fpRVZwrEULn+WTmQKjUa9v5+v3aNG6widok7RC/yYQc4v9pLWHPGcMeyadI1MbGhGGjYpluIWvmL3YDt7Fw8MNzYJwe5zZPcUcaMq3w8XdgHenX6uyI2Zuc8UYbsVEcXfw+JQ9iqewI5b+J7goDgKPMPy/zJJ3uRVk72mL4sjwlmbTIzivfOpZszgLm41eFEVRFEVRFMURuLr6DcGvwCkQR8LjAAAAAElFTkSuQmCC\" />&nbsp;</p>', '', NULL, '2020-06-22 20:52:52', '2020-06-22 20:52:52'),
(71, 1, '<p>55.&nbsp;9 elementi olan &ccedil;oxluğun alt &ccedil;oxluqlarının sayı bu &ccedil;oxluğun 7 elementli alt &ccedil;oxluqların sayından nə&nbsp; qədər &ccedil;oxdur?&nbsp;</p>', '', NULL, '2020-06-22 20:53:38', '2020-06-22 20:53:38'),
(73, 1, '<p>56 Yemişin &ccedil;əkisinin&nbsp; 3/4&nbsp; hissəsi qarpızın&nbsp; &ccedil;əkisinin&nbsp; 2/3&nbsp; hissəsinə bərabərdir. Yemişin &ccedil;əkisinin&nbsp; 9/10&nbsp; &nbsp;hissəsi qarpızın &ccedil;əkisinin hansı hissəsinə bərabər olar?&nbsp;</p>', '', NULL, '2020-06-22 20:55:12', '2020-06-22 20:55:12'),
(74, 1, '<p>57.  A,B,C boş olmayan sonlu çoxluqlardır və A<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAAATCAYAAAAnMdWSAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEwSURBVFhH7ZbBrcIwDIY7AAswAAMwBogBuMMGbwAGQCzAjSMTcIAJ2IEN2OHxf6KWokCSiiRcyCf9ahs1tmM7abtGo9EowEH697SXRlJt5tJVwuddWkqLXl9jJ5EEmEg3yZ5rQGIvEgufMiAY20gkwcaqY4G42WbhtRZv/pDfXWPp3F+jMJH29FvW1Z+UgixTARwOzX6Ob8ZD9rG7et6G4aWjxB7JhWDcoFk89kPk+GYuFc/qqlKHggVjtqgGVYl1TI5vuovzZEhHBllLJQ4Ft+UNqkKAoX2X43tIcpOUqjxBuC1onfDuMDJKVD7U9rP+GgUjJ4l9Fwoyhd/yPPPJY9/HFpfrG9v4wJfN5/O6lULd9gITPz1xLQBfbAF+PFLk+Ab35wZhiwQ0Go3Gr9N1DyVyY1Jn/f9JAAAAAElFTkSuQmCC\"  style=\"width:10px;height:50px;\" />, n (A) + n (B) + n (C)=27 və n (A)<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAA9SURBVDhPYxgFGEASiFdAaZJBLxD7QJj4AUjRfzyYKENAgGTnUsVmQyBeCcQ8YB4JAKQBpBFkwCjAAAwMAPczE3wAQe8dAAAAAElFTkSuQmCCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==\"  style=\"width:10px;height:50px;\" />n (B),   n (B)<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAA9SURBVDhPYxgFGEASiFdAaZJBLxD7QJj4AUjRfzyYKENAgGTnUsVmQyBeCcQ8YB4JAKQBpBFkwCjAAAwMAPczE3wAQe8dAAAAAElFTkSuQmCCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==\"  style=\"width:10px;height:50px;\" />n (C).  A-nın ən çoxu neçə elementi ola bilər? </p>', '', NULL, '2020-06-22 20:56:06', '2020-06-23 04:55:22'),
(75, 1, '<p>58. <strong>a + b = -12</strong>&nbsp;və <strong>ab = 2</strong>&nbsp;olarsa, <strong>1/a-|b|b<sup>2</sup></strong>&nbsp;ifadəsinin qiymətini tapın.</p>', '', NULL, '2020-06-22 20:57:40', '2020-06-22 20:57:40'),
(76, 1, '<p>59.&nbsp;ABCD trapesiyasiyasının xaricinə &ccedil;əkilmiş &ccedil;evrənin mərkəzi CD diametrinin &uuml;zərindədir. AC=4 sm, BC=3 sm olarsa, trapesiyanın orta xətinin uzunluğunu tapın.&nbsp;</p>', '', NULL, '2020-06-22 20:58:40', '2020-06-22 20:58:40'),
(78, 1, '<p>60.&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAI0AAAAxCAYAAADnViqrAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAQhSURBVHhe7ZvLrRQxEEUnAAKAAAiAAHgBINaIPSSAWCMCABJgx5IIWMCWDRmwIIOXA9R5miuVjN3uNz12t7vrSKWZ6Z8/dV0ue2ZOQRAEQRAEQRAEQRAEQRAEa/HM7JvZ87tPp9MTs1uzH2YPONAInv3J7G/BqAN1CTYGQpGTvpjdmL006wHleZGk1lq0wQL8iH/LgQ68NpM435sRUagHQlHE2zKpwKdst2hK6u2wR2aIBqjDV7MRosvuBTEHjfJekUZQnoTKdNW7/Es5vGCAXOa7Wc88wkcWIs6v87ERiChj9sqMEf/HDAe25rEZZSnK+LKpS486LOEwomEUfzBTJGEqIAF+YYaTOP/7/PrmfKwFPBeB+KhGXZQnpFMU11BvhLYFVM+rQQM/m+nBWwm5rFTYi/EdzzGf/FJ3HEmdWzqI8tJ+kWhKOQ3X/jTrtR0wRSqYRT5PRwSvPGDtTSo5aeshv4YilES+Fl40i33ORemFfOYBpRHUmq109LWgHcp91sKLponP5bTSA1Am04ZCG6sJqRbS8+kUA5SBujlPmKTS2vug3J4rotZoCl1rEMoPU9R8XgUHKrlM0Tm++wEpVE5W4ZrHEQvn0tDH/gY7rMB9fMbUwbzfE7RnrYFQEwxM+XwWNDCnuNKIYTWj8CvneyQsHZew/PTDMW3Nc+1epiZBn6UDx0PfKCKULO3XuXBvDZ69KMqUtsVrDp0KcVRKwpL4aAxTk5+6eDbH9yaa1u2aEkZNNFM+r8JN3FwaDTXR6HxONByTaICy0iUf4mnZuS1Hc41W7Urrn6N0HGo+r8I0M9UoiaLUsYo0ufOpaMRDM31bzXnyHN7n6qGOmWPXJPf8mqXURHOpoDkudJ0n/ZxS8/kk6c0oUDmG4JimFc7xWXAvRsNy4uC4IhD3vTu/Cu4lEWM7vhStRob20K6LR/RM7iOaOT4vopGeWi7bp5DctRKKohH3KldhJeWFxDM5T7l6vq7Rais3qkaG9uT689rIH8K/99zH5/+BsnI3Y6XRznJbeyxYug/De38+TXapFBHlqZmuU04DlNujg3tBO2hPr+ipfge9ei7x+eZRbuRD58jQjtyU3QovgkPRu6NbsdYAOKRoQN9ot04eW8F0y9S91rfchxQN0PH+9zSCpTmComPIh9aKSCS4uaSd+lLvUQW/O/xfVhAV4X+NxFkryb2t9HYHwuBXex4y/t75D2Ux9RDtQjQDgtN6LxXZ52DrAbGGaAaCqIPz/CZhD5iWEKlWRiGaQdDOsxLhXgkn4vx4fg3RDApTBOLpldMQYbTnEqIZGK1iWm+gEc2YCkWIZmDkvNaiIcogzpyNvAl5SHDWot+3XkhEmkGQQPg3Jcko1vMbZE+IZhBwFCLRtMDKSf+M6E2IJgiCIAiCIAj2zen0D/Imi+WbNYYiAAAAAElFTkSuQmCC\" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;triqonometrik&nbsp;&nbsp;tənliyini&nbsp;&nbsp; həll&nbsp; edin.</p>', '', NULL, '2020-06-22 21:03:16', '2020-06-22 21:03:16'),
(79, 1, '<p>61. Choose&nbsp;the&nbsp;correct&nbsp;variant.&nbsp;</p>\n\n<p>&nbsp; &nbsp; &nbsp; No&hellip; car was as good as&hellip;.&nbsp;</p>', '', NULL, '2020-06-23 04:56:57', '2020-06-23 04:56:57'),
(80, 1, '<p>62.&nbsp;Choose the correct tense forms.&nbsp;</p>\n\n<p>&nbsp; &nbsp; &nbsp; Yesterday Samra&hellip; her&nbsp; pen she &hellip; before.&nbsp;</p>', '', NULL, '2020-06-23 04:59:58', '2020-06-23 04:59:58'),
(81, 1, '<p>63. Choose the correct sentences</p>\n\n<ol>\n	<li>Where are the child&rsquo;s shorts?</li>\n	<li>These researchers&rsquo; discovery are very interesting.</li>\n	<li>Each of these answers are wrong.</li>\n	<li>The number of the employed is rising.</li>\n</ol>', '', NULL, '2020-06-23 05:01:04', '2020-06-23 05:01:04'),
(82, 1, '<p>64.&nbsp;&nbsp;Choose the correct variant</p>\n\n<p>What&nbsp;&hellip;&nbsp;they&nbsp;&nbsp;are!&nbsp;</p>\n\n<ol>\n	<li>a nice pair of shoes</li>\n	<li>big&nbsp;sheep&nbsp;</li>\n	<li>a&nbsp;pleasant lace&nbsp;&nbsp;</li>\n	<li>big&nbsp;eyes&nbsp;&nbsp;</li>\n	<li>two&nbsp; pairs of shoes&nbsp;</li>\n</ol>', '', NULL, '2020-06-23 05:04:29', '2020-06-23 05:04:29'),
(83, 1, '<p>65.&nbsp;Choose the correct answer in the Passive voice&nbsp;</p>\n\n<p>- What do they export to this country?&nbsp;</p>\n\n<p>- &hellip;.&nbsp;</p>', '', NULL, '2020-06-23 05:05:34', '2020-06-23 05:05:34'),
(84, 1, '<p>66.&nbsp;Choose the correct variant.&nbsp;</p>\n\n<p>They have brought &hellip; this month&nbsp;</p>', '', NULL, '2020-06-23 05:06:11', '2020-06-23 05:06:11'),
(85, 1, '<p>67.&nbsp;Make u a sentence.</p>\n\n<ol>\n	<li>too</li>\n	<li>jewellery</li>\n	<li>that</li>\n	<li>was</li>\n	<li>antique</li>\n</ol>', '', NULL, '2020-06-23 05:07:15', '2020-06-23 05:07:15'),
(86, 1, '<p>68.&nbsp;Leyla was very happy because the party was so&hellip;&nbsp;</p>\n\n<ol>\n	<li>boring&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</li>\n	<li>noisy</li>\n	<li>interesting&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\n	<li>enjoyable</li>\n</ol>', '', NULL, '2020-06-23 05:08:00', '2020-06-23 05:08:00'),
(87, 1, '<p>69.&nbsp;&nbsp;Choose the correct preposition&nbsp;</p>\n\n<p>&nbsp; &nbsp; &nbsp; &nbsp;How much is rent &hellip; the house?&nbsp;</p>', '', NULL, '2020-06-23 05:08:41', '2020-06-23 05:08:41'),
(88, 1, '<p>70.&nbsp;Choose the correct variant.&nbsp;&nbsp;</p>\n\n<p>The teacher scolded the student for his &hellip;&nbsp;&nbsp;</p>\n\n<ol>\n	<li>laziness</li>\n	<li>rudeness</li>\n	<li>attention</li>\n	<li>diligence</li>\n</ol>', '', NULL, '2020-06-23 05:09:55', '2020-06-23 05:09:55'),
(89, 1, '<p>71.&nbsp;Choose the correct variant.</p>\n\n<p>&nbsp; &nbsp; &nbsp; All the people were made &hellip; the place at once.&nbsp;&nbsp;</p>\n\n<p>&nbsp;</p>', '', NULL, '2020-06-23 05:10:41', '2020-06-23 05:10:41'),
(90, 1, '<p>72.&nbsp;Choose&nbsp;correct variant.</p>\n\n<p>&nbsp; &nbsp; &nbsp; It was&hellip; fault, not&hellip; .&nbsp;</p>', '', NULL, '2020-06-23 05:11:44', '2020-06-23 05:11:44'),
(91, 1, '<p>73. Choose the correct variant.&nbsp;&nbsp;</p>\n\n<p>If you go&hellip; London &hellip; business, try to see all the places of interest there.&nbsp;&nbsp;</p>', '', NULL, '2020-06-23 05:12:30', '2020-06-23 05:12:30'),
(92, 1, '<p>74.&nbsp;Choose the correct form of the verb.</p>\n\n<p>The girl had her hair &hellip; at the hair &ndash;&nbsp;dresser&rsquo;s&nbsp;</p>', '', NULL, '2020-06-23 05:13:03', '2020-06-23 05:13:03'),
(93, 1, '<p>75. Choose the correct form of the verb.</p>\n\n<p>Every time when&nbsp;Jane&nbsp; was&nbsp;ill, her mother made her&hellip; medicine.&nbsp;</p>', '', NULL, '2020-06-23 05:13:37', '2020-06-23 05:13:37'),
(94, 1, '<p>76.&nbsp;Choose the correct variant.</p>\n\n<p>&nbsp;&nbsp;I&nbsp; think&nbsp;he is&hellip; talented man I have ever met.&nbsp;</p>', '', NULL, '2020-06-23 05:14:18', '2020-06-23 05:14:18'),
(95, 1, '<p>77.&nbsp;&nbsp;Choose the correct article.&nbsp;</p>\n\n<p>&hellip;&nbsp;quickest&nbsp;way to get there is by &hellip; car.&nbsp;</p>', '', NULL, '2020-06-23 05:14:54', '2020-06-23 05:14:54'),
(96, 1, '<p>78. Choose the compound nouns.&nbsp;</p>\n\n<ol>\n	<li>movement&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\n	<li>&nbsp;cherry&nbsp;&ndash; tree&nbsp;&nbsp;</li>\n	<li>demonstration&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\n	<li>pen&nbsp;&ndash; name&nbsp;</li>\n	<li>snowman&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\n	<li>Leadership</li>\n</ol>', '', NULL, '2020-06-23 05:15:51', '2020-06-23 05:15:51'),
(97, 1, '<p>79.&nbsp;Complete the sentence.&nbsp;</p>\n\n<p>People who go&nbsp;in for sports are stronger than those&hellip;&nbsp;&nbsp;</p>', '', NULL, '2020-06-23 05:16:38', '2020-06-23 05:16:38'),
(98, 1, '<p>80. Choose the correct tense form.&nbsp;</p>\n\n<p>If they&hellip; the train, they will arrive in time.&nbsp;</p>', '', NULL, '2020-06-23 05:17:30', '2020-06-23 05:17:30'),
(99, 1, '<p>81.&nbsp;Choose the correct variant.&nbsp;&nbsp;</p>\n\n<p>&nbsp;&nbsp;Let the judge &hellip; the issue.&nbsp;</p>', '', NULL, '2020-06-23 05:18:05', '2020-06-23 05:18:05'),
(100, 1, '<p>82.&nbsp;&nbsp;Choose the correct translation.&nbsp;</p>\n\n<p>He&ccedil; kəs m&uuml;əllimin sualına cavab verə bilmədi:&nbsp;</p>', '', NULL, '2020-06-23 05:18:42', '2020-06-23 05:18:42'),
(101, 1, '<p>83.&nbsp;&nbsp;Choose the irregular verbs.&nbsp;</p>\n\n<ol>\n	<li>. To cut&nbsp;&nbsp;&nbsp;&nbsp;</li>\n	<li>. To write&nbsp;&nbsp;&nbsp;</li>\n	<li>&nbsp;To send&nbsp;&nbsp;&nbsp;&nbsp;</li>\n	<li>To open&nbsp;&nbsp;&nbsp;</li>\n	<li>&nbsp;To translate&nbsp;&nbsp;&nbsp;&nbsp;</li>\n	<li>To walk&nbsp;</li>\n	<li>To watch&nbsp;</li>\n	<li>To make</li>\n</ol>', '', NULL, '2020-06-23 05:19:45', '2020-06-23 05:19:45'),
(102, 1, '<p>84. Choose the correct variant&nbsp;</p>\n\n<p>Look at &hellip; potter over there&nbsp;</p>', '', NULL, '2020-06-23 05:20:13', '2020-06-23 05:20:13'),
(103, 1, '<p>85.&nbsp;Choose the correct variant.&nbsp;</p>\n\n<ol>\n	<li>&nbsp;women&rsquo; rights</li>\n	<li>the&nbsp;children&rsquo;s room&nbsp;</li>\n	<li>Ben and Tom&rsquo;s eyes</li>\n	<li>the&nbsp;drivers&rsquo;&nbsp;licences&nbsp;</li>\n	<li>&nbsp;the&nbsp;hostess&rsquo;s welcome</li>\n</ol>', '', NULL, '2020-06-23 05:21:07', '2020-06-23 05:21:07'),
(104, 1, '<p style=\"margin-left:280px\"><strong>&nbsp;&nbsp;&nbsp;READ THE PASSAGE&nbsp;AND&nbsp;&nbsp;ANSWER&nbsp;QUESTIONS 26-30&nbsp;</strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;<em>April 1st&nbsp;is April&nbsp;Fool&rsquo;s&nbsp;Day,&nbsp;or a holiday of jokes.&nbsp;This&nbsp;day&nbsp; is&nbsp;celebrated not only in Great Britain and the USA, but also in many countries.&nbsp;This very old tradition dates from the Middle Ages when servants become masters for one day of the year.&nbsp;They&nbsp;gave orders to their masters, and their masters had to obey them.&nbsp;But nowadays April&nbsp;Fool&rsquo;s&nbsp;Day is quite different.&nbsp;Now the first day of April is&nbsp;considered&nbsp;&nbsp;&nbsp;to be the day when you can play jokes on your friends.&nbsp;</em></p>\n\n<p>86.The passage is mainly about&hellip; .</p>', '', NULL, '2020-06-23 05:22:44', '2020-06-23 05:22:44'),
(105, 1, '<p>87.&nbsp;&nbsp;The underlined word&nbsp;&ldquo;they&rdquo; refers to&hellip;&nbsp;&nbsp;</p>\n\n<p>&nbsp;</p>', '', NULL, '2020-06-23 05:23:17', '2020-06-23 05:23:17'),
(106, 1, '<p>88.&nbsp;Which&nbsp; word&nbsp;in the passage means &ldquo;a custom or way of doing something that has existed for a long&nbsp;time&rdquo;?&nbsp;&nbsp;</p>', '', NULL, '2020-06-23 05:24:13', '2020-06-23 05:24:13'),
(107, 1, '<p>89.&nbsp;&nbsp;Choose the correct variant</p>\n\n<ol>\n	<li>April&nbsp;Fool&rsquo;s&nbsp; Day&nbsp;appeared not very long ago.&nbsp;</li>\n	<li>On&nbsp; April&nbsp;Fool&rsquo;s&nbsp; Day masters had no right to order their servants.&nbsp;</li>\n	<li>April&nbsp;Fool&rsquo;s&nbsp; Day&nbsp;isn&rsquo;t celebrated in Great Britain .&nbsp;</li>\n	<li>Today April&nbsp;Fool&rsquo;s&nbsp; Day&nbsp;is a day of making jokes.&nbsp;</li>\n</ol>', '', NULL, '2020-06-23 05:24:54', '2020-06-23 05:24:54'),
(108, 1, '<p>90.&nbsp;Which question has no answer in the passage?&nbsp;</p>\n\n<p>&nbsp;</p>', '', NULL, '2020-06-23 05:25:28', '2020-06-23 05:25:28');

-- --------------------------------------------------------

--
-- Table structure for table `questions_options`
--

CREATE TABLE `questions_options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED DEFAULT NULL,
  `option` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `correct` tinyint(4) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions_options`
--

INSERT INTO `questions_options` (`id`, `question_id`, `option`, `correct`, `created_at`, `updated_at`) VALUES
(1, 1, '<p>m&uuml;btəda,&nbsp;xəbər, tamamlıq, m&uuml;btəda, xəbər, tamamlıq&nbsp;&nbsp;</p>', 1, '2020-06-22 17:57:26', '2020-06-22 17:57:26'),
(2, 1, '<p>m&uuml;btəda,&nbsp;xəbər, zərflik, m&uuml;btəda, xəbər, tamamlıq&nbsp;</p>', 0, '2020-06-22 17:57:26', '2020-06-22 17:57:26'),
(3, 1, '<p>m&uuml;btəda, m&uuml;btəda,&nbsp;xəbər, tamamlıq, m&uuml;btəda, xəbər, tamamlıq&nbsp;</p>', 0, '2020-06-22 17:57:26', '2020-06-22 17:57:26'),
(4, 1, '<p>m&uuml;btəda,&nbsp;xəbər, tamamlıq, m&uuml;btəda, xəbər&nbsp;</p>', 0, '2020-06-22 17:57:26', '2020-06-22 17:57:26'),
(5, 1, '<p>m&uuml;btəda, xəbər, m&uuml;btəda, xəbər, tamamlıq&nbsp;</p>', 0, '2020-06-22 17:57:26', '2020-06-22 17:57:26'),
(6, 2, '<p>orfoqrafiya qaydalarının tənzimlənməsi&nbsp;</p>', 0, '2020-06-22 18:04:30', '2020-06-22 18:04:30'),
(7, 2, '<p>əlifbanın tərtibi&nbsp;</p>', 0, '2020-06-22 18:04:30', '2020-06-22 18:04:30'),
(8, 2, '<p>intonasiya&nbsp;</p>', 0, '2020-06-22 18:04:30', '2020-06-22 18:04:30'),
(9, 2, '<p>s&ouml;z&uuml;n l&uuml;ğəvi mənası&nbsp;</p>', 1, '2020-06-22 18:04:30', '2020-06-22 18:04:30'),
(10, 2, '<p>fonem sisteminin m&uuml;əyyənləşdirilməsi&nbsp;&nbsp;</p>', 0, '2020-06-22 18:04:30', '2020-06-22 18:04:30'),
(11, 3, '<p>Şəkil&ccedil;iləşmiş forması olur.&nbsp;</p>', 0, '2020-06-22 18:05:27', '2020-06-22 18:05:27'),
(12, 3, '<p>Feilin təsriflənməyən formaları ilə işlənir.&nbsp;</p>', 1, '2020-06-22 18:05:27', '2020-06-22 18:05:27'),
(13, 3, '<p>İndiki zaman feillərinə artırılır.&nbsp;</p>', 0, '2020-06-22 18:05:27', '2020-06-22 18:05:27'),
(14, 3, '<p>Qeyri-qəti gələcək zamanda işlənmiş feillərə artırılır.&nbsp;</p>', 0, '2020-06-22 18:05:27', '2020-06-22 18:05:27'),
(15, 3, '<p>Qəti gələcək zamanda işlənmiş feillərə artırılır.&nbsp;</p>', 0, '2020-06-22 18:05:27', '2020-06-22 18:05:27'),
(21, 5, '<p>karusel</p>', 0, '2020-06-22 18:10:04', '2020-06-22 18:10:04'),
(22, 5, '<p>yer</p>', 1, '2020-06-22 18:10:04', '2020-06-22 18:10:04'),
(23, 5, '<p>Tazıların&nbsp;</p>', 0, '2020-06-22 18:10:04', '2020-06-22 18:10:04'),
(24, 5, '<p>Dovşanlar&nbsp;</p>', 0, '2020-06-22 18:10:04', '2020-06-22 18:10:04'),
(25, 5, '<p>D&uuml;nya</p>', 0, '2020-06-22 18:10:04', '2020-06-22 18:10:04'),
(26, 6, '<p>Hara dedilərsə ora da getdik&nbsp;</p>', 1, '2020-06-22 18:12:24', '2020-06-22 18:12:24'),
(27, 6, '<p>Kimsiniz&nbsp;hara&nbsp;gedirsiniz&nbsp;</p>', 0, '2020-06-22 18:12:24', '2020-06-22 18:12:24'),
(28, 6, '<p>Kəndə nə zaman gedəcəksən&nbsp;</p>', 0, '2020-06-22 18:12:24', '2020-06-22 18:12:24'),
(29, 6, '<p>Axı siz ondan nə istəyirsiniz&nbsp;</p>', 0, '2020-06-22 18:12:24', '2020-06-22 18:12:24'),
(30, 6, '<p>Bu işləri g&ouml;rməkdə onun məqsədi nədir&nbsp;</p>', 0, '2020-06-22 18:12:24', '2020-06-22 18:12:24'),
(31, 7, '<p>1, 3&nbsp;</p>', 1, '2020-06-22 18:13:34', '2020-06-22 18:13:34'),
(32, 7, '<p>2, 3&nbsp;</p>', 0, '2020-06-22 18:13:34', '2020-06-22 18:13:34'),
(33, 7, '<p>1, 4&nbsp;&nbsp;</p>', 0, '2020-06-22 18:13:34', '2020-06-22 18:13:34'),
(34, 7, '<p>2, 4</p>', 0, '2020-06-22 18:13:34', '2020-06-22 18:13:34'),
(35, 7, '<p>1, 2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-22 18:13:34', '2020-06-22 18:13:34'),
(36, 8, '<p>3, 3, 2&nbsp;</p>', 1, '2020-06-22 18:19:10', '2020-06-22 18:19:10'),
(37, 8, '<p>5, 3, 2</p>', 0, '2020-06-22 18:19:10', '2020-06-22 18:19:10'),
(38, 8, '<p>3, 3, 1&nbsp;</p>', 0, '2020-06-22 18:19:10', '2020-06-22 18:19:10'),
(39, 8, '<p>4, 3, 1&nbsp;&nbsp;</p>', 0, '2020-06-22 18:19:10', '2020-06-22 18:19:10'),
(40, 8, '<p>4, 3, 2&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-22 18:19:10', '2020-06-22 18:19:10'),
(41, 9, '<p>və, ilə, ancaq&nbsp;</p>', 0, '2020-06-22 18:21:56', '2020-06-22 18:21:56'),
(42, 9, '<p>ilə, ancaq, hətta&nbsp;&nbsp;</p>', 1, '2020-06-22 18:21:56', '2020-06-22 18:21:56'),
(43, 9, '<p>&nbsp;gah, isə, hətta&nbsp;</p>', 0, '2020-06-22 18:21:56', '2020-06-22 18:21:56'),
(44, 9, '<p>ona g&ouml;rə ki, ancaq, ilə</p>', 0, '2020-06-22 18:21:56', '2020-06-22 18:21:56'),
(45, 9, '<p>hər&ccedil;ənd, belə ki, istər</p>', 0, '2020-06-22 18:21:56', '2020-06-22 18:21:56'),
(46, 10, '<p>Həmcins &uuml;zvləri bağlayır.</p>', 0, '2020-06-22 18:22:43', '2020-06-22 18:22:43'),
(47, 10, '<p>və, ilə, -la2&nbsp;birləşdirmə bağlayıcılarıdır.&nbsp;</p>', 0, '2020-06-22 18:22:43', '2020-06-22 18:22:43'),
(48, 10, '<p>&ldquo;&nbsp;İlə&rdquo; həmcins &uuml;zvlərin hamısını bağlayır.</p>', 1, '2020-06-22 18:22:43', '2020-06-22 18:22:43'),
(49, 10, '<p>Tabesiz m&uuml;rəkkəb c&uuml;mlənin tərəflərini bağlayır.</p>', 0, '2020-06-22 18:22:43', '2020-06-22 18:22:43'),
(50, 10, '<p>&ldquo;Fəqət&rdquo; qarşılaşdırma bağlayıcısıdır.</p>', 0, '2020-06-22 18:22:43', '2020-06-22 18:22:43'),
(51, 11, '<p>quruluq, qurulmaq, qurucu</p>', 0, '2020-06-22 18:23:17', '2020-06-22 18:23:17'),
(52, 11, '<p>sağıcı, sağlıq, sağalmaq&nbsp;</p>', 0, '2020-06-22 18:23:17', '2020-06-22 18:23:17'),
(53, 11, '<p>ovlaq, ovdakı, ovla&nbsp;</p>', 1, '2020-06-22 18:23:17', '2020-06-22 18:23:17'),
(54, 11, '<p>yara, yarıq, yaramaq</p>', 0, '2020-06-22 18:23:17', '2020-06-22 18:23:17'),
(55, 11, '<p>g&uuml;ll&uuml;k, g&uuml;l&uuml;ş, g&uuml;ldan&nbsp;</p>', 0, '2020-06-22 18:23:17', '2020-06-22 18:23:17'),
(56, 12, '<p>Artıq d&uuml;şmən məhv edilmişdi.</p>', 0, '2020-06-22 18:24:10', '2020-06-22 18:24:10'),
(57, 12, '<p>Dahi alim L&uuml;tvi Zadə 2014-c&uuml; ildə Kaliforniyada rəhmətə getmişdir.&nbsp;</p>', 0, '2020-06-22 18:24:10', '2020-06-22 18:24:10'),
(58, 12, '<p>Səhra heyvanları susuzluqdan &ouml;l&uuml;rd&uuml;lər.</p>', 0, '2020-06-22 18:24:10', '2020-06-22 18:24:10'),
(59, 12, '<p>Yağış cansız&nbsp;təbiəti diriltmişdi.</p>', 1, '2020-06-22 18:24:10', '2020-06-22 18:24:10'),
(60, 12, '<p>Nənə Cavanşirin əlindən cana gəlmişdi.</p>', 0, '2020-06-22 18:24:10', '2020-06-22 18:24:10'),
(61, 13, '<p>1b&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2 c d e&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3 a</p>', 1, '2020-06-22 18:25:09', '2020-06-22 18:25:09'),
(62, 13, '<p>1b d&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2 a c e&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-22 18:25:09', '2020-06-22 18:25:09'),
(63, 13, '<p>1b&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2 a c&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3 d</p>', 0, '2020-06-22 18:25:09', '2020-06-22 18:25:09'),
(64, 13, '<p>1d&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2 e&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3 a</p>', 0, '2020-06-22 18:25:09', '2020-06-22 18:25:09'),
(65, 13, '<p>1d&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2 b e&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3 a&nbsp;</p>', 0, '2020-06-22 18:25:09', '2020-06-22 18:25:09'),
(66, 14, '<p>Sızlayır əhvalıma ş&uuml;bhə qədər tarım mənim...(Şəhriyar)</p>', 0, '2020-06-22 18:25:49', '2020-06-22 18:25:49'),
(67, 14, '<p>De, kim sevər səni məncə?</p>', 0, '2020-06-22 18:25:49', '2020-06-22 18:25:49'),
(68, 14, '<p>Sənə doğru getdikcə itirdim səni.</p>', 0, '2020-06-22 18:25:49', '2020-06-22 18:25:49'),
(69, 14, '<p>Xocalı faciəsinə aid materialların &ccedil;oxu &Ccedil;ingiz Mustafayevə məxsusdur.</p>', 0, '2020-06-22 18:25:49', '2020-06-22 18:25:49'),
(70, 14, '<p>Artıq məktəbəqədər hazırlıq kurslarına qəbul başlamışdı.&nbsp;</p>', 1, '2020-06-22 18:25:49', '2020-06-22 18:25:49'),
(71, 15, '<p>kitabxana</p>', 0, '2020-06-22 18:26:16', '2020-06-22 18:26:16'),
(72, 15, '<p>əməkdar</p>', 0, '2020-06-22 18:26:16', '2020-06-22 18:26:16'),
(73, 15, '<p>zəhmətkeş</p>', 1, '2020-06-22 18:26:16', '2020-06-22 18:26:16'),
(74, 15, '<p>aşkar</p>', 0, '2020-06-22 18:26:16', '2020-06-22 18:26:16'),
(75, 15, '<p>g&uuml;ldan&nbsp;</p>', 0, '2020-06-22 18:26:16', '2020-06-22 18:26:16'),
(76, 16, '<p>fayda-qazanc-mənfəət</p>', 0, '2020-06-22 18:27:12', '2020-06-22 18:27:12'),
(77, 16, '<p>zavallı-yazıq-bi&ccedil;arə</p>', 1, '2020-06-22 18:27:12', '2020-06-22 18:27:12'),
(78, 16, '<p>&uuml;rək-qəlb-k&ouml;n&uuml;l</p>', 0, '2020-06-22 18:27:12', '2020-06-22 18:27:12'),
(79, 16, '<p>zabitə-n&uuml;fuz-ciddiyyət&nbsp;</p>', 0, '2020-06-22 18:27:12', '2020-06-22 18:27:12'),
(80, 16, '<p>təcr&uuml;bə-sınaq-eksperiment&nbsp;</p>', 0, '2020-06-22 18:27:12', '2020-06-22 18:27:12'),
(81, 17, '<p>1, 6</p>', 1, '2020-06-22 18:27:59', '2020-06-22 18:27:59'),
(82, 17, '<p>1, 3, 4</p>', 0, '2020-06-22 18:27:59', '2020-06-22 18:27:59'),
(83, 17, '<p>1, 5&nbsp;</p>', 0, '2020-06-22 18:27:59', '2020-06-22 18:27:59'),
(84, 17, '<p>2, 4, 5&nbsp;</p>', 0, '2020-06-22 18:27:59', '2020-06-22 18:27:59'),
(85, 17, '<p>3, 4, 6&nbsp;</p>', 0, '2020-06-22 18:27:59', '2020-06-22 18:27:59'),
(86, 18, '<p>Əlifba hərflərin m&uuml;əyyən sıra ilə d&uuml;z&uuml;l&uuml;ş&uuml;d&uuml;r.&nbsp;</p>', 0, '2020-06-22 18:28:31', '2020-06-22 18:28:31'),
(87, 18, '<p>L&uuml;ğətlər əlifba prinsipinə əsasən d&uuml;z&uuml;l&uuml;r.</p>', 0, '2020-06-22 18:28:31', '2020-06-22 18:28:31'),
(88, 18, '<p>90-cı illərin əvvəllərindən xalqımız latın əlifbasından istifadə edir.&nbsp;&nbsp;</p>', 0, '2020-06-22 18:28:31', '2020-06-22 18:28:31'),
(89, 18, '<p>&nbsp;Əlifbadakı hər bir hərfin &ouml;z adı var: &ldquo;t&rdquo; hərfi, &ldquo;s&rdquo; hərfi&nbsp;</p>', 1, '2020-06-22 18:28:31', '2020-06-22 18:28:31'),
(90, 18, '<p>Əlifba hərflər sistemidir.&nbsp;</p>', 0, '2020-06-22 18:28:31', '2020-06-22 18:28:31'),
(91, 19, '<p>s&ouml;z birləşmələrinin sayı</p>', 1, '2020-06-22 18:29:16', '2020-06-22 18:29:16'),
(92, 19, '<p>c&uuml;mlə &uuml;zvlərinin sayı</p>', 0, '2020-06-22 18:29:16', '2020-06-22 18:29:16'),
(93, 19, '<p>həmcins &uuml;zvlərin sayı</p>', 0, '2020-06-22 18:29:17', '2020-06-22 18:29:17'),
(94, 19, '<p>c&uuml;mlə &uuml;zvlərinin s&ouml;z sırasına uyğunluğu</p>', 0, '2020-06-22 18:29:17', '2020-06-22 18:29:17'),
(95, 19, '<p>sintaktik təhlil sxemi&nbsp;</p>', 0, '2020-06-22 18:29:17', '2020-06-22 18:29:17'),
(96, 20, '<p>alın&nbsp;&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-22 18:32:23', '2020-06-22 18:32:23'),
(97, 20, '<p>qayın</p>', 0, '2020-06-22 18:32:23', '2020-06-22 18:32:23'),
(98, 20, '<p>m&ouml;h&uuml;r</p>', 1, '2020-06-22 18:32:23', '2020-06-22 18:32:23'),
(99, 20, '<p>boyun</p>', 0, '2020-06-22 18:32:23', '2020-06-22 18:32:23'),
(100, 20, '<p>ovuc</p>', 0, '2020-06-22 18:32:23', '2020-06-22 18:32:23'),
(101, 21, '<p>Səhər getdi.</p>', 0, '2020-06-22 18:33:30', '2020-06-22 18:33:30'),
(102, 21, '<p>İrəliyə getdi.</p>', 0, '2020-06-22 18:33:30', '2020-06-22 18:33:30'),
(103, 21, '<p>Axşam getdi.</p>', 0, '2020-06-22 18:33:30', '2020-06-22 18:33:30'),
(104, 21, '<p>Şəhərə getdi.</p>', 1, '2020-06-22 18:33:30', '2020-06-22 18:33:30'),
(105, 21, '<p>Əvvəl getdi.</p>', 0, '2020-06-22 18:33:30', '2020-06-22 18:33:30'),
(106, 22, '<p>&Uuml;mumi qrammatik mənasına g&ouml;rə yaranıb.</p>', 0, '2020-06-22 18:34:40', '2020-06-22 18:34:40'),
(107, 22, '<p>Leksik və qrammatik mənaya malikdir.</p>', 0, '2020-06-22 18:34:40', '2020-06-22 18:34:40'),
(108, 22, '<p>Adlıq halda işlənərkən c&uuml;mlədə yalnız m&uuml;btəda&nbsp;olur.&nbsp;</p>', 0, '2020-06-22 18:34:40', '2020-06-22 18:34:40'),
(109, 22, '<p>Yiyəlik halda olarkən başqa s&ouml;zlərlə birlikdə&nbsp;m&uuml;xtəlif c&uuml;mlə &uuml;zv&uuml; olur.&nbsp;</p>', 0, '2020-06-22 18:34:40', '2020-06-22 18:34:40'),
(110, 22, '<p>Təsirlik halda olarkən şəxs şəkil&ccedil;isi qəbul edə bilir.</p>', 1, '2020-06-22 18:34:40', '2020-06-22 18:34:40'),
(111, 23, '<p>necə?</p>', 0, '2020-06-22 18:35:09', '2020-06-22 18:35:09'),
(112, 23, '<p>hansı?&nbsp;</p>', 0, '2020-06-22 18:35:09', '2020-06-22 18:35:09'),
(113, 23, '<p>nə?</p>', 1, '2020-06-22 18:35:09', '2020-06-22 18:35:09'),
(114, 23, '<p>nə c&uuml;r?&nbsp;</p>', 0, '2020-06-22 18:35:09', '2020-06-22 18:35:09'),
(115, 23, '<p>ne&ccedil;ənci?</p>', 0, '2020-06-22 18:35:09', '2020-06-22 18:35:09'),
(116, 24, '<p>Sevinmək &ndash; şadlanmaq hər vaxt xoşbəxt olmaq &uuml;&ccedil;&uuml;n deyil.&nbsp;</p>', 0, '2020-06-22 18:35:47', '2020-06-22 18:35:47'),
(117, 24, '<p>Yalan, pis, xəyanət &ndash; hər şey ke&ccedil;mişdə qalır, amma he&ccedil; bir şey ke&ccedil;mir.&nbsp;&nbsp;</p>', 1, '2020-06-22 18:35:47', '2020-06-22 18:35:47'),
(118, 24, '<p>Bizi heyatın ağır y&uuml;k&uuml;ndən&nbsp;&ndash;&nbsp;iztirabından qurtaran tək s&ouml;z sevgidir.&nbsp;&nbsp;</p>', 0, '2020-06-22 18:35:47', '2020-06-22 18:35:47'),
(119, 24, '<p>&ldquo;M&uuml;vəffəqiyyətin d&ouml;rd şərti: bilmək, istəmək, cəsarət etmək və susmaqdır&rdquo;,&nbsp;&ndash;&nbsp;deyə&nbsp;Axel Munthe d&uuml;ş&uuml;n&uuml;rd&uuml;.&nbsp;</p>', 0, '2020-06-22 18:35:47', '2020-06-22 18:35:47'),
(120, 24, '<p>B&ouml;y&uuml;k adamların səhvləri g&uuml;nəş tutulmasına bənzər &ndash; oxşayar demişlər.&nbsp;</p>', 0, '2020-06-22 18:35:47', '2020-06-22 18:35:47'),
(121, 25, '<p>2, 4</p>', 1, '2020-06-22 18:36:49', '2020-06-22 18:36:49'),
(122, 25, '<p>2, 3</p>', 0, '2020-06-22 18:36:49', '2020-06-22 18:36:49'),
(123, 25, '<p>1, 4</p>', 0, '2020-06-22 18:36:49', '2020-06-22 18:36:49'),
(124, 25, '<p>1, 2</p>', 0, '2020-06-22 18:36:49', '2020-06-22 18:36:49'),
(125, 25, '<p>1, 3</p>', 0, '2020-06-22 18:36:49', '2020-06-22 18:36:49'),
(126, 26, '<p>təzələdi&nbsp;&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-22 18:38:04', '2020-06-22 18:38:04'),
(127, 26, '<p>darıxdı&nbsp;</p>', 0, '2020-06-22 18:38:04', '2020-06-22 18:38:04'),
(128, 26, '<p>fikrə getdi&nbsp;</p>', 1, '2020-06-22 18:38:04', '2020-06-22 18:38:04'),
(129, 26, '<p>kədərləndi&nbsp;&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-22 18:38:04', '2020-06-22 18:38:04'),
(130, 26, '<p>sevindi&nbsp;&nbsp;</p>', 0, '2020-06-22 18:38:04', '2020-06-22 18:38:04'),
(131, 27, '<p>can almaq&nbsp;&ndash; Əzrayıl Dəli Domrulun canını almağa gəldi.&nbsp;</p>', 0, '2020-06-22 18:38:34', '2020-06-22 18:38:34'),
(132, 27, '<p>təngə gəlmək&nbsp;&ndash; Hamı Cavanşirin hərəkətlərindən q&uuml;rur duyurdu.</p>', 0, '2020-06-22 18:38:34', '2020-06-22 18:38:34'),
(133, 27, '<p>can vermək &ndash; Quraqlıqdan sonra qəfil yağan yağış təbiətə can vermişdi.</p>', 1, '2020-06-22 18:38:34', '2020-06-22 18:38:34'),
(134, 27, '<p>g&ouml;zlərini yummaq&nbsp;&ndash; Artıq elə bir həddə &ccedil;atmışdı ki, hər şeyə g&ouml;z&nbsp; yumurdu.</p>', 0, '2020-06-22 18:38:34', '2020-06-22 18:38:34'),
(135, 27, '<p>can vermək- Qoca diqqətlə qarşısındakı insana baxaraq sakitcə can&nbsp; verirdi.</p>', 0, '2020-06-22 18:38:34', '2020-06-22 18:38:34'),
(136, 28, 'məktəb - li əlamət bildirir', 0, '2020-06-22 18:41:59', '2020-06-22 18:43:33'),
(137, 28, 'əkin -çi insan anlayışı bildirir', 0, '2020-06-22 18:41:59', '2020-06-22 18:44:15'),
(139, 28, 'şəhər -li insan anlayışı bildirir', 1, '2020-06-22 18:41:59', '2020-06-22 18:45:30'),
(140, 28, 'saat - saz məkan bildirir', 0, '2020-06-22 18:41:59', '2020-06-22 18:45:12'),
(141, 28, 'dairə - vi nisbə bildirir', 0, '2020-06-22 18:44:34', '2020-06-22 18:44:34'),
(142, 29, '<p>C&uuml;mlənin m&uuml;btədasıdır.&nbsp;</p>', 0, '2020-06-22 18:47:13', '2020-06-22 18:47:13'),
(143, 29, '<p>Qoşma ilə işlənib.</p>', 0, '2020-06-22 18:47:13', '2020-06-22 18:47:13'),
(144, 29, '<p>Adlıq haldadır.&nbsp;</p>', 0, '2020-06-22 18:47:13', '2020-06-22 18:47:13'),
(145, 29, '<p>Təsirlik haldadır.&nbsp;</p>', 1, '2020-06-22 18:47:13', '2020-06-22 18:47:13'),
(146, 29, '<p>Təsriflənən feilə aiddir.&nbsp;</p>', 0, '2020-06-22 18:47:13', '2020-06-22 18:47:13'),
(147, 30, '<p>1</p>', 1, '2020-06-22 18:47:46', '2020-06-22 18:47:46'),
(148, 30, '<p>2</p>', 0, '2020-06-22 18:47:46', '2020-06-22 18:47:46'),
(149, 30, '<p>3</p>', 0, '2020-06-22 18:47:46', '2020-06-22 18:47:46'),
(150, 30, '<p>4</p>', 0, '2020-06-22 18:47:46', '2020-06-22 18:47:46'),
(151, 30, '<p>5</p>', 0, '2020-06-22 18:47:46', '2020-06-22 18:47:46'),
(152, 31, '<p>1, 2</p>', 0, '2020-06-22 18:48:11', '2020-06-22 18:48:11'),
(153, 31, '<p>2, 3</p>', 0, '2020-06-22 18:48:11', '2020-06-22 18:48:11'),
(154, 31, '<p>1, 3&nbsp;</p>', 0, '2020-06-22 18:48:11', '2020-06-22 18:48:11'),
(155, 31, '<p>2, 4</p>', 0, '2020-06-22 18:48:11', '2020-06-22 18:48:11'),
(156, 31, '<p>1, 4</p>', 1, '2020-06-22 18:48:11', '2020-06-22 18:48:11'),
(157, 32, '<p>6</p>', 0, '2020-06-22 18:48:50', '2020-06-22 18:50:21'),
(158, 32, '<p>0</p>', 1, '2020-06-22 18:48:50', '2020-06-22 18:50:17'),
(159, 32, '<p>13</p>', 0, '2020-06-22 18:48:50', '2020-06-22 18:48:50'),
(160, 32, '<p>1</p>', 0, '2020-06-22 18:48:50', '2020-06-22 18:48:50'),
(161, 32, '<p>17</p>', 0, '2020-06-22 18:48:50', '2020-06-22 18:48:50'),
(162, 33, '<p>1</p>', 0, '2020-06-22 18:50:56', '2020-06-22 18:50:56'),
(163, 33, '<p>2</p>', 1, '2020-06-22 18:50:56', '2020-06-22 18:50:56'),
(164, 33, '<p>0</p>', 0, '2020-06-22 18:50:56', '2020-06-22 18:50:56'),
(165, 33, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAyCAYAAAC6VTBiAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAC/SURBVEhL7ZbbDYJAEEW3ABqwAAuwD5sxFmABhAb889MerIAO/LADe9B7IBMJYjLsfJnMSU54hFwW2Fy2JH9IIw/yLo+cWAsBV/mUL1kVYuwkQRnyIUO+CYdcJLPVfMiNTBKYzo1fJonBX/AsbW70kpJyQ0Art8PRuCWElnMHceH84nBVAtVIRYZLm2WG+3GWoLjDo2C9wguvwhY8ocfo5H7crWMewKhO0j0qAqZtZt6k691wt6UADH2hZBWlvAH68z3/Q2NesAAAAABJRU5ErkJggg==\" />&nbsp;&nbsp;</p>', 0, '2020-06-22 18:50:56', '2020-06-22 18:50:56'),
(166, 33, '<p>4</p>', 0, '2020-06-22 18:50:56', '2020-06-22 18:50:56'),
(167, 34, '<p>1</p>', 0, '2020-06-22 18:53:48', '2020-06-22 18:53:48'),
(168, 34, '<p>2</p>', 1, '2020-06-22 18:53:48', '2020-06-22 18:53:48'),
(169, 34, '<p>0</p>', 0, '2020-06-22 18:53:48', '2020-06-22 18:53:48'),
(170, 34, '<p>1/2</p>', 0, '2020-06-22 18:53:48', '2020-06-22 18:53:48'),
(171, 34, '<p>4</p>', 0, '2020-06-22 18:53:49', '2020-06-22 18:53:49'),
(172, 35, '<p>18</p>', 0, '2020-06-22 19:07:22', '2020-06-22 19:07:22'),
(173, 35, '<p>72</p>', 0, '2020-06-22 19:07:22', '2020-06-22 19:07:22'),
(174, 35, '<p>16</p>', 0, '2020-06-22 19:07:23', '2020-06-22 19:07:23'),
(175, 35, '<p>4</p>', 1, '2020-06-22 19:07:23', '2020-06-22 19:07:23'),
(176, 35, '<p>32</p>', 0, '2020-06-22 19:07:23', '2020-06-22 19:07:23'),
(177, 36, '<p>(-3;8;4)</p>', 0, '2020-06-22 19:10:16', '2020-06-22 19:10:16'),
(178, 36, '<p>(4;8,2;-3)</p>', 0, '2020-06-22 19:10:16', '2020-06-22 19:10:16'),
(179, 36, '<p>(4;8;-3)</p>', 1, '2020-06-22 19:10:16', '2020-06-22 19:10:16'),
(180, 36, '<p>(4;8;3)</p>', 0, '2020-06-22 19:10:16', '2020-06-22 19:10:16'),
(181, 36, '<p>(4;-8,2-2)</p>', 0, '2020-06-22 19:10:16', '2020-06-22 19:10:16'),
(182, 37, '<p>&plusmn;4</p>\n\n<p>&nbsp;</p>', 1, '2020-06-22 19:11:12', '2020-06-22 19:11:12'),
(183, 37, '<p>&plusmn;8</p>', 0, '2020-06-22 19:11:12', '2020-06-22 19:11:12'),
(184, 37, '<p>8</p>', 0, '2020-06-22 19:11:12', '2020-06-22 19:11:12'),
(185, 37, '<p>4</p>', 0, '2020-06-22 19:11:12', '2020-06-22 19:11:12'),
(186, 37, '<p>&plusmn;2</p>', 0, '2020-06-22 19:11:12', '2020-06-22 19:11:12'),
(187, 38, '3 < 2x/y < 8', 0, '2020-06-22 19:14:48', '2020-06-22 19:16:54'),
(188, 38, '4 < 2x/y < 8', 0, '2020-06-22 19:14:48', '2020-06-22 19:17:12'),
(189, 38, '3 < 2x/y < 4', 0, '2020-06-22 19:14:48', '2020-06-22 19:17:21'),
(190, 38, '6 < 2x/y < 8', 0, '2020-06-22 19:14:48', '2020-06-22 19:17:30'),
(191, 38, '4 < 2x/y < 12', 1, '2020-06-22 19:14:48', '2020-06-22 19:17:45'),
(192, 39, '<p>10,5 sm2&nbsp;</p>', 0, '2020-06-22 19:28:32', '2020-06-22 19:28:32'),
(193, 39, '<p>&nbsp; 21 sm2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 1, '2020-06-22 19:28:32', '2020-06-22 19:28:32'),
(194, 39, '<p>&nbsp; 10,25 sm2&nbsp;&nbsp;</p>', 0, '2020-06-22 19:28:32', '2020-06-22 19:28:32'),
(195, 39, '<p>&nbsp;12 sm2&nbsp;</p>', 0, '2020-06-22 19:28:32', '2020-06-22 19:28:32'),
(196, 39, '<p>11 sm2&nbsp;</p>', 0, '2020-06-22 19:28:32', '2020-06-22 19:28:32'),
(197, 40, '<p>50<sup>0</sup></p>', 1, '2020-06-22 19:31:38', '2020-06-22 19:31:38'),
(198, 40, '<p>100<sup>0</sup></p>', 0, '2020-06-22 19:31:38', '2020-06-22 19:31:38'),
(199, 40, '<p>96<sup>0</sup></p>', 0, '2020-06-22 19:31:38', '2020-06-22 19:31:38'),
(200, 40, '<p>25<sup>0</sup></p>', 0, '2020-06-22 19:31:38', '2020-06-22 19:31:38'),
(201, 40, '<p>48<sup>0</sup></p>', 0, '2020-06-22 19:31:38', '2020-06-22 19:31:38'),
(202, 41, '<p>50<sup>0</sup></p>', 1, '2020-06-22 19:32:56', '2020-06-22 19:32:56'),
(203, 41, '<p>100<sup>0</sup></p>', 0, '2020-06-22 19:32:56', '2020-06-22 19:32:56'),
(204, 41, '<p>96<sup>0</sup></p>', 0, '2020-06-22 19:32:56', '2020-06-22 19:32:56'),
(205, 41, '<p>25<sup>0</sup></p>', 0, '2020-06-22 19:32:56', '2020-06-22 19:32:56'),
(206, 41, '<p>48<sup>0</sup></p>', 0, '2020-06-22 19:32:56', '2020-06-22 19:32:56'),
(207, 42, '<p>5</p>', 0, '2020-06-22 19:36:09', '2020-06-22 19:36:09'),
(208, 42, '<p>6</p>', 1, '2020-06-22 19:36:09', '2020-06-22 19:36:09'),
(209, 42, '<p>3</p>', 0, '2020-06-22 19:36:09', '2020-06-22 19:36:09'),
(210, 42, '<p>12</p>', 0, '2020-06-22 19:36:09', '2020-06-22 19:36:09'),
(211, 42, '<p>18</p>', 0, '2020-06-22 19:36:09', '2020-06-22 19:36:09'),
(212, 43, '<p>5</p>', 0, '2020-06-22 19:39:41', '2020-06-22 19:39:41'),
(213, 43, '<p>6</p>', 1, '2020-06-22 19:39:41', '2020-06-22 19:39:41'),
(214, 43, '<p>3</p>', 0, '2020-06-22 19:39:41', '2020-06-22 19:39:41'),
(215, 43, '<p>12</p>', 0, '2020-06-22 19:39:41', '2020-06-22 19:39:41'),
(216, 43, '<p>18</p>', 0, '2020-06-22 19:39:41', '2020-06-22 19:39:41'),
(217, 44, '<p>7</p>', 0, '2020-06-22 20:12:19', '2020-06-22 20:12:19'),
(218, 44, '<p>9</p>', 0, '2020-06-22 20:12:19', '2020-06-22 20:12:19'),
(219, 44, '<p>35</p>', 1, '2020-06-22 20:12:19', '2020-06-22 20:12:19'),
(220, 44, '<p>10</p>', 0, '2020-06-22 20:12:19', '2020-06-22 20:12:19'),
(221, 44, '<p>15</p>', 0, '2020-06-22 20:12:19', '2020-06-22 20:12:19'),
(222, 45, '<p>64 sm<sup>2</sup></p>', 0, '2020-06-22 20:13:10', '2020-06-22 20:13:10'),
(223, 45, '<p>4 sm<sup>2</sup></p>', 0, '2020-06-22 20:13:10', '2020-06-22 20:13:10'),
(224, 45, '<p>16 sm<sup>2</sup></p>', 0, '2020-06-22 20:13:10', '2020-06-22 20:13:10'),
(225, 45, '<p>96 sm<sup>2</sup></p>', 1, '2020-06-22 20:13:10', '2020-06-22 20:13:10'),
(226, 45, '<p>216 sm<sup>2</sup></p>', 0, '2020-06-22 20:13:10', '2020-06-22 20:13:10'),
(227, 46, '<p>1</p>', 1, '2020-06-22 20:15:36', '2020-06-22 20:15:36'),
(228, 46, '<p>4</p>', 0, '2020-06-22 20:15:36', '2020-06-22 20:15:36'),
(229, 46, '<p>9</p>', 0, '2020-06-22 20:15:36', '2020-06-22 20:15:36'),
(230, 46, '<p>3</p>', 0, '2020-06-22 20:15:36', '2020-06-22 20:15:36'),
(231, 46, '<p>2</p>', 0, '2020-06-22 20:15:36', '2020-06-22 20:15:36'),
(232, 47, '<p>9</p>', 0, '2020-06-22 20:17:03', '2020-06-22 20:17:03'),
(233, 47, '<p>4</p>', 0, '2020-06-22 20:17:03', '2020-06-22 20:17:03'),
(234, 47, '<p>10</p>', 1, '2020-06-22 20:17:03', '2020-06-22 20:17:03'),
(235, 47, '<p>15</p>', 0, '2020-06-22 20:17:03', '2020-06-22 20:17:03'),
(236, 47, '<p>5</p>', 0, '2020-06-22 20:17:03', '2020-06-22 20:17:03'),
(237, 48, '<p>-8</p>', 0, '2020-06-22 20:17:33', '2020-06-22 20:17:33'),
(238, 48, '<p>1/4</p>', 0, '2020-06-22 20:17:33', '2020-06-22 20:17:33'),
(239, 48, '<p>1</p>', 0, '2020-06-22 20:17:33', '2020-06-22 20:17:33'),
(240, 48, '<p>2</p>', 0, '2020-06-22 20:17:33', '2020-06-22 20:17:33'),
(241, 48, '<p>8</p>', 1, '2020-06-22 20:17:33', '2020-06-22 20:17:33'),
(242, 49, '<p>4m</p>', 0, '2020-06-22 20:19:41', '2020-06-22 20:19:41'),
(243, 49, '<p>5m</p>', 0, '2020-06-22 20:19:41', '2020-06-22 20:19:41'),
(244, 49, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAcCAYAAADm63ZmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEqSURBVFhH7ZRRDcIwFEUrAAMIQAAGEED4JgjAAEEACjDAHyr4QQEO+MABHuAe4JGl6ba2S9gSepKbtd3yet/ba12hEOaRoJ/x8w1jGJwhKKZi+I9+Wkt3icAXaSylEjJ1lKyCaCtFMZNW76GbSDfpLI1YSMA3NZUsUdNCaoWNl+/hF7LBWGq1fFNUKboybeQEa6rSQeIPZEHV9h91/XUkxZoJg1G/rko1MxqdeQq+KSAx4pwk3ue0xIu5hDk/gGUc2hzq1g0OEt9k9xhlJoCVu7ohY9+AP6+jU+NTISoV6oHeTNEH188zhG8s1tROiuopM7CRaEzExdmWUZMpYmCA/gTmxI8+fTjHhGXPybNgTdj3viHABHeTvef0Zd9TqdSZ6p1BmioUnHPuCczvZhRHTy8pAAAAAElFTkSuQmCC\" style=\"height:15px; width:20px\" />m</p>', 0, '2020-06-22 20:19:41', '2020-06-22 20:19:41'),
(245, 49, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAcCAYAAACdz7SqAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADFSURBVEhL7ZPLDcIwEERdAA2kAAqgDZQCaAOlAPrgRhVUQwfpAWaCRlpZGyUW6xBEnvQOXkeerD9pY608ZxpK+IJTVOliiv8KXZTVbO0Nqk47GEoeeoA9tKEtDMUGEnYZ3plFnQjb5RXuYTh5KDtUjfIHqmytDSU7yI7vkHMP2MAivIXFWF2cIL8pOmMt6gV7NY+PLtZXQokNmRt6gcVnaslDLbxADDgOo/f4DENurzr0Qvk2NcfbG/ZOx0Krs3jgxi+R0gu1m1NqGtjjKQAAAABJRU5ErkJggg==\" style=\"height:19px; width:20px\" />m&nbsp;&nbsp;&nbsp;&nbsp;</p>', 1, '2020-06-22 20:19:41', '2020-06-22 20:19:41'),
(246, 49, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAcCAYAAADiHqZbAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEKSURBVFhH7ZThCcIwEEY7gAs4gAO4hjiAa4gDuIAu4D+ncBo3cAe9Bz04QtPcpYUWzIMPMbGXlzNN12jU8XVmERZbuMSiXSnR5Gr53649+9SQk6OezpGLJMxRwsNzyu0lH4nOEdYJsZW8JBSaIpdCrapOWe6Sg+QtqZHTrlhs1x6SnSQMbWZ3dG9OOWrqOEE09JduJLf+c245oC4d5MgwT33WccHudDcluZwA5MYtJwm/c51BdsRZU8bkdPEhwaGxHNR2yaVnwobzgfwQzFv0GQ9uuZTImbMyEbmrxH3mLFPkUngREOF6Ar6fJeELWInIgXYsJ8fdpvO8rVX3XC1jcqtgtWKNRpmu+wERLHDVRQ3LXQAAAABJRU5ErkJggg==\" style=\"height:20px; width:20px\" />m</p>', 0, '2020-06-22 20:19:41', '2020-06-22 20:19:41'),
(247, 50, '<p>-8</p>', 0, '2020-06-22 20:21:33', '2020-06-22 20:21:33'),
(248, 50, '<p>1/4</p>', 0, '2020-06-22 20:21:33', '2020-06-22 20:21:33'),
(249, 50, '<p>1</p>', 0, '2020-06-22 20:21:33', '2020-06-22 20:21:33'),
(250, 50, '<p>2</p>', 0, '2020-06-22 20:21:33', '2020-06-22 20:21:33'),
(251, 50, '<p>8</p>', 1, '2020-06-22 20:21:33', '2020-06-22 20:21:33'),
(252, 51, '<p>-8</p>', 0, '2020-06-22 20:22:26', '2020-06-22 20:22:26'),
(253, 51, '<p>1/4</p>', 0, '2020-06-22 20:22:26', '2020-06-22 20:22:26'),
(254, 51, '<p>1</p>', 0, '2020-06-22 20:22:26', '2020-06-22 20:22:26'),
(255, 51, '<p>2</p>', 0, '2020-06-22 20:22:26', '2020-06-22 20:22:26'),
(256, 51, '<p>8</p>', 1, '2020-06-22 20:22:26', '2020-06-22 20:22:26'),
(257, 52, '<p>-8</p>', 0, '2020-06-22 20:23:17', '2020-06-22 20:23:17'),
(258, 52, '<p>1/4</p>', 0, '2020-06-22 20:23:17', '2020-06-22 20:23:17'),
(259, 52, '<p>1</p>', 0, '2020-06-22 20:23:17', '2020-06-22 20:23:17'),
(260, 52, '<p>2</p>', 0, '2020-06-22 20:23:17', '2020-06-22 20:23:17'),
(261, 52, '<p>8</p>', 1, '2020-06-22 20:23:17', '2020-06-22 20:23:17'),
(262, 53, '<p>-8</p>', 0, '2020-06-22 20:24:02', '2020-06-22 20:24:02'),
(263, 53, '<p>1/4</p>', 0, '2020-06-22 20:24:02', '2020-06-22 20:24:02'),
(264, 53, '<p>1</p>', 0, '2020-06-22 20:24:02', '2020-06-22 20:24:02'),
(265, 53, '<p>2</p>', 0, '2020-06-22 20:24:02', '2020-06-22 20:24:02'),
(266, 53, '<p>8</p>', 1, '2020-06-22 20:24:02', '2020-06-22 20:24:02'),
(267, 54, '<p>-8</p>', 0, '2020-06-22 20:25:34', '2020-06-22 20:25:34'),
(268, 54, '<p>1/4</p>', 0, '2020-06-22 20:25:34', '2020-06-22 20:25:34'),
(269, 54, '<p>1</p>', 0, '2020-06-22 20:25:34', '2020-06-22 20:25:34'),
(270, 54, '<p>2</p>', 0, '2020-06-22 20:25:34', '2020-06-22 20:25:34'),
(271, 54, '<p>8</p>', 1, '2020-06-22 20:25:34', '2020-06-22 20:25:34'),
(272, 55, '<p>-8</p>', 0, '2020-06-22 20:25:56', '2020-06-22 20:25:56'),
(273, 55, '<p>1/4</p>', 0, '2020-06-22 20:25:56', '2020-06-22 20:25:56'),
(274, 55, '<p>1</p>', 0, '2020-06-22 20:25:56', '2020-06-22 20:25:56'),
(275, 55, '<p>2</p>', 0, '2020-06-22 20:25:56', '2020-06-22 20:25:56'),
(276, 55, '<p>8</p>', 1, '2020-06-22 20:25:56', '2020-06-22 20:25:56'),
(277, 56, '<p>4m</p>', 0, '2020-06-22 20:26:57', '2020-06-22 20:26:57'),
(278, 56, '<p>5m</p>', 0, '2020-06-22 20:26:57', '2020-06-22 20:26:57'),
(279, 56, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAcCAYAAADm63ZmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEqSURBVFhH7ZRRDcIwFEUrAAMIQAAGEED4JgjAAEEACjDAHyr4QQEO+MABHuAe4JGl6ba2S9gSepKbtd3yet/ba12hEOaRoJ/x8w1jGJwhKKZi+I9+Wkt3icAXaSylEjJ1lKyCaCtFMZNW76GbSDfpLI1YSMA3NZUsUdNCaoWNl+/hF7LBWGq1fFNUKboybeQEa6rSQeIPZEHV9h91/XUkxZoJg1G/rko1MxqdeQq+KSAx4pwk3ue0xIu5hDk/gGUc2hzq1g0OEt9k9xhlJoCVu7ohY9+AP6+jU+NTISoV6oHeTNEH188zhG8s1tROiuopM7CRaEzExdmWUZMpYmCA/gTmxI8+fTjHhGXPybNgTdj3viHABHeTvef0Zd9TqdSZ6p1BmioUnHPuCczvZhRHTy8pAAAAAElFTkSuQmCC\" />m&nbsp;&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-22 20:26:57', '2020-06-22 20:26:57'),
(280, 56, '<p>&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAcCAYAAACdz7SqAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADFSURBVEhL7ZPLDcIwEERdAA2kAAqgDZQCaAOlAPrgRhVUQwfpAWaCRlpZGyUW6xBEnvQOXkeerD9pY608ZxpK+IJTVOliiv8KXZTVbO0Nqk47GEoeeoA9tKEtDMUGEnYZ3plFnQjb5RXuYTh5KDtUjfIHqmytDSU7yI7vkHMP2MAivIXFWF2cIL8pOmMt6gV7NY+PLtZXQokNmRt6gcVnaslDLbxADDgOo/f4DENurzr0Qvk2NcfbG/ZOx0Krs3jgxi+R0gu1m1NqGtjjKQAAAABJRU5ErkJggg==\" />m&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 1, '2020-06-22 20:26:57', '2020-06-22 20:26:57'),
(281, 56, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAcCAYAAADiHqZbAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEKSURBVFhH7ZThCcIwEEY7gAs4gAO4hjiAa4gDuIAu4D+ncBo3cAe9Bz04QtPcpYUWzIMPMbGXlzNN12jU8XVmERZbuMSiXSnR5Gr53649+9SQk6OezpGLJMxRwsNzyu0lH4nOEdYJsZW8JBSaIpdCrapOWe6Sg+QtqZHTrlhs1x6SnSQMbWZ3dG9OOWrqOEE09JduJLf+c245oC4d5MgwT33WccHudDcluZwA5MYtJwm/c51BdsRZU8bkdPEhwaGxHNR2yaVnwobzgfwQzFv0GQ9uuZTImbMyEbmrxH3mLFPkUngREOF6Ar6fJeELWInIgXYsJ8fdpvO8rVX3XC1jcqtgtWKNRpmu+wERLHDVRQ3LXQAAAABJRU5ErkJggg==\" />m</p>', 0, '2020-06-22 20:26:57', '2020-06-22 20:26:57'),
(282, 57, '<p>4m</p>', 0, '2020-06-22 20:27:52', '2020-06-22 20:27:52'),
(283, 57, '<p>5m</p>', 0, '2020-06-22 20:27:52', '2020-06-22 20:27:52'),
(284, 57, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAcCAYAAADm63ZmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEqSURBVFhH7ZRRDcIwFEUrAAMIQAAGEED4JgjAAEEACjDAHyr4QQEO+MABHuAe4JGl6ba2S9gSepKbtd3yet/ba12hEOaRoJ/x8w1jGJwhKKZi+I9+Wkt3icAXaSylEjJ1lKyCaCtFMZNW76GbSDfpLI1YSMA3NZUsUdNCaoWNl+/hF7LBWGq1fFNUKboybeQEa6rSQeIPZEHV9h91/XUkxZoJg1G/rko1MxqdeQq+KSAx4pwk3ue0xIu5hDk/gGUc2hzq1g0OEt9k9xhlJoCVu7ohY9+AP6+jU+NTISoV6oHeTNEH188zhG8s1tROiuopM7CRaEzExdmWUZMpYmCA/gTmxI8+fTjHhGXPybNgTdj3viHABHeTvef0Zd9TqdSZ6p1BmioUnHPuCczvZhRHTy8pAAAAAElFTkSuQmCC\" style=\"height:19px; width:25px\" />m&nbsp;&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-22 20:27:52', '2020-06-22 20:27:52'),
(285, 57, '<p>&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAcCAYAAACdz7SqAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADFSURBVEhL7ZPLDcIwEERdAA2kAAqgDZQCaAOlAPrgRhVUQwfpAWaCRlpZGyUW6xBEnvQOXkeerD9pY608ZxpK+IJTVOliiv8KXZTVbO0Nqk47GEoeeoA9tKEtDMUGEnYZ3plFnQjb5RXuYTh5KDtUjfIHqmytDSU7yI7vkHMP2MAivIXFWF2cIL8pOmMt6gV7NY+PLtZXQokNmRt6gcVnaslDLbxADDgOo/f4DENurzr0Qvk2NcfbG/ZOx0Krs3jgxi+R0gu1m1NqGtjjKQAAAABJRU5ErkJggg==\" style=\"height:24px; width:25px\" />m&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 1, '2020-06-22 20:27:52', '2020-06-22 20:27:52'),
(286, 57, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAcCAYAAADiHqZbAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEKSURBVFhH7ZThCcIwEEY7gAs4gAO4hjiAa4gDuIAu4D+ncBo3cAe9Bz04QtPcpYUWzIMPMbGXlzNN12jU8XVmERZbuMSiXSnR5Gr53649+9SQk6OezpGLJMxRwsNzyu0lH4nOEdYJsZW8JBSaIpdCrapOWe6Sg+QtqZHTrlhs1x6SnSQMbWZ3dG9OOWrqOEE09JduJLf+c245oC4d5MgwT33WccHudDcluZwA5MYtJwm/c51BdsRZU8bkdPEhwaGxHNR2yaVnwobzgfwQzFv0GQ9uuZTImbMyEbmrxH3mLFPkUngREOF6Ar6fJeELWInIgXYsJ8fdpvO8rVX3XC1jcqtgtWKNRpmu+wERLHDVRQ3LXQAAAABJRU5ErkJggg==\" style=\"height:18px; width:25px\" />m</p>', 0, '2020-06-22 20:27:52', '2020-06-22 20:27:52'),
(287, 58, '<p>4m</p>', 0, '2020-06-22 20:28:21', '2020-06-22 20:28:21'),
(288, 58, '<p>5m</p>', 0, '2020-06-22 20:28:21', '2020-06-22 20:28:21'),
(289, 58, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAcCAYAAADm63ZmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEqSURBVFhH7ZRRDcIwFEUrAAMIQAAGEED4JgjAAEEACjDAHyr4QQEO+MABHuAe4JGl6ba2S9gSepKbtd3yet/ba12hEOaRoJ/x8w1jGJwhKKZi+I9+Wkt3icAXaSylEjJ1lKyCaCtFMZNW76GbSDfpLI1YSMA3NZUsUdNCaoWNl+/hF7LBWGq1fFNUKboybeQEa6rSQeIPZEHV9h91/XUkxZoJg1G/rko1MxqdeQq+KSAx4pwk3ue0xIu5hDk/gGUc2hzq1g0OEt9k9xhlJoCVu7ohY9+AP6+jU+NTISoV6oHeTNEH188zhG8s1tROiuopM7CRaEzExdmWUZMpYmCA/gTmxI8+fTjHhGXPybNgTdj3viHABHeTvef0Zd9TqdSZ6p1BmioUnHPuCczvZhRHTy8pAAAAAElFTkSuQmCC\" style=\"height:10px; width:1px\" />m&nbsp;&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-22 20:28:21', '2020-06-22 20:28:21'),
(290, 58, '<p>&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAcCAYAAACdz7SqAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADFSURBVEhL7ZPLDcIwEERdAA2kAAqgDZQCaAOlAPrgRhVUQwfpAWaCRlpZGyUW6xBEnvQOXkeerD9pY608ZxpK+IJTVOliiv8KXZTVbO0Nqk47GEoeeoA9tKEtDMUGEnYZ3plFnQjb5RXuYTh5KDtUjfIHqmytDSU7yI7vkHMP2MAivIXFWF2cIL8pOmMt6gV7NY+PLtZXQokNmRt6gcVnaslDLbxADDgOo/f4DENurzr0Qvk2NcfbG/ZOx0Krs3jgxi+R0gu1m1NqGtjjKQAAAABJRU5ErkJggg==\" style=\"height:24px; width:25px\" />m&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 1, '2020-06-22 20:28:21', '2020-06-22 20:28:21'),
(291, 58, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAcCAYAAADiHqZbAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEKSURBVFhH7ZThCcIwEEY7gAs4gAO4hjiAa4gDuIAu4D+ncBo3cAe9Bz04QtPcpYUWzIMPMbGXlzNN12jU8XVmERZbuMSiXSnR5Gr53649+9SQk6OezpGLJMxRwsNzyu0lH4nOEdYJsZW8JBSaIpdCrapOWe6Sg+QtqZHTrlhs1x6SnSQMbWZ3dG9OOWrqOEE09JduJLf+c245oC4d5MgwT33WccHudDcluZwA5MYtJwm/c51BdsRZU8bkdPEhwaGxHNR2yaVnwobzgfwQzFv0GQ9uuZTImbMyEbmrxH3mLFPkUngREOF6Ar6fJeELWInIgXYsJ8fdpvO8rVX3XC1jcqtgtWKNRpmu+wERLHDVRQ3LXQAAAABJRU5ErkJggg==\" style=\"height:18px; width:25px\" />m</p>', 0, '2020-06-22 20:28:21', '2020-06-22 20:28:21'),
(292, 59, '<p>4m</p>', 0, '2020-06-22 20:29:43', '2020-06-22 20:29:43'),
(293, 59, '<p>5m</p>', 0, '2020-06-22 20:29:43', '2020-06-22 20:29:43'),
(294, 59, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAcCAYAAADm63ZmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEqSURBVFhH7ZRRDcIwFEUrAAMIQAAGEED4JgjAAEEACjDAHyr4QQEO+MABHuAe4JGl6ba2S9gSepKbtd3yet/ba12hEOaRoJ/x8w1jGJwhKKZi+I9+Wkt3icAXaSylEjJ1lKyCaCtFMZNW76GbSDfpLI1YSMA3NZUsUdNCaoWNl+/hF7LBWGq1fFNUKboybeQEa6rSQeIPZEHV9h91/XUkxZoJg1G/rko1MxqdeQq+KSAx4pwk3ue0xIu5hDk/gGUc2hzq1g0OEt9k9xhlJoCVu7ohY9+AP6+jU+NTISoV6oHeTNEH188zhG8s1tROiuopM7CRaEzExdmWUZMpYmCA/gTmxI8+fTjHhGXPybNgTdj3viHABHeTvef0Zd9TqdSZ6p1BmioUnHPuCczvZhRHTy8pAAAAAElFTkSuQmCC\" style=\"height:30px; width:20px\" />m&nbsp;&nbsp;</p>', 0, '2020-06-22 20:29:43', '2020-06-22 20:29:43'),
(295, 59, '<p>&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAcCAYAAACdz7SqAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADFSURBVEhL7ZPLDcIwEERdAA2kAAqgDZQCaAOlAPrgRhVUQwfpAWaCRlpZGyUW6xBEnvQOXkeerD9pY608ZxpK+IJTVOliiv8KXZTVbO0Nqk47GEoeeoA9tKEtDMUGEnYZ3plFnQjb5RXuYTh5KDtUjfIHqmytDSU7yI7vkHMP2MAivIXFWF2cIL8pOmMt6gV7NY+PLtZXQokNmRt6gcVnaslDLbxADDgOo/f4DENurzr0Qvk2NcfbG/ZOx0Krs3jgxi+R0gu1m1NqGtjjKQAAAABJRU5ErkJggg==\" style=\"height:24px; width:25px\" />m&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 1, '2020-06-22 20:29:43', '2020-06-22 20:29:43'),
(296, 59, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAcCAYAAADiHqZbAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEKSURBVFhH7ZThCcIwEEY7gAs4gAO4hjiAa4gDuIAu4D+ncBo3cAe9Bz04QtPcpYUWzIMPMbGXlzNN12jU8XVmERZbuMSiXSnR5Gr53649+9SQk6OezpGLJMxRwsNzyu0lH4nOEdYJsZW8JBSaIpdCrapOWe6Sg+QtqZHTrlhs1x6SnSQMbWZ3dG9OOWrqOEE09JduJLf+c245oC4d5MgwT33WccHudDcluZwA5MYtJwm/c51BdsRZU8bkdPEhwaGxHNR2yaVnwobzgfwQzFv0GQ9uuZTImbMyEbmrxH3mLFPkUngREOF6Ar6fJeELWInIgXYsJ8fdpvO8rVX3XC1jcqtgtWKNRpmu+wERLHDVRQ3LXQAAAABJRU5ErkJggg==\" style=\"height:18px; width:25px\" />m</p>', 0, '2020-06-22 20:29:43', '2020-06-22 20:29:43'),
(297, 60, '<p>4m</p>', 0, '2020-06-22 20:31:24', '2020-06-22 20:31:24'),
(298, 60, '<p>5m</p>', 0, '2020-06-22 20:31:24', '2020-06-22 20:31:24'),
(299, 60, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAcCAYAAADm63ZmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEqSURBVFhH7ZRRDcIwFEUrAAMIQAAGEED4JgjAAEEACjDAHyr4QQEO+MABHuAe4JGl6ba2S9gSepKbtd3yet/ba12hEOaRoJ/x8w1jGJwhKKZi+I9+Wkt3icAXaSylEjJ1lKyCaCtFMZNW76GbSDfpLI1YSMA3NZUsUdNCaoWNl+/hF7LBWGq1fFNUKboybeQEa6rSQeIPZEHV9h91/XUkxZoJg1G/rko1MxqdeQq+KSAx4pwk3ue0xIu5hDk/gGUc2hzq1g0OEt9k9xhlJoCVu7ohY9+AP6+jU+NTISoV6oHeTNEH188zhG8s1tROiuopM7CRaEzExdmWUZMpYmCA/gTmxI8+fTjHhGXPybNgTdj3viHABHeTvef0Zd9TqdSZ6p1BmioUnHPuCczvZhRHTy8pAAAAAElFTkSuQmCC\" style=\"height:20px; width:4px\" />m&nbsp;&nbsp;</p>', 0, '2020-06-22 20:31:24', '2020-06-22 20:31:24'),
(300, 60, '<p>&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAcCAYAAACdz7SqAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADFSURBVEhL7ZPLDcIwEERdAA2kAAqgDZQCaAOlAPrgRhVUQwfpAWaCRlpZGyUW6xBEnvQOXkeerD9pY608ZxpK+IJTVOliiv8KXZTVbO0Nqk47GEoeeoA9tKEtDMUGEnYZ3plFnQjb5RXuYTh5KDtUjfIHqmytDSU7yI7vkHMP2MAivIXFWF2cIL8pOmMt6gV7NY+PLtZXQokNmRt6gcVnaslDLbxADDgOo/f4DENurzr0Qvk2NcfbG/ZOx0Krs3jgxi+R0gu1m1NqGtjjKQAAAABJRU5ErkJggg==\" style=\"height:20px; width:4px\" />m&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 1, '2020-06-22 20:31:24', '2020-06-22 20:31:24'),
(301, 60, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAcCAYAAADiHqZbAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEKSURBVFhH7ZThCcIwEEY7gAs4gAO4hjiAa4gDuIAu4D+ncBo3cAe9Bz04QtPcpYUWzIMPMbGXlzNN12jU8XVmERZbuMSiXSnR5Gr53649+9SQk6OezpGLJMxRwsNzyu0lH4nOEdYJsZW8JBSaIpdCrapOWe6Sg+QtqZHTrlhs1x6SnSQMbWZ3dG9OOWrqOEE09JduJLf+c245oC4d5MgwT33WccHudDcluZwA5MYtJwm/c51BdsRZU8bkdPEhwaGxHNR2yaVnwobzgfwQzFv0GQ9uuZTImbMyEbmrxH3mLFPkUngREOF6Ar6fJeELWInIgXYsJ8fdpvO8rVX3XC1jcqtgtWKNRpmu+wERLHDVRQ3LXQAAAABJRU5ErkJggg==\" style=\"height:20px; width:4px\" />m</p>', 0, '2020-06-22 20:31:24', '2020-06-22 20:31:24'),
(302, 61, '<p>4m</p>', 0, '2020-06-22 20:32:55', '2020-06-22 20:32:55'),
(303, 61, '<p>5m</p>', 0, '2020-06-22 20:32:55', '2020-06-22 20:32:55'),
(304, 61, '3 √5 m', 0, '2020-06-22 20:32:55', '2020-06-22 21:04:35'),
(305, 61, '√5', 1, '2020-06-22 20:32:55', '2020-06-22 21:04:44'),
(306, 61, '4√5', 0, '2020-06-22 20:32:55', '2020-06-22 21:04:52'),
(307, 62, '<p>50 q</p>', 0, '2020-06-22 20:44:21', '2020-06-22 20:44:21'),
(308, 62, '<p>300 q</p>', 1, '2020-06-22 20:44:21', '2020-06-22 20:44:21'),
(309, 62, '<p>200 q</p>', 0, '2020-06-22 20:44:21', '2020-06-22 20:44:21'),
(310, 62, '<p>100 q</p>', 0, '2020-06-22 20:44:21', '2020-06-22 20:44:21'),
(311, 62, '<p>25 q</p>', 0, '2020-06-22 20:44:21', '2020-06-22 20:44:21'),
(312, 63, '<p>121</p>', 0, '2020-06-22 20:44:54', '2020-06-22 20:44:54'),
(313, 63, '<p>111</p>', 0, '2020-06-22 20:44:54', '2020-06-22 20:44:54'),
(314, 63, '<p>112</p>', 0, '2020-06-22 20:44:54', '2020-06-22 20:44:54'),
(315, 63, '<p>219</p>', 0, '2020-06-22 20:44:54', '2020-06-22 20:44:54'),
(316, 63, '<p>129</p>', 1, '2020-06-22 20:44:54', '2020-06-22 20:44:54'),
(317, 64, '<p>7</p>', 0, '2020-06-22 20:45:58', '2020-06-22 20:45:58'),
(318, 64, '<p>8</p>', 0, '2020-06-22 20:45:58', '2020-06-22 20:45:58'),
(319, 64, '<p>78</p>', 0, '2020-06-22 20:45:58', '2020-06-22 20:45:58'),
(320, 64, '<p>56</p>', 1, '2020-06-22 20:45:58', '2020-06-22 20:45:58'),
(321, 64, '<p>911</p>', 0, '2020-06-22 20:45:58', '2020-06-22 20:45:58'),
(322, 65, '<p>3</p>', 1, '2020-06-22 20:47:01', '2020-06-22 20:47:01'),
(323, 65, '<p>7</p>', 0, '2020-06-22 20:47:01', '2020-06-22 20:47:01'),
(324, 65, '<p>12</p>', 0, '2020-06-22 20:47:01', '2020-06-22 20:47:01'),
(325, 65, '<p>6</p>', 0, '2020-06-22 20:47:01', '2020-06-22 20:47:01'),
(326, 65, '<p>126</p>', 0, '2020-06-22 20:47:01', '2020-06-22 20:47:01'),
(327, 66, '<p>0</p>', 0, '2020-06-22 20:47:34', '2020-06-22 20:47:34'),
(328, 66, '<p>1</p>', 0, '2020-06-22 20:47:34', '2020-06-22 20:47:34'),
(329, 66, '<p>2</p>', 0, '2020-06-22 20:47:34', '2020-06-22 20:47:34'),
(330, 66, '<p>4</p>', 0, '2020-06-22 20:47:34', '2020-06-22 20:47:34'),
(331, 66, '<p>5</p>', 1, '2020-06-22 20:47:34', '2020-06-22 20:47:34'),
(332, 67, '<p>20 sm</p>', 0, '2020-06-22 20:48:24', '2020-06-22 20:48:24'),
(333, 67, '<p>30 sm&nbsp;</p>', 1, '2020-06-22 20:48:24', '2020-06-22 20:48:24'),
(334, 67, '<p>24 sm</p>', 0, '2020-06-22 20:48:24', '2020-06-22 20:48:24'),
(335, 67, '<p>16 sm</p>', 0, '2020-06-22 20:48:24', '2020-06-22 20:48:24'),
(336, 67, '<p>40 sm</p>', 0, '2020-06-22 20:48:24', '2020-06-22 20:48:24'),
(337, 68, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMsAAAAjCAYAAAA67P7cAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATSSURBVHhe7ZzNjRQ9EIY3AAKAAAiAACAAxBl9d0gAcUYEgEiAG0ci4PARARlwIANyAD+z80KpVOVxz86Pu7ceyZput3/Kb5Xd7p7ZvSmKoiiKoiiKoihOyuOWXt4eFsVJIJ4e3R5uBwb0oaUHu7Pt86SlX/vEcXE8PS2Jp3f7z83wsaX7EjQsDP/vP+1xsZwRLYkr4msTbGowA7xo6fPt4Q6OySuWM6Ild5UvLW1iMb5vwfJ2nwTHNVmOY1RLP6lWCbfM7y3dly0YjDr4GnCH/9mS38rMyqiWxBdxNuW4uPV9aun3PmUTgoF9a2mGB7DXLfGQKHsjYbHzTUs/WrJOsvBWj/pqh3OLX+XO9byGrWi7RN9TTZbZtKR/xjXLovQXRODNlgamAUdvKxjoDJPlWUv/3R7u7EVYbxfH7H0VBJGD5RS1xacPPo7tQymLyrXHf0pm1JI8bJhuK8aE8JOCc4SxoswyAOzw3+9gp3eMiMYi/OTPxsgKR5BEC8iamVlL314XCtOoTb7jc4FQCGZFifI8rExfW5K9rEa6YwlfhmNbhn50K2flQdD3LfVAl8yuzMHZeDjPgmUJBEVPK4t8HfWb6UEQsXpzTXUetsTWRlsgba1tmUPMouVwXQpIEPGqpaFZdgIQhb0pnyITSqjO893Zv/J2dZCo9rbPdbvK4Cz20EA9zkkRXCc4SJk2md3K9/tizq09x7JksgDlo5U004PydoJxjfrkYT/PF+RpnJmGgrIzaUkfR9VFrF4lhECkXjokloWymSBRAMhR/hrC29WBdr0dapd8TSYruhYOj+oxNlbOTJ/MbgVb5OAofynUj7TKiLQ5pAflrb7g8+Qb37ZlRi3pg74yW0IozEPYpaA/tk9+dcmEAl3riSLHR/XlYN1pEJgthN2eZXAno28fNCKz+xwOtlA/GmuEtPF9KtAzPU41WcRMWtIHfdHnEBh8yR8rImz27WkmFOhaT5ReffLkIGzQXpuk/XePnlOyfjObFztpD8Eom7PUC0K/7RU9PU49WWAGLWFRXQY5+qOyEUeNiMW2KRIJMqFA13p94DycGJWhTe90PaxiexZkQm0vcbDq+PzIlmPAlkirCMoR0D1fR3qgpbfV5y2dLNLl2lpSd3iyXPrnyn6iIDJ7YxmbCQVyCI70LyZoU+1GzgXyaZd6foGgbrbqCq5lZXp2068NUo1jNLB6YHfUZwT9RX0e0oM6Xk+ft3RMs2hJH0OThWcUW4iOeRt2LrRi+WQHj/jR6iFwYtSGdZzEpl1tJeyXVxIYe9Sv/3KLNnCm3vaoTmZXz8G6prdzlLF93YXRyWJ1ZUy2754e6Mc1a6/K2zy1T77aEDNrOVRfnfqgQ/xzwJ3A96VkRZGQvZWCB0T21Krvv0MBzm0Z++BKHywKT1tSGf/MgnjYofpc1+tqD7aqHCkSH72zvu7C6GSRv6PvpHp60LbGxTjlH+UxVtrmU3l+wsysJf15e1fF6gdQrAJNfOJttbBaHrw1FsUd0WQ5127qIjBJuL1yuy2Kc6Ht3OoXZW6Nq57xxfQQX6vegglmffQNf1GcCt7+bWb3sqnBFFNBXBFfm4G7yub+ZU1xdYgn/hBxcy+QGNAlf7dWbJ9L/2qlKIqiKIqiKIqiKIrrcHPzBxMb/Km7ExrHAAAAAElFTkSuQmCC\" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-22 20:48:53', '2020-06-22 20:48:53'),
(338, 68, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANUAAAAjCAYAAAADi0+HAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATTSURBVHhe7ZzBsdMwEIZTAAVAARRAAVAAw5nhDg0wnBkKYGiAG0cq4AAV0AEHOqAHnr4kP+wsK1tKYiU2+83sPEe2pNW/K1l2ArskSZIkSZIkSZLkv+JhseeHwyQZCnn34HC4HRjQ+2L39p8Sy6Niv47GcXIaUzqSd2+PfzfDh2KZMP/CYvP1+NceJ3206Ej+kYebYFODuTDPin06HO7hmLKkjxYduUt9LraJxT0Tpc6bowmOU6t+WnX0k2+VcAv+Xiy3fjGtyXAN2F38LOa3UbdIq47kIfl4k2PiVvqx2O+j1SYOA/tWbG0PiPj7utiPYjZYllfFeCjW+KNA8caTc7qGzxa/ci717Ml4iENPLM6dVC0awkgdaZsx3crC9QfE4k2eBqYBR29dGOjaJhW+svdWoKOEeFLsxeFwP34C5cepAOo6/vok5dg+YLNQrUmrGi0awmgdKaP9m9sCMnH85OEzAlrxbnYAjURjAsblv2/jGh9ov6DU9GDVJPGiRWnt1DSEa+no25uEi2nUmu94KRABMax4UdklYEX7UkxjZEXUXRP8eY7tefzSVoIVjQC8K+aZSggPOreMnc8+aZZGeRH1W9OChOOOwDnVuV+MbZW2X9r+22s8PRrCCB2b63KBxBAvizXNxguAeOyd+St6BW1B/Tzdf/rbh1YeBcFuKThnVy8Cxz4eqMNnzNPiP/VJNMxqrbp+785n68so6DdanWtaaNVX8nGO+pThP89KlGmckX7QoiHQ1igd8eWkugg1VQkREGjKakJFcK0XrlXQVhRY3x6BUPDxw/stPyjXpLNB0oLkmfNf59GK1drqraSMkiEqX5pIlzktuF66Cl+mmPi2RUsOjNYRX+jP9jMLF/MAOAr6YwtmVxiQWJeaVGqvJqSSJOpPyaA7FwFh+2K3hZ5W/7lrcp1NtiWS4VSki+9TE6KmxahJJUbp2D2pcGbkj1YRtfYt9ZygBAJhpswGS+3VhJzqjzIFC5/1PIDpGcEz57/FB7nma3dADb16CfryW3MxpQVtjZxUMELHrroMsPVHg6cGyMPWyw9Y9Ao6h9qr+UWgCXh0Hh98gujBm7H6c9Djv/qWFvrs60Z+LA19kvhTeRFpgY7eV1926Ukl3ZbUkbrNk2r0z9v9hEJg9uNytlfQORRAAu9fyuAHFiUCUI4f1PELD/WilbzHf671bdCnTWb5X0vApaC/qM85LajjtfRlc2Pq0RBG6Ng8qXiGshfRMW//lkKrmjc7eISPVplzIOhRvwq0gogf2sbYLwsVEPyXn9GXiaC2vP+UE3i9AVOb0XXU15tIzkf9LImNAf7avqe0QDvOWX91vS1T+5SrDUtNQ7iWjk311alPNBJwCbhL+L5kVhCJdOqKUoMHWvb+6tN/D8WxPW8fwvGJxeZxMV0TPVPhs+pjPpEYl85RX6/4PcRmqp+lUW747/JgSgviqPGhhWKpMvSgbf6qzE+sKQ3hWjril/d1Vax+AMmm0OJAXq4W7pazt9okGYQm1VK7uCEwmbhdc/tOkmujbeTqF3lutateGZLNQB6ueusnWB2iX1wkyWh407mZXdOmBpOsEvKPPNwM3KU2919EJauBvOMf1m7uhRkDGvm7xCQRo39tlCRJkiRJkiRJkiRJcg673R1RWv+W32T1bgAAAABJRU5ErkJggg==\" /></p>', 0, '2020-06-22 20:48:53', '2020-06-22 20:48:53'),
(339, 68, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMsAAAAjCAYAAAA67P7cAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATSSURBVHhe7ZzNjRQ9EIY3AAKAAAiAACAAxBl9d0gAcUYEgEiAG0ci4PARARlwIANyAD+z80KpVOVxz86Pu7ceyZput3/Kb5Xd7p7ZvSmKoiiKoiiKoihOyuOWXt4eFsVJIJ4e3R5uBwb0oaUHu7Pt86SlX/vEcXE8PS2Jp3f7z83wsaX7EjQsDP/vP+1xsZwRLYkr4msTbGowA7xo6fPt4Q6OySuWM6Ild5UvLW1iMb5vwfJ2nwTHNVmOY1RLP6lWCbfM7y3dly0YjDr4GnCH/9mS38rMyqiWxBdxNuW4uPV9aun3PmUTgoF9a2mGB7DXLfGQKHsjYbHzTUs/WrJOsvBWj/pqh3OLX+XO9byGrWi7RN9TTZbZtKR/xjXLovQXRODNlgamAUdvKxjoDJPlWUv/3R7u7EVYbxfH7H0VBJGD5RS1xacPPo7tQymLyrXHf0pm1JI8bJhuK8aE8JOCc4SxoswyAOzw3+9gp3eMiMYi/OTPxsgKR5BEC8iamVlL314XCtOoTb7jc4FQCGZFifI8rExfW5K9rEa6YwlfhmNbhn50K2flQdD3LfVAl8yuzMHZeDjPgmUJBEVPK4t8HfWb6UEQsXpzTXUetsTWRlsgba1tmUPMouVwXQpIEPGqpaFZdgIQhb0pnyITSqjO893Zv/J2dZCo9rbPdbvK4Cz20EA9zkkRXCc4SJk2md3K9/tizq09x7JksgDlo5U004PydoJxjfrkYT/PF+RpnJmGgrIzaUkfR9VFrF4lhECkXjokloWymSBRAMhR/hrC29WBdr0dapd8TSYruhYOj+oxNlbOTJ/MbgVb5OAofynUj7TKiLQ5pAflrb7g8+Qb37ZlRi3pg74yW0IozEPYpaA/tk9+dcmEAl3riSLHR/XlYN1pEJgthN2eZXAno28fNCKz+xwOtlA/GmuEtPF9KtAzPU41WcRMWtIHfdHnEBh8yR8rImz27WkmFOhaT5ReffLkIGzQXpuk/XePnlOyfjObFztpD8Eom7PUC0K/7RU9PU49WWAGLWFRXQY5+qOyEUeNiMW2KRIJMqFA13p94DycGJWhTe90PaxiexZkQm0vcbDq+PzIlmPAlkirCMoR0D1fR3qgpbfV5y2dLNLl2lpSd3iyXPrnyn6iIDJ7YxmbCQVyCI70LyZoU+1GzgXyaZd6foGgbrbqCq5lZXp2068NUo1jNLB6YHfUZwT9RX0e0oM6Xk+ft3RMs2hJH0OThWcUW4iOeRt2LrRi+WQHj/jR6iFwYtSGdZzEpl1tJeyXVxIYe9Sv/3KLNnCm3vaoTmZXz8G6prdzlLF93YXRyWJ1ZUy2754e6Mc1a6/K2zy1T77aEDNrOVRfnfqgQ/xzwJ3A96VkRZGQvZWCB0T21Krvv0MBzm0Z++BKHywKT1tSGf/MgnjYofpc1+tqD7aqHCkSH72zvu7C6GSRv6PvpHp60LbGxTjlH+UxVtrmU3l+wsysJf15e1fF6gdQrAJNfOJttbBaHrw1FsUd0WQ5127qIjBJuL1yuy2Kc6Ht3OoXZW6Nq57xxfQQX6vegglmffQNf1GcCt7+bWb3sqnBFFNBXBFfm4G7yub+ZU1xdYgn/hBxcy+QGNAlf7dWbJ9L/2qlKIqiKIqiKIqiKIrrcHPzBxMb/Km7ExrHAAAAAElFTkSuQmCC\" /></p>', 0, '2020-06-22 20:48:53', '2020-06-22 20:48:53'),
(340, 68, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANMAAAAjCAYAAAAOlT/AAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAUaSURBVHhe7ZzNsRM7EEZvAAQAARAAAUAAFGvq7SEBijVFABQJsGNJBCweEZABCzIgB9Cx/RVNl3567LHH1+pT1eUZaSS1PrU0mrHvvUuSJEmSJEmSJEmm5XGxl/vDJDkJ4ujR/nA+6PiHYg92Z7fLk2K/DsZxcjw9LYmjd4fP6fhY7NaDiwXj/8OnPU6WE9GSeCKupmKWTr8o9nl/uINj0pLlRLTkrvSl2K0v0v8wS1C9PZjgOCfTcUS19JPupuHW/L3YDKtHNAC2gt3Bz2J+u3SNRLUkroiv+9CnKtxePxX7fbDeZEGAb8W2fFCk7TfFfhSzA2R5XYwHXfWnNTi8kSRf13Eu/Cp5rudE+oOmS3U9dTJFdISIlj0dIaolddOna1q0wiAob+XUeYnSenuFIFtOJtplX63BrQXBs2L/7Q93/WFwaj5r4HQtnzY4+bQPzSw4W/V7bSI6QkTLkY4Q1ZI06r+XWz0mjJ80nCOyF/iaOtrz0X/3xTV+cMEvDLX+sUISbK3F5b7T0hGiWkZ0hKiWvr4mXEiF1nyjW4NQCOYFbqVbWL2+FlPfWP101wOfz7HNB9rRloEVDOHfF7P0gsCDvtG+cF6beEshcKK+0c9Wmy0tCDTuAOSp3MNibJ+0zdLW3V7jWaIjeC2pd20dQ2XJlBDiVbHhDLwwCMw+mk/LSHiVe747+3u9VhkJb7cN5PmVigFjnw6U4xyzjHwByhJcmNdY5f3enHPvzzFEJxNwbWslbmlBGTsJyaMO0vCfZyHS1E+vn4joCNRV0/IcOuLL4rKI1CuAAIjTs5ZIp0CdNXF7wmswfR7ia8Cp1/urOpWuCWcHR4uQpecLKB+NWJm9zgrGWhDU0pdC+ZZvnpouMNKCMtJW+DSNS61+GOkIPS1berXSI+AL7fkxa8KFPNxdG/jF1syuPkKi1oRXXks8BUatrA0ADT4DwTbFbwFFzxcLd0mu80F3jiCwUH7kG0iXWnsjLS41mURNy5ZerfQI+EI7+DYER879A1HEozM98wIjfO8b6J7wymuJ1ytLmg8A7fcxPQNYevV5agPb8nfRQBoievvAB9qpbalFTws/ccCnrTmZwGu5to4QLkvnoj/oiwxQS6RjYEvmRbH0hFdeyx8Gl0Gu5VNfLdD0QE0/fX7PF4/atn1Tmi/f8mUptBXxjWsI9lE81LRAS++rT1t7Mkk3aanzNXWkbGgyXetPzf1EYhDYm9sO9YTXoDHY/iUL9WK1wQfSVSfl/GJDWb96LwkCrq2t/rRrA1l9aAXeEvA54htttdobaUE5r6dPG/VpiY5Q05K619QRX4aTiWckewGN8jZva7TiebMCAQNUW4UEA12rR4OrgaNebVX8F3waCHxS27UvAVWX94V0Bltvs1RfzWfVobeLXOPbOZbIZLJ64q9vt6cF+pFX086mqQ3SVYelpSNEtVxbx2F5NegDDdG3hLuI90nmRZOYvRWHh1T29arDf4/Esc33D9a0wQLztJiu889MtK/ymA8efFQeZfWqvgbj0mrnFCKTSTHhv4sTPS2oW31ED42N0tCE+vlUmp9QPR1hiZZr6ohf3tebZJqOJpugRYE4u3lYcbu34CQ5AU2mrXdsF4FJxG2c23qSrI22i9Ms1tyCp1g5kotDXE2xxROsHq1fSSTJKfDmcrpdz5SdTs4K8URcTQd3pWn/NVOyOsQRf6Q67YstOn7u3xgmc3CtvwxKkiRJkiRJkiRJkmQe7u7+ADstBfJWH1MtAAAAAElFTkSuQmCC\" /></p>', 0, '2020-06-22 20:48:53', '2020-06-22 20:48:53'),
(341, 68, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMkAAAAjCAYAAAA+GS7hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATKSURBVHhe7ZzNzdQwEIa/AigACqAACoACEGfEHRpAnBEFIBrgxpEKOEAFdMCBDuiBz8/uvjAa2bGT3WQd7zzS6Mv6d/yO7TjZhbsgCIIgCIIgCIJgFo+TvTxeBsFZMI8eHS/HgQF9TPbg8GlsniT7czKug2VM6cg8en/6Owyfkt3ChGEz+H76a6+DebToyHxiXg3BUIOp8CLZl+PlAa5JC+bRoiN3ka/Jhth8b2mivDuZ4DoWyXxadfSLaZdwi/yZ7BaOWtAa3GvA3fx3Mn9s6ZFWHZlXzK8ux8St7nOyvycrLQQG9iPZNR+w6Pttsl/JrPCeN8l4SNR4csLzho48leGzxe9saz2LMSZ0naPtuYukRx1pmzH1shH9A7F4U6WBacC5txAM9JqLhH45typopeA+S/bqeHkYD8J7vxUQleOvn3Rc2wdONpJrjf2S9KojabTf3ZGLheAXA58R0IrX0wBy/gn89N/fUM4Hzi/40vjY1ZhIuU1j7/Soo29vEgrTqDXf8VogAmJY8XJpOdhxviWTz+xYukuBz+fa5tOPbt3sOAj6IZllKrg50K1lLHz2k2AJTIg5vjHWXL8lLZhA7Njkqc7DZBxjdNzR8dmW8fSoY3NdCkgM8TpZ0+q6AIjHWZW/okVQ1Xt++PS/jnYGiWpv4eTZ3YVAcA4G6vAZs7T4AtRn4mBWO9X3Z18+W1+WMmeRAOVzu2dJC8rbhUUe9UnDf541SNM4vX6iRx3xZVFdhJqqhAgINGUloXJQ1gtXE1SB8vkIq2DSrvdD7ZKuRWRF14ZhqfkCKsPY2U2tfppkueDm0udC/SnfPDldalpQXroKn6aY+LZFjzriC/3ZfqpQmAeoraA/jkh2xwCJVRJU+SVhFPRcfQVXdxYE5rhgj2GWmi8W7mqUtZNnjeBaqN/iG0gX36cmeEmLrRaJ2EpHfKEffGsCZ7b8ESGilr71rAmq/JIwU/VJk/j4oPM0pjO2peaLxwet5OvsAJ1gEsrfkvkJLejLH23FlBZbLxJYW0eYVZcBtv7oqyVIJaEsHI38gEVNUOWX+iFwBDCXT5s+4HoQxXefV/PFo741Nn329XN+LIF+Wn2jHBN5Ks45LdDR++rTLr1IpNuaOlK3eZFs/fNhv0AQmPOvnK0JqoAQSP/SgXaxXGCBdNqljt8YqLfkJYKF8r4N+rSTU/6XJtQc8LnVN/rL9VnTgjpeS59WG1OPOuJL0yLhGcQWomPebq2FdilvdvAIn9s1LAQx144Cp6DQro4N9ssnCYw/6jf35ZTayflCHoHUGx616cuqDb1pI9/3s5TWRWI1xV/b95QWaEee9VflbZraJ11tWHrUsam+OvUTDeHXgF3f9yWzgkik2g7BAx5nZ7Xhvwfh2ubbh1L6YDN4mkxl/DMJ/asu5gXlGj+VT329kvagdamfc2hdJIq1/y4JprSgbY0PPRQbpaEJbfNXaX6h9Kojfnlfd8XuBxB0jRY782y3sENWb4VBsBAtkrVOTZvA4uB2yu01CC6Njm2734S5Fe56pQfdwrza9VFLsNpz38gHwbnwJm+YU8pQgwm6gPnEvBoG7iLD/RcwwdVgHvEP/4Z7IcSAtvxdWTAuW/+6JAiCIAiCIAiCIAiCy3J3dw8sY/ypr5aGDwAAAABJRU5ErkJggg==\" /></p>', 1, '2020-06-22 20:48:53', '2020-06-22 20:48:53'),
(342, 69, '<p>8,5 sm&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-22 20:51:29', '2020-06-22 20:51:29'),
(343, 69, '<p>9,5 sm&nbsp;&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-22 20:51:29', '2020-06-22 20:51:29'),
(344, 69, '<p>10 sm</p>', 1, '2020-06-22 20:51:29', '2020-06-22 20:51:29'),
(345, 69, '<p>11 sm</p>', 0, '2020-06-22 20:51:29', '2020-06-22 20:51:29'),
(346, 69, '<p>10,5 sm</p>', 0, '2020-06-22 20:51:29', '2020-06-22 20:51:29'),
(347, 70, '<p>200 sm<sup>2</sup></p>', 0, '2020-06-22 20:52:52', '2020-06-22 20:52:52'),
(348, 70, '<p>120 sm<sup>2</sup></p>', 0, '2020-06-22 20:52:52', '2020-06-22 20:52:52'),
(349, 70, '<p>160 sm<sup>2</sup></p>', 1, '2020-06-22 20:52:52', '2020-06-22 20:52:52'),
(350, 70, '<p>150 sm<sup>2</sup></p>', 0, '2020-06-22 20:52:52', '2020-06-22 20:52:52');
INSERT INTO `questions_options` (`id`, `question_id`, `option`, `correct`, `created_at`, `updated_at`) VALUES
(351, 70, '<p>140 sm<sup>2</sup></p>', 0, '2020-06-22 20:52:52', '2020-06-22 20:52:52'),
(352, 71, '<p>512</p>', 0, '2020-06-22 20:53:38', '2020-06-22 20:53:38'),
(353, 71, '<p>128</p>', 0, '2020-06-22 20:53:38', '2020-06-22 20:53:38'),
(354, 71, '<p>36</p>', 0, '2020-06-22 20:53:38', '2020-06-22 20:53:38'),
(355, 71, '<p>576</p>', 0, '2020-06-22 20:53:38', '2020-06-22 20:53:38'),
(356, 71, '<p>476</p>', 1, '2020-06-22 20:53:38', '2020-06-22 20:53:38'),
(357, 72, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAArCAYAAACTkhN2AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADISURBVFhH7ZXRCcIwFEUzgAs4hgs4QbdxDhfwz093cJVu4A56D/ggtDFNSgItvgMHAim3SUjeC85/cpDPr4ybcZSjfMvm4cZFevgMD0/SNfwum4fHjwhf8iQdpyN23dbqbBAKEwUqVaSmdWaQxeQ6PeOb5BugBFf/AFK1+yzjamg7pBRXUdoYCO4SbitvcixTOKKHXNrdjJLwq1zVkZbCma8+DiMXTijzBqvnJhXzq9MTbA/I5F3Y3c+S6/SsNg41q6+is0tC+ADfVE2CPUmEpwAAAABJRU5ErkJggg==\" /></p>', 0, '2020-06-22 20:54:14', '2020-06-22 20:54:14'),
(358, 72, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAArCAYAAACejGMxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAC1SURBVEhL7ZXBDYMwDEUzAAswRvdgG+Zggd66RydgBzZgB/j/YMm1rOIkEuLgJ70LEMeOiF2Se5nhYdzhC4YY4QZtkC8cYAjuxkCaCTK7LhYYLsWDWb1huBSPLOWXZ5SSPA3bQzyTTj5QDrPpJjMAJdL9qwLx6q9Qd3z2FQbSz/7iLWBgDi++C8EP7bSrDiJnoCeeF/gS2Vn/7lVnYmE2zKqrYXNx1TDXSAZNZejz6J45STOlnFJXN8btORyhAAAAAElFTkSuQmCC\" /></p>', 0, '2020-06-22 20:54:14', '2020-06-22 20:54:14'),
(359, 72, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAArCAYAAACTkhN2AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAD9SURBVFhH7ZXhCQIxDIVvABdwDBdwArdxDhfwnz/dwVXcwB28fGAgxNom1QOVfvDg4K7v0tC8ToP/ZyW6PMSzZS+6O91EG1GTtegqYpE3t++sSkVUoUK/iOr4gWUn4tsUJfMSB1GoJZaIObs4ilItgYh5V0sgYt7VEmiZd7cEWubdLYGTqGbe1RI/KKXpe6slg0EMm9NZDb4UMoPQKgWXzx7SMYxd7M15JrD4Bojc9A+glOdbkU1I3SHxnKJ1WSgYL2KulX+kLR5adBa1dvdExHyx25/36XYoNXN/81M9JynMq9sfYx0gFXOhZ7+Kn0B7+1OtNVWlj+LgJ5mmGRUdXse4gl2TAAAAAElFTkSuQmCC\" /></p>', 0, '2020-06-22 20:54:14', '2020-06-22 20:54:14'),
(360, 72, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAArCAYAAACejGMxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACwSURBVEhL7ZTBDcIwEARdAA2kjDRABXSTAmiAJvJMD7RCB/RAdhxZOkXw8BopeXikkchnuXOcTZ3juMmnvOQng0G+ZFPIXT6kHcIaOEkrhDWYAuwQAggCK2SU1+1nxgqZ5eeHnJGFfSaR84R0zsK3W7q30wi98pblQKmIavjw4lup7pLYsTb8qz0B0Bv0R1zFOo9CDLT7tcAkTdMAU/wlpGkdLt0iq4p6f1N70x9DSis5ZjgHI4ld7wAAAABJRU5ErkJggg==\" /></p>', 1, '2020-06-22 20:54:14', '2020-06-22 20:54:14'),
(361, 72, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAArCAYAAACejGMxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAClSURBVEhL7ZTNCYNAEEa3ABuwjDRgBenGOmwgtxztIa3YQXrQ74EDGw/GcSAQmQcPvPixu/NTkt/TyNcq325aOclZng4xepkhn1ws5ClPh9TNhm95k8nfYyXdMwmwnZ27dMHEPiRBwDpwB3WynlhCWQeshRAEhELsJO53qeFqowytyEGGthqVCV2DnwkxOA2VOwwB1mgmzWe98xVrrq3hPklclLIAJhY0JapASgkAAAAASUVORK5CYII=\" /></p>', 0, '2020-06-22 20:54:14', '2020-06-22 20:54:14'),
(362, 73, '<p>1/12</p>', 0, '2020-06-22 20:55:12', '2020-06-22 20:55:12'),
(363, 73, '<p>7/9</p>', 0, '2020-06-22 20:55:12', '2020-06-22 20:55:12'),
(364, 73, '<p>17/12</p>', 0, '2020-06-22 20:55:12', '2020-06-22 20:55:12'),
(365, 73, '<p>4/5</p>', 1, '2020-06-22 20:55:12', '2020-06-22 20:55:12'),
(366, 73, '<p>1/2</p>', 0, '2020-06-22 20:55:12', '2020-06-22 20:55:12'),
(367, 74, '<p>9</p>', 0, '2020-06-22 20:56:06', '2020-06-22 20:56:06'),
(368, 74, '<p>10</p>', 0, '2020-06-22 20:56:06', '2020-06-22 20:56:06'),
(369, 74, '<p>8</p>', 1, '2020-06-22 20:56:06', '2020-06-22 20:56:06'),
(370, 74, '<p>7</p>', 0, '2020-06-22 20:56:06', '2020-06-22 20:56:06'),
(371, 74, '<p>6</p>', 0, '2020-06-22 20:56:06', '2020-06-22 20:56:06'),
(372, 75, '<p>-6</p>', 1, '2020-06-22 20:57:40', '2020-06-22 20:57:40'),
(373, 75, '<p>9</p>', 0, '2020-06-22 20:57:40', '2020-06-22 20:57:40'),
(374, 75, '<p>8</p>', 0, '2020-06-22 20:57:40', '2020-06-22 20:57:40'),
(375, 75, '<p>6</p>', 0, '2020-06-22 20:57:40', '2020-06-22 20:57:40'),
(376, 75, '<p>-8</p>', 0, '2020-06-22 20:57:40', '2020-06-22 20:57:40'),
(377, 76, '<p>1,5 sm&nbsp;</p>', 0, '2020-06-22 20:58:40', '2020-06-22 20:58:40'),
(378, 76, '<p>3.2 sm&nbsp;</p>', 1, '2020-06-22 20:58:40', '2020-06-22 20:58:40'),
(379, 76, '<p>2,4 sm</p>', 0, '2020-06-22 20:58:40', '2020-06-22 20:58:40'),
(380, 76, '<p>1,8 sm</p>', 0, '2020-06-22 20:58:40', '2020-06-22 20:58:40'),
(381, 76, '<p>6,4 sm</p>', 0, '2020-06-22 20:58:40', '2020-06-22 20:58:40'),
(382, 77, '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAArCAYAAACaebMMAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADkSURBVEhL7ZXBCQIxEEW3ABuwAAuwASvwbAXePViABYgNePNoDdqIB2vR/zAfQkDcnY2IkAePLAt+xkl20jUaP2Yij/LxxpPsBUEbOZM7OU3vznIuQxBCGBBCGKEh1tKV8LeWr8fhUNVeUgnPl7SG2EpXwtq74SX8+CrdH4IIZ0NWvOgLPbql1RDEcTjI8AY0/plyMnyy8UUYN3fpZodnGR8z89/zyx96KHAh86lBOCMpPNNKCKoS5srCfcsZfTPlMGHzHobJL5ZREEKYoTp2ejAE+cBaDvHgu9OHtLTaOWtUp+ueIG46T1u2hMcAAAAASUVORK5CYII=\" />&nbsp;+<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAARCAYAAADUryzEAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACDSURBVDhPYxgFWIEqEJ8C4v848EIgxgkMgbgQiHmAuB6IJaHslUAMkiMagDSCDAABkMZeCJN4UAzEMBtBTvaBMIkDINu7gRjkdBB7N5QmGoBsh9kIovEGGjqIAGJQgIFsBwGQZpCBoJgJAwngAyA/b4fSMADSDIo6UCDCDB0F1AUMDAD4GBWnEcXIKAAAAABJRU5ErkJgggAAAAAAAAAAAAAAAAAAAAAAAA==\" /><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAB9SURBVDhP5ZDBDUBAFAW3AAVQgAL0oRlRhwbcVOGiAh046EAPzCQ2cZBYVyaZy+f997PhG2TY4IiFA8ixwx0HB3cYnPD6U409+u0R21Y0pDYmBcWAYU83+ArPXXDD1kEq15Nd4oIKk4gnu0Tn0xIfsS2+so02+/Ial/6cEA5TEhl7T/XoLQAAAABJRU5ErkJgggAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==\" />;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACsAAAARCAYAAABEvFULAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEWSURBVEhL7ZSxDcIwEEUzAAPAAAzAAgyAGIAeFkDUjMACdJQU1DRMwAYUbMAO8F/CoSiK7YurCOVLTyQotv/dP7kY9GcaiZt4B3iJmeiFMHsWTUM7gdll+dYTYXZdPf6EcTp6Kt8cYpOVuIoJf0hTcRSpihfiLupxPoXtE5ONhff7UlTFIVbdRmwFm8VEEQeR+i6krPipiupYRIcxmxIG99/fHHEWRjHcSSzELN30GEVWoEVfJxWrrWUEOhdL/A/BoHsj4UBmOqeznJd1TVmVxM8mqa6YMNl2DaUUi5+95tVju2wEMGjXyEWMRUqs5Qbh5vAoFr+reLppt4CZtdnzDD+JMEK2BkLpcE79uybeVAcN6rGK4gPqO1AImQofcAAAAABJRU5ErkJggg==\" />&nbsp;&nbsp;</p>', 0, '2020-06-22 20:59:30', '2020-06-22 20:59:30'),
(383, 77, '<p>+&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAArCAYAAACaebMMAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADdSURBVEhL7ZXBCQIxEEVTgAVoARZgH56twLsHC7AAsQFvHq1BK7ADD9ai/2EGQkCMM4oKefDIsrCf2cnuJHU6X2Ygt/L6wJ1sgqCFHMuVHOV7ezmRLgghDAghjFAXc2mV8FrT++XrUNVaUgnXh7y6WEqrhLW54TU8fJTWH4IIZ0Nm3GiFHp3zahDE57CR7g3o/DP1ZHhm54Mwbi7Smh2aZeX84kcn2DXPhrKcDoScpHv+lxDC+AlDUOggMWwoortnNRbKGoZX5Ex4SxiEzs2S0CZQgTUey2Ov85ukdAN4MDZj+bJTegAAAABJRU5ErkJggg==\" />+&nbsp;6<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAARCAYAAADUryzEAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACDSURBVDhPYxgFWIEqEJ8C4v848EIgxgkMgbgQiHmAuB6IJaHslUAMkiMagDSCDAABkMZeCJN4UAzEMBtBTvaBMIkDINu7gRjkdBB7N5QmGoBsh9kIovEGGjqIAGJQgIFsBwGQZpCBoJgJAwngAyA/b4fSMADSDIo6UCDCDB0F1AUMDAD4GBWnEcXIKAAAAABJRU5ErkJgggAAAAAAAAAAAAAAAAAAAAAAAA==\" /><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAB9SURBVDhP5ZDBDUBAFAW3AAVQgAL0oRlRhwbcVOGiAh046EAPzCQ2cZBYVyaZy+f997PhG2TY4IiFA8ixwx0HB3cYnPD6U409+u0R21Y0pDYmBcWAYU83+ArPXXDD1kEq15Nd4oIKk4gnu0Tn0xIfsS2+so02+/Ial/6cEA5TEhl7T/XoLQAAAABJRU5ErkJgggAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==\" />;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACsAAAARCAYAAABEvFULAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEWSURBVEhL7ZSxDcIwEEUzAAPAAAzAAgyAGIAeFkDUjMACdJQU1DRMwAYUbMAO8F/CoSiK7YurCOVLTyQotv/dP7kY9GcaiZt4B3iJmeiFMHsWTUM7gdll+dYTYXZdPf6EcTp6Kt8cYpOVuIoJf0hTcRSpihfiLupxPoXtE5ONhff7UlTFIVbdRmwFm8VEEQeR+i6krPipiupYRIcxmxIG99/fHHEWRjHcSSzELN30GEVWoEVfJxWrrWUEOhdL/A/BoHsj4UBmOqeznJd1TVmVxM8mqa6YMNl2DaUUi5+95tVju2wEMGjXyEWMRUqs5Qbh5vAoFr+reLppt4CZtdnzDD+JMEK2BkLpcE79uybeVAcN6rGK4gPqO1AImQofcAAAAABJRU5ErkJggg==\" />&nbsp;&nbsp;</p>', 0, '2020-06-22 20:59:30', '2020-06-22 20:59:30'),
(384, 77, '<p>+<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAArCAYAAACaebMMAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADRSURBVEhL7ZXLDcIwEAVdAA1QAAXQABVwpoLcOVAABQANcONIDaERDtQCb4gtRRYIs8tX8kgjW5byZK+TTahUvsxAbuX5jjtZBEFzOZJLOYxrezmWJgghDAghjFATjUw74VjTbvo87Gol2QnzNo4mFjLthLG44Dk8fJCpPgQRzoXMWCiFGh3jmCCI12EjzRdQ+WfyzvDIyofIP3wz9LCTfEkYbXst3WEcD2lBrrD+z8Qdln5x4Aqjy0666RVXGD3/1reI1NCFu2Z9fjes8hZCuADsRjfmsn363wAAAABJRU5ErkJggg==\" />&nbsp;+&nbsp;2<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAARCAYAAADUryzEAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACDSURBVDhPYxgFWIEqEJ8C4v848EIgxgkMgbgQiHmAuB6IJaHslUAMkiMagDSCDAABkMZeCJN4UAzEMBtBTvaBMIkDINu7gRjkdBB7N5QmGoBsh9kIovEGGjqIAGJQgIFsBwGQZpCBoJgJAwngAyA/b4fSMADSDIo6UCDCDB0F1AUMDAD4GBWnEcXIKAAAAABJRU5ErkJgggAAAAAAAAAAAAAAAAAAAAAAAA==\" /><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAB9SURBVDhP5ZDBDUBAFAW3AAVQgAL0oRlRhwbcVOGiAh046EAPzCQ2cZBYVyaZy+f997PhG2TY4IiFA8ixwx0HB3cYnPD6U409+u0R21Y0pDYmBcWAYU83+ArPXXDD1kEq15Nd4oIKk4gnu0Tn0xIfsS2+so02+/Ial/6cEA5TEhl7T/XoLQAAAABJRU5ErkJgggAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==\" /></p>', 0, '2020-06-22 20:59:30', '2020-06-22 20:59:30'),
(385, 77, '<p>+&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAArCAYAAACJrvP4AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEWSURBVFhH7ZbLDcIwEERTAAVAARRAA1TAmQq4c6AACgAa4MaRGqACOuBALTAvyUrGciRCWCSInzTyJ5HH9m5iF5lMW0bSTbrXmkkuYHSqS1hJGFv7owylQVUtweQiTcqWM5jsquoTTGgv2VbHOkitwOgohSsF2ktpLK0lVk8f7761A8TKZtkUM/owg6aJtcJMKWMWkq2Ebeuctcz0LMVmrGoj8Zx6mMGdSM0ac+ujbJ0QKVKxYHBWa30YYU7CzOl4FQayxEDhoID5tS4NiyufSPhuJvNDhGnfVZkeE/+M3eDc4vT+ihlXga3kbsb2IY4VV7PwguNuZtc2cDXjZJ5W1RJXM+4Zqf8fIoauuMcs5H/NMr2gKB4uc1O8t4/M1gAAAABJRU5ErkJggg==\" />+&nbsp;2<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAARCAYAAADUryzEAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACDSURBVDhPYxgFWIEqEJ8C4v848EIgxgkMgbgQiHmAuB6IJaHslUAMkiMagDSCDAABkMZeCJN4UAzEMBtBTvaBMIkDINu7gRjkdBB7N5QmGoBsh9kIovEGGjqIAGJQgIFsBwGQZpCBoJgJAwngAyA/b4fSMADSDIo6UCDCDB0F1AUMDAD4GBWnEcXIKAAAAABJRU5ErkJgggAAAAAAAAAAAAAAAAAAAAAAAA==\" /><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAB9SURBVDhP5ZDBDUBAFAW3AAVQgAL0oRlRhwbcVOGiAh046EAPzCQ2cZBYVyaZy+f997PhG2TY4IiFA8ixwx0HB3cYnPD6U409+u0R21Y0pDYmBcWAYU83+ArPXXDD1kEq15Nd4oIKk4gnu0Tn0xIfsS2+so02+/Ial/6cEA5TEhl7T/XoLQAAAABJRU5ErkJgggAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==\" />;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACsAAAARCAYAAABEvFULAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEWSURBVEhL7ZSxDcIwEEUzAAPAAAzAAgyAGIAeFkDUjMACdJQU1DRMwAYUbMAO8F/CoSiK7YurCOVLTyQotv/dP7kY9GcaiZt4B3iJmeiFMHsWTUM7gdll+dYTYXZdPf6EcTp6Kt8cYpOVuIoJf0hTcRSpihfiLupxPoXtE5ONhff7UlTFIVbdRmwFm8VEEQeR+i6krPipiupYRIcxmxIG99/fHHEWRjHcSSzELN30GEVWoEVfJxWrrWUEOhdL/A/BoHsj4UBmOqeznJd1TVmVxM8mqa6YMNl2DaUUi5+95tVju2wEMGjXyEWMRUqs5Qbh5vAoFr+reLppt4CZtdnzDD+JMEK2BkLpcE79uybeVAcN6rGK4gPqO1AImQofcAAAAABJRU5ErkJggg==\" /></p>', 0, '2020-06-22 20:59:30', '2020-06-22 20:59:30'),
(386, 77, '<p>+&nbsp;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAArCAYAAACJrvP4AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEWSURBVFhH7ZbLDcIwEERTAAVAARRAA1TAmQq4c6AACgAa4MaRGqACOuBALTAvyUrGciRCWCSInzTyJ5HH9m5iF5lMW0bSTbrXmkkuYHSqS1hJGFv7owylQVUtweQiTcqWM5jsquoTTGgv2VbHOkitwOgohSsF2ktpLK0lVk8f7761A8TKZtkUM/owg6aJtcJMKWMWkq2Ebeuctcz0LMVmrGoj8Zx6mMGdSM0ac+ujbJ0QKVKxYHBWa30YYU7CzOl4FQayxEDhoID5tS4NiyufSPhuJvNDhGnfVZkeE/+M3eDc4vT+ihlXga3kbsb2IY4VV7PwguNuZtc2cDXjZJ5W1RJXM+4Zqf8fIoauuMcs5H/NMr2gKB4uc1O8t4/M1gAAAABJRU5ErkJggg==\" />+<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAArCAYAAACJrvP4AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEWSURBVFhH7ZbLDcIwEERTAAVAARRAA1TAmQq4c6AACgAa4MaRGqACOuBALTAvyUrGciRCWCSInzTyJ5HH9m5iF5lMW0bSTbrXmkkuYHSqS1hJGFv7owylQVUtweQiTcqWM5jsquoTTGgv2VbHOkitwOgohSsF2ktpLK0lVk8f7761A8TKZtkUM/owg6aJtcJMKWMWkq2Ebeuctcz0LMVmrGoj8Zx6mMGdSM0ac+ujbJ0QKVKxYHBWa30YYU7CzOl4FQayxEDhoID5tS4NiyufSPhuJvNDhGnfVZkeE/+M3eDc4vT+ihlXga3kbsb2IY4VV7PwguNuZtc2cDXjZJ5W1RJXM+4Zqf8fIoauuMcs5H/NMr2gKB4uc1O8t4/M1gAAAABJRU5ErkJggg==\" />+&nbsp;6<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAARCAYAAADUryzEAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACDSURBVDhPYxgFWIEqEJ8C4v848EIgxgkMgbgQiHmAuB6IJaHslUAMkiMagDSCDAABkMZeCJN4UAzEMBtBTvaBMIkDINu7gRjkdBB7N5QmGoBsh9kIovEGGjqIAGJQgIFsBwGQZpCBoJgJAwngAyA/b4fSMADSDIo6UCDCDB0F1AUMDAD4GBWnEcXIKAAAAABJRU5ErkJgggAAAAAAAAAAAAAAAAAAAAAAAA==\" /><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAB9SURBVDhP5ZDBDUBAFAW3AAVQgAL0oRlRhwbcVOGiAh046EAPzCQ2cZBYVyaZy+f997PhG2TY4IiFA8ixwx0HB3cYnPD6U409+u0R21Y0pDYmBcWAYU83+ArPXXDD1kEq15Nd4oIKk4gnu0Tn0xIfsS2+so02+/Ial/6cEA5TEhl7T/XoLQAAAABJRU5ErkJgggAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==\" />;<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACsAAAARCAYAAABEvFULAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEWSURBVEhL7ZSxDcIwEEUzAAPAAAzAAgyAGIAeFkDUjMACdJQU1DRMwAYUbMAO8F/CoSiK7YurCOVLTyQotv/dP7kY9GcaiZt4B3iJmeiFMHsWTUM7gdll+dYTYXZdPf6EcTp6Kt8cYpOVuIoJf0hTcRSpihfiLupxPoXtE5ONhff7UlTFIVbdRmwFm8VEEQeR+i6krPipiupYRIcxmxIG99/fHHEWRjHcSSzELN30GEVWoEVfJxWrrWUEOhdL/A/BoHsj4UBmOqeznJd1TVmVxM8mqa6YMNl2DaUUi5+95tVju2wEMGjXyEWMRUqs5Qbh5vAoFr+reLppt4CZtdnzDD+JMEK2BkLpcE79uybeVAcN6rGK4gPqO1AImQofcAAAAABJRU5ErkJggg==\" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 1, '2020-06-22 20:59:30', '2020-06-22 20:59:30'),
(387, 78, '<p>&pi;/2 +&pi;k; k&nbsp;&isin; Z</p>', 0, '2020-06-22 21:03:16', '2020-06-22 21:03:16'),
(388, 78, '<p>&plusmn;&pi;/3&nbsp;+6&pi;k; k&nbsp;&isin; Z</p>', 0, '2020-06-22 21:03:16', '2020-06-22 21:03:16'),
(389, 78, '<p>&plusmn;&pi;/4&nbsp;+2&pi;k; k&nbsp;&isin; Z</p>', 0, '2020-06-22 21:03:16', '2020-06-22 21:03:16'),
(390, 78, '<p>&plusmn;3&pi;/4&nbsp;+2&pi;k; k&nbsp;&isin; Z</p>', 0, '2020-06-22 21:03:16', '2020-06-22 21:03:16'),
(391, 78, '<p>&plusmn;3&pi;/4&nbsp;+2&pi;k; k&nbsp;&isin; Z</p>', 1, '2020-06-22 21:03:16', '2020-06-22 21:03:16'),
(392, 79, '<p>other, hers</p>', 1, '2020-06-23 04:56:57', '2020-06-23 04:56:57'),
(393, 79, '<p>another, his</p>', 0, '2020-06-23 04:56:57', '2020-06-23 04:56:57'),
(394, 79, '<p>other, our</p>', 0, '2020-06-23 04:56:57', '2020-06-23 04:56:57'),
(395, 79, '<p>another, theirs</p>', 0, '2020-06-23 04:56:57', '2020-06-23 04:56:57'),
(396, 79, '<p>the, ours</p>', 0, '2020-06-23 04:56:57', '2020-06-23 04:56:57'),
(397, 80, '<p>found, have lost&nbsp;</p>', 0, '2020-06-23 04:59:58', '2020-06-23 04:59:58'),
(398, 80, '<p>have found, lost</p>', 0, '2020-06-23 04:59:58', '2020-06-23 04:59:58'),
(399, 80, '<p>found, had lost</p>', 1, '2020-06-23 04:59:58', '2020-06-23 04:59:58'),
(400, 80, '<p>had found, lost</p>', 0, '2020-06-23 04:59:58', '2020-06-23 04:59:58'),
(401, 80, '<p>had found, loses</p>', 0, '2020-06-23 04:59:58', '2020-06-23 04:59:58'),
(402, 81, '<p>1,3</p>', 0, '2020-06-23 05:01:04', '2020-06-23 05:01:04'),
(403, 81, '<p>3,4</p>', 0, '2020-06-23 05:01:04', '2020-06-23 05:01:04'),
(404, 81, '<p>2,4</p>', 0, '2020-06-23 05:01:04', '2020-06-23 05:01:04'),
(405, 81, '<p>1,4</p>', 1, '2020-06-23 05:01:04', '2020-06-23 05:01:04'),
(406, 81, '<p>2,3</p>', 0, '2020-06-23 05:01:04', '2020-06-23 05:01:04'),
(407, 82, '<p>2,4,5</p>', 1, '2020-06-23 05:04:29', '2020-06-23 05:04:29'),
(408, 82, '<p>1,2</p>', 0, '2020-06-23 05:04:29', '2020-06-23 05:04:29'),
(409, 82, '<p>2,4</p>', 0, '2020-06-23 05:04:29', '2020-06-23 05:04:29'),
(410, 82, '<p>3,5</p>', 0, '2020-06-23 05:04:29', '2020-06-23 05:04:29'),
(411, 82, '<p>1,2,3</p>', 0, '2020-06-23 05:04:29', '2020-06-23 05:04:29'),
(412, 83, '<p>This country is exported oil by&nbsp;them</p>', 0, '2020-06-23 05:05:34', '2020-06-23 05:05:34'),
(413, 83, '<p>&nbsp;Oil has been exported to this country</p>', 0, '2020-06-23 05:05:34', '2020-06-23 05:05:34'),
(414, 83, '<p>Oil was exported to this country</p>', 0, '2020-06-23 05:05:34', '2020-06-23 05:05:34'),
(415, 83, '<p>They are exported oil to this country</p>', 0, '2020-06-23 05:05:34', '2020-06-23 05:05:34'),
(416, 83, '<p>Oil is exported to this country by them</p>', 1, '2020-06-23 05:05:34', '2020-06-23 05:05:34'),
(417, 84, '<p>a&nbsp;few furniture</p>', 0, '2020-06-23 05:06:11', '2020-06-23 05:06:11'),
(418, 84, '<p>another furniture</p>', 0, '2020-06-23 05:06:11', '2020-06-23 05:06:11'),
(419, 84, '<p>a few pieces of furniture&nbsp;</p>', 1, '2020-06-23 05:06:11', '2020-06-23 05:06:11'),
(420, 84, '<p>many furniture</p>', 0, '2020-06-23 05:06:11', '2020-06-23 05:06:11'),
(421, 84, '<p>a furniture&nbsp;</p>', 0, '2020-06-23 05:06:11', '2020-06-23 05:06:11'),
(422, 85, '<p>3,2,5,1,4&nbsp;</p>', 0, '2020-06-23 05:07:15', '2020-06-23 05:07:15'),
(423, 85, '<p>&nbsp;2,4,3,1,5&nbsp;</p>', 0, '2020-06-23 05:07:15', '2020-06-23 05:07:15'),
(424, 85, '<p>3,2,4,1,5</p>', 1, '2020-06-23 05:07:15', '2020-06-23 05:07:15'),
(425, 85, '<p>3,4,5,1,2</p>', 0, '2020-06-23 05:07:15', '2020-06-23 05:07:15'),
(426, 85, '<p>&nbsp;2,4,5,1,3</p>', 0, '2020-06-23 05:07:15', '2020-06-23 05:07:15'),
(427, 86, '<p>1,4</p>', 0, '2020-06-23 05:08:00', '2020-06-23 05:08:00'),
(428, 86, '<p>2,4</p>', 1, '2020-06-23 05:08:00', '2020-06-23 05:08:00'),
(429, 86, '<p>2,3</p>', 0, '2020-06-23 05:08:00', '2020-06-23 05:08:00'),
(430, 86, '<p>1,2</p>', 0, '2020-06-23 05:08:00', '2020-06-23 05:08:00'),
(431, 86, '<p>1,3</p>', 0, '2020-06-23 05:08:00', '2020-06-23 05:08:00'),
(432, 87, '<p>with</p>', 0, '2020-06-23 05:08:41', '2020-06-23 05:08:41'),
(433, 87, '<p>at</p>', 0, '2020-06-23 05:08:41', '2020-06-23 05:08:41'),
(434, 87, '<p>to</p>', 0, '2020-06-23 05:08:41', '2020-06-23 05:08:41'),
(435, 87, '<p>in</p>', 0, '2020-06-23 05:08:41', '2020-06-23 05:08:41'),
(436, 87, '<p>for</p>', 1, '2020-06-23 05:08:41', '2020-06-23 05:08:41'),
(437, 88, '<p>3,4</p>', 0, '2020-06-23 05:09:55', '2020-06-23 05:09:55'),
(438, 88, '<p>1,2</p>', 1, '2020-06-23 05:09:55', '2020-06-23 05:09:55'),
(439, 88, '<p>2,4</p>', 0, '2020-06-23 05:09:55', '2020-06-23 05:09:55'),
(440, 88, '<p>1,3</p>', 0, '2020-06-23 05:09:55', '2020-06-23 05:09:55'),
(441, 88, '<p>1,4</p>', 0, '2020-06-23 05:09:55', '2020-06-23 05:09:55'),
(442, 89, '<p>to&nbsp;leave</p>', 1, '2020-06-23 05:10:41', '2020-06-23 05:10:41'),
(443, 89, '<p>leave&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-23 05:10:41', '2020-06-23 05:10:41'),
(444, 89, '<p>left</p>', 0, '2020-06-23 05:10:41', '2020-06-23 05:10:41'),
(445, 89, '<p>leaves</p>', 0, '2020-06-23 05:10:41', '2020-06-23 05:10:41'),
(446, 89, '<p>leaving</p>', 0, '2020-06-23 05:10:41', '2020-06-23 05:10:41'),
(447, 90, '<p>my, yours&nbsp;</p>', 1, '2020-06-23 05:11:44', '2020-06-23 05:11:44'),
(448, 90, '<p>his,their</p>', 0, '2020-06-23 05:11:44', '2020-06-23 05:11:44'),
(449, 90, '<p>our, your&nbsp;</p>', 0, '2020-06-23 05:11:44', '2020-06-23 05:11:44'),
(450, 90, '<p>hers,her</p>', 0, '2020-06-23 05:11:44', '2020-06-23 05:11:44'),
(451, 90, '<p>hers,his</p>', 0, '2020-06-23 05:11:44', '2020-06-23 05:11:44'),
(452, 91, '<p>&nbsp;to, on&nbsp;&nbsp;</p>', 1, '2020-06-23 05:12:30', '2020-06-23 05:12:30'),
(453, 91, '<p>for, for</p>', 0, '2020-06-23 05:12:30', '2020-06-23 05:12:30'),
(454, 91, '<p>at, with</p>', 0, '2020-06-23 05:12:30', '2020-06-23 05:12:30'),
(455, 91, '<p>to, with</p>', 0, '2020-06-23 05:12:30', '2020-06-23 05:12:30'),
(456, 91, '<p>for, on</p>', 0, '2020-06-23 05:12:30', '2020-06-23 05:12:30'),
(457, 92, '<p>to dye</p>', 0, '2020-06-23 05:13:03', '2020-06-23 05:13:03'),
(458, 92, '<p>dye</p>', 0, '2020-06-23 05:13:03', '2020-06-23 05:13:03'),
(459, 92, '<p>dyed</p>', 1, '2020-06-23 05:13:03', '2020-06-23 05:13:03'),
(460, 92, '<p>dying</p>', 0, '2020-06-23 05:13:03', '2020-06-23 05:13:03'),
(461, 92, '<p>dyes</p>', 0, '2020-06-23 05:13:03', '2020-06-23 05:13:03'),
(462, 93, '<p>take</p>', 1, '2020-06-23 05:13:37', '2020-06-23 05:13:37'),
(463, 93, '<p>taking</p>', 0, '2020-06-23 05:13:37', '2020-06-23 05:13:37'),
(464, 93, '<p>takes</p>', 0, '2020-06-23 05:13:37', '2020-06-23 05:13:37'),
(465, 93, '<p>to take</p>', 0, '2020-06-23 05:13:37', '2020-06-23 05:13:37'),
(466, 93, '<p>took</p>', 0, '2020-06-23 05:13:37', '2020-06-23 05:13:37'),
(467, 94, '<p>very</p>', 0, '2020-06-23 05:14:18', '2020-06-23 05:14:18'),
(468, 94, '<p>most</p>', 0, '2020-06-23 05:14:18', '2020-06-23 05:14:18'),
(469, 94, '<p>more</p>', 0, '2020-06-23 05:14:18', '2020-06-23 05:14:18'),
(470, 94, '<p>the best</p>', 0, '2020-06-23 05:14:18', '2020-06-23 05:14:18'),
(471, 94, '<p>the most</p>', 1, '2020-06-23 05:14:18', '2020-06-23 05:14:18'),
(472, 95, '<p>The, -</p>', 1, '2020-06-23 05:14:54', '2020-06-23 05:14:54'),
(473, 95, '<p>-, the</p>', 0, '2020-06-23 05:14:54', '2020-06-23 05:14:54'),
(474, 95, '<p>A, -</p>', 0, '2020-06-23 05:14:54', '2020-06-23 05:14:54'),
(475, 95, '<p>The, the</p>', 0, '2020-06-23 05:14:54', '2020-06-23 05:14:54'),
(476, 95, '<p>The, a</p>', 0, '2020-06-23 05:14:54', '2020-06-23 05:14:54'),
(477, 96, '<p>2,4,5</p>', 1, '2020-06-23 05:15:51', '2020-06-23 05:15:51'),
(478, 96, '<p>1,2</p>', 0, '2020-06-23 05:15:51', '2020-06-23 05:15:51'),
(479, 96, '<p>3,5,6</p>', 0, '2020-06-23 05:15:51', '2020-06-23 05:15:51'),
(480, 96, '<p>1,3,6</p>', 0, '2020-06-23 05:15:51', '2020-06-23 05:15:51'),
(481, 96, '<p>4,6</p>', 0, '2020-06-23 05:15:51', '2020-06-23 05:15:51'),
(482, 97, '<p>who&nbsp;doesn&rsquo;t&nbsp;</p>', 0, '2020-06-23 05:16:38', '2020-06-23 05:16:38'),
(483, 97, '<p>who don&rsquo;t&nbsp;&nbsp;</p>', 1, '2020-06-23 05:16:38', '2020-06-23 05:16:38'),
(484, 97, '<p>that doesn&rsquo;t&nbsp;</p>', 0, '2020-06-23 05:16:38', '2020-06-23 05:16:38'),
(485, 97, '<p>who aren&rsquo;t</p>', 0, '2020-06-23 05:16:38', '2020-06-23 05:16:38'),
(486, 97, '<p>which don&rsquo;t</p>', 0, '2020-06-23 05:16:38', '2020-06-23 05:16:38'),
(487, 98, '<p>&nbsp;would&nbsp;catch&nbsp;</p>', 0, '2020-06-23 05:17:30', '2020-06-23 05:17:30'),
(488, 98, '<p>won&rsquo;t&nbsp;catch&nbsp;&nbsp;</p>', 0, '2020-06-23 05:17:30', '2020-06-23 05:17:30'),
(489, 98, '<p>will catch</p>', 0, '2020-06-23 05:17:30', '2020-06-23 05:17:30'),
(490, 98, '<p>catch</p>', 1, '2020-06-23 05:17:30', '2020-06-23 05:17:30'),
(491, 98, '<p>hadn&rsquo;t caught</p>', 0, '2020-06-23 05:17:30', '2020-06-23 05:17:30'),
(492, 99, '<p>to decide&nbsp;</p>', 0, '2020-06-23 05:18:05', '2020-06-23 05:18:05'),
(493, 99, '<p>deciding&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-23 05:18:05', '2020-06-23 05:18:05'),
(494, 99, '<p>decides&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-23 05:18:05', '2020-06-23 05:18:05'),
(495, 99, '<p>decide&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 1, '2020-06-23 05:18:05', '2020-06-23 05:18:05'),
(496, 99, '<p>decided</p>', 0, '2020-06-23 05:18:05', '2020-06-23 05:18:05'),
(497, 100, '<p>Somebody answered the teacher&rsquo;s question&nbsp;</p>', 0, '2020-06-23 05:18:42', '2020-06-23 05:18:42'),
(498, 100, '<p>Anybody could answer the teacher&rsquo;s question&nbsp;</p>', 0, '2020-06-23 05:18:42', '2020-06-23 05:18:42'),
(499, 100, '<p>There was nobody who couldn&rsquo;t answer the teacher&rsquo;s question&nbsp;</p>', 0, '2020-06-23 05:18:42', '2020-06-23 05:18:42'),
(500, 100, '<p>Nobody could answer the teacher&rsquo;s question&nbsp;</p>', 1, '2020-06-23 05:18:42', '2020-06-23 05:18:42'),
(501, 100, '<p>Everybody could answer the teacher&rsquo;s question.&nbsp;</p>', 0, '2020-06-23 05:18:42', '2020-06-23 05:18:42'),
(502, 101, '<p>1,2,3,7&nbsp;&nbsp;</p>', 0, '2020-06-23 05:19:45', '2020-06-23 05:19:45'),
(503, 101, '<p>1,5,6,8&nbsp;</p>', 0, '2020-06-23 05:19:45', '2020-06-23 05:19:45'),
(504, 101, '<p>3,6,7,8&nbsp;</p>', 0, '2020-06-23 05:19:45', '2020-06-23 05:19:45'),
(505, 101, '<p>1,3&nbsp;4,7&nbsp;</p>', 0, '2020-06-23 05:19:45', '2020-06-23 05:19:45'),
(506, 101, '<p>1,2,3,8</p>', 1, '2020-06-23 05:19:45', '2020-06-23 05:19:45'),
(507, 102, '<p>that</p>', 1, '2020-06-23 05:20:13', '2020-06-23 05:20:13'),
(508, 102, '<p>this</p>', 0, '2020-06-23 05:20:13', '2020-06-23 05:20:13'),
(509, 102, '<p>their</p>', 0, '2020-06-23 05:20:13', '2020-06-23 05:20:13'),
(510, 102, '<p>these</p>', 0, '2020-06-23 05:20:13', '2020-06-23 05:20:13'),
(511, 102, '<p>those</p>', 0, '2020-06-23 05:20:13', '2020-06-23 05:20:13'),
(512, 103, '<p>1,2</p>', 0, '2020-06-23 05:21:07', '2020-06-23 05:21:07'),
(513, 103, '<p>1,3</p>', 0, '2020-06-23 05:21:07', '2020-06-23 05:21:07'),
(514, 103, '<p>1,4,5</p>', 0, '2020-06-23 05:21:07', '2020-06-23 05:21:07'),
(515, 103, '<p>2,4,5</p>', 1, '2020-06-23 05:21:07', '2020-06-23 05:21:07'),
(516, 103, '<p>2,3,4</p>', 0, '2020-06-23 05:21:07', '2020-06-23 05:21:07'),
(517, 104, '<p>master&rsquo;s&nbsp;orders</p>', 0, '2020-06-23 05:22:44', '2020-06-23 05:22:44'),
(518, 104, '<p>British Holidays&nbsp;&nbsp;</p>', 0, '2020-06-23 05:22:44', '2020-06-23 05:22:44'),
(519, 104, '<p>the&nbsp;Middle Ages&nbsp;&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-23 05:22:44', '2020-06-23 05:22:44'),
(520, 104, '<p>a holiday of joke&nbsp;</p>', 1, '2020-06-23 05:22:44', '2020-06-23 05:22:44'),
(521, 104, '<p>playing jokes on friends</p>', 0, '2020-06-23 05:22:44', '2020-06-23 05:22:44'),
(522, 105, '<p>servants&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 1, '2020-06-23 05:23:17', '2020-06-23 05:23:17'),
(523, 105, '<p>jokes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-23 05:23:17', '2020-06-23 05:23:17'),
(524, 105, '<p>friends</p>', 0, '2020-06-23 05:23:17', '2020-06-23 05:23:17'),
(525, 105, '<p>traditions</p>', 0, '2020-06-23 05:23:17', '2020-06-23 05:23:17'),
(526, 105, '<p>holidays</p>', 0, '2020-06-23 05:23:17', '2020-06-23 05:23:17'),
(527, 106, '<p>a fool</p>', 0, '2020-06-23 05:24:13', '2020-06-23 05:24:13'),
(528, 106, '<p>a servant</p>', 0, '2020-06-23 05:24:13', '2020-06-23 05:24:13'),
(529, 106, '<p>a joke</p>', 0, '2020-06-23 05:24:13', '2020-06-23 05:24:13'),
(530, 106, '<p>an order</p>', 0, '2020-06-23 05:24:13', '2020-06-23 05:24:13'),
(531, 106, '<p>a tradition&nbsp;</p>', 1, '2020-06-23 05:24:13', '2020-06-23 05:24:13'),
(532, 107, '<p>1,2</p>', 0, '2020-06-23 05:24:54', '2020-06-23 05:24:54'),
(533, 107, '<p>2,3</p>', 0, '2020-06-23 05:24:54', '2020-06-23 05:24:54'),
(534, 107, '<p>2,4</p>', 1, '2020-06-23 05:24:54', '2020-06-23 05:24:54'),
(535, 107, '<p>1,4</p>', 0, '2020-06-23 05:24:54', '2020-06-23 05:24:54'),
(536, 107, '<p>3,4</p>', 0, '2020-06-23 05:24:54', '2020-06-23 05:24:54'),
(537, 108, '<p>Who had to obey servants on April&nbsp;Fool&rsquo;s&nbsp; Day?&nbsp;&nbsp;&nbsp;</p>', 0, '2020-06-23 05:25:28', '2020-06-23 05:25:28'),
(538, 108, '<p>&nbsp;When did the tradition of&nbsp;celebrating&nbsp; April&nbsp;Fool&rsquo;s&nbsp; Day appear?&nbsp;</p>', 0, '2020-06-23 05:25:28', '2020-06-23 05:25:28'),
(539, 108, '<p>&nbsp;Who gave orders on April&nbsp;Fool&rsquo;s&nbsp;&nbsp;Day?</p>', 0, '2020-06-23 05:25:28', '2020-06-23 05:25:28'),
(540, 108, '<p>Which&nbsp;joke makes people laugh most of all?</p>', 1, '2020-06-23 05:25:28', '2020-06-23 05:25:28'),
(541, 108, '<p>When is&nbsp;April&nbsp;Fool&rsquo;s&nbsp; Day&nbsp;celebrated?&nbsp;&nbsp;</p>', 0, '2020-06-23 05:25:28', '2020-06-23 05:25:28');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `correct` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `question_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `test_id` bigint(20) UNSIGNED DEFAULT NULL,
  `result` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `user_id`, `test_id`, `result`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '8', '2020-06-23 05:27:31', '2020-06-23 05:27:31');

-- --------------------------------------------------------

--
-- Table structure for table `test_answers`
--

CREATE TABLE `test_answers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `test_id` int(10) UNSIGNED DEFAULT NULL,
  `question_id` int(10) UNSIGNED DEFAULT NULL,
  `correct` tinyint(4) DEFAULT 0,
  `option_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `test_answers`
--

INSERT INTO `test_answers` (`id`, `user_id`, `test_id`, `question_id`, `correct`, `option_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, 1, 1, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(2, 2, 1, 2, 0, 10, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(3, 2, 1, 3, 1, 12, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(4, 2, 1, 5, 1, 22, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(5, 2, 1, 6, 1, 26, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(6, 2, 1, 7, 1, 31, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(7, 2, 1, 8, 1, 36, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(8, 2, 1, 9, 1, 42, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(9, 2, 1, 10, 1, 48, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(10, 2, 1, 11, 0, 51, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(11, 2, 1, 12, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(12, 2, 1, 13, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(13, 2, 1, 14, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(14, 2, 1, 15, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(15, 2, 1, 16, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(16, 2, 1, 17, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(17, 2, 1, 18, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(18, 2, 1, 19, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(19, 2, 1, 20, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(20, 2, 1, 21, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(21, 2, 1, 22, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(22, 2, 1, 23, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(23, 2, 1, 24, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(24, 2, 1, 25, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(25, 2, 1, 26, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(26, 2, 1, 27, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(27, 2, 1, 28, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(28, 2, 1, 29, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(29, 2, 1, 30, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(30, 2, 1, 31, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(31, 2, 1, 32, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(32, 2, 1, 34, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(33, 2, 1, 35, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(34, 2, 1, 36, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(35, 2, 1, 37, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(36, 2, 1, 38, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(37, 2, 1, 39, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(38, 2, 1, 41, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(39, 2, 1, 43, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(40, 2, 1, 44, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(41, 2, 1, 45, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(42, 2, 1, 46, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(43, 2, 1, 47, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(44, 2, 1, 55, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(45, 2, 1, 61, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(46, 2, 1, 62, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(47, 2, 1, 63, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(48, 2, 1, 64, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(49, 2, 1, 65, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(50, 2, 1, 66, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(51, 2, 1, 67, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(52, 2, 1, 68, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(53, 2, 1, 69, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(54, 2, 1, 70, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(55, 2, 1, 71, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(56, 2, 1, 73, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(57, 2, 1, 74, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(58, 2, 1, 75, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(59, 2, 1, 76, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(60, 2, 1, 78, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(61, 2, 1, 79, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(62, 2, 1, 80, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(63, 2, 1, 81, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(64, 2, 1, 82, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(65, 2, 1, 83, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(66, 2, 1, 84, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(67, 2, 1, 85, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(68, 2, 1, 86, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(69, 2, 1, 87, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(70, 2, 1, 88, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(71, 2, 1, 89, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(72, 2, 1, 90, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(73, 2, 1, 91, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(74, 2, 1, 92, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(75, 2, 1, 93, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(76, 2, 1, 94, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(77, 2, 1, 95, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(78, 2, 1, 96, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(79, 2, 1, 97, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(80, 2, 1, 98, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(81, 2, 1, 99, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(82, 2, 1, 100, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(83, 2, 1, 101, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(84, 2, 1, 102, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(85, 2, 1, 103, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(86, 2, 1, 104, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(87, 2, 1, 105, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(88, 2, 1, 106, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(89, 2, 1, 107, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31'),
(90, 2, 1, 108, 0, NULL, '2020-06-23 05:27:31', '2020-06-23 05:27:31');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ticket_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ticket_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `ticket_name`, `ticket_code`, `created_at`, `updated_at`) VALUES
(1, 'Abituriyent Online Sinaq 2020', 'Abituriyent2020', '2020-06-22 17:12:28', '2020-06-22 17:12:28');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ticket_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `ticket_code`, `email`, `phone`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Coders', 'Azerbaijan', 'coders', 'coders@gmail.com', '(+99412) 345-67-89', NULL, '$2y$12$R/AEqbIfnVqsk6SO9Mv1Gun6FyB1agCuHVdHFuZglVSDMTbD/Iwpq', NULL, '2020-06-22 17:11:09', '2020-06-22 17:11:09'),
(2, 'zxczx', 'rwer', 'Abituriyent2020', 'erwewerwe@asd.asdas', '(+99434) 234-23-42', NULL, '$2y$10$xjZ3Op2YFYIR8p0g6u8Jce4uOXSMBeiVXuVeiN5Yvz5alDs.dHa06', NULL, '2020-06-22 17:58:01', '2020-06-22 17:58:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_registers`
--
ALTER TABLE `exam_registers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_subjects`
--
ALTER TABLE `group_subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions_options`
--
ALTER TABLE `questions_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_answers`
--
ALTER TABLE `test_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_registers`
--
ALTER TABLE `exam_registers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `group_subjects`
--
ALTER TABLE `group_subjects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `questions_options`
--
ALTER TABLE `questions_options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=542;

--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `test_answers`
--
ALTER TABLE `test_answers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

@extends('admin.newbase')
@section('content')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="{{asset('admin/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('admin/ckeditor/samples/js/sample.js')}}"></script>
    <link rel="stylesheet" href="{{asset('admin/ckeditor/samples/css/samples.css')}}"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card exam_add">
                    <div class="card-body">
                        <form method="POST" action="" enctype="multipart/form-data" id="ielts_form">
                            @csrf
                            <input type="hidden" name="slug" id="slug" value="{{$slug}}">
                            <div class="form-group">
                                <label for="ielts_id" class="font-weight-bold">Testin adı <span
                                        class="text-danger">*</span></label>
                                <select name="ielts_id" class="form-control" id="ielts_id">
                                    <option value="{{$ielts[0]->id}}" selected>{{$ielts[0]->ielts_name}}</option>
                                </select>
                                <div class="invalid-feedback font-weight-bold ">
                                    Test seçin.
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="texto" class="font-weight-bold">Sual <span
                                        class="text-danger">*</span></label>
                                <div id='texto'></div>

                                <div class="invalid-feedback font-weight-bold question_text">
                                    Sual daxil edin.
                                </div>
                                <hr>
                                <div id="ques"></div>
                            </div>
                            <div class="form-group">
                                <label for="option1" class="font-weight-bold">Variant A<span
                                        class="text-danger">*</span></label>
                                <textarea class="form-control" name="option1"
                                          id="option1"></textarea>
                                <div class="invalid-feedback font-weight-bold ">
                                    Variant daxil edin.
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="option2" class="font-weight-bold">Variant B<span
                                        class="text-danger">*</span></label>
                                <textarea class="form-control" name="option2"
                                          id="option2"></textarea>
                                <div class="invalid-feedback font-weight-bold ">
                                    Variant daxil edin.
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="option3" class="font-weight-bold">Variant C<span
                                        class="text-danger">*</span></label>
                                <textarea class="form-control" name="option3"
                                          id="option3"></textarea>
                                <div class="invalid-feedback font-weight-bold ">
                                    Variant daxil edin.
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="correct" class="font-weight-bold">Düzgün cavab</label>
                                <select name="correct" class="form-control" id="correct">
                                    <option value>Seçin</option>

                                    @foreach($correct_options as $key=>$item)
                                        <option value="{{$key}}">{{$item}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback font-weight-bold ">
                                    Düzgün cavabı seçin.
                                </div>
                            </div>
                            <button class="btn btn-primary px-5 btn-block" type="submit">
                                Saxla
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.min.js"></script>
    <script type="text/javascript" async
            src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML"></script>

    <script type="text/x-mathjax-config">
        MathJax.Hub.Config({
        extensions: ["tex2jax.js"],
        jax: ["input/TeX", "output/HTML-CSS"],
        tex2jax: {
        inlineMath: [
        ["$", "$"],
        ["\\(", "\\)"]
        ]
        },
        "HTML-CSS": {
        linebreaks: { automatic: true }
        }
        });





    </script>

    <script src="{{asset('admin/summernote-ext.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#texto,#option1,#option2,#option3').summernote({
                height: 400, // set editor height,
                toolbar: [
                    ['insert', ['equation', 'draw', 'hello']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['style', ['style', 'bold', 'italic', 'underline', 'strikethrough', 'clear', 'superscript', 'subscript']],
                    ['fonts', ['fontsize']],
                    ['color', ['color']],
                    ['undo', ['undo', 'redo']],
                    ['ckMedia', ['ckImageUploader', 'ckVideoEmbeeder']],
                    ['misc', ['link', 'picture', 'table', 'hr', 'codeview', 'fullscreen']],
                    ['height', ['lineheight']]
                ],
            });
            $('#ielts_form').submit(function (e) {
                e.preventDefault();
                var editordata = $('#texto').summernote('code');
                var test = $("#ielts_id option:selected");
                var slug = $("#slug").val();
                var option1 = $('#option1').summernote('code');
                var option2 = $('#option2').summernote('code');
                var option3 = $('#option3').summernote('code');
                var correct = $("#correct option:selected");
                if (test.val() == '') {
                    $('select#test_id').addClass('is-invalid');
                }
                if (option1 == " ") {
                    option1.addClass('is-invalid');
                }
                if (option2 == " ") {
                    option2.addClass('is-invalid');
                }
                if (option3 == " ") {
                    option3.addClass('is-invalid');
                }

                if (correct.val() == " ") {
                    $('select#correct').addClass('is-invalid');
                }
                if (
                    test.val() != " " &&
                    option1 != " " &&
                    option2 != " " &&
                    option3 != " " &&
                    correct.val() != " "
                ) {
                    var formData = new FormData(this);
                    formData.append('question', editordata);
                    formData.append('option1', option1);
                    formData.append('option2', option2);
                    formData.append('option3', option3);
                    formData.append('correct', correct.val());
                    formData.append('slug', slug);
                    $.ajax({
                        type: "POST",
                        url: "{{route('reading_store_question')}}",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data.status == true) {
                                // {{--var exam = '{{$exam->test_title_slug}}';--}}
                                // {{--window.location = `/coders/question/${exam}`;--}}
                                console.log(data);
                            }

                        }
                    });
                }
            })
        });
    </script>
@endsection

@extends('admin.base')
@section('content')
    <link href="{{asset('admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div id="alert"></div>
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h5>{{$exam[0]->test_title}} <span class="text-muted text-small">/ Sualların sayı - <span
                                                    class="total-question">{{$exam[0]->total_question}}</span></span>
                                        </h5>
                                        <a href="{{route('question_create',$exam[0]->id)}}"
                                           class="btn btn-info add_question">Sual əlavə edin.</a>
                                        <hr>
                                        <?php $color = ['info','warning','primary','danger','success'] ?>
                                        @foreach($group_subject as $key=>$subject)
                                            <a href="" class="btn btn-{{$color[$key]}}">{{$subject->subject}}
                                                - {{$subject->point}} bal </a>
                                        @endforeach
                                        <table id="question_table"
                                               class="table table-striped table-bordered dataTable hover">
                                            <thead>
                                            <tr>
                                                <th>Sual</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if (count($questions) > 0)
                                                @foreach ($questions as $question)
                                                    <tr id="{{ $question->id}}" class="question">
                                                        <td>{!! ($question->question_text) !!}</td>
                                                        <td>
                                                            <a href="{{route('question_show_detail',$question->id)}}"
                                                               class="btn btn-primary btn-sm">Bax</a>
                                                            <a href="{{route('question_edit',$question->id)}}"
                                                               class="btn btn-info btn-sm">Yenilə</a>
                                                            <a href="" class="btn btn-danger btn-sm">Sil</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="7">Melumat yoxdur</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
    <script src="{{asset('admin/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('admin/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#question_table').DataTable({
            responsive: true,
        });
        var questions = $('.question');
        var total_question = $('.total-question');
        if (questions.length == parseInt(total_question.text())) {
            $('.add_question').addClass('d-none');
        }
        $(document).on('click', '#question_table .btn-danger', function (e) {
            e.preventDefault();
            var id = $(this).parents('tr').attr('id');
            console.log(id);
            $.ajax({
                url: '{{route("question_delete")}}',
                type: "POST",
                data: {
                    "_token": "{{csrf_token()}}",
                    id: id
                },
                success: function (data) {
                    console.log(data);
                    if (data.status == true) {
                        var output = `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                             Silindi.
                                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                         <span aria-hidden="true">&times;</span>
                                     </button>
                                     </div>`;
                        $('#alert').html(output);
                        $('#' + id).remove();
                        setTimeout(function () {
                            $(".alert").alert('close')
                        }, 2000);
                        $('.options .btn').removeClass('d-none');
                        var questions = $('.question');
                        var total_question = $('.total-question');
                        if (questions.length != parseInt(total_question.text())) {
                            $('.add_question').removeClass('d-none');
                        }
                    }
                }
            })
        });
    </script>
@endsection

<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SubjectController extends Controller
{
    public function index()
    {
        $subjects = Subject::all();
        return view('admin.subject', compact('subjects'));
    }

    public function create(Request $request)
    {
        $subject_name = $request->subject;
        $subject = new Subject();
        $subject->subject = $subject_name;
        $subject->save();
        return response()->json(['status' => true, 'id' => $subject->id]);
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $subject = $request->subject;
        $update = DB::table('subjects')
            ->where('id', $id)
            ->update(['subject' => $subject]);

        return response()->json(['status' => true]);
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $delete = Subject::find($id);
        $delete->delete();
        return response()->json(['status' => true]);
    }
}

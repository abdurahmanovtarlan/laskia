@extends('front.base')
@section('content')
    @if (Route::has('login'))
        <section id="about" class="my-5">
            <div class="container">
                <header class="section-header">
                    <h3>Bizim haqqımızda</h3>
                    <p>Laskia.com Azərbaycanda online şəkilde təhsil və intellektual sahədə sınaqlar, yarışlar keçirən
                        bir platformadır.</p>
                </header>
                <div class="row about-container">
                    <div class="col-lg-6 content order-lg-1 order-2">
                        <p>
                            Abituriyentlərə özəl təşkil etdiyimiz "Online Sınaq" yarışısmasında ilk 3 yerin qaliblərinə
                            şirkətimiz tərəfindən pul mükafatı veriləcək.
                        </p>

                        <div class="icon-box wow fadeInUp">
                            <div class="icon"><i class="fa fa-shopping-bag"></i></div>
                            <p class="description">
                                Abituriyentlərə özəl təşkil etdiyimiz "Online Sınaq" yarışısmasında ilk 3 yerin
                                qaliblərinə şirkətimiz tərəfindən pul mükafatı veriləcək.
                                <span class="d-block font-weight-bold">1-ci yer   500 azn</span>
                                <span class="d-block font-weight-bold">2-ci yer   200 azn</span>
                                <span class="d-block font-weight-bold">3-ci yer   100 azn</span>

                            </p>
                        </div>

                        <div class="icon-box wow fadeInUp " data-wow-delay="0.2s">
                            <div class="icon"><i class="fa fa-bar-chart"></i></div>
                            <p class="description mb-5 pt-3 ">
                                Yüksək nəticə göstərmiş hər bir şəxsə sertifikat təqdim olunacaq.
                            </p>
                        </div>

                        <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
                            <div class="icon"><i class="fa fa-trophy" aria-hidden="true"></i></div>

                            <p class="description pt-2">
                                Qaliblərdən başqa digər hər bir abituriyentin nəticələri fərdi olaraq saxlanacaq və heç
                                bir yerdə paylaşılmayacaq.
                            </p>
                        </div>
                        <hr>
                        @auth
                            @if($ticket_code == '')
                                <a href=""
                                   class="btn btn-danger wow fadeInUp d-block shadow exam">Test tapılmadı</a>
                            @elseif($ilets == false)
                                <a href="/exam/{{\Illuminate\Support\Facades\Auth::user()->ticket_code}}/"
                                   class="btn btn-warning wow fadeInUp d-block shadow exam disabled">İmtahan
                                    gözlənilir...</a>
                            @elseif($ilets == true)
                                <a href="/ielts/{{\Illuminate\Support\Facades\Auth::user()->ticket_code}}/"
                                   class="btn btn-warning wow fadeInUp d-block shadow exam disabled">İmtahan
                                    gözlənilir...</a>
                            @endif
                        @endauth
                        @guest
                            <a href="{{route('login')}}" class="btn btn-warning wow fadeInUp d-block  shadow exam">Testə
                                başla <i
                                    class="fa fa-angle-right"></i></a>
                        @endguest
                    </div>
                    <div class="col-lg-6 background order-lg-2 order-1 wow fadeInUp">
                        <img src="{{asset('front/img/exams2.svg')}}" class="img-fluid " alt="" />
                    </div>
                </div>
            </div>
        </section>
    @endif
    <script src="{{asset('front/js/time.js')}}"></script>

@endsection

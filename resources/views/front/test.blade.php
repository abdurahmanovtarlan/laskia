@extends('front.base')
@section('content')
    <link rel="stylesheet" href="{{asset('front/css/timer.css')}}">
    <script src="{{asset('front/js/timer.js')}}"></script>
    <div class="container my-5">
        <div class="row my-5">
            <div class="col-md-8 mt-5">
                <div class="card question-card border-top mb-2">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <h3 class="question-name m-0">{{$exams[0]->test_title}}</h3>
                        <span class="badge badge-primary p-2">{{$exams[0]->total_question}} sual</span>
                    </div>
                </div>
                <div class="card question-card mb-2">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <h5 class="question-name m-0">Suallar</h5>
                    </div>
                </div>
                <form method="POST" action="{{ route('test_store') }}" id="test_form">
                    @csrf
                    <input type="hidden" name="test_id" value="{{ $exams[0]->id }}">
                    <input type="hidden" id="all_time" name="all_time" value="{{$exams[0]->timer}}">
                    <input type="hidden" id="clock" name="clock">
                    <input type="hidden" id="minute" name="minute">
                    <input type="hidden" id="second" name="second">
                    <?php $i = 1;
                    $mass = ['a', 'b', 'c', 'd', 'e'];
                    ?>
                    @foreach($questions as $question)
                        <div class="card question-card mb-2">
                            <div class="card-body">
                                <div class="question ml-2 ">
                                    {!! ($question->question_text) !!}
                                </div>
                                @if($question->question_image != '')
                                    <br>
                                    <img src="{{url('question_image/'.$question->question_image)}}" alt="">
                                @endif
                                <input type="hidden" name="questions[{{ $i }}]" value="{{ $question->id }}">
                                <div class="answers">
                                    @foreach($question->options as $key=>$option)
                                        <div class="form-check my-2">
                                            <label class="form-check-label ml-1 py-0" for="{{ $option->id }}">
                                                <input type="radio" class="form-check-input" id="{{ $option->id }}"
                                                       name="answers[{{ $question->id }}]" value="{{ $option->id }}">
                                                <span
                                                    class="checkmark d-flex justify-content-center align-items-center">{{$mass[$key]}}</span>
                                                <span class="text">
                                                    {!! $option->option  !!}</span>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <?php $i++; ?>
                    @endforeach
                </form>
                <button type="submit" class="btn btn-block submit btn-success rounded shadow">Təsdiqlə</button>
            </div>
            <div class="col-md-4 mt-5">
                <div id="DateCountdown" data-timer="{{$exams[0]->timer}}" style="width: 100%;"></div>
                <button class="btn btn-secondary">Saat</button>
                <hr>
            </div>
        </div>
    </div>
    <script src="{{asset('front/js/bootbox.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="{{asset('front/css/timer.css')}}">
    <script src="{{asset('front/js/timer.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.15.2/dist/sweetalert2.all.min.js"></script>
    <script src="{{asset('front/js/test.js')}}"></script>
@endsection

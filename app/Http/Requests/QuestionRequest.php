<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'test_id'           => 'required',
            'question_text'      => 'required',
            'option1' => 'required',
            'option2' => 'required',
            'option3' => 'required',
            'option4' => 'required',
            'option5' => 'required',
            'correct' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'test_id.required' => 'Testin adını daxil edin.',
            'question_text.required'  => 'Sual daxil edin.',
            'option1.required'  => 'Variant daxil edin.',
            'option2.required'  => 'Variant daxil edin.',
            'option3.required'  => 'Variant daxil edin.',
            'option4.required'  => 'Variant daxil edin.',
            'option5.required'  => 'Variant daxil edin.',
            'correct.required'  => 'Düzgün cavabı seçin.',
        ];
    }
}

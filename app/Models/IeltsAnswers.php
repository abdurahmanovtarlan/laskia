<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IeltsAnswers extends Model
{
    //
    protected $guarded = [];

    protected $fillable = ['user_id', 'ielts_id', 'listening_id', 'reading_id', 'writing_id', 'question_id', 'option_id', 'correct', 'text_answer'];

    public function question()
    {
        return $this->belongsTo(IeltsQuestions::class, 'question_id');
    }
}

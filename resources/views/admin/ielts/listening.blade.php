@extends('admin.newbase')
@section('content')
    <link href="{{asset('admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div id="alert"></div>
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <a class="btn btn-warning" data-toggle="collapse" href="#collapseExample"
                                           role="button" aria-expanded="false" aria-controls="collapseExample">
                                            Vaxt əlavə edin.
                                        </a>
                                        <a href="{{route('listening_create_audio',$ielts[0]->ielts_name_slug)}}"
                                           class="btn btn-info add_question shadow">Audio əlavə edin.</a>
                                        <a href="{{route('listening_create_question',$ielts[0]->ielts_name_slug)}}"
                                           class="btn btn-warning add_question shadow ">Variant sual əlavə edin.</a>
                                        <a href="{{route('listening_create_text_question',$ielts[0]->ielts_name_slug)}}"
                                           class="btn btn-info add_question shadow ">Mətn sual əlavə
                                            edin.</a>

                                        <div class="collapse " id="collapseExample">
                                            <div class="card  mt-4">
                                                <input type="hidden" value="{{$ielts[0]->id}}" id="ielts_id">
                                                <input type="hidden" value="{{$ielts[0]->ticket_id}}"
                                                       id="ticket_id">
                                                <input type="number" id="listening_time" class="form-control mb-2"
                                                       placeholder="Vaxt...">
                                                <div class="invalid-feedback font-weight-bold ">
                                                    Vaxt daxil edin.
                                                </div>
                                                <button class="time_Btn btn btn-success mt-2" type="submit">Saxla
                                                </button>
                                            </div>
                                        </div>
                                        <div class="listening_time">
                                            @if(count($ieltslistening_time) > 0)
                                                <div class="card border ">
                                                    <div class="card-body">
                                                        Vaxt :
                                                        <span>{{$ieltslistening_time[0]->ielts_listening_time}}</span>
                                                        dəqiqə
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <hr>
                                        <table id="question_table"
                                               class="table table-striped table-bordered dataTable hover">
                                            <thead>
                                            <tr>
                                                <th>Audio</th>
                                                <th>Sual</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if (count($ieltsquestions) > 0)
                                                @foreach ($ieltsquestions as $listen)
                                                    <tr id="{{ $listen->id}}" class="question">
                                                        <td>
                                                            <audio controls class="shadow" style="border-radius: 120px">
                                                                <source
                                                                    src="{{url('audio/'.$listen->audio_file)}}"
                                                                    type="audio/mpeg">
                                                            </audio>
                                                        </td>
                                                        <td>{!! $listen->question !!}</td>
                                                        <td>
                                                            <a href=""
                                                               class="btn btn-primary btn-sm">Bax</a>

                                                            <a href="" class="btn btn-danger btn-sm">Sil</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="7">Melumat yoxdur</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
    <script src="{{asset('admin/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('admin/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script>
        $('.time_Btn').click(function () {
            var listening_time = $('#listening_time');
            var ielts_id = $('#ielts_id');
            var ticket_id = $('#ticket_id');
            if (listening_time.val() != '') {
                $.ajax({
                    type: "POST",
                    url: "{{route('listening_time')}}",
                    data: {
                        "_token": "{{csrf_token()}}",
                        'listening_time': listening_time.val(),
                        'ielts_id': ielts_id.val(),
                        'ticket_id': ticket_id.val(),
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.status == true) {
                            $('.collapse').removeClass('show');
                            if ($('.listening_time').val() == '') {
                                $('.listening_time').html(`<div class="card border ">
                                                    <div class="card-body">
                                                        Vaxt : <span>${listening_time.val()}</span> dəqiqə
                                                    </div>
                                                </div>`);
                            } else {
                                $('.listening_time span').html(listening_time.val());
                            }
                            listening_time.val('');
                        }
                    }
                });
            } else {
                listening_time.addClass('is-invalid');
            }
        })
       
    </script>
@endsection

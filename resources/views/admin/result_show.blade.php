@extends('admin.newbase')
@section('content')
                <div class="card-box" style="margin-bottom:50px;">
                    <div class="card-body">
                        <h5 class="card-title">İmtahan adı</h5>
                        <div class="table-responsive">
                            <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="zero_config" class="data-table table stripe hover  dataTable no-footer dtr-inline"
                                               role="grid" aria-describedby="zero_config_info">
                                            <thead>
                                            <tr>
                                                <th>№</th>
                                                <th>Ad</th>
                                                <th>Soyad</th>
                                                <th>Telefon nömrə</th>
                                                <th>Düz suallar</th>
                                                <th>Cavabsız suallar</th>
                                                <th>Sehv suallar</th>
                                                <th>Faiz göstəricisi</th>
                                                <th>İmtahan bitirmə vaxtı</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php $i = 1; @endphp
                                            @foreach($passtests as $test)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td> {{$test->name}}</td>
                                                    <td>
                                                        {{$test->surname}}
                                                    </td>
                                                    <td>
                                                        {{$test->phone}}
                                                    </td>

                                                    <td>
                                                        {{$test->result}}
                                                    </td>
                                                    @php $j = 0;$i=0; @endphp
                                                    <td>
                                                        @foreach($option_mass as $mass)
                                                            @if($mass->user_id == $test->user_id)
                                                                @php ++$i @endphp
                                                            @endif
                                                        @endforeach
                                                        {{$i}}
                                                    </td>
                                                    <td>
                                                        @foreach($questions as $question)
                                                            @if($question->test_id == $test->test_id)
                                                                @php ++$j @endphp
                                                            @endif
                                                        @endforeach
                                                        {{abs($j - $i - $test->result)}}
                                                    </td>

                                                    <td>
                                                        {{round((100 * $test->result)/$j,2)}}%
                                                    </td>
                                                    <td>
                                                        {{$test->time}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@extends('front.base')
@section('content')
    <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
    />
    <div class="container my-5">
        <div class="row justify-content-center ">
            <div class="col-md-8 mt-5">
                <h4 class="mt-5 text-center">{{$ticket[0]->ticket_name}}</h4>
                @foreach($exams as $exam)
                    <div class="card shadow mb-3">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-content-center">
                                <span class="font-weight-bold">{{$exam->test_title}} <span class="text-muted">/ {{$exam->total_question}} sual / {{$exam->timer/60}} dəqiqə vaxt</span></span>
                                @if($end_exam->isEmpty())
                                    <a href="" class="btn rounded btn-outline-primary disabled exam_ticket">İmtahan
                                        gözlənilir...</a>
                                @else
                                    {{--                                    /test/{{$exam->test_title_slug}}--}}
                                    <a href="{{route('result_show',[$result[0]->id,\Illuminate\Support\Str::random(30)])}}"
                                       class="btn rounded btn-outline-danger">Test bitib</a>
                                @endif

                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.15.2/dist/sweetalert2.all.min.js"></script>

    <script>
        var text = `<ol>
        <li style="font-size: 16px; text-align: left;">
            İlk 3 yeri tutan iştirakçılar cavablarının təsdiqi üçün  yoxlanacaqlar, onlara suallar veriləcək və imtahanda tam şəffaf şəkildə iştirak etdikləri məlum olduqda qaliblikləri təsdiq olunacaq.
        </li>
        <li style="font-size: 16px;text-align: left;">
            Sualların hamısına düzgün cavab vermiş eyni sayda bir neçə iştirakçımız olarsa, ekspertlərimizin yoxlamasından sonra digərlərinə görə ən tez imtahanı yekunlaşdırmış şəxs qalib olacaq.
        </li>
        <li style="font-size: 16px;text-align: left;">
            Köçürmək və əlavə resurslardan istifadə etmək qəti qadağandır, bu halda cavablar ləğv olunacaq.
        </li>
        <li style="font-size: 16px;text-align: left;">
            Ödənilən bilet məbləği heç bir halda geri qaytarılmır.
        </li>
        <li style="font-size: 16px;text-align: left;">
            Yalnız bir dəfə imtahan verə bilərsiniz.
        </li>
    </ol>
<hr>
    <p style="font-size: 14px; font-weight: bold">Mükafatların və sertifikatların verilməsi 14 iyulda keçiriləcək.</p>
<p style="font-size: 14px; font-weight: bold"> Şərtləri oxudum, şərtlərlə razıyam, heç bir iradım, iddiam və şikayətim yoxdur, qəbul edirəm.</p>
`;
        $('.btn-outline-primary').click(function (e) {
            e.preventDefault();
            Swal.fire({
                title: 'İmtahanda iştirak üçün vacib olan şərtlər',
                html: text,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#1ba94c',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Razıyam',
                cancelButtonText: 'Bağla',
                showClass: {
                    popup: 'animate__animated animate__bounceIn'
                },
                hideClass: {
                    popup: 'animate__animated animate__bounceOut'
                }
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        title: 'İmtahan başladılır',
                        timer: 1000,
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                                const content = Swal.getContent()
                                if (content) {
                                    const b = content.querySelector('b')
                                    if (b) {
                                        b.textContent = Swal.getTimerLeft()
                                    }
                                }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval);
                            window.location = `/test/{{$exam->test_title_slug}}`;

                        }
                    })
                }
            })
        })

    </script>
    <script src="{{asset('front/js/time.js')}}"></script>
@endsection

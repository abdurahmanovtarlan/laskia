<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>

    <title>Test.az</title>
    <meta content="" name="descriptison"/>
    <meta content="" name="keywords"/>

    <!-- Favicons -->
    <link href="{{asset('favicon.png')}}" rel="icon"/>

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,700"
        rel="stylesheet"
    />

    <!-- Vendor CSS Files -->
    <link
        href="{{asset('front/assets/vendor/bootstrap/css/bootstrap.min.css')}}"
        rel="stylesheet"
    />
    <link
        href="{{asset('front/assets/vendor/font-awesome/css/font-awesome.min.css')}}"
        rel="stylesheet"
    />
    <link href="{{asset('front/assets/vendor/animate.css/animate.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('front/assets/vendor/ionicons/css/ionicons.min.css')}}" rel="stylesheet"/>
    <link
        href="{{asset('front/assets/vendor/owl.carousel/assets/owl.carousel.min.css')}}"
        rel="stylesheet"
    />
    <link href="{{asset('front/assets/vendor/venobox/venobox.css')}}" rel="stylesheet"/>

    <!-- Template Main CSS File -->
    <link href="{{asset('front/assets/css/style.css')}}" rel="stylesheet"/>
    <link href="{{asset('front/css/style.css')}}" rel="stylesheet"/>
    <!-- Vendor JS Files -->
    <script src="{{asset('front/assets/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('front/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('front/assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
    <script src="{{asset('front/assets/vendor/php-email-form/validate.js')}}"></script>
    <script src="{{asset('front/assets/vendor/counterup/counterup.min.js')}}"></script>
    <script src="{{asset('front/assets/vendor/mobile-nav/mobile-nav.js')}}"></script>
    <script src="{{asset('front/assets/vendor/wow/wow.min.js')}}"></script>
    <script src="{{asset('front/assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('front/assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('front/assets/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('front/assets/vendor/venobox/venobox.min.js')}}"></script>

    <!-- Template Main JS File -->
    <script src="{{asset('front/assets/js/main.js')}}"></script>
    <!-- =======================================================
  * Template Name: NewBiz - v2.0.0
  * Template URL: https://bootstrapmade.com/newbiz-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
<nav id="navbar" class="navbar navbar-expand-lg navbar-light fixed-top">
    <div class="container px-3">
            <a href="/" class="logo"
            >
                <i class="fa fa-leanpub"></i> LASKIA</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse main-nav" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item {{ Request::is('/') ? 'active' : '' }}"><a href="/" class="nav-link">Əsas</a></li>
                @if (Route::has('login'))
                    @auth
                        @if(\Illuminate\Support\Facades\Auth::id() == 1)
                            <script>
                                window.location.href = '/coders/ticket';
                            </script>
                        @endif
                        <li class="nav-item {{ Request::is('results') ? 'active' : '' }}"><a
                                href="{{route('result_all')}}" class="nav-link">Nəticələr</a>
                        </li>
                        <li class="drop-down nav-item">
                            <a href="">{{\Illuminate\Support\Facades\Auth::user()->name}}</a>
                            <ul>
                                <li><a href="/main/{{\Illuminate\Support\Facades\Auth::id()}}">Profil</a></li>
                                <li><a href="{{ route('logout') }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Çıxış</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;"> @csrf </form>
                                </li>
                            </ul>
                        </li>
                    @else
                        <li class="nav-item ">
                            <a href="{{ route('login') }}" class="nav-link">Daxil ol</a>
                        </li>
                        @if (Route::has('register'))

                            <li class="nav-item ">
                                <a href="{{ route('register') }}" class="nav-link">Qeydiyyat</a>
                            </li>
                        @endif
                    @endauth
                @endif
            </ul>
        </div>
    </div>
</nav>
<main id="main">
    @yield('content')
</main>
<!-- End #main -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>


</body>
</html>

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ielts;
use App\Models\IeltsAnswers as ModelsIeltsAnswers;
use App\Models\IeltsListening;
use App\Models\IeltsReading;
use App\Models\IeltsWriting;
use App\Models\Test;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class IeltsAnswers extends Controller
{
    public function index()
    {
        $ieltsresults = DB::table('ielts')->orderByDesc('id')->get();
        return view('admin.ielts_results', compact('ieltsresults'));
    }
    public function ielts_result_show($slug)
    {
        $passtests = DB::table('tests')->select('*', 'tests.id as result_id', 'users.id as user_id')
            ->leftJoin('users', 'users.id', '=', 'tests.user_id')
            ->leftJoin('ielts', 'ielts.id', '=', 'tests.ielts_id')
            // ->where('ielts.ielts_name_slug', '=', $slug)
            ->get();
        // if (Auth::user()) {
        //     $results = $results->where('user_id', '=', Auth::id());
        // }
        // dd($passtests);
        return view('admin.ielts_results_users', compact('passtests'));
    }
    public function ielts_result_user_answer($slug, $user_id)
    {
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        $listening = IeltsListening::where('ielts_id', $ielts[0]->id)->get();
        $reading = IeltsReading::where('ielts_id', $ielts[0]->id)->get();
        $writing = IeltsWriting::where('ielts_id', $ielts[0]->id)->get();
        // $listeninganswers_text = DB::table('ielts_answers')
        //     ->leftJoin('ielts_questions', 'ielts_questions.listening_id', '=', 'ielts_answers.listening_id')
        //     ->where('user_id', $user_id)
        //     ->where('ielts_answers.ielts_id', $ielts[0]->id)
        //     ->where('ielts_answers.listening_id', $listening[0]->id)
        //     ->where('option_id', Null)
        //     ->get();


        $listeninganswers_text = ModelsIeltsAnswers::where('user_id', $user_id)
            ->whereNull('option_id')
            ->where('listening_id', $listening[0]->id)
            ->with('question')
            ->with('question.options')
            ->get();


        $listening_question_options = ModelsIeltsAnswers::where('user_id', $user_id)
            ->whereNotNull('option_id')
            ->where('listening_id', $listening[0]->id)
            ->with('question')
            ->with('question.options')
            ->get();



        return view('admin.ielts_result_user_answer', compact('listening_question_options', 'listeninganswers_text'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{
    public function index(){
        $groups = Group::all();
        return view('admin.group', compact('groups'));
    }
    public function create(Request $request)
    {
        $group_name =  $request->group_name;
        $group = new Group();
        $group->group = $group_name;
        $group->save();
        return response()->json(['status' => true, 'id' => $group->id]);
    }
    public function update(Request $request)
    {
        $id = $request->id;
        $group = $request->group;
        $update = DB::table('groups')
            ->where('id', $id)
            ->update(['group' => $group]);

        return response()->json(['status' => true]);
    }
    public function delete(Request $request)
    {
        $id = $request->id;
        $delete = Group::find($id);
        $delete->delete();
        return response()->json(['status' => true]);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionsOption extends Model
{
    protected $fillable = ['option', 'correct', 'question_id'];
    public function question()
    {
        return $this->belongsTo(Questions::class, 'question_id');
    }
}

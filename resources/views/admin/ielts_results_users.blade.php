@extends('admin.newbase')
@section('title')
Laskia.com Ielts Exam Users
@endsection
@section('content')
<div class="card-box">
  <table class="table hover multiple-select-row data-table-export nowrap dataTable no-footer dtr-inline">
    <thead>
      <tr>
        <th>№</th>
        <th>Ad</th>
        <th>Soyad</th>
        <th>Telefon nömrə</th>
        <th>Cavablar</th>
      </tr>
    </thead>
    <tbody>
      @php $i = 1; @endphp
      @foreach($passtests as $test)
      <tr>
        <td>{{$i++}}</td>
        <td> {{$test->name}}</td>
        <td>
          {{$test->surname}}
        </td>
        <td>
          {{$test->phone}}
        </td>
        <td>
          <a href="{{route('ielts_result_user_answer',[$test->ielts_name_slug,$test->user_id])}}"><i class="dw dw-eye"></i> Bax</a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
<script src="{{asset('newadmin/src/plugins/datatables/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('newadmin/src/plugins/datatables/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('newadmin/src/plugins/datatables/js/buttons.print.min.js')}}"></script>
<script src="{{asset('newadmin/src/plugins/datatables/js/buttons.html5.min.js')}}"></script>

<script src="{{asset('newadmin/src/plugins/datatables/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('newadmin/src/plugins/datatables/js/pdfmake.min.js')}}"></script>
<script src="{{asset('newadmin/src/plugins/datatables/js/vfs_fonts.js')}}"></script>
@endsection
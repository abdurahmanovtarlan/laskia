<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Daxil ol</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css"
          integrity="sha256-h20CPZ0QyXlBuAw7A+KluUYx/3pK+c7lYEpqLTlxjYQ=" crossorigin="anonymous"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css"/>

          <link
        href="{{asset('front/assets/vendor/bootstrap/css/bootstrap.min.css')}}"
        rel="stylesheet"
    />
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{{asset('front/css/style.css')}}"/>
    <link rel="stylesheet" href="{{asset('front/assets/css/style.css')}}"/>
</head>

<body class="bg-gray">
<div class="container">
    <div class="row justify-content-center align-items-center mt-3" style="height: 80vh;">
        <div id="form-layout" class="mt-5">
            <div class="form-header text-center">
                <h2>Qeydiyyat</h2>
                
                <img src="{{asset('front/img/login.svg')}}" class="logo" alt="" style="width:50% !important; ">
            </div>
            <form class="form-group" method="POST" action="{{ route('register') }}" id="register-form">
                @csrf
                <div class="input-group mt-4 @error('name') is-invalid @enderror">
                    <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default"><svg
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14" aria-hidden="true"
                                    role="img">
                                    <path
                                        d="M7 8c-3.314 0-6 1.85-6 3.297v2.027c0 .373.358.676.8.676h10.4c.442 0 .8-.303.8-.676v-2.027C13 9.85 10.314 8 7 8zm3-5a3 3 0 11-6 0 3 3 0 016 0z"></path>
                                </svg></span>
                    </div>
                    <input id="name" type="text" name="name" class="form-control" value="{{ old('name') }}"
                           placeholder="Ad" aria-describedby="inputGroup-sizing-default"/>
                </div>
                @error('name')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="input-group my-2 @error('surname') is-invalid @enderror">
                    <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default"><svg
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14" aria-hidden="true"
                                    role="img">
                                    <path
                                        d="M7 8c-3.314 0-6 1.85-6 3.297v2.027c0 .373.358.676.8.676h10.4c.442 0 .8-.303.8-.676v-2.027C13 9.85 10.314 8 7 8zm3-5a3 3 0 11-6 0 3 3 0 016 0z"></path>
                                </svg></span>
                    </div>
                    <input id="surname" type="text" name="surname" class="form-control" value="{{ old('surname') }}"
                           placeholder="Soyad" aria-describedby="inputGroup-sizing-default"/>
                </div>
                @error('surname')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="input-group mt-2 @error('phone') is-invalid @enderror">
                    <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="35px"
                                     height="35px" viewBox="0 0 35 35" style="enable-background:new 0 0 35 35;"
                                     xml:space="preserve">
                                    <g>
                                        <path d="M25.302,0H9.698c-1.3,0-2.364,1.063-2.364,2.364v30.271C7.334,33.936,8.398,35,9.698,35h15.604
		c1.3,0,2.364-1.062,2.364-2.364V2.364C27.666,1.063,26.602,0,25.302,0z M15.004,1.704h4.992c0.158,0,0.286,0.128,0.286,0.287
		c0,0.158-0.128,0.286-0.286,0.286h-4.992c-0.158,0-0.286-0.128-0.286-0.286C14.718,1.832,14.846,1.704,15.004,1.704z M17.5,33.818
		c-0.653,0-1.182-0.529-1.182-1.183s0.529-1.182,1.182-1.182s1.182,0.528,1.182,1.182S18.153,33.818,17.5,33.818z M26.021,30.625
		H8.979V3.749h17.042V30.625z"/>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                            </span>
                    </div>
                    <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}"
                           autocomplete="phone" placeholder="Telefon">
                </div>
                @error('phone')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="input-group mt-2 @error('ticket_code') is-invalid @enderror">
                    <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">
                              <svg id="Capa_1" enable-background="new 0 0 512 512" height="35" viewBox="0 0 512 512"
                                   width="35" xmlns="http://www.w3.org/2000/svg"><g><path
                                          d="m30 30h53.857v-30h-83.857v83.857h30z"/><path
                                          d="m428.143 0v30h53.857v53.857h30v-83.857z"/><path
                                          d="m30 428.143h-30v83.857h83.857v-30h-53.857z"/><path
                                          d="m482 482h-53.857v30h83.857v-83.857h-30z"/><path
                                          d="m68.857 236.571h167.714v-167.714h-167.714zm30-137.714h107.714v107.714h-107.714z"/><path
                                          d="m443.143 275.429h-167.714v167.714h167.714zm-137.714 30h107.714v38.857h-68.856v68.856h-38.857v-107.713zm68.857 107.714v-38.856h38.856v38.856z"/><path
                                          d="m206.571 344.286h-38.857v-68.857h-98.857v167.714h167.714v-152.714h-30zm0 68.857h-107.714v-107.714h38.857v68.857h68.857z"/><path
                                          d="m137.714 137.714h30v30h-30z"/><path
                                          d="m305.429 98.857h38.857v68.856h68.856v38.857h-137.705v30h167.705v-167.713h-167.713v98.643h30zm68.857 0h38.856v38.856h-38.856z"/></g></svg>
                            </span>
                    </div>
                    <input id="ticket_code" type="text" class="form-control" name="ticket_code"
                           value="{{ old('ticket_code') }}"
                           autocomplete="ticket_code" placeholder="Bilet kodu">
                </div>
                <div class="mt-2 ml-1 small text-muted">Bilet kodu bir dəfə daxil edilə bilər</div>

                @error('ticket_code')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                </span>
                @enderror

                <div class="input-group mt-2 @error('password') is-invalid @enderror">
                    <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14" aria-hidden="true"
                                     role="img">
                                    <path
                                        d="M4.5 4.657C4.5 3.192 5.621 2 7 2s2.5 1.19 2.5 2.656V6h-5V4.657zM11.5 6V4.657C11.5 2.09 9.481 0 7 0S2.5 2.09 2.5 4.657V6h-.992C1.228 6 1 6.23 1 6.5v7c0 .276.229.5.5.5h11c.276 0 .5-.231.5-.5v-7a.5.5 0 00-.508-.5H11.5z"></path>
                                </svg>
                            </span>
                    </div>
                    <input name="password" type="password" class="form-control" id="password" placeholder="Şifrə"/>
                    <div class="input-group-append">
                            <span class="input-group-text password">
                                <i class="far fa-eye"></i>
                            </span>
                    </div>
                </div>
                @error('password')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div id="password-strength-status"></div>
                <div class="input-group mt-2">
                    <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14" aria-hidden="true"
                                     role="img">
                                    <path
                                        d="M4.5 4.657C4.5 3.192 5.621 2 7 2s2.5 1.19 2.5 2.656V6h-5V4.657zM11.5 6V4.657C11.5 2.09 9.481 0 7 0S2.5 2.09 2.5 4.657V6h-.992C1.228 6 1 6.23 1 6.5v7c0 .276.229.5.5.5h11c.276 0 .5-.231.5-.5v-7a.5.5 0 00-.508-.5H11.5z"></path>
                                </svg>
                            </span>
                    </div>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                           autocomplete="new-password" placeholder="Təkrar şifrə">
                </div>
                <div class="invalid-feedback"></div>

                <div class="form-group mt-2">
                    <button type="submit" class="btn btn-success btn-block rounded-0 shadow register-btn">Qeydiyyat
                    </button>
                </div>
                <div class="form-group mt-2">
                    <a class="btn btn-primary btn-block rounded-0 shadow" href="{{ route('login') }}">Daxil ol</a>
                </div>

            </form>
        </div>
    </div>
</div>
<script src="{{asset('admin/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js')}}"></script>
<script>
    $("#phone").inputmask("(+99488) 888-88-88");
    $('.password').click(function () {
        $(this).find('i').toggleClass('fa-eye-slash');
        var password_input = $(this).parent().prev();

        if (password_input[0].type === "password") {
            password_input[0].type = "text";
        } else {
            password_input[0].type = "password";
        }
    });
    if ($("#password-strength-status").html() == '') {
        $('#password-strength-status').css('display', 'none');
    }
    $('#password').keyup(function () {
        var number = /([0-9])/;
        var alphabets = /([a-zA-Z])/;
        var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
        if ($('#password').val().length < 6) {
            $('#password-strength-status').removeClass();
            $('#password-strength-status').addClass('weak-password');
            $('#password-strength-status').html("Zəif (6-dan artıq simvol olmalıdır.)");
        } else {
            if ($('#password').val().match(number) && $('#password').val().match(alphabets) && $('#password').val().match(special_characters)) {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('strong-password');
                $('#password-strength-status').html("Güclüdür");
            } else {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('medium-password');
                $('#password-strength-status').html("Orta (əlifbalar, nömrələr və xüsusi simvol daxil edilməlidir.)");
            }
        }
        if ($("#password-strength-status").html() == '') {
            $('#password-strength-status').css('display', 'none');
        } else {
            $('#password-strength-status').css('display', 'block');
        }
    });
</script>
</body>

</html>

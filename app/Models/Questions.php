<?php

namespace App\Models;

use App\Models\Exam;
use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    protected $fillable = ['test_id', 'question_text', 'question_image', 'answer_explanation'];

    public function exam()
    {
        return $this->belongsTo(Exam::class, 'test_id');
    }
    public function options()
    {
        return $this->hasMany(QuestionsOption::class, 'question_id');
    }
}

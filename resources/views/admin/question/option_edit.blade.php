@extends('admin.base')
@section('content')
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="{{asset('admin/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('admin/ckeditor/samples/js/sample.js')}}"></script>
    <link rel="stylesheet" href="{{asset('admin/ckeditor/samples/css/samples.css')}}"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div id="alert"></div>
            <div class="card">
                <div class="card-body">
                    <form action="{{route('question_option_update',$questions_option->id)}}" method="POST" id="option_form">
                        @csrf
                        <div class="form-group">
                            <label for="option" class="font-weight-bold">Option <span class="text-danger">*</span></label>
                            <textarea type="text" id="option" class="form-control"name="option">{{$questions_option->option}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="option" class="font-weight-bold">Testin adı <span class="text-danger">*</span></label>
                            <select name="correct" id="correct" class="form-control">
                                {!! $questions_option->correct == 1 ? '<span class="text-success">Düz</span>' : '<span class="text-danger">Yanlış</span>' !!}
                                <option value="1" @if($questions_option->correct == 1) selected @endif >Düz</option>
                                <option value="0" @if($questions_option->correct == 0) selected @endif >Yanlış</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-success">Yenilə</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.min.js"></script>
    <script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML"></script>
        
    <script type="text/x-mathjax-config">
        MathJax.Hub.Config({
        extensions: ["tex2jax.js"],
        jax: ["input/TeX", "output/HTML-CSS"],
        tex2jax: {
        inlineMath: [
        ["$", "$"],
        ["\\(", "\\)"]
        ]
        },
        "HTML-CSS": {
        linebreaks: { automatic: true }
        }
        });
    </script>

    <script src="{{asset('admin/summernote-ext.js')}}"></script>
    <script>
      $(document).ready(function () {
            CKEDITOR.plugins.addExternal(
                "ckeditor_wiris",
                "./node_modules/@wiris/mathtype-ckeditor4/",
                "plugin.js"
            );
            var editor = CKEDITOR.replace("option", {
                extraPlugins: "ckeditor_wiris",
            });
            $('#option_form').submit(function (e) {
                    e.preventDefault();
                    var editordata = editor.getData();
                    console.log(editordata)
                    var correct = $("#correct option:selected");
                    var formData = new FormData(this);
                    formData.append('option', editordata);
                    formData.append('correct', correct.val());
                    $.ajax({
                        type: "POST",
                        url: "{{route('question_option_update',$questions_option->id)}}",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            // if (data.status == true) {
                            //     window.location = `/coders/question_show/${data.id}/edit`;
                            // }
                        }
                    });
            })
        });

    </script>
@endsection

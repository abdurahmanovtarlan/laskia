<?php

namespace App\Http\Controllers;

use App\Models\Exam;
use App\Models\Group;
use App\Models\GroupSubject;
use App\Models\Test;
use App\Models\Ticket;
use App\ModelsTicket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ExamController extends Controller
{
    public function index()
    {
        $tests = DB::table('exams')->select('*', 'exams.id as exam_id', 'tickets.id as ticketId')
            ->leftJoin('tickets', 'tickets.id', '=', 'exams.ticket_id')
//            ->leftJoin('groups','groups.id','=','exams.group_id')
            ->where('admin_id', '=', Auth::id())->orderByDesc('exams.id')->get();
        $tickets = Ticket::all();
        $groups = Group::all();
        $data['tests'] = $tests;
        $data['groups'] = $groups;
        $data['tickets'] = $tickets;
        return view('admin.exam', $data);
    }

    public function create(Request $request)
    {
        $test_title = $request->test_title;
        //$group_id =  $request->group_id;
        $total_question = $request->total_question;
        $timer = $request->time;
        $ticket_id = $request->ticket_id;
        $admin_id = Auth::id();
        $exam = new Exam();
        $exam->test_title = $test_title;
        $exam->admin_id = $admin_id;
        //$exam->group_id = $group_id;
        $exam->total_question = $total_question;
        $exam->timer = $time * 60;
        $exam->ticket_id = $ticket_id;
        $exam->test_title_slug = Str::slug($test_title, Str::random(10));
        $exam->save();
        return response()->json(['status' => true, 'id' => $exam->id, 'slug' => $exam->test_title_slug]);
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $test_title = $request->test_title;
        $ticket_id = $request->ticket_id;
        $total_question = $request->total_question;
        $time = 60 * ($request->time);

        $update = DB::table('exams')
            ->where('id', $id)
            ->update(['test_title' => $test_title, 'ticket_id' => $ticket_id,'timer' => $time, 'total_question' => $total_question, 'test_title_slug' => Str::slug($test_title, '-')]);

        return response()->json(['status' => true]);
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $delete = Exam::find($id);
        $delete->delete();
        return response()->json(['status' => true]);
    }
}

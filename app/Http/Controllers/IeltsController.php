<?php

namespace App\Http\Controllers;

use App\Models\Ielts;
use App\Models\IeltsListening;
use App\Models\IeltsOption;
use App\Models\IeltsQuestions;
use App\Models\IeltsReading;
use App\Models\IeltsWriting;
use App\Models\Questions;
use App\Models\QuestionsOption;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class IeltsController extends Controller
{
    public function index()
    {
        $ielts = DB::table('ielts')->select('*', 'ielts.id as ielts_id', 'tickets.id as ticketId')
            ->leftJoin('tickets', 'tickets.id', '=', 'ielts.ticket_id')
            ->orderByDesc('ielts.id')->get();
        $tickets = Ticket::all();

        return view('admin.ielts.index', compact('tickets', 'ielts'));
    }

    // Ielts Test Create
    public function ieltscreate(Request $request)
    {
        $ieltsName = $request->ielts_name;
        $ieltsTicket = $request->ticket_id;
        $ielts = new Ielts();
        $ielts->ielts_name = $ieltsName;
        $ielts->ticket_id = $ieltsTicket;
        $ielts->ielts_name_slug = Str::slug($ieltsName, Str::random(20));
        $ielts->save();
        return response()->json(['status' => true, 'id' => $ielts->id, 'slug' => $ielts->ielts_name_slug]);
    }

    // Ielts Test Update
    public function ieltsupdate(Request $request)
    {
        $id = $request->id;
        $ieltsName = $request->ielts_name;
        $ieltsTicket = $request->ticket_id;
        $update = DB::table('ielts')
            ->where('id', $id)
            ->update(['ielts_name' => $ieltsName, 'ticket_id' => $ieltsTicket, 'ielts_name_slug' => Str::slug($ieltsName, '-')]);

        return response()->json(['status' => true]);
    }
     // Ielts Test Delete
    public function ieltsdelete(Request $request)
    {
        $id = $request->id;
        $delete = Ielts::find($id);
        $delete->delete();
        return response()->json(['status' => true]);
    }

    public function ielts_show($slug)
    {
        return view('admin.ielts.show', compact('slug'));
    }

//    Listening Ielts
    public function listening($slug)
    {
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        $ieltslistening_time = DB::table('ielts_listenings')->where('ielts_id', '=', $ielts[0]->id)->get();
        $ieltsquestions = DB::table('ielts_questions')
            ->select('*', 'ielts_listenings.id as listen_id', 'ielts_listenings.ticket_id as listen_ticket_id')
            ->leftJoin('ielts_listenings', 'ielts_listenings.id', '=', 'ielts_questions.listening_id')
            ->where('ielts_listenings.ielts_id', $ielts[0]->id)
            ->get();

        return view('admin.ielts.listening', compact('ielts', 'ieltsquestions', 'ieltslistening_time'));
    }

    public function listening_create_audio($slug)
    {
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        return view('admin.ielts.listening_create_audio', compact('ielts', 'slug'));
    }

//   Listening Audio save
    public function listening_store_audio(Request $request)
    {
        $slug = $request->slug;
        $audio = $request->file('audio');
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        $audio_name = time() . '.' . $audio->getClientOriginalExtension();
        $request->audio->move('audio/', $audio_name);
        $null = IeltsListening::where('ticket_id', $ielts[0]->ticket_id)->where('ielts_id', $ielts[0]->id)->where('audio_file', Null)->get();
        if (!$null->isEmpty()) {
            IeltsListening::where('ticket_id', $ielts[0]->ticket_id)->where('ielts_id', $ielts[0]->id)->where('audio_file', Null)
                ->update(['audio_file' => $audio_name]);
        } else {
            $time = IeltsListening::where('ticket_id', $ielts[0]->ticket_id)->where('ielts_id', $ielts[0]->id)->get();
            $listening = new IeltsListening();
            $listening->ticket_id = $ielts[0]->ticket_id;
            $listening->ielts_id = $ielts[0]->id;
            $listening->audio_file = $audio_name;
            $listening->ielts_listening_time = 60 * ($time[0]->ielts_listening_time);
            $listening->save();
        }
        return response()->json(['status' => true, 'slug' => $slug]);
    }

    public function listening_create_question($slug)
    {
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        $listening = IeltsListening::all();
        $correct_options = [
            'option1' => 'Variant #A',
            'option2' => 'Variant #B',
            'option3' => 'Variant #C'

        ];
        return view('admin.ielts.listening_create_question', compact('ielts', 'slug', 'correct_options', 'listening'));
    }

    public function listening_create_text_question($slug)
    {
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        $listening = IeltsListening::all();
        return view('admin.ielts.listening_create_text_question', compact('ielts', 'slug', 'listening'));
    }

    public function listening_store_question(Request $request)
    {
        $ieltsquestion = new IeltsQuestions();
        $slug = $request->slug;
        $question = $request->question;
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        $listening_id = $request->listening_id;
        $ieltsquestion->ticket_id = $ielts[0]->ticket_id;
        $ieltsquestion->ielts_id = $ielts[0]->id;
        $ieltsquestion->listening_id = $listening_id;
        $ieltsquestion->question = $question;
        $ieltsquestion->save();
        $id = $ieltsquestion->id;
        foreach ($request->input() as $key => $value) {
            if (strpos($key, 'option') !== false && $value != '') {
                $status = $request->input('correct') == $key ? 1 : 0;
                $new = new IeltsOption();
                $new->question_id = $id;
                $new->option = $value;
                $new->correct = $status;
                $new->save();
            }
        }
        return response()->json(['status' => true]);
    }

    public function listening_store_text_question(Request $request)
    {
        $ieltsquestion = new IeltsQuestions();
        $slug = $request->slug;
        $question = $request->question;
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();

        $listening_id = $request->listening_id;
        $ieltsquestion->ticket_id = $ielts[0]->ticket_id;
        $ieltsquestion->ielts_id = $ielts[0]->id;
        $ieltsquestion->listening_id = $listening_id;
        $ieltsquestion->question = $question;
        $ieltsquestion->save();
        return response()->json(['status' => true]);
    }

    public function listening_time(Request $request)
    {
        $time = $request->listening_time;
        $ielts_id = $request->ielts_id;
        $ticket_id = $request->ticket_id;
        $time_have = IeltsListening::where('ielts_id', $ielts_id)
            ->where('ticket_id', $ticket_id)->get();
        if ($time_have->isEmpty()) {
            $new_time = new IeltsListening();
            $new_time->ticket_id = $ticket_id;
            $new_time->ielts_id = $ielts_id;
            $new_time->ielts_listening_time = 60 * ($time);
            $new_time->save();
        } else {
            IeltsListening::where('ielts_id', $ielts_id)
                ->where('ticket_id', $ticket_id)
                ->update(['ielts_listening_time' => $time]);
        }


        return response()->json(['status' => true]);
    }

//    Writing Ielts
    public function writing($slug)
    {
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        $ieltswritings = DB::table('ielts_writings')
            ->leftJoin('ielts_questions', 'ielts_questions.writing_id', '=', 'ielts_writings.id')
            ->where('ielts_writings.ielts_id', $ielts[0]->id)
            ->orderBy('ielts_writings.id', 'asc')
            ->get();

        return view('admin.ielts.writing', compact('ielts', 'ieltswritings'));
    }

    public function writing_create_question($slug)
    {
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        return view('admin.ielts.writing_create_question', compact('ielts', 'slug'));
    }

    public function writing_store_question(Request $request)
    {
        $ieltsquestion = new IeltsQuestions();
        $slug = $request->slug;
        $question = $request->question;
        $ielts_writing_time = $request->time;
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();

        $ieltswriting = new IeltsWriting();
        $ieltswriting->ticket_id = $ielts[0]->ticket_id;
        $ieltswriting->ielts_id = $ielts[0]->id;
        $ieltswriting->ielts_writing_time = 60 * ($ielts_writing_time);
        $ieltswriting->save();

        $writing_id = IeltsWriting::latest()->first()->id;
        $ieltsquestion->ticket_id = $ielts[0]->ticket_id;
        $ieltsquestion->ielts_id = $ielts[0]->id;
        $ieltsquestion->writing_id = $writing_id;
        $ieltsquestion->question = $question;
        $ieltsquestion->save();
        return response()->json(['status' => true]);
    }


//    Reading Ielts
    public function reading($slug)
    {
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        $ieltsreading_time = DB::table('ielts_readings')->where('ielts_id', '=', $ielts[0]->id)->get();
        $ieltsreading = DB::table('ielts_questions')
            ->leftJoin('ielts_readings', 'ielts_questions.reading_id', '=', 'ielts_readings.id')
            ->where('ielts_readings.ielts_id', $ielts[0]->id)
            ->orderBy('ielts_readings.id', 'asc')
            ->get();

        return view('admin.ielts.reading', compact('ielts', 'ieltsreading', 'ieltsreading_time'));
    }

    public function reading_time(Request $request)
    {
        $time = $request->reading_time;
        $ielts_id = $request->ielts_id;
        $ticket_id = $request->ticket_id;
        $time_have = IeltsReading::where('ielts_id', $ielts_id)
            ->where('ticket_id', $ticket_id)->get();
        if ($time_have->isEmpty()) {
            $new_time = new IeltsReading();
            $new_time->ticket_id = $ticket_id;
            $new_time->ielts_id = $ielts_id;
            $new_time->ielts_reading_time = 60 * ($time);
            $new_time->save();
        } else {
            IeltsReading::where('ielts_id', $ielts_id)
                ->where('ticket_id', $ticket_id)
                ->update(['ielts_reading_time' => $time]);
        }
        return response()->json(['status' => true]);
    }

    public function reading_create_question($slug)
    {
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        $correct_options = [
            'option1' => 'Variant #A',
            'option2' => 'Variant #B',
            'option3' => 'Variant #C'

        ];
        return view('admin.ielts.reading_create_question', compact('ielts', 'slug', 'correct_options'));
    }

    public function reading_create_text_question($slug)
    {
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        return view('admin.ielts.reading_create_text_question', compact('ielts', 'slug'));
    }

//Option Questions
    public function reading_store_question(Request $request)
    {
        $ieltsquestion = new IeltsQuestions();
        $slug = $request->slug;
        $question = $request->question;
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        $reading_id = IeltsReading::latest()->first()->id;

        $ieltsquestion->ticket_id = $ielts[0]->ticket_id;
        $ieltsquestion->ielts_id = $ielts[0]->id;
        $ieltsquestion->reading_id = $reading_id;
        $ieltsquestion->question = $question;
        $ieltsquestion->save();

        $id = $ieltsquestion->id;
        foreach ($request->input() as $key => $value) {
            if (strpos($key, 'option') !== false && $value != '') {
                $status = $request->input('correct') == $key ? 1 : 0;
                $new = new IeltsOption();
                $new->question_id = $id;
                $new->option = $value;
                $new->correct = $status;
                $new->save();
            }
        }
        return response()->json(['status' => true]);
    }

    public function reading_store_text_question(Request $request)
    {
        $ieltsquestion = new IeltsQuestions();
        $slug = $request->slug;
        $question = $request->question;
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        $reading_id = IeltsReading::latest()->first()->id;

        $ieltsquestion->ticket_id = $ielts[0]->ticket_id;
        $ieltsquestion->ielts_id = $ielts[0]->id;
        $ieltsquestion->reading_id = $reading_id;
        $ieltsquestion->question = $question;
        $ieltsquestion->save();
        return response()->json(['status' => true]);
    }

    public function reading_create_text($slug)
    {
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        return view('admin.ielts.reading_create_text', compact('ielts', 'slug'));
    }

//   Listening Audio save
    public function reading_store_text(Request $request)
    {
        $slug = $request->slug;
        $text = $request->question;
        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        $null = IeltsReading::where('ticket_id', $ielts[0]->ticket_id)->where('ielts_id', $ielts[0]->id)->where('text', Null)->get();
        if (!$null->isEmpty()) {
            IeltsReading::where('ticket_id', $ielts[0]->ticket_id)->where('ielts_id', $ielts[0]->id)->where('text', Null)
                ->update(['text' => $text]);
        } else {
            $time = IeltsReading::where('ticket_id', $ielts[0]->ticket_id)->where('ielts_id', $ielts[0]->id)->get();
            $reading = new IeltsReading();
            $reading->ticket_id = $ielts[0]->ticket_id;
            $reading->ielts_id = $ielts[0]->id;
            $reading->text = $text;
            $reading->ielts_reading_time = 60 * ($time[0]->ielts_reading_time);
            $reading->save();
        }

        return response()->json(['status' => true, 'slug' => $slug]);
    }

//    Ielts Exam Delete
    public function delete(Request $request)
    {
        $id = $request->id;
        $delete = Ielts::find($id);
        $delete->delete();
        return response()->json(['status' => true]);
    }
}

window.onload = function () {
    history.pushState(null, null, this.href);
}

$(window).on('popstate', function (event) {
    history.pushState(null, null, this.href);
    location.href = '';
});

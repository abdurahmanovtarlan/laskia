@extends('admin.newbase')
@section('content')
<div id="accordion" class="mb-4">
  <div class="card card-box" style="overflow: hidden !important;">
    <div class="card-header py-3 bg-light-blue font-weight-bold" style="cursor: pointer;" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
      İelts əlavə edin
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <form id="ielts_add">
          <div class="form-group">
            <label>
              <i class="icon-copy dw dw-ticket font-weight-bold mr-2"></i>Bilet</label>
            <select class="form-control" type="text" id="ticket">
              <option value="">Seçin</option>
              @foreach($tickets as $ticket)
              <option value="{{$ticket->id}}">{{$ticket->ticket_name}}</option>
              @endforeach
            </select>
            <div id="ticket_feedback" class="form-control-feedback"></div>
          </div>
          <div class="form-group">
            <label><i class="icon-copy fa fa-code mr-2 font-weight-bold" aria-hidden="true"></i>İlets adı
            </label>
            <input class="form-control" type="text" id="ielts" />
            <div id="ielts_feedback" class="form-control-feedback"></div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success btn-block">
              Saxla
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="card-box mb-30">
  <div class="pb-20 pd-20">
    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
      <div class="row">
        <div class="col-sm-12">
          <table class="data-table table stripe hover nowrap dataTable no-footer dtr-inline" id="ielts_table">
            <thead>
              <tr>
                <th>Bilet</th>
                <th>İlets adı</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($ielts as $ielt)
              <tr id="{{$ielt->ielts_id}}">
                <td id="{{$ielt->ticketId}}">{{$ielt->ticket_name}}</td>
                <td class="btn-link">
                  <a href="{{route('ielts_show',$ielt->ielts_name_slug)}}" class="text-primary">{{$ielt->ielts_name}}</a>
                </td>
                <td>
                  <div class="dropdown">
                    <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                      <i class="dw dw-more"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                      <a class="dropdown-item" href="#" id="edit" data-toggle="modal" data-target="#tikcet_edit_modal" type="button"><i class="dw dw-edit2"></i> Edit</a>

                      <a class="dropdown-item" href="#" id="delete" data-toggle="modal" data-target="#confirmation-modal" type="button"><i class="dw dw-delete-3"></i> Delete</a>
                    </div>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Edit model start -->
<div id="tikcet_edit_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tikcet_edit_modal_title" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="tikcet_edit_modal_title">
          İelts yenile
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          ×
        </button>
      </div>
      <div class="modal-body">
        <form action="" id="ielts_edit">
          <div class="form-group">
            <label for="">Bilet</label>
            <select name="ticket_update" id="ticket_update" class="form-control">
              <option value="">Seçin</option>
              @foreach($tickets as $ticket)
              <option value="{{$ticket->id}}">{{$ticket->ticket_name}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="">İelts adı</label>
            <input type="text" class="form-control" id="ielts_update" />
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" data-dismiss="modal">
          Bağla
        </button>
        <button id="ielts_update_btn" class="btn btn-success">
          Yenilə
        </button>
      </div>
    </div>
  </div>
</div>
<!-- End edit -->
<!-- Delete Modal -->
<div class="modal fade" id="confirmation-modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body text-center font-18">
        <h4 class="padding-top-30 mb-30 weight-500">
          Davam etmək istədiyinizə əminsiniz?
        </h4>
        <div class="padding-bottom-30 row" style="max-width: 170px; margin: 0 auto;">
          <div class="col-6">
            <button type="button" class="btn btn-secondary border-radius-100 btn-block confirmation-btn" data-dismiss="modal">
              <i class="fa fa-times"></i>
            </button>
            Yox
          </div>
          <div class="col-6">
            <button id="confirmation-btn-delete" class="btn btn-danger border-radius-100 btn-block confirmation-btn" data-dismiss="modal">
              <i class="fa fa-check"></i>
            </button>
            Hə
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  // Create Ticket

  var save_btn = $("#save_btn");
  var ielts_table = $("#ielts_table");

  $("#ielts_add").submit(function(e) {
    e.preventDefault();
    console.log(e)
    var ticket = $("#ticket option:selected");
    var ticket_feedback = $("#ticket_feedback");
    var ielts = $("#ielts");
    var ielts_feedback = $("#ielts_feedback");

    // input value control
    if (ticket.val() == "") {
      var form_group = ticket.parents(".form-group").addClass("has-danger");
      ticket_feedback.text("Bilet daxil edin.");
    }
    if (ielts.val() == "") {
      var form_group = ielts.parent().addClass("has-danger");
      var input = ielts
        .parent()
        .children("input")
        .addClass("form-control-danger");
      ielts_feedback.text("İelts adı daxil edin.");
    }
    ticket.change(function() {
      var form_group = ticket
        .parents(".form-group")
        .removeClass("has-danger")
        .addClass("has-success");
      ticket_feedback.text("");
    });
    ielts.keyup(function() {
      var form_group = ielts
        .parent()
        .removeClass("has-danger")
        .addClass("has-success");
      var input = ielts
        .parent()
        .children("input")
        .removeClass("form-control-danger")
        .addClass("form-control-success");
      ielts_feedback.text("");
    });
    if (ticket.val() != "" && ielts.val() != "") {
      $.ajax({
        url: '/coders/ielts/create',
        type: "POST",
        data: {
          '_token': '{{csrf_token()}}',
          'ielts_name': ielts.val(),
          'ticket_id': ticket.val(),
        },
        success: function(data) {
          if (data.status == true) {
            Swal.fire({
              position: "center",
              icon: "success",
              title: "İelts daxil edildi",
              showConfirmButton: false,
              timer: 1000,
            });
            var table_tr = ielts_table.find("tbody tr");
            if (table_tr.length % 2 == 0) {
              var output = `<tr id="2" class="even">
                          <td>${ticket.text()}</td>
                          <td  class="btn-link">
                                                        <a href="/coders/ielts_show/${data.slug}"
                                                           class="text-primary">${ielts.val()}</a>
                                                    </td>
                          <td>
                            <div class="dropdown">
                              <a
                                class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle"
                                href="#"
                                role="button"
                                data-toggle="dropdown"
                              >
                                <i class="dw dw-more"></i>
                              </a>
                              <div
                                class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list"
                              >
                                <a
                                  class="dropdown-item"
                                  href="#"
                                  id="edit"
                                  data-toggle="modal"
                                  data-target="#tikcet_edit_modal"
                                  type="button"
                                  ><i class="dw dw-edit2"></i> Edit</a
                                >

                                <a
                                  class="dropdown-item"
                                  href="#"
                                  id="delete"
                                  data-toggle="modal"
                                  data-target="#confirmation-modal"
                                  type="button"
                                  ><i class="dw dw-delete-3"></i> Delete</a
                                >
                              </div>
                            </div>
                          </td>
                        </tr>`;
            } else {
              var output = `<tr id="2" class="odd">
                          <td>${ticket.text()}</td>
                          <td  class="btn-link">
                                                        <a href="/coders/ielts_show/${data.slug}"
                                                           class="text-primary">${ielts.val()}</a>
                                                    </td>
                          <td>
                            <div class="dropdown">
                              <a
                                class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle"
                                href="#"
                                role="button"
                                data-toggle="dropdown"
                              >
                                <i class="dw dw-more"></i>
                              </a>
                              <div
                                class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list"
                              >
                                <a
                                  class="dropdown-item"
                                  href="#"
                                  id="edit"
                                  data-toggle="modal"
                                  data-target="#tikcet_edit_modal"
                                  type="button"
                                  ><i class="dw dw-edit2"></i> Edit</a
                                >

                                <a
                                  class="dropdown-item"
                                  href="#"
                                  id="delete"
                                  data-toggle="modal"
                                  data-target="#confirmation-modal"
                                  type="button"
                                  ><i class="dw dw-delete-3"></i> Delete</a
                                >
                              </div>
                            </div>
                          </td>
                        </tr>`;
            }
            ielts_table.find("tbody").prepend(output);

            $("#ticket option[value='']").attr("selected", "selected");
            ielts.val("");
            ticket.parent().removeClass("has-success");
            ticket.parent().children("input").removeClass("form-control-success");
            ielts.parent().removeClass("has-success");
            ielts.parent().children("input").removeClass("form-control-success");
          }
        }
      })


    }
  });

  //   Update ticket
  var ielts_update_btn = $("#ielts_update_btn");
  var edit_btn = $("#edit");
  edit_btn.click(function() {
    var ielts_id = $(this).parents("tr").attr('id');
    var ticket_update = $("#ticket_update option:selected");
    var ielts_update = $("#ielts_update");
    var ticket = $(this).parents("tr").children("td:nth-child(1)");
    var ielts = $(this).parents("tr").children("td:nth-child(2)");
    console.log(ielts.text());

    $("#ticket_update")
      .find("option")
      .each(function(index, value) {
        if (value.textContent == ticket.text().trim()) {
          $(value).attr("selected", "selected");
        }
      });
    ielts_update.val(ielts.text().trim());
    ielts_update_btn.click(function() {
      if (
        $("#ticket_update option:selected").val() != "" &&
        ielts_update.val() != ""
      ) {
        $.ajax({
          url: '/coders/ielts/update',
          type: "POST",
          data: {
            "_token": "{{csrf_token()}}",
            id: ielts_id,
            ticket_id: $('#ticket_update option:selected').val(),
            ielts_name: $('#ielts_update').val()
          },
          success: function(data) {
            if (data.status == true) {
              $("#tikcet_edit_modal").modal("hide");
              Swal.fire({
                position: "center",
                icon: "success",
                title: "Bilet yeniləndi",
                showConfirmButton: false,
                timer: 1000,
              });

              ticket.text($('#ticket_update option:selected').text())
              ielts.text($('#ielts_update').val())
            }
          }
        })


      }
    });
  });

  //   Delete
  var deleteBtn = $("#delete");
  var confirmation_btn_delete = $("#confirmation-btn-delete");
  deleteBtn.click(function(e) {
    var ticket_id = $(this).parents("tr").attr("id");
    confirmation_btn_delete.click(function() {
      $.ajax({
        url: '/coders/ielts/delete',
        type: "POST",
        data: {
          "_token": "{{csrf_token()}}",
          id: ticket_id
        },
        success: function(data) {
          console.log(data);
          if (data.status == true) {
            $("#" + ticket_id).fadeOut();
            Swal.fire({
              position: "center",
              icon: "success",
              title: "Bilet silindi",
              showConfirmButton: false,
              timer: 1000,
            });
          }
        }
      })
    });
  });
</script>
</body>

</html>

@endsection
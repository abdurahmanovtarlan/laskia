<?php

use App\Models\Questions;
use App\Models\QuestionsOption;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class QuestionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i = 0; $i < 3; $i++) {
            $question = \App\User::create([
                'name' => $faker->firstName,
                'surname' => $faker->lastName,
                'ticket_code' => 'ielts',
                'phone' => '(+99412) 345-67-8' . $i,
                'password' => Hash::make('123456789'),
            ]);
        }

        $question->save();
    }
}

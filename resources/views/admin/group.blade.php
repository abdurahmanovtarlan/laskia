@extends('admin.base')
@section('content')
    <link href="{{asset('admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div id="alert"></div>
                <div class="card exam_add">
                    <a class="btn-block p-3 font-16 font-weight-bold collapsed bg-warning text-light "
                       data-toggle="collapse"
                       href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                        Qrup əlavə edin
                    </a>
                    <div class="collapse" id="collapseExample">
                        <div class="card-body">
                            <form action="" id="group_form" method="POST">
                                <label for="group" class="font-weight-bold">Qrup<span
                                        class="text-danger">*</span></label>
                                <div class="form-group mb-3">
                                    <input type="text" id="group" name="group" class="form-control"/>
                                    <div class="invalid-feedback font-weight-bold">Qrup qeyd edin.</div>

                                </div>
                                <button class="btn btn-primary px-5 btn-block" type="submit">
                                    Saxla
                                </button>
                            </form>

                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Qruplar cədvəli</h5>
                        <div class="table-responsive">
                            <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="zero_config" class="table table-striped table-bordered dataTable"
                                               role="grid" aria-describedby="zero_config_info">
                                            <thead>
                                            <tr>
                                                <th>Qrup</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($groups as $group)
                                                <tr id="{{$group->id}}">
                                                    <td>
                                                        {{$group->group}}
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-primary btn-sm"
                                                                data-toggle="modal" data-target="#update"><i
                                                                class="mdi mdi-pencil"></i></button>
                                                        <button class="btn btn-danger btn-sm"><i
                                                                class="mdi mdi-delete"></i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Yenilə</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="group_update" class="font-weight-bold">Qrup<span
                            class="text-danger">*</span></label>
                    <div class="form-group mb-3">
                        <input type="text" id="group_update" name="group_update" class="form-control"/>
                        <div class="invalid-feedback font-weight-bold">Qrup qeyd edin.</div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Bağla</button>
                    <button type="button" class="btn btn-success update">Yenilə</button>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('admin/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('admin/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#zero_config').DataTable();
        $(document).ready(function () {
            $('#group_form').submit(function (e) {
                e.preventDefault();
                var group = $('#group');
                if (group.val() == '') {
                    $('#group').addClass('is-invalid');
                } else {
                    $.ajax({
                        url: '{{route('create_groups')}}',
                        type: "POST",
                        data: {
                            '_token': '{{csrf_token()}}',
                            'group_name': group.val(),
                        },
                        success: function (data) {
                            if (data.status == true) {
                                var tr = `<tr id="${data.id}" class="${data.id % 2 == 0 ? 'even' : 'odd'}">
                                                <td>${group.val()}</td>
                                                <td>
                                                <button class="btn btn-primary btn-sm"><i class="mdi mdi-pencil"></i></button>
                                                <button class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i></button>
                                                </td>
                                            </tr>`;
                                $('#zero_config tbody').append(tr);
                                var output = `<div class="alert alert-success alert-dismissible fade show" role="alert">
                            Qrup yaradıldı.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>`;
                                $('#alert').html(output);
                                setTimeout(function () {
                                    $(".alert").alert('close')
                                }, 2000);
                                $('#group').val('');
                            }
                        }
                    })
                }
                group.keyup(function () {
                    $('#group').removeClass('is-invalid');
                    if (group.val() == '') {
                        $('#group').addClass('is-invalid');
                    }
                });
            });
            $(document).on('click', '.btn-danger', function () {
                var id = $(this).parent().parent().attr('id');
                $.ajax({
                    url: '{{route('delete_groups')}}',
                    type: "POST",
                    data: {
                        "_token": "{{csrf_token()}}",
                        id: id
                    },
                    success: function (data) {
                        if (data.status == true) {
                            var output = `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Silindi.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>`;
                            $('#alert').html(output);
                            $('#' + id).remove();
                            setTimeout(function () {
                                $(".alert").alert('close')
                            }, 2000);
                        }
                    }
                })
            });
            $(document).on('click', '#zero_config .btn-primary', function () {
                var id = $(this).parent().parent().attr('id');
                var group = $(this).parents('tr').find('td')[0].innerText;
                $('#group_update').val(group);
                $(document).on('click', '.modal .btn-success', function () {
                    $.ajax({
                        url: '{{route('update_groups')}}',
                        type: "POST",
                        data: {
                            "_token": "{{csrf_token()}}",
                            id: id,
                            group: $('#group_update').val(),
                        },
                        success: function (data) {
                            if (data.status == true) {
                                var group = $('#' + id).find('td')[0];
                                $('#update').modal('hide')
                                var output = `<div class="alert alert-success alert-dismissible fade show" role="alert">
                                Yeniləndi.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>`;
                                $('#alert').html(output);
                                location.reload();
                                setTimeout(function () {
                                    $(".alert").alert('close')
                                }, 2000);
                            }
                        }
                    })
                });

            });
        })
    </script>
@endsection

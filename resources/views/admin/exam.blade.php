@extends('admin.newbase')
@section('content')
<div id="accordion" class="mb-4" >
          <div class="card card-box shadow" style="overflow: hidden !important;">
            <div
              class="card-header py-3 bg-blue font-weight-bold "
              style="cursor: pointer;"
              id="headingThree"
              data-toggle="collapse"
              data-target="#collapseThree"
              aria-expanded="false"
              aria-controls="collapseThree"
            >
              Test əlavə edin
            </div>
            <div
              id="collapseThree"
              class="collapse"
              aria-labelledby="headingThree"
              data-parent="#accordion"
              
            >
              <div class="card-body">
                <form>
                  <div class="form-group">
                    <label
                      ><i
                        class="icon-copy dw dw-ticket font-weight-bold mr-2"
                      ></i
                      >Bilet</label
                    >
                    <select class="form-control" name="ticket" id="ticket">
                      <option value="">Seçin</option>
                      @foreach($tickets as $ticket)
                                            <option value="{{$ticket->id}}">{{$ticket->ticket_name}}</option>
                      @endforeach
                    </select>
                    <div
                      id="ticket_feedback"
                      class="form-control-feedback"
                    ></div>
                  </div>
                  <div class="form-group">
                    <label>
                      <i class="icon-copy dw dw-open-book font-weight-bold"></i>
                      Testin adı
                    </label>
                    <input class="form-control" type="text" id="test_name" />
                    <div
                      id="test_name_feedback"
                      class="form-control-feedback"
                    ></div>
                  </div>
                  <div class="form-group">
                    <label>
                      <i class="icon-copy dw dw-question font-weight-bold"></i>
                      Sualların sayı
                    </label>
                    <input
                      class="form-control"
                      type="number"
                      id="test_question_number"
                    />
                    <div
                      id="test_question_number_feedback"
                      class="form-control-feedback"
                    ></div>
                  </div>
                  <div class="form-group">
                    <label>
                      <span
                        class="icon-copy ti-time mr-2 font-weight-bold"
                      ></span>
                      İmtahan vaxtı
                    </label>
                    <input
                      class="form-control"
                      type="number"
                      id="test_exam_time"
                    />
                    <div
                      id="test_exam_time_feedback"
                      class="form-control-feedback"
                    ></div>
                  </div>
                  <div class="form-group">
                    <button id="save_btn" class="btn bg-green btn-block">
                      Saxla
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="card-box mb-30">
          <div class="pb-20 pd-20">
            <div
              id="DataTables_Table_0_wrapper"
              class="dataTables_wrapper dt-bootstrap4 no-footer"
            >
              <div class="row">
                <div class="col-sm-12">
                  <table
                    class="data-table table stripe hover nowrap dataTable no-footer dtr-inline"
                    id="test_table"
                  >
                    <thead>
                      <tr>
                        <th>Bilet</th>
                        <th>Testin adı</th>
                        <th>Sualların sayı</th>
                        <th>Vaxt (dəqiqə)</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                       @foreach($tests as $test)
                                                <tr id="{{$test->exam_id}}">
                                                    <td id="{{$test->ticketId}}">{{$test->ticket_name}}</td>
                                                    <td>
                                                        <a href="{{route('question_show',$test->test_title_slug)}}"
                                                           class="text-primary">{{$test->test_title}}</a>
                                                    </td>
                                                    <td>{{$test->total_question}}</td>
                                                    <td>{{$test->timer/60}} </td>
                                                    <td>
                          <div class="dropdown">
                            <a
                              class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle"
                              href="#"
                              role="button"
                              data-toggle="dropdown"
                            >
                              <i class="dw dw-more"></i>
                            </a>
                            <div
                              class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list"
                            >
                              <a
                                class="dropdown-item"
                                href="#"
                                id="edit"
                                data-toggle="modal"
                                data-target="#tikcet_edit_modal"
                                type="button"
                                ><i class="dw dw-edit2"></i> Edit</a
                              >

                              <a
                                class="dropdown-item"
                                href="#"
                                id="delete"
                                data-toggle="modal"
                                data-target="#confirmation-modal"
                                type="button"
                                ><i class="dw dw-delete-3"></i> Delete</a
                              >
                            </div>
                          </div>
                        </td>
                                                </tr>
                                            @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    <!-- Edit model start -->
    <div
      id="tikcet_edit_modal"
      class="modal fade"
      tabindex="-1"
      role="dialog"
      aria-labelledby="tikcet_edit_modal_title"
      style="display: none;"
      aria-hidden="true"
    >
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="tikcet_edit_modal_title">
              Test yenile
            </h4>
            <button
              type="button"
              class="close"
              data-dismiss="modal"
              aria-hidden="true"
            >
              ×
            </button>
          </div>
          <div class="modal-body">
            <form id="test_edit">
              <div class="form-group">
                <label
                  ><i class="icon-copy dw dw-ticket font-weight-bold mr-2"></i
                  >Bilet</label
                >
                <select
                  class="form-control"
                  name="ticket_update"
                  id="ticket_update"
                >
                  <option value="">Seçin</option>
                  @foreach($tickets as $ticket)
                                            <option value="{{$ticket->id}}">{{$ticket->ticket_name}}</option>
                      @endforeach
                </select>
              </div>
              <div class="form-group">
                <label>
                  <i class="icon-copy dw dw-open-book font-weight-bold"></i>
                  Testin adı
                </label>
                <input
                  class="form-control"
                  type="text"
                  id="test_name_update"
                  name="test_name_update"
                />
                <div
                  id="test_name_feedback"
                  class="form-control-feedback"
                ></div>
              </div>
              <div class="form-group">
                <label>
                  <i class="icon-copy dw dw-question font-weight-bold"></i>
                  Sualların sayı
                </label>
                <input
                  class="form-control"
                  type="number"
                  id="test_question_number_update"
                  name="test_question_number_update"
                />
              </div>
              <div class="form-group">
                <label>
                  <span class="icon-copy ti-time mr-2 font-weight-bold"></span>
                  İmtahan vaxtı
                </label>
                <input
                  class="form-control"
                  type="number"
                  id="test_exam_time_update"
                  name="test_exam_time_update"
                />
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" data-dismiss="modal">
              Bağla
            </button>
            <button id="test_update_btn" class="btn btn-success">
              Yenilə
            </button>
          </div>
        </div>
      </div>
    </div>
    <!-- End edit -->
    <!-- Delete Modal -->
    <div
      class="modal fade"
      id="confirmation-modal"
      tabindex="-1"
      role="dialog"
      style="display: none;"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body text-center font-18">
            <h4 class="padding-top-30 mb-30 weight-500">
              Davam etmək istədiyinizə əminsiniz?
            </h4>
            <div
              class="padding-bottom-30 row"
              style="max-width: 170px; margin: 0 auto;"
            >
              <div class="col-6">
                <button
                  type="button"
                  class="btn btn-secondary border-radius-100 btn-block confirmation-btn"
                  data-dismiss="modal"
                >
                  <i class="fa fa-times"></i>
                </button>
                Yox
              </div>
              <div class="col-6">
                <button
                  id="confirmation-btn-delete"
                  class="btn btn-danger border-radius-100 btn-block confirmation-btn"
                  data-dismiss="modal"
                >
                  <i class="fa fa-check"></i>
                </button>
                Hə
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- js -->
  
    <script>
      // Create Ticket

      var save_btn = $("#save_btn");
      var test_table = $("#test_table");

      save_btn.click(function (e) {
        e.preventDefault();
        var ticket = $("#ticket option:selected");
        var ticket_feedback = $("#ticket_feedback");
        var test_name = $("#test_name");
        var test_name_feedback = $("#test_name_feedback");
        var test_question_number = $("#test_question_number");
        var test_question_number_feedback = $("#test_question_number_feedback");
        var test_exam_time = $("#test_exam_time");
        var test_exam_time_feedback = $("#test_exam_time_feedback");
        // input value control
        if (ticket.val() == "") {
          var form_group = ticket.parents(".form-group").addClass("has-danger");
          ticket_feedback.text("Bilet daxil edin.");
        }
        if (test_question_number.val() == "") {
          var form_group = test_question_number.parent().addClass("has-danger");
          var input = test_question_number
            .parent()
            .children("input")
            .addClass("form-control-danger");
          test_question_number_feedback.text("Sualların sayını daxil edin.");
        }
        if (test_exam_time.val() == "") {
          var form_group = test_exam_time.parent().addClass("has-danger");
          var input = test_exam_time
            .parent()
            .children("input")
            .addClass("form-control-danger");
          test_exam_time_feedback.text("Testin vaxtını daxil edin.");
        }
        if (test_name.val() == "") {
          var form_group = test_name.parent().addClass("has-danger");
          var input = test_name
            .parent()
            .children("input")
            .addClass("form-control-danger");
          test_name_feedback.text("Testin adını daxil edin.");
        }
        ticket.keyup(function () {
          console.log(this);
          var form_group = ticket
            .parents(".form-group")
            .removeClass("has-danger")
            .addClass("has-success");

          ticket_feedback.text("");
        });
        test_question_number.keyup(function () {
          var form_group = test_question_number
            .parent()
            .removeClass("has-danger")
            .addClass("has-success");
          var input = test_question_number
            .parent()
            .children("input")
            .removeClass("form-control-danger")
            .addClass("form-control-success");
          test_question_number.text("");
        });
        test_exam_time.keyup(function () {
          var form_group = test_exam_time
            .parent()
            .removeClass("has-danger")
            .addClass("has-success");
          var input = test_exam_time
            .parent()
            .children("input")
            .removeClass("form-control-danger")
            .addClass("form-control-success");
          test_exam_time.text("");
        });
        test_name.keyup(function () {
          var form_group = test_name
            .parent()
            .removeClass("has-danger")
            .addClass("has-success");
          var input = test_name
            .parent()
            .children("input")
            .removeClass("form-control-danger")
            .addClass("form-control-success");
          test_name.text("");
        });
        if (
          ticket.val() != "" &&
          test_exam_time.val() != "" &&
          test_name.val() != "" &&
          test_question_number.val() != ""
        ) {

  $.ajax({
                        url: '/coders/exam/create',
                        type: "POST",
                        data: {
                            '_token': '{{csrf_token()}}',
                            'test_title': test_name.val(),
                            'ticket_id': ticket.val(),
                            'total_question': test_question_number.val(),
                            'question_timer': test_exam_time.val(),
                        },
                        success: function (data) {
                            if (data.status == true) {

          Swal.fire({
            position: "center",
            icon: "success",
            title: "Test daxil edildi",
            showConfirmButton: false,
            timer: 1000,
          });
          var table_tr = test_table.find("tbody tr");
          if (table_tr.length % 2 == 0) {
            var output = `<tr id="2" class="even">
                        <td>${ticket.val()}</td>
                        <td>${test_name.val()}</td>
                        <td>${test_question_number.val()}</td>
                        <td>${test_exam_time.val()}</td>
                        <td id="">
                          <div class="dropdown">
                            <a
                              class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle"
                              href="#"
                              role="button"
                              data-toggle="dropdown"
                            >
                              <i class="dw dw-more"></i>
                            </a>
                            <div
                              class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list"
                            >
                              <a
                                class="dropdown-item"
                                href="#"
                                id="edit"
                                data-toggle="modal"
                                data-target="#tikcet_edit_modal"
                                type="button"
                                ><i class="dw dw-edit2"></i> Edit</a
                              >

                              <a
                                class="dropdown-item"
                                href="#"
                                id="delete"
                                data-toggle="modal"
                                data-target="#confirmation-modal"
                                type="button"
                                ><i class="dw dw-delete-3"></i> Delete</a
                              >
                            </div>
                          </div>
                        </td>
                      </tr>`;
          } else {
            var output = `<tr id="2" class="odd">
                        <td>${ticket.val()}</td>
                        <td>${test_name.val()}</td>
                        <td>${test_question_number.val()}</td>
                        <td>${test_exam_time.val()}</td>
                        <td id="">
                          <div class="dropdown">
                            <a
                              class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle"
                              href="#"
                              role="button"
                              data-toggle="dropdown"
                            >
                              <i class="dw dw-more"></i>
                            </a>
                            <div
                              class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list"
                            >
                              <a
                                class="dropdown-item"
                                href="#"
                                id="edit"
                                data-toggle="modal"
                                data-target="#tikcet_edit_modal"
                                type="button"
                                ><i class="dw dw-edit2"></i> Edit</a
                              >

                              <a
                                class="dropdown-item"
                                href="#"
                                id="delete"
                                data-toggle="modal"
                                data-target="#confirmation-modal"
                                type="button"
                                ><i class="dw dw-delete-3"></i> Delete</a
                              >
                            </div>
                          </div>
                        </td>
                      </tr>`;
          }
          test_table.find("tbody").prepend(output);

          $("#ticket option[value='']").attr("selected", "selected");
          test_name.val("");
          test_exam_time.val("");
          test_question_number.val("");
          ticket.parent().removeClass("has-success");
          ticket.parent().children("input").removeClass("form-control-success");
          test_name.parent().removeClass("has-success");
          test_name
            .parent()
            .children("input")
            .removeClass("form-control-success");
          test_question_number.parent().removeClass("has-success");
          test_question_number
            .parent()
            .children("input")
            .removeClass("form-control-success");
          test_exam_time.parent().removeClass("has-success");
          test_exam_time
            .parent()
            .children("input")
            .removeClass("form-control-success");
                        }
                        }
                    })
        }
      });

      //   Update ticket
      var test_update_btn = $("#test_update_btn");

      var edit_btn = $("#edit");
      edit_btn.click(function () {
        var test_id = $(this).parents("tr").attr('id');
        var ticket_update = $("#ticket_update option:selected");
        var test_name_update = $("#test_name_update");
        var test_question_number_update = $("#test_question_number_update");
        var test_exam_time_update = $("#test_exam_time_update");

        var ticket = $(this).parents("tr").children("td:nth-child(1)")
        var test_name = $(this)
          .parents("tr")
          .children("td:nth-child(2)")
        var test_question_number = $(this)
          .parents("tr")
          .children("td:nth-child(3)")
        var test_exam_time = $(this)
          .parents("tr")
          .children("td:nth-child(4)")
        $("#ticket_update")
          .find("option")
          .each(function (index, value) {
            if (value.textContent == ticket.text()) {
              $(value).attr("selected", "selected");
            }
          });
        test_name_update.val(test_name.text().trim());
        test_question_number_update.val(test_question_number.text().trim());
        test_exam_time_update.val(test_exam_time.text().trim());
        test_update_btn.click(function () {
              $.ajax({
                        url: '/coders/exam/update',
                        type: "POST",
                        data: {
                            "_token": "{{csrf_token()}}",
                            id: test_id,
                            ticket_id: $('#ticket_update option:selected').val(),
                            test_title: test_name_update.val(),
                            total_question: test_question_number_update.val(),
                            time: test_exam_time_update.val(),
                        },
                        success: function (data) {
                            if (data.status == true) {
                                $("#tikcet_edit_modal").modal("hide");
            Swal.fire({
              position: "center",
              icon: "success",
              title: "Bilet yeniləndi",
              showConfirmButton: false,
              timer: 1000,
            });
            ticket.text($('#ticket_update option:selected').text())
            test_name.text(test_name_update.val())
            test_question_number.text(test_question_number_update.val())
            test_exam_time.text(test_exam_time_update.val())
                            }
                        }
                    })
           
        });
      });

      //   Delete
      var deleteBtn = $("#delete");
      var confirmation_btn_delete = $("#confirmation-btn-delete");
      deleteBtn.click(function () {
        var ticket_id = $(this).parents("tr").attr("id");
        confirmation_btn_delete.click(function () {
             $.ajax({
                    url: '/coders/exam/delete',
                    type: "POST",
                    data: {
                        "_token": "{{csrf_token()}}",
                        id: ticket_id
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.status == true) {
                            $("#" + ticket_id).fadeOut();
          Swal.fire({
            position: "center",
            icon: "success",
            title: "Bilet silindi",
            showConfirmButton: false,
            timer: 1000,
          });
                        }
                    }
                })
        });
      });
    </script>
  </body>
</html>

@endsection

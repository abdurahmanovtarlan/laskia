<?php

namespace App\Http\Controllers;

use App\Models\Exam;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TicketController extends Controller
{
    public function index()
    {
        $tickets = Ticket::all();
        return view('admin.ticket', compact('tickets'));
    }

    public function create(Request $request)
    {
        $ticket_name =  $request->ticket_name;
        $ticket_code =  $request->ticket_code;
        $ticket = new Ticket();
        $ticket->ticket_name = $ticket_name;
        $ticket->ticket_code = $ticket_code;
        $ticket->save();
        return response()->json(['status' => true, 'id' => $ticket->id]);
    }
    public function update(Request $request)
    {
        $id = $request->id;
        $ticket_name = $request->ticket_name;
        $ticket_code = $request->ticket_code;
        $update = DB::table('tickets')
            ->where('id', $id)
            ->update(['ticket_name' => $ticket_name,'ticket_code' => $ticket_code]);

        return response()->json(['status' => true]);
    }
    public function delete(Request $request)
    {
        $id = $request->id;
        $delete = Ticket::find($id);
        $delete->delete();
        return response()->json(['status' => true]);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIeltsAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ielts_answers', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('ielts_id')->unsigned()->nullable();
            $table->integer('listening_id')->unsigned()->nullable();
            $table->integer('reading_id')->unsigned()->nullable();
            $table->integer('writing_id')->unsigned()->nullable();
            $table->integer('question_id')->unsigned()->nullable();
            $table->tinyInteger('correct')->nullable()->default(0);
            $table->integer('option_id')->nullable()->unsigned()->nullable();
            $table->longText('text_answer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ielts_answers');
    }
}

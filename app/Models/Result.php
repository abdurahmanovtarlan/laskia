<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = ['correct', 'date', 'user_id', 'question_id'];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withTrashed();
    }

    public function question()
    {
        return $this->belongsTo(Questions::class, 'question_id')->withTrashed();
    }
    public function exam()
    {
        return $this->hasMany(Exam::class, 'test_id');
    }
}

@extends('admin.newbase')
@section('content')
    {{--    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>--}}
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card ">
                    <div class="card-body">
                        <form method="POST" action="" enctype="multipart/form-data" id="ielts_listening_form">
                            @csrf
                            <input type="hidden" name="slug" id="slug" value="{{$slug}}">
                            <div class="form-group">
                                <label for="ielts_id" class="font-weight-bold">İelts Testin adı <span
                                        class="text-danger">*</span></label>
                                <select name="ielts_id" class="form-control" id="test_id">
                                    <option value="{{$ielts[0]->id}}" selected>{{$ielts[0]->ielts_name}}</option>
                                </select>
                                <div class="invalid-feedback font-weight-bold ">
                                    Test seçin.
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="audio" class="font-weight-bold">Audio fayl <span
                                        class="text-danger">*</span></label>
                                <input type="file" id="audio" name="audio" class="form-control">
                                <div class="invalid-feedback font-weight-bold ">
                                    Audio daxil edin.
                                </div>
                            </div>
                            <div id="audio_section" class="form-group d-none">
                                <audio id="sound" controls class="w-100"></audio>
                            </div>
                            <hr>

                            <button class="btn btn-primary px-5 btn-block" type="submit">
                                Saxla
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#audio').change(function (e) {
                $('#audio_section').removeClass('d-none');
                var sound = document.getElementById('sound');
                sound.src = URL.createObjectURL(this.files[0]);
                sound.onend = function (e) {
                    URL.revokeObjectURL(this.src);
                }
            })

            $('#ielts_listening_form').submit(function (e) {
                e.preventDefault();
                var ielts = $("#ielts_id option:selected");
                if (ielts.val() == '') {
                    $('select#ielts_id').addClass('is-invalid');
                }
                if (
                    ielts.val() != " "
                ) {
                    var formData = new FormData(this);
                    $.ajax({
                        type: "POST",
                        url: "{{route('listening_store_audio')}}",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data.status == true) {
                                window.location = `/coders/ielts/${data.slug}/listening`;

                            }
                        }
                    });
                }
            })
        });
    </script>
@endsection

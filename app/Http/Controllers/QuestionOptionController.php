<?php

namespace App\Http\Controllers;

use App\Models\Questions;
use App\Models\QuestionsOption;
use Illuminate\Http\Request;

class QuestionOptionController extends Controller
{
    public function add(Request $request)
    {
        $question_option_exists = QuestionsOption::where('question_id', '=', $request->question_id)->where('option', '=', $request->option)->count();
        $question_option_correct = QuestionsOption::where('question_id', '=', $request->question_id)
            ->where('correct', '=', 1)->count();
        if ($question_option_exists == 0) {
            if ($question_option_correct == 1) {
                $correct = $request->correct = 0;
                $question_option = new QuestionsOption();
                $question_option->question_id = $request->question_id;
                $question_option->option = $request->option;
                $question_option->correct = $correct;
                $question_option->save();
                // $question_option = QuestionsOption::create($request->all());
                return response()->json(['status' => true, 'id' => $question_option->id]);
            } else if ($question_option_correct == 0) {
                $question_option = QuestionsOption::create($request->all());
                return response()->json(['status' => true, 'id' => $question_option->id]);
            }
        } else {
            return response()->json(['exists' => false]);
        }
    }
    public function edit($id)
    {
        $questions_option = QuestionsOption::findOrFail($id);

        $data = [
            'questions' => Questions::find($questions_option->question_id),
        ];
        return view('admin.question.option_edit', compact('questions_option') + $data);
    }
    public function update(Request $request, $id)
    {
        $questionsoption = QuestionsOption::findOrFail($id);
        $questionsoption->update($request->all());
        $questions = Questions::find($questionsoption->question_id);
        // return response()->json('question_show_detail', $questions->id);
        return response()->json(['status'=>true,'id'=>$questions->id]);
    }
    public function delete(Request $request)
    {
        $questionsoption = QuestionsOption::findOrFail($request->id);
        $questionsoption->delete();

        return response()->json(['status' => true]);
    }
}

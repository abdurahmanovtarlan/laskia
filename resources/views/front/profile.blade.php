@extends('front.base')
@section('content')

<div class="container setting ">
    <div class="row">
        <div class="col-md-3 mb-4">
            <div class="">
                <ul class="list">
                    <li class="list-item active"><a href="/main/{{Auth::id()}}">Əsas</a></li>
                    <li class="list-item "><a href="{{route('result_all')}}">Nəticələr</a></li>
                    <li class="list-item"><a href="/password_change/{{Auth::id()}}">Parolu dəyişdirin</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-8">
            <div id="alert"></div>
            <div class="card">
                <div class="card-body">
                    <form action="" method="POST" id="main_setting">
                        <div class="form-group">
                            <label for="name">Ad</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}" />
                        </div><div class="form-group">
                            <label for="name">Soyad</label>
                            <input type="text" class="form-control" id="surname" name="surname" value="{{$user->surname}}" />
                        </div>
                        <div class="form-group">
                            <label for="email">Email Ünvanı</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}" />
                        </div>
                        <div class="form-group">
                            <label for="phone">Telefon</label>
                            <input type="phone" class="form-control" id="phone" name="phone" value="{{$user->phone}}" />
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block rounded-0 shadow">
                                Yenilə
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="{{asset('admin/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js')}}"></script>
<script>
    $("#phone").inputmask("(+99488) 888-88-88");
    $('.btn-success').click(function(e) {
        e.preventDefault();
        var name = $('#name');
        var email = $('#email');
        var phone = $('#phone');
        var surname = $('#surname');

        $.ajax({
            type: "POST",
            url: '{{route("update_user_profile")}}',
            data: {
                '_token': '{{csrf_token()}}',
                'surname': surname.val().trim(),
                'name': name.val().trim(),
                'email': email.val().trim(),
                'phone': phone.val().trim(),
            },
            success: function(data) {
                console.log(data);
                if (data.status == true) {
                    $('#alert').append(`
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                    Profil Yeniləndi.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                    `);
                    setTimeout(function() {
                        $(".alert").alert('close')
                    }, 2000)
                }
            }
        })
    })
</script>
@endsection

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = ['ticket_name'];

    public function exam()
    {
        return $this->belongsTo(Exam::class, 'ticket_id');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\GroupSubject;
use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GroupSubjectController extends Controller
{
    public function index()
    {
        $subject_groups = DB::table('group_subjects')->select('*', 'group_subjects.id as subject_groups_id','groups.id as groups_id','subjects.id as subjects_id')
            ->leftJoin('groups', 'groups.id', '=', 'group_subjects.group_id')
            ->leftJoin('subjects', 'subjects.id', '=', 'group_subjects.subject_id')->get();
        $groups = Group::all();
        $subjects = Subject::all();
        return view('admin.subject_group', compact('subject_groups', 'groups', 'subjects'));
    }

    public function create(Request $request)
    {
        $subject = $request->subject_id;
        $group = $request->group_id;
        $point = $request->point;
        $subject_group = new GroupSubject();
        $subject_group->group_id = $group;
        $subject_group->subject_id = $subject;
        $subject_group->point = $point;
        $subject_group->save();
        return response()->json(['status' => true, 'id' => $subject_group->id]);
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $group = $request->group_id_update;
        $subject = $request->subject_id_update;
        $point = $request->point_update;
        $update = DB::table('group_subjects')
            ->where('id', $id)
            ->update(['group_id' => $group, 'subject_id' => $subject, 'point' => $point]);

        return response()->json(['status' => true]);
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $delete = GroupSubject::find($id);
        $delete->delete();
        return response()->json(['status' => true]);
    }
}

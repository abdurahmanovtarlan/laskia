@extends('front.base')active
@section('content')
<link href="{{asset('admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.7/css/fixedHeader.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap.min.css">
<div class="container setting">
    <div class="row">
        <div class="col-md-3 mb-4">
            <div class="">
                <ul class="list">
                    <li class="list-item "><a href="/main/{{Auth::id()}}">Əsas</a></li>
                    <li class="list-item active"><a href="{{route('result_all')}}">Nəticələr</a></li>
                    <li class="list-item"><a href="/password_change/{{Auth::id()}}">Parolu dəyişdirin</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card shadow">
                <div class="card-body">
                    @if (count($results) > 0)
                    <table class="table table-bordered table-striped  {{ count($results) > 0 ? 'datatable' : '' }}">
                        <thead>
                            <tr>
                                <th>Test</th>
                                <th>Vaxt</th>
                                <th>Nəticə</th>
                                <th>Action&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($results as $result)
                            <tr>
                                <td>{{ $result->exam->test_title }}</td>
                                <td>{{ date_format($result->created_at,"d.m.Y / H:i:s")}}</td>
                                <td>{{ $result->result }} / {{ $result->exam->total_question }}</td>
                                <td>
                                    <a href="{{route('result_show',[$result->id,\Illuminate\Support\Str::random(30)])}}" class="btn btn-xs btn-primary">Bax</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    Məlumat yoxdur
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('admin/assets/libs/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('admin/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.5/js/responsive.bootstrap.min.js"></script>
<script>
    var table = $('.table').DataTable( {
        responsive: true
    } );

</script>
@endsection

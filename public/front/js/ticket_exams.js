var text = `<ol>
        <li style="font-size: 16px; text-align: left;">
            İlk 3 yeri tutan iştirakçılar cavablarının təsdiqi üçün  yoxlanacaqlar, onlara suallar veriləcək və imtahanda tam şəffaf şəkildə iştirak etdikləri məlum olduqda qaliblikləri təsdiq olunacaq.
        </li>
        <li style="font-size: 16px;text-align: left;">
            Sualların hamısına düzgün cavab vermiş eyni sayda bir neçə iştirakçımız olarsa, ekspertlərimizin yoxlamasından sonra digərlərinə görə ən tez imtahanı yekunlaşdırmış şəxs qalib olacaq.
        </li>
        <li style="font-size: 16px;text-align: left;">
            Köçürmək və əlavə resurslardan istifadə etmək qəti qadağandır, bu halda cavablar ləğv olunacaq.
        </li>
        <li style="font-size: 16px;text-align: left;">
            Ödənilən bilet məbləği heç bir halda geri qaytarılmır.
        </li>
    </ol>`;

$('.btn-outline-primary').click(function (e) {
    e.preventDefault();
    Swal.fire({
        title: 'İmtahanda iştirak üçün vacib olan şərtlər',
        html: text,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#1ba94c',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Razıyam',
        cancelButtonText: 'Bağla',
        showClass: {
            popup: 'animate__animated animate__bounceIn'
        },
        hideClass: {
            popup: 'animate__animated animate__bounceOut'
        }
    }).then((result) => {
        if (result.value) {
            Swal.fire({
                title: 'İmtahan başladılır',
                timer: 1000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                onClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.value) {
                }
            })
        }
    })
})

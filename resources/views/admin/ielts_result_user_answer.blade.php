@extends('admin.newbase')
@section('title')
Laskia.com Ielts Exm User Answers
@endsection
@section('content')


<div class="accordion" id="accordionExample">
    <div class="card ">
        <div class="card-header card-header py-3 bg-light-blue font-weight-bold collapsed" style="cursor: pointer;" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            Listening Answers
        </div>
        <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                <?php $i = 1;
                $mass = ['a', 'b', 'c', 'd', 'e'];
                ?>
                @foreach($listening_question_options as $result)
                <div class="card question-card mb-2">
                    <div class="card-body">
                        <div class="question ml-2 "> {!! ($result->question->question) !!}
                        </div>
                        <div class="answers read-only">
                            @foreach($result->question->options as $key=>$option)
                            <div class="form-check my-2 @if ($option->correct == 1) success-answer @elseif($result->option_id == $option->id) wrong-answer @endif">
                                <label class="form-check-label ml-1 py-1" for="exampleRadios{{$option->id}}">
                                    <input class="form-check-input" disabled type="radio" name="exampleRadios{{$option->id}}" id="exampleRadios{{$option->id}}" @if($option->correct == 1) checked @endif />
                                    <span class="checkmark d-flex justify-content-center align-items-center">{{$mass[$key]}}</span>
                                    <span class="text @if ($option->correct == 1) font-weight-bold @endif">
                                        {!! $option->option !!}
                                        @if ($option->correct == 1) <em>(Düzgün cavab)</em> @endif
                                        @if ($result->option_id == $option->id)
                                        <em>(Seçilən cavab)</em> @endif
                                    </span>
                                </label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <?php $i++ ?>
                @endforeach
                @foreach($listeninganswers_text as $listening_text)
                <div class="card-box mb-2 card-body question_text_answer ">
                    <h6>{!! $listening_text->question->question !!}</h6>
                    <div class="answer">
                        {!! $listening_text->text_answer !!}
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="card ">
        <div class="card-header py-3 bg-orange font-weight-bold collapsed" style="cursor: pointer;" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            Reading Answers
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
            <div class="card-body">
                Reading
            </div>
        </div>
    </div>
    <div class="card ">
        <div class="card-header py-3 bg-green font-weight-bold collapsed" style="cursor: pointer;" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            Writing Answers
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
            <div class="card-body">
                Writing
            </div>
        </div>
    </div>
</div>


<div class="card-box mt-5">
    <div class="card-body">
        <h4>Qiymətləndirmə</h4>
        <hr>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label font-weight-bold">Listening</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control form-control-sm" type="text">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label font-weight-bold">Reading </label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control form-control-sm" type="text">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label font-weight-bold"> Writing </label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control form-control-sm" type="text">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label font-weight-bold">Speaking </label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control form-control-sm" type="text">
            </div>
        </div>
        <div class="form-group">
            <button class="btn bg-green btn-block">Təsdiqlə</button>
        </div>
    </div>
</div>
@endsection
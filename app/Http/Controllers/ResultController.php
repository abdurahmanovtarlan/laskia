<?php

namespace App\Http\Controllers;

use App\Models\Exam;
use App\Models\Questions;
use App\Models\Test;
use App\Models\TestAnswers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Symfony\Component\Console\Question\Question;

class ResultController extends Controller
{
    public function index()
    {
        $results = Test::all()->load('user');
        if (Auth::user()) {
            $results = $results->where('user_id', '=', Auth::id());
        }
        return view('front.result_all', compact('results'));
    }

    /**
     * Display Result.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $param)
    {
        $test = Test::find($id)->load('user');
        $question = Questions::where('test_id', $test->test_id)->get();
        $test_answers = TestAnswers::where('test_id',$test->test_id)->where('user_id',Auth::id())->get();
        $option_mass = [];
        foreach ($test_answers as $answer){
            if($answer->option_id == null){
                array_push($option_mass, $answer);
            }
        }
        if ($test) {
            $results = TestAnswers::where('test_id', $test->test_id)->where('user_id',Auth::id())
                ->with('question')
                ->with('question.options')
                ->get();
        }

        return view('front.result_show', compact('results', 'test', 'question','option_mass'));
    }
}

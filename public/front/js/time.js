function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    var day = today.getDay();
    var month = today.getDate();
    var year = today.getFullYear();
    m = checkTime(m);
    s = checkTime(s);
    return `${h}:${m}:${s} ${day}.${parseInt(month)}.${year}`;
}

function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

const exam_time = "18:20:00";
const finish_exam_time = "19:42:00";
setInterval(function () {
    if (startTime()) {
        $('.exam').addClass('btn-primary').removeClass('btn-warning').removeClass('disabled').html(`Testlərə keçid <i
                                        class="fa fa-angle-right"></i>`)
        $('.exam_ticket').removeClass('disabled').html('İmtahana keçid');
    }

    // if(startTime()){
    //     $('.exam_ticket').addClass('disabled').removeClass('btn-outline-primary').addClass('btn-danger').html('İmtahan bitib');
    //     $('.exam').addClass('disabled').addClass('btn-danger').removeClass('btn-success').html(`İmtahan bitib <i
    //                                     class="fa fa-angle-right"></i>`)
    // }

}, 1000);

<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/exam/{ticket_code}', 'HomeController@ticket_exams')->name('ticket_exams');
Route::get('/ielts/{ticket_code}', 'HomeController@ticket_ielts')->name('ticket_ielts');
Route::get('/test/{slug}/', 'TestController@index')->name('test')->middleware('auth');
Route::get('/ielts/listening/test/{slug}/', 'TestController@ielts_listening')->name('ielts_listening')->middleware('auth');
Route::get('/ielts/reading/test/{slug}/', 'TestController@ielts_reading')->name('ielts_reading')->middleware('auth');
Route::get('/ielts/writing/test/{slug}/', 'TestController@ielts_writing')->name('ielts_writing')->middleware('auth');
Route::get('/wait-answers/{slug}/', 'TestController@wait_answers')->name('wait_answers')->middleware('auth');
Route::get('/result/{id}/{params}', 'ResultController@show')->name('result_show')->middleware('auth');
Route::get('/results', 'ResultController@index')->name('result_all')->middleware('auth');
Route::get('/main/{id}', 'ProfileController@index')->name('profile')->middleware('auth');
Route::get('/tests/{id}', 'ProfileController@tests_user')->name('tests_user')->middleware('auth');
Route::get('/password_change/{id}', 'ProfileController@password_change')->name('password_change')->middleware('auth');

//Google
Route::get('/auth/redirect/{provider}', 'SocialController@redirect');
Route::get('/callback/{provider}', 'SocialController@callback');


Route::post('/test/store', 'TestController@store')->name('test_store')->middleware('auth');
Route::post('/test/ielts_store_listening', 'TestController@ielts_store_listening')->name('ielts_store_listening')->middleware('auth');
Route::post('/test/ielts_store_reading', 'TestController@ielts_store_reading')->name('ielts_store_reading')->middleware('auth');
Route::post('/test/ielts_store_writing', 'TestController@ielts_store_writing')->name('ielts_store_writing')->middleware('auth');
Route::post('/search', 'HomeController@search')->name('autosearch')->middleware('auth');
Route::post('/register_exam', 'HomeController@register_exam')->name('register_exam')->middleware('auth');
Route::post('/update_user_profile', 'ProfileController@update_user_profile')->name('update_user_profile')->middleware('auth');
Route::post('/pass_change', 'ProfileController@pass_change')->name('pass_change')->middleware('auth');


Auth::routes(['verify' => true]);


//Admin Get method

Route::get('/coders/exam', 'ExamController@index')->name('exam')->middleware('auth', 'admin');
Route::get('/coders/ticket', 'TicketController@index')->name('ticket')->middleware('auth', 'admin');
Route::get('/coders/profile/{id}', 'AdminProfile@index')->name('profile_user_admin')->middleware('auth', 'admin');
Route::get('/coders/password_change/{id}', 'AdminProfile@password_change')->name('password_change')->middleware('auth', 'admin');
Route::get('/coders/questions/{id}/add/', 'QuestionsController@create')->name('question_create')->middleware('auth', 'admin');
Route::get('/coders/question/{slug}', 'QuestionsController@index')->name('question_show')->middleware('auth', 'admin');
Route::get('/coders/question_show/{id}', 'QuestionsController@show')->name('question_show_detail')->middleware('auth', 'admin');
Route::get('/coders/question/option/{id}/edit', 'QuestionOptionController@edit')->name('question_option_edit')->middleware('auth', 'admin');
Route::get('/coders/question/{id}/edit', 'QuestionsController@edit')->name('question_edit')->middleware('auth', 'admin');
Route::get('/coders/results', 'Admin\ResultsController@index')->name('admin_result_all')->middleware('auth');
Route::get('/coders/result-show/{test_title}', 'Admin\ResultsController@show')->name('admin_result_show')->middleware('auth');

Route::get('/coders/ielts-results', 'IeltsAnswers@index')->name('admin_results_ielts')->middleware('auth');
Route::get('/coders/ielts-result-show/{slug}', 'IeltsAnswers@ielts_result_show')->name('ielts_result_show')->middleware('auth');
Route::get('/coders/ielts-result-user-answers/{slug}/{id}', 'IeltsAnswers@ielts_result_user_answer')->name('ielts_result_user_answer')->middleware('auth');


// Admin ielts
Route::get('/coders/ielts', 'IeltsController@index')->name('ielts')->middleware('auth', 'admin');
Route::get('/coders/ielts_show/{slug}', 'IeltsController@ielts_show')->name('ielts_show')->middleware('auth', 'admin');

//Listening
Route::get('/coders/ielts/{slug}/listening', 'IeltsController@listening')->name('listening')->middleware('auth', 'admin');
Route::get('/coders/ielts/{slug}/listening/create/audio', 'IeltsController@listening_create_audio')->name('listening_create_audio')->middleware('auth', 'admin');
Route::get('/coders/ielts/{slug}/listening/create/question', 'IeltsController@listening_create_question')->name('listening_create_question')->middleware('auth', 'admin');
Route::get('/coders/ielts/{slug}/listening/create/text_question', 'IeltsController@listening_create_text_question')->name('listening_create_text_question')->middleware('auth', 'admin');

//Writing
Route::get('/coders/ielts/{slug}/writing', 'IeltsController@writing')->name('writing')->middleware('auth', 'admin');
Route::get('/coders/ielts/{slug}/writing/create/question', 'IeltsController@writing_create_question')->name('writing_create_question')->middleware('auth', 'admin');

//Reading
Route::get('/coders/ielts/{slug}/reading', 'IeltsController@reading')->name('reading')->middleware('auth', 'admin');
Route::get('/coders/ielts/{slug}/reading_create_text', 'IeltsController@reading_create_text')->name('reading_create_text')->middleware('auth', 'admin');
Route::get('/coders/ielts/{slug}/reading/create/question', 'IeltsController@reading_create_question')->name('reading_create_question')->middleware('auth', 'admin');
Route::get('/coders/ielts/{slug}/reading/create/text_question', 'IeltsController@reading_create_text_question')->name('reading_create_text_question')->middleware('auth', 'admin');

//Admin ielts post method
Route::post('/coders/ielts/create', 'IeltsController@ieltscreate')->name('ieltscreate')->middleware('auth', 'admin');
Route::post('/coders/ielts/update', 'IeltsController@ieltsupdate')->name('ieltsupdate')->middleware('auth', 'admin');
Route::post('/coders/ielts/delete', 'IeltsController@ieltsdelete')->name('ieltsdelete')->middleware('auth', 'admin');

//Listening
Route::post('/coders/ielts/listening_time', 'IeltsController@listening_time')->name('listening_time')->middleware('auth', 'admin');
Route::post('/coders/ielts/listening_store_audio', 'IeltsController@listening_store_audio')->name('listening_store_audio')->middleware('auth', 'admin');
Route::post('/coders/ielts/listening_store_question', 'IeltsController@listening_store_question')->name('listening_store_question')->middleware('auth', 'admin');
Route::post('/coders/ielts/listening_store_text_question', 'IeltsController@listening_store_text_question')->name('listening_store_text_question')->middleware('auth', 'admin');

//Writing
Route::post('/coders/ielts/writing_store_question', 'IeltsController@writing_store_question')->name('writing_store_question')->middleware('auth', 'admin');

//Reading
Route::post('/coders/ielts/reading_time', 'IeltsController@reading_time')->name('reading_time')->middleware('auth', 'admin');
Route::post('/coders/ielts/reading_store_question', 'IeltsController@reading_store_question')->name('reading_store_question')->middleware('auth', 'admin');
Route::post('/coders/ielts/reading_store_text_question', 'IeltsController@reading_store_text_question')->name('reading_store_text_question')->middleware('auth', 'admin');
Route::post('/coders/ielts/reading_store_text', 'IeltsController@reading_store_text')->name('reading_store_text')->middleware('auth', 'admin');


//Group
Route::get('/coders/groups', 'GroupController@index')->name('groups')->middleware('auth', 'admin');
Route::post('/groups/create', 'GroupController@create')->name('create_groups')->middleware('auth');
Route::post('/coders/groups/update', 'GroupController@update')->name('update_groups')->middleware('auth');
Route::post('/coders/groups/delete', 'GroupController@delete')->name('delete_groups')->middleware('auth');

//Subject
Route::get('/coders/subjects', 'SubjectController@index')->name('subjects')->middleware('auth', 'admin');
Route::post('/coders/subjects/create', 'SubjectController@create')->name('create_subject')->middleware('auth');
Route::post('/coders/subjects/update', 'SubjectController@update')->name('update_subject')->middleware('auth');
Route::post('/coders/subjects/delete', 'SubjectController@delete')->name('delete_subject')->middleware('auth');

//Subject in Group
Route::get('/coders/subject_groups', 'GroupSubjectController@index')->name('subject_groups')->middleware('auth', 'admin');
Route::post('/coders/subject_groups/create', 'GroupSubjectController@create')->name('create_subject_groups')->middleware('auth');
Route::post('/coders/subject_groups/update', 'GroupSubjectController@update')->name('update_subject_groups')->middleware('auth');
Route::post('/coders/subject_groups/delete', 'GroupSubjectController@delete')->name('delete_subject_groups')->middleware('auth');


//Admin Post method
Route::post('/coders/exam/create', 'ExamController@create')->name('create_exam')->middleware('auth');
Route::post('/coders/exam/update', 'ExamController@update')->name('update_exam')->middleware('auth');
Route::post('/coders/exam/delete', 'ExamController@delete')->name('delete_exam')->middleware('auth');

Route::post('/coders/ticket/create', 'TicketController@create')->name('create_ticket')->middleware('auth');
Route::post('/coders/ticket/update', 'TicketController@update')->name('update_ticket')->middleware('auth');
Route::post('/coders/ticket/delete', 'TicketController@delete')->name('delete_ticket')->middleware('auth');

Route::post('/coders/questions/store', 'QuestionsController@store')->name('question_store')->middleware('auth');
Route::post('/coders/questions/{id}/update', 'QuestionsController@update')->name('question_update')->middleware('auth');
Route::post('/coders/questions/delete', 'QuestionsController@delete')->name('question_delete')->middleware('auth');
Route::post('/coders/question/{id}/edit/option', 'QuestionOptionController@update')->name('question_option_update')->middleware('auth');
Route::post('/coders/question/add/option', 'QuestionOptionController@add')->name('question_option_add')->middleware('auth');
Route::post('/coders/question/delete/option', 'QuestionOptionController@delete')->name('question_option_delete')->middleware('auth');



<!DOCTYPE html>
<html>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>@yield('title')</title>
	<!-- Site favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="{{asset('newadmin/vendors/images/apple-touch-icon.png')}}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{asset('newadmin/vendors/images/favicon-32x32.png')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{asset('newadmin/vendors/images/favicon-16x16.png')}}">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https:/fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="{{asset('newadmin/vendors/styles/core.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('newadmin/vendors/styles/icon-font.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('newadmin/src/plugins/datatables/css/dataTables.bootstrap4.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('newadmin/src/plugins/datatables/css/responsive.bootstrap4.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('newadmin/vendors/styles/style.css')}}">
	<script src="{{asset('newadmin/vendors/scripts/core.js')}}"></script>
	<script src="{{asset('newadmin/vendors/scripts/script.min.js')}}"></script>
	<script src="{{asset('newadmin/vendors/scripts/process.js')}}"></script>
	<script src="{{asset('newadmin/vendors/scripts/layout-settings.js')}}"></script>
	<script src="{{asset('newadmin/src/plugins/apexcharts/apexcharts.min.js')}}"></script>
	<script src="{{asset('newadmin/src/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('newadmin/src/plugins/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
	<script src="{{asset('newadmin/src/plugins/datatables/js/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('newadmin/src/plugins/datatables/js/responsive.bootstrap4.min.js')}}"></script>

	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script src="{{asset('newadmin/vendors/scripts/datatable-setting.js')}}"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https:/www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-119386393-1');
	</script>
</head>

<body>

	<div class="header">
		<div class="header-left">
			<div class="menu-icon dw dw-menu"></div>
		</div>
		<div class="header-right">
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
						<span class="user-icon">
							<img src="{{asset('newadmin/vendors/images/photo1.jpg')}}" alt="">
						</span>
						<span class="user-name">Ross C. Lopez</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<a class="dropdown-item" href=""><i class="dw dw-user1"></i> Profile</a>
						<a class="dropdown-item" href=""><i class="dw dw-settings2"></i> Setting</a>
						<a class="dropdown-item" href=""><i class="dw dw-help"></i> Help</a>
						<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="dw dw-logout"></i> Log Out</a>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							@csrf
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="index.html">
				<img src="{{asset('newadmin/vendors/images/deskapp-logo.svg')}}" alt="" class="dark-logo">
				<img src="{{asset('newadmin/vendors/images/deskapp-logo-white.svg')}}" alt="" class="light-logo">
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li>
						<a href="" class="dropdown-toggle no-arrow">
							<span class="micon dw dw-house-1"></span><span class="mtext">Home</span>
						</a>
					</li>
					<li>
						<a href="{{route('ticket')}}" class="dropdown-toggle no-arrow">
							<span class="micon dw dw-ticket"></span><span class="mtext">Bilet</span>
						</a>
					</li>
					<li>
						<a href="{{route('exam')}}" class="dropdown-toggle no-arrow">
							<span class="micon dw dw-open-book"></span>
							<span class="mtext">Test</span>
						</a>
					</li>
					<li>
						<a href="{{route('ielts')}}" class="dropdown-toggle no-arrow">
							<span class="micon dw dw-clipboard1"></span><span class="mtext">İelts</span>
						</a>
					</li>
					<li>
						<a href="{{route('admin_result_all')}}" class="dropdown-toggle no-arrow">
							<span class="micon dw dw-file3"></span><span class="mtext">İmtahan Nəticələri</span>
						</a>
					</li>
					<li>
						<a href="{{route('admin_results_ielts')}}" class="dropdown-toggle no-arrow">
							<span class="micon dw dw-file-45"></span><span class="mtext">İelts İmtahan Nəticələri</span>
						</a>
					</li>

				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20">
			@yield('content')
		</div>
	</div>
	<!-- js -->

</body>

</html>
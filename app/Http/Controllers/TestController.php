<?php

namespace App\Http\Controllers;

use App\Models\Exam;
use App\Models\Ielts;
use App\Models\IeltsListening;
use App\Models\IeltsOption;
use App\Models\IeltsQuestions;
use App\Models\IeltsReading;
use App\Models\IeltsWriting;
use App\Models\Questions;
use App\Models\QuestionsOption;
use App\Models\Test;
use App\Models\TestAnswers;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

class TestController extends Controller
{
    public function index($slug)
    {
        $exams = Exam::where('test_title_slug', $slug)->get();
        $questions = Questions::where('test_id', '=', $exams[0]->id)->get();
        foreach ($questions as &$question) {
            $question->options = QuestionsOption::where('question_id', $question->id)->inRandomOrder()->get();
        }

        return view('front.test', compact('questions', 'exams'));
    }


// For Simple Exam
    public function store(Request $request)
    {
        $tests = Test::where('test_id', $request->test_id)->where('user_id', Auth::id())->get();
        if ($tests->isEmpty()) {
            DB::transaction(function () use ($request) {
                $result = 0;
                $all_time = $request->all_time;
                $clock = $request->clock;
                $minute = $request->minute;
                $second = $request->second;
                $deqiqe = ($clock * 60) + $minute;
                $all_deqiqe = $all_time / 60;
                $vaxt = $all_deqiqe - $deqiqe;
                $vaxt = ($vaxt - 1) . ' deqiqe ' . (60 - $second) . ' saniye';

                $test = new Test();
                $test->user_id = Auth::id();
                $test->test_id = $request->test_id;
                $test->result = $result;
                $test->time = $vaxt;
                $test->save();

                foreach ($request->input('questions', []) as $key => $question) {
                    $status = 0;
                    if (
                        $request->input('answers.' . $question) && QuestionsOption::find($request->input('answers.' . $question))->correct
                    ) {
                        $status = 1;
                        $result++;
                    }
                    TestAnswers::create([
                        'user_id' => Auth::id(),
                        'test_id' => $request->test_id,
                        'question_id' => $question,
                        'option_id' => $request->input('answers.' . $question),
                        'correct' => $status,
                    ]);
                }
            });
        } else {
            return redirect()->route('results');
        }
        $testend = Test::orderbyDesc('id')->get();
        return redirect()->route('result_show', [$testend[0]->id, Str::random(300)]);
    }

//    Ielts Listening
    public function ielts_listening($slug)
    {
        $ticket_code = Ticket::where('ticket_code', $slug)->get();
        $ielts = Ielts::where('ticket_id', $ticket_code[0]->id)->get();
        $ielts_listening = IeltsListening::where('ielts_id', '=', $ielts[0]->id)->where('ticket_id', '=', $ticket_code[0]->id)->get();
        $questions = IeltsQuestions::where('ielts_id', '=', $ielts[0]->id)
            ->where('ticket_id', '=', $ticket_code[0]->id)
            ->where('listening_id', '=', $ielts_listening[0]->id)
            ->get();
        $ieltsoption = DB::table('ielts_options')
            ->leftJoin('ielts_questions', 'ielts_questions.id', '=', 'ielts_options.question_id')->get();
        return view('front.listening', compact('questions', 'ielts', 'ielts_listening', 'slug'));
    }

    public function ielts_store_listening(Request $request)
    {
        $ieltsListening = IeltsListening::where('ielts_id', $request->ielts_id)->get();
        $result = 0;
        $ielts = Test::where('ielts_id', $request->ielts_id)->where('user_id', Auth::id())->where('listening_id', $ieltsListening[0]->id)->get();
        if ($ielts->isEmpty()) {
            $ielts_test = new Test();
            $ielts_test->user_id = Auth::id();
            $ielts_test->ielts_id = $request->ielts_id;
            $ielts_test->listening_id = $ieltsListening[0]->id;
            foreach ($request->input('questions', []) as $key => $question) {
                $status = 0;
                if (
                    $request->input('answers.' . $question) && IeltsOption::find($request->input('answers.' . $question))->correct
                ) {
                    $status = 1;
                    $result++;
                }
                \App\Models\IeltsAnswers::create([
                    'user_id' => Auth::id(),
                    'ielts_id' => $request->ielts_id,
                    'listening_id' => $request->listening_id,
                    'question_id' => $question,
                    'option_id' => $request->input('answers.' . $question),
                    'correct' => $status
                ]);


            }
            foreach ($request->input('text_answer') as $number => $answer) {
                \App\Models\IeltsAnswers::where('user_id', Auth::id())
                    ->where('ielts_id', $request->ielts_id)
                    ->where('listening_id', $request->listening_id)
                    ->where('question_id', $request->text_question[$number])
                    ->where('option_id', Null)
                    ->update(['text_answer' => $answer]);
            }
            $ielts_test->result = $result;
            $ielts_test->save();
        }
        return redirect()->route('ielts_reading', ['slug' => $request->slug]);
    }

//    Ielts Reading
    public function ielts_reading($slug)
    {
        $ticket_code = Ticket::where('ticket_code', $slug)->get();
        $ielts = Ielts::where('ticket_id', $ticket_code[0]->id)->get();
        $ielts_reading = IeltsReading::where('ielts_id', '=', $ielts[0]->id)->where('ticket_id', '=', $ticket_code[0]->id)->get();
        $questions = IeltsQuestions::where('ielts_id', '=', $ielts[0]->id)
            ->where('ticket_id', '=', $ticket_code[0]->id)
            ->where('reading_id', '=', $ielts_reading[0]->id)
            ->get();
        $ieltsoption = DB::table('ielts_options')
            ->leftJoin('ielts_questions', 'ielts_questions.id', '=', 'ielts_options.question_id')->get();
        return view('front.reading', compact('questions', 'ielts', 'ielts_reading', 'slug'));
    }

    public function ielts_store_reading(Request $request)
    {
        $ielts = Test::where('ielts_id', $request->ielts_id)->where('user_id', Auth::id())->get();
        $ieltsReading = IeltsReading::where('ielts_id', $request->ielts_id)->get();

        $result = 0;
        
        if (!$ielts->isEmpty()) {
         
            // $ielts_test = new Test();
            // $ielts_test->user_id = Auth::id();
            // $ielts_test->ielts_id = $request->ielts_id;
            // $ielts_test->reading_id = $ieltsReading[0]->id;
           
            foreach ($request->input('questions', []) as $key => $question) {
                $status = 0;
                if (
                    $request->input('answers.' . $question) && IeltsOption::find($request->input('answers.' . $question))->correct
                ) {
                    $status = 1;
                    $result++;
                }
                \App\Models\IeltsAnswers::create([
                    'user_id' => Auth::id(),
                    'ielts_id' => $request->ielts_id,
                    'reading_id' => $request->reading_id,
                    'question_id' => $question,
                    'option_id' => $request->input('answers.' . $question),
                    'correct' => $status
                ]);
            }
            foreach ($request->input('text_answer') as $number => $answer) {
                \App\Models\IeltsAnswers::where('user_id', Auth::id())
                    ->where('ielts_id', $request->ielts_id)
                    ->where('reading_id', $request->reading_id)
                    ->where('question_id', $request->text_question[$number])
                    ->where('option_id', Null)
                    ->update(['text_answer' => $answer]);
            }
            // $ielts_test->result = $result;
            \App\Models\Test::where('user_id', Auth::id())
            ->where('ielts_id', $request->ielts_id)
            ->update(['reading_id' => $ieltsReading[0]->id,'result'=>$result]);
            // $ielts_test->save();
        }
        return redirect()->route('ielts_writing', ['slug' => $request->slug]);
    }

    public function ielts_writing($slug)
    {
        $ticket_code = Ticket::where('ticket_code', $slug)->get();
        $ielts = Ielts::where('ticket_id', $ticket_code[0]->id)->get();
        $ielts_writing = IeltsWriting::where('ielts_id', '=', $ielts[0]->id)->where('ticket_id', '=', $ticket_code[0]->id)->get();
        $questions = IeltsQuestions::where('ielts_id', '=', $ielts[0]->id)
            ->where('ticket_id', '=', $ticket_code[0]->id)
            ->where('writing_id', '=', $ielts_writing[0]->id)
            ->get();
        return view('front.writing', compact('ielts', 'questions', 'ielts_writing', 'slug'));
    }

    public function ielts_store_writing(Request $request)
    {
        $ieltsWriting = IeltsWriting::where('ielts_id', $request->ielts_id)->get();
        $ielts = Test::where('ielts_id', $request->ielts_id)->where('user_id', Auth::id())->get();
        if (!$ielts->isEmpty()) {
            \App\Models\IeltsAnswers::create([
                'user_id' => Auth::id(),
                'ielts_id' => $request->ielts_id,
                'writing_id' => $request->writing_id,
                'question_id' => $request->questions,
                'text_answer' => $request->text_answer
            ]);
            // $ielts_test = new Test();
            // $ielts_test->user_id = Auth::id();
            // $ielts_test->test_id = $request->ielts_id;
            // $ielts_test->writing_id = $ieltsWriting[0]->id;
            // $ielts_test->save();
            \App\Models\Test::where('user_id', Auth::id())
            ->where('ielts_id', $request->ielts_id)
            ->update(['writing_id' => $ieltsWriting[0]->id]);
        }
        return redirect()->route('wait_answers', ['slug' => $request->slug]);
    }

    public function wait_answers($slug)
    {

        $ielts = Ielts::where('ielts_name_slug', $slug)->get();
        return view('front.wait_answers');
    }
}


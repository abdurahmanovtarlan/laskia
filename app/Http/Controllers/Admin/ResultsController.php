<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Questions;
use App\Models\Test;
use App\Models\TestAnswers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ResultsController extends Controller
{
    public function index()

    {
        $results = DB::table('exams')->orderByDesc('id')->get();
        return view('admin.results', compact('results'));
    }

    public function show($test_title)
    {
        $passtests = DB::table('tests')->select('*','tests.id as result_id')
            ->leftJoin('users', 'users.id', '=', 'tests.user_id')
            ->leftJoin('exams', 'exams.id', '=', 'tests.test_id')
            ->where('exams.test_title', '=', $test_title)
            ->get();
        $questions = Questions::all();
        $test_answers = DB::table('test_answers')
            ->leftJoin('users', 'users.id', '=', 'test_answers.user_id')
            ->get();
        $option_mass = [];
        foreach ($test_answers as $answer) {
            if ($answer->option_id == null) {
                array_push($option_mass, $answer);
            }
        }
        return view('admin.result_show', compact('passtests', 'option_mass', 'questions'));
    }
}

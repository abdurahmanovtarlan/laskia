<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IeltsQuestions extends Model
{
    protected $fillable = ['test_id', 'question', 'listening_id', 'reading_id', 'writing_id'];


    public function options()
    {
        return $this->hasMany(IeltsOption::class, 'question_id');
    }
}

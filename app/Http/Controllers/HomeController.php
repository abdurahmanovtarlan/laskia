<?php

namespace App\Http\Controllers;

use App\Models\Exam;
use App\Models\ExamRegister;
use App\Models\Ielts;
use App\Models\Result;
use App\Models\Test;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::check()) {
            $ticket_code = Auth::user()->ticket_code;
            $tickets = Ticket::where('ticket_code', $ticket_code)->get();
            if (strpos($ticket_code, 'ielts') !== false) {
                $ilets = true;
                return view('front.index', compact('ticket_code', 'ilets'));
            } else {
                $ilets = false;
                return view('front.index', compact('ticket_code', 'ilets'));
            }
        } else {
            return view('front.index');
        }

    }

//    For Simple Exam
    public function ticket_exams($ticket_code)
    {

        $ticket = Ticket::where('ticket_code', $ticket_code)->get();
        $exams = Exam::where('ticket_id', $ticket[0]->id)->get();
        $end_exam = Test::where('user_id', Auth::id())->get();
        $result = Test::where('user_id', Auth::id())->get();
        return view('front.ticket_exams', compact('exams', 'ticket', 'end_exam', 'result'));
    }

//    For Simple Ielts
    public function ticket_ielts($ticket_code)
    {

        $ticket = Ticket::where('ticket_code', $ticket_code)->get();
        $ielts = Ielts::where('ticket_id', $ticket[0]->id)->get();
        $ielts = DB::table('ielts')
            ->leftJoin('ielts_listenings', 'ielts_listenings.ielts_id', '=', 'ielts.id')
            ->leftJoin('ielts_writings', 'ielts_writings.ielts_id', '=', 'ielts.id')
            ->leftJoin('ielts_readings', 'ielts_readings.ielts_id', '=', 'ielts.id')
            ->get();
//        $end_exam = Test::where('user_id', Auth::id())->get();
//        $result = Test::where('user_id', Auth::id())->get();
//        , 'end_exam', 'result'
        return view('front.ticket_ielts', compact('ielts', 'ticket'));
    }


    public function search(Request $request)
    {
        if ($request->search_val) {
            $query = $request->search_val;
            $data = DB::table('exams')->where('test_title', 'like', '%' . $query . '%')->get();
            $output = '';
            foreach ($data as $row) {
                $output .= '<div class="card mb-2 shadow">
                        <div class="card-body">
                        <div class="d-flex justify-content-between">
                        ' . $row->test_title . '
                        <button id="' . $row->id . '" class="btn btn-sm btn-info register_exam">Qeydiyyat</button>
                    </div>

                            </div>
                </div>';
            }
            echo($output);
        } else {
            $data = Exam::orderBy('id', 'DESC')->get();
            $output = '';
            foreach ($data as $row) {
                $output .= '<div class="card mb-2">
                        <div class="card-body">
                        ' . $row->test_title . '
                            </div>
                </div>';
            }
            echo($output);
        }
    }

    public function register_exam(Request $request)
    {
        $have_exam = DB::table('exam_registers')->where('user_id', '=', Auth::id())->where('exam_id', '=', $request->id)->get();
        if (count($have_exam) > 0) {
            return response()->json(['status' => false]);
        } else {
            $register_exam = new  ExamRegister();
            $register_exam->exam_id = $request->id;
            $register_exam->user_id = Auth::id();
            $register_exam->save();
            return response()->json(['status' => true]);
        }
    }
}

@extends('admin.newbase')
@section('content')
    <link href="{{asset('admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <a href="{{route('listening',$slug)}}" class="btn btn-primary shadow btn-block btn-lg">Listening</a>
            </div>
            <div class="col-md-3">
                <a href="{{route('writing',$slug)}}" class="btn btn-danger shadow btn-block btn-lg">Writing</a>
            </div>
            <div class="col-md-3">
                <a href="{{route('reading',$slug)}}" class="btn btn-success shadow btn-block btn-lg">Reading</a>
            </div>
            <div class="col-md-3">
                <a href="" class="btn btn-warning shadow btn-block btn-lg">Speaking</a>
            </div>
        </div>
    </div>

    <script src="{{asset('admin/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('admin/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
@endsection

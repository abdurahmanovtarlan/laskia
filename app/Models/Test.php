<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $fillable = ['user_id', 'test_id', 'result'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function exam()
    {
        return $this->belongsTo(Exam::class, 'test_id');
    }
}

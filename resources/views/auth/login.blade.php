<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Daxil ol</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css"
          integrity="sha256-h20CPZ0QyXlBuAw7A+KluUYx/3pK+c7lYEpqLTlxjYQ=" crossorigin="anonymous"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css"/>

          <link
        href="{{asset('front/assets/vendor/bootstrap/css/bootstrap.min.css')}}"
        rel="stylesheet"
    />
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{{asset('front/css/style.css')}}"/>
    <link rel="stylesheet" href="{{asset('front/assets/css/style.css')}}"/>
</head>

<body class="bg-gray">
<div class="container">
    <div class="row justify-content-center  align-items-center mt-3" style="height: 80vh;">
    
        <div id="form-layout" class="mt-5">
            <div class="form-header text-center">
                <h2>Daxil ol</h2>
                <img src="{{asset('front/img/login.svg')}}" class="" alt="" style="width:50% !important; ">
            </div>
            <form class="form-group" action="{{ route('login') }}" method="POST" id="login-form">
                @csrf
                <div class="input-group mb-3 mt-5 @error('phone') is-invalid @enderror">
                    <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default"><svg
                                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14" aria-hidden="true"
                                    role="img">
                                    <path
                                        d="M7 8c-3.314 0-6 1.85-6 3.297v2.027c0 .373.358.676.8.676h10.4c.442 0 .8-.303.8-.676v-2.027C13 9.85 10.314 8 7 8zm3-5a3 3 0 11-6 0 3 3 0 016 0z"></path>
                                </svg></span>
                    </div>
                    <input type="text" name="phone" id="phone" class="form-control" aria-label="Default"
                           placeholder="Telefon nömrə" aria-describedby="inputGroup-sizing-default"/>
                </div>
                @error('phone')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="input-group mt-2 @error('password') is-invalid @enderror">
                    <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-default">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14" aria-hidden="true"
                                     role="img">
                                    <path
                                        d="M4.5 4.657C4.5 3.192 5.621 2 7 2s2.5 1.19 2.5 2.656V6h-5V4.657zM11.5 6V4.657C11.5 2.09 9.481 0 7 0S2.5 2.09 2.5 4.657V6h-.992C1.228 6 1 6.23 1 6.5v7c0 .276.229.5.5.5h11c.276 0 .5-.231.5-.5v-7a.5.5 0 00-.508-.5H11.5z"></path>
                                </svg>
                            </span>
                    </div>
                    <input type="password" class="form-control " id="password" name="password" aria-label="Default"
                           placeholder="Şifrə" aria-describedby="inputGroup-sizing-default"/>
                    <div class="input-group-append">
                            <span class="input-group-text password">
                                <i class="far fa-eye" style="color:#6C63FF"></i>
                            </span>
                    </div>
                </div>
                @error('password')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="form-row">
                    <div class="form-group form-check mt-3 ">
                        <input class="form-check-input" type="checkbox" name="remember"
                               id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-check-label" for="remember">Yadda saxla</label>
                        <span class="checkmark"></span>
                    </div>

                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block rounded-0 shadow">Daxil ol</button>
                </div>
                <div class="form-group">
                    <a class="btn btn-primary btn-block rounded-0 shadow" href="{{ route('register') }}">Qeydiyyat</a>
                </div>


            </form>
        </div>
    </div>
</div>
<script src="{{asset('admin/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js')}}"></script>

<script>
    $("#phone").inputmask("(+99488) 888-88-88");
    $('.password').click(function () {
        $(this).find('i').toggleClass('fa-eye-slash');
        var password_input = $(this).parent().prev();

        if (password_input[0].type === "password") {
            password_input[0].type = "text";
        } else {
            password_input[0].type = "password";
        }
    });
</script>
</body>

</html>

$(".submit").click(function () {
    Swal.fire({
        title: 'İmtahan təsdiqlənsin ?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Təsdiqlə',
        cancelButtonText: 'Bağla',
    }).then((result) => {
        if (result.value) {
            Swal.fire({
                title: 'İmtahan yoxlanılır...',
                timer: 1000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                onClose: () => {
                    clearInterval(timerInterval);
                    $("#test_form").submit();
                }
            })
        }
    });
});
$("#DateCountdown").TimeCircles({
    animation: "smooth",
    bg_width: 1,
    fg_width: 0.05,
    circle_bg_color: "#60686F",
    time: {
        Days: {
            show: false,
        },
        Hours: {
            text: "Saat",
            color: "#3f00a8",
            show: true,
        },
        Minutes: {
            text: "Dəqiqə",
            color: "#00fa00",
            show: true,
        },
        Seconds: {
            text: "Saniyə",
            color: "#fe0b0b",
            show: true,
        },
    },
});
setInterval(function () {
    var remaining_second = $("#DateCountdown").TimeCircles().getTime();
    if (remaining_second < 1) {
        $('#test_form').submit();
    }
}, 1000);
window.onload = function () {
    Swal.fire({
        icon: 'warning',
        html: `<p style="font-weight: bold">İmtahana daxil olduqdan sonra çıxış etsəniz imtahanınız tamamlanmış hesab olunacaq!</p>`,

    })
    history.pushState(null, null, this.href);
}

$(window).on('popstate', function (event) {
    Swal.fire({
        title: 'İmtahan təsdiqlənsin ?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Təsdiqlə',
        cancelButtonText: 'Bağla',
    }).then((result) => {
        if (result.value) {
            Swal.fire({
                title: 'İmtahan yoxlanılır...',
                timer: 1000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                onClose: () => {
                    clearInterval(timerInterval);
                    $("#test_form").submit();
                }
            })
        }
    })
});
var saat = $('.time_circles').find('span')[0];
var deqiqe = $('.time_circles').find('span')[1];
var saniye = $('.time_circles').find('span')[2];
// $('.time_circles').find('span').each(function (e) {
//     vaxt += `${$(this).text()}:`;
// });

setInterval(function () {
    $("#clock").val($(saat).text());
    $("#minute").val($(deqiqe).text());
    $("#second").val($(saniye).text());
}, 1000);

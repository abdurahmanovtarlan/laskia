@extends('front.base')
@section('content')

    <div class="container mt-5">
        <div class="row justify-content-center mt-4">
            <div class="col-md-7 mt-5 mb-2">
                <div class="card question-card border-top">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <h3 class="question-name m-0">{{ $test->exam->test_title }}</h3>
                        <span
                            class="badge badge-primary p-2 font-weight-bold">{{ $test->result }} / {{$test->exam->total_question}}</span>
                    </div>
                </div>
            </div>
            <div class="col-md-7 mb-2">
                <div class="card question-card  ">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <h5 class=" m-0 font-weight-bold text-danger">Yenidən cavablara müdaxilə edə bilməzsiniz!</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-7 mb-2">
                <div class="card question-card">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <h5 class="question-name m-0 font-weight-bold">Suallar</h5>
                        <span>
                        <div class="text-success font-weight-bold">Düz cavablar: {{$test->result}} </div>
                        <div
                            class="text-danger font-weight-bold">Səhv cavablar:{{abs(count($question) - $test->result - count($option_mass))}}</div>
                        <div class="text-warning font-weight-bold">Cavabsız suallar: {{count($option_mass)}}</div>
                        <div class="text-info font-weight-bold">Faizlə göstəriciniz: {{round((100 * $test->result)/$test->exam->total_question,2)}}%</div>
                    </span>
                    </div>
                </div>
            </div>

            <div class="col-md-7 mb-2">
                <?php $i = 1;
                $mass = ['a', 'b', 'c', 'd', 'e'];
                ?>
                @foreach($results as $result)
                    <div class="card question-card mb-2">
                        <div class="card-body">
                            <div class="question ml-2 "> {!! ($result->question->question_text) !!}
                            </div>
                            <div class="answers read-only">
                                @foreach($result->question->options as $key=>$option)
                                    <div
                                        class="form-check my-2 @if ($option->correct == 1) success-answer @elseif($result->option_id == $option->id) wrong-answer @endif">
                                        <label class="form-check-label ml-1 py-1" for="exampleRadios{{$option->id}}">
                                            <input class="form-check-input" disabled type="radio"
                                                   name="exampleRadios{{$option->id}}" id="exampleRadios{{$option->id}}"
                                                   @if($option->correct == 1) checked @endif />
                                            <span
                                                class="checkmark d-flex justify-content-center align-items-center">{{$mass[$key]}}</span>
                                            <span class="text @if ($option->correct == 1) font-weight-bold @endif">
                                                {!!  $option->option !!}
                                                @if ($option->correct == 1) <em>(Düzgün cavab)</em> @endif
                                                @if ($result->option_id == $option->id)
                                                    <em>(Sizin cavabınız)</em> @endif
                                </span>
                                        </label>
                                    </div>
                                @endforeach
                                @if($result->question->answer_explanation)
                                    <div class="answer_explanation  text-light p-3">
                                        {!! $result->question->answer_explanation !!}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <?php $i++ ?>
                    @endforeach
                    </form>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.15.2/dist/sweetalert2.all.min.js"></script>
    <script src="{{asset('front/js/result_show.js')}}"></script>

@endsection

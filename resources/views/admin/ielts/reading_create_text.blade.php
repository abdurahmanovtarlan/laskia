@extends('admin.newbase')
@section('content')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="{{asset('admin/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('admin/ckeditor/samples/js/sample.js')}}"></script>
    <link rel="stylesheet" href="{{asset('admin/ckeditor/samples/css/samples.css')}}"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card exam_add">
                    <div class="card-body">
                        <form method="POST" action="" enctype="multipart/form-data" id="ielts_form">
                            @csrf
                            <input type="hidden" name="slug" id="slug" value="{{$slug}}">
                            <div class="form-group">
                                <label for="ielts_id" class="font-weight-bold">Testin adı <span
                                        class="text-danger">*</span></label>
                                <select name="ielts_id" class="form-control" id="ielts_id">
                                    <option value="{{$ielts[0]->id}}" selected>{{$ielts[0]->ielts_name}}</option>
                                </select>
                                <div class="invalid-feedback font-weight-bold ">
                                    Test seçin.
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="texto" class="font-weight-bold">Sual <span
                                        class="text-danger">*</span></label>
                                <div id='texto'></div>

                                <div class="invalid-feedback font-weight-bold question_text">
                                    Sual daxil edin.
                                </div>
                                <hr>
                                <div id="ques"></div>
                            </div>
                            <button class="btn btn-primary px-5 btn-block" type="submit">
                                Saxla
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.min.js"></script>
    <script type="text/javascript" async
            src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML"></script>

    <script type="text/x-mathjax-config">
        MathJax.Hub.Config({
        extensions: ["tex2jax.js"],
        jax: ["input/TeX", "output/HTML-CSS"],
        tex2jax: {
        inlineMath: [
        ["$", "$"],
        ["\\(", "\\)"]
        ]
        },
        "HTML-CSS": {
        linebreaks: { automatic: true }
        }
        });



    </script>

    <script src="{{asset('admin/summernote-ext.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#texto,#option1,#option2,#option3').summernote({
                height: 400, // set editor height,
                toolbar: [
                    ['insert', ['equation', 'draw', 'hello']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['style', ['style', 'bold', 'italic', 'underline', 'strikethrough', 'clear', 'superscript', 'subscript']],
                    ['fonts', ['fontsize']],
                    ['color', ['color']],
                    ['undo', ['undo', 'redo']],
                    ['ckMedia', ['ckImageUploader', 'ckVideoEmbeeder']],
                    ['misc', ['link', 'picture', 'table', 'hr', 'codeview', 'fullscreen']],
                    ['height', ['lineheight']]
                ],
            });
            $('#ielts_form').submit(function (e) {
                e.preventDefault();
                var editordata = $('#texto').summernote('code');
                var test = $("#ielts_id option:selected");
                var slug = $("#slug").val();
                if (test.val() == '') {
                    $('select#test_id').addClass('is-invalid');
                }

                if (
                    test.val() != " "
                ) {
                    var formData = new FormData(this);
                    formData.append('question', editordata);
                    formData.append('slug', slug);
                    $.ajax({
                        type: "POST",
                        url: "{{route('reading_store_text')}}",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data.status == true) {
                                window.location = `oders/ielts/${slug}/reading`;
                            }

                        }
                    });
                }
            })
        });
    </script>
@endsection

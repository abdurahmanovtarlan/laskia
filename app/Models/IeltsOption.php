<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IeltsOption extends Model
{
    protected $fillable = ['option', 'correct', 'question_id'];

    public function question()
    {
        return $this->belongsTo(IeltsQuestions::class, 'question_id');
    }
}

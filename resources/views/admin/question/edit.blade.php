@extends('admin.base')
@section('content')
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="{{asset('admin/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('admin/ckeditor/samples/js/sample.js')}}"></script>
    <link rel="stylesheet" href="{{asset('admin/ckeditor/samples/css/samples.css')}}"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div id="alert"></div>
            <div class="card">
                <div class="card-body">
                    <form action="{{route('question_update',$question->id)}}" method="POST" id="update_form">
                        @csrf
                        <div class="form-group">
                            <label for="question_text" class="font-weight-bold">Sual <span class="text-danger">*</span></label>
                            <textarea type="text" id="question_text" class="form-control" name="question_text">{!! $question->question_text !!}</textarea>

                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-success">Yenilə</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.min.js"></script>
    <script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML"></script>
        
    <script type="text/x-mathjax-config">
        MathJax.Hub.Config({
        extensions: ["tex2jax.js"],
        jax: ["input/TeX", "output/HTML-CSS"],
        tex2jax: {
        inlineMath: [
        ["$", "$"],
        ["\\(", "\\)"]
        ]
        },
        "HTML-CSS": {
        linebreaks: { automatic: true }
        }
        });
    </script>

    <script src="{{asset('admin/summernote-ext.js')}}"></script>
    <script>
      $(document).ready(function () {
            CKEDITOR.plugins.addExternal(
                "ckeditor_wiris",
                "./node_modules/@wiris/mathtype-ckeditor4/",
                "plugin.js"
            );
            var editor = CKEDITOR.replace("question_text", {
                extraPlugins: "ckeditor_wiris",
            });
            $('#update_form').submit(function (e) {
                    e.preventDefault();
                    var editordata = editor.getData();
                    var formData = new FormData(this);
                    formData.append('question_text', editordata);
                    $.ajax({
                        type: "POST",
                        url: "{{route('question_update',$question->id)}}",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data.status == true) {
                   
                                window.location = `{{route('question_show_detail', $question->id)}}`;
                            }
                        }
                    });
            })
        });

    </script>
@endsection

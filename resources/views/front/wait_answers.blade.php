@extends('front.base')
@section('content')
    <style>
        .text {
            position: relative;
            font-weight: 900;
            font-size: 3.3em;
        }

        .text .text-wrapper {
            position: relative;
            display: inline-block;
            padding-top: 0.2em;
            padding-right: 0.05em;
            padding-bottom: 0.1em;
            overflow: hidden;
        }

        .text .letter {
            display: inline-block;
            line-height: 1em;
        }
    </style>
    <section id="about" class="my-5">
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-lg-12  mt-5">
                    <h1 class="text">
  <span class="text-wrapper">
    <span class="letters">Cavablar 1 həftə ərzində hesablanacaq.</span>
  </span>
                    </h1>
                    <img src="{{asset('front/img/test1.svg')}}" class="img-fluid " data-aos="zoom-in" alt=""/>
                </div>
            </div>
        </div>
    </section>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
    <script>
        var textWrapper = document.querySelector('.letters');
        textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

        anime.timeline({loop: true})
            .add({
                targets: '.letter',
                translateY: ["1.1em", 0],
                translateZ: 0,
                duration: 1000,
                delay: (el, i) => 50 * i
            }).add({
            targets: '.text',
            opacity: 0,
            duration: 100,
            easing: "easeOutExpo",
            delay: 1000
        });
    </script>
@endsection

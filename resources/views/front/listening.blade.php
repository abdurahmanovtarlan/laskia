@extends('front.base')
@section('content')
    <link rel="stylesheet" href="{{asset('front/css/timer.css')}}">
    <script src="{{asset('front/js/timer.js')}}"></script>
    <style>
        audio {
            transition: all 0.5s linear;
            box-shadow: 2px 2px 4px 0px rgba(0, 0, 0, 0.3);
            border-radius: 25px;
            outline: none !important;
        }
    </style>
    <div class="container" style="margin-top: 100px;">
        <div class="row justify-content-center">
            <div class="col-md-7">
                <form action="{{ route('ielts_store_listening') }}" enctype="multipart/form-data" id="listening_form"
                      method="POST">
                    @csrf
                    <input type="hidden" name="ielts_id" value="{{ $ielts[0]->id }}">
                    <input type="hidden" name="listening_id" value="{{ $ielts_listening[0]->id }}">
                    <input type="hidden" name="slug" value="{{ $slug}}">
                    <div class="card mb-2 shadow-sm">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <h2>Listening</h2>
                            </div>
                            <hr/>
                            <audio id="music" controls class="w-100">
                                <source src="{{url('audio/'.$ielts_listening[0]->audio_file)}}" type="audio/mpeg"/>
                            </audio>
                        </div>
                    </div>
                    <?php $i = 1;
                    ?>
                    @foreach($questions as $key=>$question)
                        <div class="card question-card mb-2 shadow-sm">
                            <div class="card-body">
                                <div class="question ml-2">
                                    {!! ($question->question) !!}
                                </div>
                                <input type="hidden" name="questions[{{$i}}]" value="{{ $question->id }}">
                                <div class="answers">
                                    @foreach($question->options as $key=>$option)
                                        <div class="form-check my-2">
                                            <label
                                                class="form-check-label ml-1 py-0"
                                                for="{{ $option->id }}"
                                            >
                                                <input
                                                    type="radio"
                                                    class="form-check-input"
                                                    id="{{ $option->id }}"
                                                    name="answers[{{ $question->id }}]"
                                                    value="{{ $option->id }}"
                                                />
                                                <span
                                                    class="checkmark d-flex justify-content-center align-items-center"
                                                ></span
                                                >
                                                <span class="text">  {!! $option->option  !!}</span>
                                            </label>
                                        </div>
                                    @endforeach
                                    @if($question->id != isset($question->options[$key]->question_id))
                                        <input type="hidden" name="text_question[]"
                                               value="{{$question->id}}">
                                        <input class="form-control mt-3 rounded"
                                               name="text_answer[]"
                                               placeholder="Answer...">
                                        <span class="text-warning text-small ml-2 number_text font-weight-bold"
                                              style="font-size: 12px"></span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <?php $i++; ?>
                    @endforeach
                </form>
            </div>
            <div class="col-md-5">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <div id="DateCountdown" data-timer="{{$ielts_listening[0]->ielts_listening_time}}"></div>
                    </div>
                </div>
                <img src="{{asset('front/img/listening.svg')}}" class="w-100 mt-5" alt="">
            </div>
            <button class="btn-block shadow btn-primary m-3 py-2 rounded next_btn">
                Növbəti
            </button>
        </div>
    </div>
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    <script src="{{asset('front/js/bootbox.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="{{asset('front/css/timer.css')}}">
    <script src="{{asset('front/js/timer.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.15.2/dist/sweetalert2.all.min.js"></script>
    <script src="{{asset('front/js/test.js')}}"></script>
    <script>
        $("#DateCountdown").TimeCircles({
            animation: "smooth",
            bg_width: 0.1,
            fg_width: 0.03,
            circle_bg_color: "#60686F",
            time: {
                Days: {
                    show: false,
                },
                Hours: {
                    text: "Hours",
                    color: "#99CCFF",
                    show: true,
                },
                Minutes: {
                    text: "Minutes",
                    color: "#BBFFBB",
                    show: true,
                },
                Seconds: {
                    text: "Seconds",
                    color: "#FF9999",
                    show: true,
                },
            },
        });
        setInterval(function () {
            var remaining_second = $("#DateCountdown").TimeCircles().getTime();
            if (remaining_second < 1) {
                $("#test_form").submit();
                console.log('Finish');
            }
        }, 1000);
        $('.next_btn').click(function () {
            $('#listening_form').submit();
        })
        $(".answers textarea").keyup(function () {
            var number_text = $(this).val().trim().length;
            $(".number_text").text(`Number of words: ` + number_text);
        });
    </script>
@endsection

@extends('admin.base')
@section('content')
    <link href="{{asset('admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <style>
        table img {
            width: 100px !important;
        }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div id="alert"></div>
                <div class="card">
                    <div class="card-body">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>Testin adı</th>
                                <td colspan="2">{{ $question->exam->test_title}}</td>
                            </tr>
                            <tr>
                                <th>Sual</th>
                                <td style="width: 200px !important;" colspan="2">{!! $question->question_text !!}</td>
                            </tr>
                            <tr>
                                <th>Şəkil</th>
                                <td colspan="2">@if($question->question_image)
                                        <a href="" class="btn btn-info btn-block" data-toggle="modal"
                                           data-target="#image">Şəkil</a>
                                        <div class="modal fade" id="image" tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" style="display: none;"
                                             aria-hidden="true">
                                            <div class="modal-dialog" role="document" style="max-width: 800px">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <img src="{{url('question_image/'.$question->question_image)}}"
                                                             width="100%">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">Bağla
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        Şəkil yoxdur
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Sualin cavabının açıqlaması</th>
                                <td colspan="2"
                                    style="width: 200px !important;">{!! $question->answer_explanation !!}</td>
                            </tr>
                            <tr class="options">
                                <th class="font-weight-bold">Variantlar</th>
                                <td colspan="2">
                                    <button class="btn btn-info float-right btn-block" data-toggle="modal"
                                            data-target="#option_add">Variant əlavə
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="option_add" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Variant Əlavə</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div id="alert"></div>
                                                    <input type="hidden" name="question_id" id="question_id"
                                                           value="{{$question->id}}">
                                                    <div class="form-group">
                                                        <label for="option" class="font-weight-bold">Variant <span
                                                                class="text-danger">*</span></label>
                                                        <input type="text" id="option" class="form-control" value=""
                                                               name="option">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="option" class="font-weight-bold">Düz yaxud Səhv
                                                            <span class="text-danger">*</span></label>
                                                        <select name="correct" id="correct" class="form-control">
                                                            <option value>Seçin</option>
                                                            <option value="1">Düz</option>
                                                            <option value="0">Yanlış</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Bağla
                                                    </button>
                                                    <button type="button" class="btn btn-primary option_add">Saxla
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @foreach ($questions_option as $key=>$questions_option)
                                <tr id="{{ $questions_option->id }}">
                                    <td style="width: 200px !important;"> {!! $questions_option->option !!}</td>
                                    <td>{!! $questions_option->correct == 1 ? '<span class="text-success">Düz</span>' : '<span class="text-danger">Yanlış</span>' !!}</td>
                                    <td>
                                        <a href="{{route('question_option_edit',$questions_option->id)}}"
                                           class="btn btn-sm btn-primary ">Yenilə</a>
                                        <a href="" class="btn btn-sm btn-danger">Sil</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('admin/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('admin/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#zero_config').DataTable();
        $(document).ready(function () {
            $(document).on('click', '.btn-danger', function (e) {
                e.preventDefault();
                var id = $(this).parents('tr').attr('id');
                console.log(id);
                $.ajax({
                    url: '{{route("question_option_delete")}}',
                    type: "POST",
                    data: {
                        "_token": "{{csrf_token()}}",
                        id: id
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.status == true) {
                            var output = `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Silindi.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>`;
                            $('#alert').html(output);
                            $('#' + id).remove();
                            setTimeout(function () {
                                $(".alert").alert('close')
                            }, 2000);
                            $('.options .btn').removeClass('d-none');

                        }
                    }
                })
            });
            // option_add
            $(document).on('click', '.modal .option_add', function (e) {
                var option = $('#option_add #option').val();
                var question_id = $('#option_add #question_id').val();
                var correct = $('#option_add #correct option:selected').val();
                $.ajax({
                    type: 'POST',
                    url: "{{route('question_option_add')}}",
                    data: {
                        '_token': '{{csrf_token()}}',
                        question_id: question_id,
                        option: option,
                        correct: correct
                    },
                    success: function (data) {
                        console.log(data);
                        if (data.status == true) {
                            if (correct == 1) {
                                correct = 'Düz';
                                color = 'success';
                            } else {
                                correct = 'Yanlış';
                                color = 'danger';

                            }
                            var output = `<tr id="${data.id}">
                            <td>${option}</td>
                            <td><span class="text-${color}">${correct}</span></td>
                            <td>
                                <a href="" class="btn btn-sm btn-primary ">Yenilə</a>
                                <a href="" class="btn btn-sm btn-danger">Sil</a>
                            </td>
                        </tr>`;
                            $('.table').append(output);
                            $('#option_add').modal('hide');
                            location.reload();

                        }
                        if (data.exists == false) {
                            $('.modal #alert').append(`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Bu variant mövcuddur    .
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>`);
                        }
                    }
                })
            });

            var trs = $("tr");
            if (trs.length == 10) {
                $('.options .btn').addClass('d-none');
            } else {
                $('.options .btn').removeClass('d-none');
            }
        })
    </script>
@endsection
